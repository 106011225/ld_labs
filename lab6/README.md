# Logic design (Verilog, FPGA): Theme park monorail (**keyboard**, **7-segment display**, **push buttons**, **LED**)


## Goal

1. Design a theme park pick-up service for visitors by using the FPGA board with I/Os (**keyboard**, **7-segment display**, **push buttons**, **LED**)

Please refer to the demo video:  
https://www.youtube.com/watch?v=OrAbTwWQxK8


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found [here](./vivado\ files/lab06.srcs/sources_1/new/lab6.v).

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
