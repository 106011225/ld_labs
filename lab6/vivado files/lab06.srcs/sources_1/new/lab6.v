`define DIV (26'h4000000 - 1) // 2^26 - 1

module lab06(clk, rst, mode, LED, DISPLAY, DIGIT, PS2_CLK, PS2_DATA);
    input rst;
    input clk;
    input mode;
    inout PS2_DATA;
    inout PS2_CLK;
    output [6:0] DISPLAY;
    output [3:0] DIGIT;
    output [15:0] LED;

    wire clk_div13;
    wire mode_db, mode_1p;

    reg [2:0] stopNum [0:2];
    reg [2:0] next_stopNum [0:2];
    reg isAuto, next_isAuto;
    reg [1:0] loc, next_loc;
    reg [1:0] preloc, next_preloc;
    reg [3:0] dir, next_dir;
    reg [1:0] state, next_state;
    reg [25:0] timer, next_timer;

    wire [8:0] last_change;// index of key_down[]
    wire [511:0] key_down; 
    wire key_valid;
    reg [1:0] pressedNum, next_pressedNum;

    parameter STAY = 0;
    parameter CLKWISE = 1;
    parameter CNTERCLKWISE = 2;

    assign LED = {!isAuto, 3'b0, (4'b1111>>(4-stopNum[2])), (4'b1111>>(4-stopNum[1])), (4'b1111>>(4-stopNum[0]))};
    Clock_divider #(13) c0(.clk_in(clk), .clk_out(clk_div13));
    Clock_divider #(16) c1(.clk_in(clk), .clk_out(clk_div16));
    Debounce d0(.out(mode_db), .in(mode), .clock(clk_div16));
    onepulse o1(.out(mode_1p), .in(mode_db), .clock(clk_div16));
    Display (.clock(clk_div13), .loc(loc), .dir(dir), .digit(DIGIT), .display(DISPLAY), .reset(rst));
    KeyboardDecoder k0(.key_down(key_down), .last_change(last_change), .key_valid(key_valid),
        .PS2_DATA(PS2_DATA), .PS2_CLK(PS2_CLK), .rst(rst), .clk(clk));


    always @ (posedge clk, posedge rst) begin
        if (rst == 1) begin
            stopNum[0] = 0;
            stopNum[1] = 0;
            stopNum[2] = 0;
            loc = 0;
            preloc = 3; // not move yet
            dir = 7;
            state = STAY;
            timer = `DIV;
            pressedNum = 2'b0;
        end
        else begin
            stopNum[0] = next_stopNum[0];
            stopNum[1] = next_stopNum[1];
            stopNum[2] = next_stopNum[2];
            loc = next_loc;
            preloc = next_preloc;
            dir = next_dir;
            state = next_state;
            timer = next_timer;
            pressedNum = next_pressedNum;
        end
    end

    always @ (posedge clk_div16, posedge rst) begin
        if (rst == 1)
            isAuto = 1;
        else
            isAuto = next_isAuto;
    end

    always @ (*) begin
        next_isAuto = isAuto;
        if ((preloc != 3) && (mode_1p))
            next_isAuto = ~isAuto;
    end

    always @ (*) begin
        next_stopNum[0] = stopNum[0];
        next_stopNum[1] = stopNum[1];
        next_stopNum[2] = stopNum[2];
        next_dir = dir;
        next_state = state;
        next_loc = loc;
        next_preloc = preloc;
        next_timer = timer;

        // add new person to the queue
        if ((stopNum[pressedNum-1] < 4) && key_valid && (key_down[last_change] == 1) )
            next_stopNum[pressedNum-1] = stopNum[pressedNum-1] + 1;

        case (state)
            STAY: begin
                //decide dir
                if (stopNum[0] + stopNum[1] + stopNum[2] - stopNum[loc] == 0) // other two stops are empty
                    next_dir = 7; // static
                else begin
                    if (isAuto) begin
                        if ((stopNum[(loc+1)%3] > 0) && (stopNum[(loc+2)%3] > 0)) // both other two stops are not empty
                            next_dir = (preloc == (loc+1)%3 ) ? 0 : 15; // same dir as before
                        else if (stopNum[(loc+1)%3] > 0)
                            next_dir = 15;
                        else 
                            next_dir = 0;
                    end
                    else begin // fixed mode
                        next_dir = (preloc == (loc+1)%3 ) ? 0 : 15; // same dir as before
                    end
                end

                //pickup or start moving depend
                if (timer == 0) begin
                    next_timer = `DIV;

                    if (stopNum[loc] > 0) // pickup
                        next_stopNum[loc] = stopNum[loc] - 1;
                    else begin // no one at current stop, look for other stops
                        if (dir == 15) begin
                            next_dir = 12;
                            next_state = CLKWISE;
                        end
                        else if (dir == 0) begin
                            next_dir = 3;
                            next_state = CNTERCLKWISE;
                        end
                    end
                end
                else 
                    next_timer = timer - 1;
            end
            CLKWISE: begin
                if (timer == 0) begin
                    next_timer = `DIV;
                    if (dir < 15)
                        next_dir = dir + 1;
                    else begin 
                        next_preloc = loc;
                        next_loc = (loc+1) % 3;
                        if (stopNum[(loc+1)%3] > 0) 
                            next_state = STAY;
                        else
                            next_dir = 12;
                    end
                end
                else 
                    next_timer = timer - 1;
            end
            CNTERCLKWISE: begin
                if (timer == 0) begin
                    next_timer = `DIV;
                    if (dir > 0) 
                        next_dir = dir - 1;
                    else begin
                        next_preloc = loc;
                        next_loc = (loc+2) % 3;
                        if (stopNum[(loc+2)%3] > 0)
                            next_state = STAY;
                        else 
                            next_dir = 3;
                    end
                end
                else 
                    next_timer = timer - 1;
            end
            default: begin
                next_state = STAY;
            end
        endcase
    end

    always @ (*) begin
        next_pressedNum = pressedNum;
        case (last_change)
            9'b0_0110_1001: next_pressedNum = 2'd1;
            9'b0_0111_0010: next_pressedNum = 2'd2;
            9'b0_0111_1010: next_pressedNum = 2'd3;
            default:        next_pressedNum = 2'd0; // undefined
        endcase
    end
endmodule


module Display (clock, loc, dir, digit, display, reset);
    input clock, reset;
    input [1:0] loc;
    input [3:0] dir;
    output reg [3:0] digit;
    output reg [6:0] display;

    reg [3:0] bcd0, bcd1, bcd2, bcd3;
    reg [3:0] value;
    
    always @ (*) begin
            bcd0 = loc + 4;
            bcd1 = dir;
            bcd2 = 7;
            bcd3 = 7;
    end

    always @ (posedge clock, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b0000;
            value = 4'd7;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd2;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd3;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1011000; 
            4'd1: display = 7'b1111000;
            4'd2: display = 7'b1111001;
            4'd3: display = 7'b1111011; // conterclockwise
            4'd4: display = 7'b1111001; //display 1
            4'd5: display = 7'b0100100; // display 2
            4'd6: display = 7'b0110000; // display 3
            4'd7: display = 7'b1111111; // nothing
            // 4'd8: display = 7'b0000000;
            // 4'd9: display = 7'b0010000;
            // 4'd10: display = 7'b0101010; 
            // 4'd11: display = 7'b0100000; 
            4'd12: display = 7'b1101111; //clockwise 
            4'd13: display = 7'b1001111; 
            4'd14: display = 7'b1001110; 
            4'd15: display = 7'b1001100; 
            default: display = 7'b1111111;
        endcase
    end

endmodule

module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule

// module Debounce(out, in, clock);
//     input in, clock;
//     output out;
    
//     reg [3:0] tmp;

//     always @ (posedge clock) begin
//         tmp[3:1] <= tmp[2:0];
//         tmp[0] <= in;
//     end

//     assign out = &(tmp[3:0]);
// endmodule

// module onepulse(out, in, clock);
//     input in, clock;
//     output reg out;

//     reg in_delayn;

//     always @ (posedge clock) begin
//         in_delayn <= ~in;

//         if (in & in_delayn)
//             out <= 1;
//         else 
//             out <= 0;
//     end
// endmodule
