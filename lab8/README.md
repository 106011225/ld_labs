# Logic design (Verilog, FPGA): Music player (**audio peripheral**, **7-segment display**, **push buttons**, **LED**)


## Goal

- Design a music player with a song. The player should have these functions:
    - Be able to **Play / Pause**
    - Be able to **Mute**
    - Be able to **Repeat** playing after the song ends
    - Be able to **Rewind** the song
    - Supporting the 5-level **volume control**
    - Supporting the 3-level **octave control**
    - Be able to **display the current note** with the 7-segment display
    - Supporting 2 Tracks (one for the melody, the other for the accompaniment part) per song


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found [here](./vivado\ files/lab08.srcs/sources_1/new/lab8.v)
- [table gen.ipynb](./table\ gen.ipynb) is used to generate the Verilog codes of notes.


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
