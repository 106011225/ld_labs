`timescale 1ns/100ps


module lab1_1(a, b, c, sub, d, e);
    input a, b, c;
    input sub;
    output d, e;

    wire aXORb, cXORs;
    
    wire b_bar, sub_bar;
    wire aANDc, aORc, bbANDs, bANDsb;
    wire w1, w2;

    xor (aXORb, a, b);
    xor (cXORs, c, sub);
    xor (d, aXORb, cXORs);

    not (b_bar, b);
    not (sub_bar, sub);
    and (aANDc, a, c);
    or (aORc, a, c);
    and (bbANDs, b_bar, sub);
    and (bANDsb, b, sub_bar);
    or (w1, bbANDs, bANDsb);
    and (w2, aORc, w1);
    or (e, aANDc, w2);

endmodule
