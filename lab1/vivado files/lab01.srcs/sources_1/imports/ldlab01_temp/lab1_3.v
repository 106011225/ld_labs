`timescale 1ns/100ps

module lab1_3(a, b, aluctr, d);

    input [3:0] a, b;
    input [1:0] aluctr;
    output reg [3:0] d;
    
    wire [3:0] add, sub;
    lab1_2 m1(.a(a), .b(b), .sub(0), .d(add));
    lab1_2 m2(.a(a), .b(b), .sub(1), .d(sub));

    always@(*) begin
        if (aluctr == 2'b00) 
            d = add;
        else if (aluctr == 2'b01) 
            d = sub;
        else if (aluctr == 2'b10) 
            d = a & b;
        else 
            d = a ^ b;
    end
endmodule
