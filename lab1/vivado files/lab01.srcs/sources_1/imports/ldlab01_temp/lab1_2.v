`timescale 1ns/100ps

module lab1_2(a, b, sub, d);
    input[3:0] a, b;
    input sub;
    output[3:0] d;

    wire w0, w1, w2, w3;

    lab1_1 m1(.a(a[0]), .b(b[0]), .c(sub), .sub(sub), .d(d[0]), .e(w0)); 
    lab1_1 m2(.a(a[1]), .b(b[1]), .c(w0), .sub(sub), .d(d[1]), .e(w1)); 
    lab1_1 m3(.a(a[2]), .b(b[2]), .c(w1), .sub(sub), .d(d[2]), .e(w2)); 
    lab1_1 m4(.a(a[3]), .b(b[3]), .c(w2), .sub(sub), .d(d[3]), .e(w3)/*dummy wire*/ ); 

endmodule
