# Logic design (Verilog, FPGA): ALU Designs


## Goal

1. Write a Verilog module that models a **1-bit full-adder** with *and*, *or*, *not*, *xor* as basic components.
2. Write a Verilog module that models a **4-bit 2’s-complement adders**. The module must be implemented by using four 1-bit full adder with necessary interconnects.
3. Write a Verilog module that models a **4-bit ALU**. The ALU must reuse the previous 4-bit adders with necessary interconnects.


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found in [this directory](./vivado\ files/lab01.srcs/sources_1/imports/ldlab01_temp/).
- The source code of testbench can be found in [this directory](./vivado\ files/lab01.srcs/sim_1/imports/ldlab01_temp/).


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**


