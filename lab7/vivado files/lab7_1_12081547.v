
module lab7_1( // top module
    input clk,
    input rst, // btnc
    input en, // sw0
    input dir, // sw1
    output [3:0] vgaRed,
    output [3:0] vgaGreen,
    output [3:0] vgaBlue,
    output hsync,
    output vsync
);

    wire clk_25MHz, clk_div22;
    wire [11:0] data;
    wire [16:0] pixel_addr;
    wire [11:0] pixel;
    wire valid;
    // wire isBlank;
    wire [9:0] h_cnt; //640 which pixel on the monitor?
    wire [9:0] v_cnt;  //480


    Clock_divider #(2) c0(.clk_in(clk), .clk_out(clk_25MHz));
    Clock_divider #(22) c1(.clk_in(clk), .clk_out(clk_div22));

    assign {vgaRed, vgaGreen, vgaBlue} = (valid) ? (pixel) : (12'h0);
   
    mem_addr_gen m0(
    .clk(clk_div22),
    .rst(rst),
    .en(en),
    .dir(dir),
    .h_cnt(h_cnt),
    .v_cnt(v_cnt),
    .pixel_addr(pixel_addr)
    );
 
    blk_mem_gen_0 b0(
      .clka(clk_25MHz),
      .wea(0),
      .addra(pixel_addr),
      .dina(data[11:0]),
      .douta(pixel)
    ); 

    vga_controller v0(
      .pclk(clk_25MHz),
      .reset(rst),
      .hsync(hsync),
      .vsync(vsync),
      .valid(valid),
      .h_cnt(h_cnt),
      .v_cnt(v_cnt)
    );

endmodule


module mem_addr_gen(
    input clk,
    input rst,
    input en,
    input dir,
    input [9:0] h_cnt,
    input [9:0] v_cnt,
    output [16:0] pixel_addr
);

    reg [7:0] lineOffset, next_lineOffset;

    assign pixel_addr = ((h_cnt>>1)+320*(v_cnt>>1)+ lineOffset*320 )% 76800;  //640*480 --> 320*240 


    always @ (posedge clk, posedge rst) begin
        if(rst)
            lineOffset = 0;
        else
            lineOffset = next_lineOffset;
    end

    always @ (*) begin
        next_lineOffset = lineOffset;
        if (en) begin
            if (dir == 0) begin
                if (lineOffset < 239)
                    next_lineOffset = lineOffset + 1;
                else 
                    next_lineOffset = 0;
            end
            else begin // dir == 1
                if (lineOffset > 0)
                    next_lineOffset = lineOffset - 1;
                else
                    next_lineOffset = 239;
            end
        end
    end

endmodule

module vga_controller (
    input wire pclk, reset,
    output wire hsync, vsync, valid,
    output wire [9:0] h_cnt,
    output wire [9:0] v_cnt
    );

    reg [9:0] pixel_cnt;
    reg [9:0] line_cnt;
    reg hsync_i, vsync_i;

    parameter HD = 640;
    parameter HF = 16;
    parameter HS = 96;
    parameter HB = 48;
    parameter HT = 800; 
    parameter VD = 480;
    parameter VF = 10;
    parameter VS = 2;
    parameter VB = 33;
    parameter VT = 525;
    parameter hsync_default = 1'b1;
    parameter vsync_default = 1'b1;

    always @(posedge pclk)
        if (reset)
            pixel_cnt <= 0;
        else
            if (pixel_cnt < (HT - 1))
                pixel_cnt <= pixel_cnt + 1;
            else
                pixel_cnt <= 0;

    always @(posedge pclk)
        if (reset)
            line_cnt <= 0;
        else
            if (pixel_cnt == (HT -1))
                if (line_cnt < (VT - 1))
                    line_cnt <= line_cnt + 1;
                else
                    line_cnt <= 0;


    always @(posedge pclk)
        if (reset)
            hsync_i <= hsync_default;
        else
            if ((pixel_cnt >= (HD + HF - 1)) && (pixel_cnt < (HD + HF + HS - 1)))
                hsync_i <= ~hsync_default;
            else
                hsync_i <= hsync_default; 

    always @(posedge pclk)
        if (reset)
            vsync_i <= vsync_default; 
        else if ((line_cnt >= (VD + VF - 1)) && (line_cnt < (VD + VF + VS - 1)))
            vsync_i <= ~vsync_default; 
        else
            vsync_i <= vsync_default; 

    assign hsync = hsync_i;
    assign vsync = vsync_i;
    assign valid = ((pixel_cnt < HD) && (line_cnt < VD));

    assign h_cnt = (pixel_cnt < HD) ? pixel_cnt : 10'd0;
    assign v_cnt = (line_cnt < VD) ? line_cnt : 10'd0;

endmodule






module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule

module Debounce(out, in, clock);
    input in, clock;
    output out;
    
    reg [3:0] tmp;

    always @ (posedge clock) begin
        tmp[3:1] <= tmp[2:0];
        tmp[0] <= in;
    end

    assign out = &(tmp[3:0]);
endmodule

module Onepulse(out, in, clock);
    input in, clock;
    output reg out;

    reg in_delayn;

    always @ (posedge clock) begin
        in_delayn <= ~in;

        if (in & in_delayn)
            out <= 1;
        else 
            out <= 0;
    end
endmodule
