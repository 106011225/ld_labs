// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Dec  8 09:00:37 2020
// Host        : temmiepc running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/liuchinlin/Documents/LDlab2020/lab07/lab07.srcs/sources_1/ip/blk_mem_gen_0/blk_mem_gen_0_sim_netlist.v
// Design      : blk_mem_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_gen_0,blk_mem_gen_v8_4_3,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module blk_mem_gen_0
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [16:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [11:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [11:0]douta;

  wire [16:0]addra;
  wire clka;
  wire [11:0]dina;
  wire [11:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [11:0]NLW_U0_doutb_UNCONNECTED;
  wire [16:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [16:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [11:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "17" *) 
  (* C_ADDRB_WIDTH = "17" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "26" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     9.042558 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_gen_0.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_gen_0.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "76800" *) 
  (* C_READ_DEPTH_B = "76800" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "12" *) 
  (* C_READ_WIDTH_B = "12" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "76800" *) 
  (* C_WRITE_DEPTH_B = "76800" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "12" *) 
  (* C_WRITE_WIDTH_B = "12" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  blk_mem_gen_0_blk_mem_gen_v8_4_3 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[11:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[16:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[16:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[11:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

(* ORIG_REF_NAME = "bindec" *) 
module blk_mem_gen_0_bindec
   (ena_array,
    addra);
  output [17:0]ena_array;
  input [4:0]addra;

  wire [4:0]addra;
  wire [17:0]ena_array;

  LUT5 #(
    .INIT(32'h00000001)) 
    ENOUT
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[0]),
        .I3(addra[1]),
        .I4(addra[2]),
        .O(ena_array[0]));
  LUT5 #(
    .INIT(32'h00000010)) 
    ENOUT__0
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[0]),
        .I3(addra[1]),
        .I4(addra[2]),
        .O(ena_array[1]));
  LUT5 #(
    .INIT(32'h00000010)) 
    ENOUT__1
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[1]),
        .I3(addra[0]),
        .I4(addra[2]),
        .O(ena_array[2]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__10
       (.I0(addra[4]),
        .I1(addra[1]),
        .I2(addra[0]),
        .I3(addra[2]),
        .I4(addra[3]),
        .O(ena_array[11]));
  LUT5 #(
    .INIT(32'h10000000)) 
    ENOUT__11
       (.I0(addra[4]),
        .I1(addra[1]),
        .I2(addra[3]),
        .I3(addra[0]),
        .I4(addra[2]),
        .O(ena_array[12]));
  LUT5 #(
    .INIT(32'h10000000)) 
    ENOUT__12
       (.I0(addra[4]),
        .I1(addra[0]),
        .I2(addra[3]),
        .I3(addra[1]),
        .I4(addra[2]),
        .O(ena_array[13]));
  LUT5 #(
    .INIT(32'h40000000)) 
    ENOUT__13
       (.I0(addra[4]),
        .I1(addra[3]),
        .I2(addra[2]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[14]));
  LUT5 #(
    .INIT(32'h00000010)) 
    ENOUT__14
       (.I0(addra[3]),
        .I1(addra[2]),
        .I2(addra[4]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[15]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__15
       (.I0(addra[3]),
        .I1(addra[2]),
        .I2(addra[1]),
        .I3(addra[0]),
        .I4(addra[4]),
        .O(ena_array[16]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__16
       (.I0(addra[3]),
        .I1(addra[2]),
        .I2(addra[0]),
        .I3(addra[1]),
        .I4(addra[4]),
        .O(ena_array[17]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__2
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[2]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[3]));
  LUT5 #(
    .INIT(32'h00000010)) 
    ENOUT__3
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[2]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[4]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__4
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[1]),
        .I3(addra[0]),
        .I4(addra[2]),
        .O(ena_array[5]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__5
       (.I0(addra[3]),
        .I1(addra[4]),
        .I2(addra[0]),
        .I3(addra[1]),
        .I4(addra[2]),
        .O(ena_array[6]));
  LUT5 #(
    .INIT(32'h00000010)) 
    ENOUT__6
       (.I0(addra[4]),
        .I1(addra[2]),
        .I2(addra[3]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[7]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__7
       (.I0(addra[4]),
        .I1(addra[2]),
        .I2(addra[1]),
        .I3(addra[0]),
        .I4(addra[3]),
        .O(ena_array[8]));
  LUT5 #(
    .INIT(32'h01000000)) 
    ENOUT__8
       (.I0(addra[4]),
        .I1(addra[2]),
        .I2(addra[0]),
        .I3(addra[1]),
        .I4(addra[3]),
        .O(ena_array[9]));
  LUT5 #(
    .INIT(32'h10000000)) 
    ENOUT__9
       (.I0(addra[4]),
        .I1(addra[2]),
        .I2(addra[3]),
        .I3(addra[0]),
        .I4(addra[1]),
        .O(ena_array[10]));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_generic_cstr" *) 
module blk_mem_gen_0_blk_mem_gen_generic_cstr
   (douta,
    clka,
    addra,
    dina,
    wea);
  output [11:0]douta;
  input clka;
  input [16:0]addra;
  input [11:0]dina;
  input [0:0]wea;

  wire [16:0]addra;
  wire clka;
  wire [11:0]dina;
  wire [11:0]douta;
  wire [18:0]ena_array;
  wire ram_douta;
  wire ram_ena__1;
  wire ram_ena_n_0;
  wire \ramloop[10].ram.r_n_0 ;
  wire \ramloop[10].ram.r_n_1 ;
  wire \ramloop[10].ram.r_n_2 ;
  wire \ramloop[10].ram.r_n_3 ;
  wire \ramloop[10].ram.r_n_4 ;
  wire \ramloop[10].ram.r_n_5 ;
  wire \ramloop[10].ram.r_n_6 ;
  wire \ramloop[10].ram.r_n_7 ;
  wire \ramloop[10].ram.r_n_8 ;
  wire \ramloop[11].ram.r_n_0 ;
  wire \ramloop[11].ram.r_n_1 ;
  wire \ramloop[11].ram.r_n_2 ;
  wire \ramloop[11].ram.r_n_3 ;
  wire \ramloop[11].ram.r_n_4 ;
  wire \ramloop[11].ram.r_n_5 ;
  wire \ramloop[11].ram.r_n_6 ;
  wire \ramloop[11].ram.r_n_7 ;
  wire \ramloop[11].ram.r_n_8 ;
  wire \ramloop[12].ram.r_n_0 ;
  wire \ramloop[12].ram.r_n_1 ;
  wire \ramloop[12].ram.r_n_2 ;
  wire \ramloop[12].ram.r_n_3 ;
  wire \ramloop[12].ram.r_n_4 ;
  wire \ramloop[12].ram.r_n_5 ;
  wire \ramloop[12].ram.r_n_6 ;
  wire \ramloop[12].ram.r_n_7 ;
  wire \ramloop[12].ram.r_n_8 ;
  wire \ramloop[13].ram.r_n_0 ;
  wire \ramloop[13].ram.r_n_1 ;
  wire \ramloop[13].ram.r_n_2 ;
  wire \ramloop[13].ram.r_n_3 ;
  wire \ramloop[13].ram.r_n_4 ;
  wire \ramloop[13].ram.r_n_5 ;
  wire \ramloop[13].ram.r_n_6 ;
  wire \ramloop[13].ram.r_n_7 ;
  wire \ramloop[13].ram.r_n_8 ;
  wire \ramloop[14].ram.r_n_0 ;
  wire \ramloop[14].ram.r_n_1 ;
  wire \ramloop[14].ram.r_n_2 ;
  wire \ramloop[14].ram.r_n_3 ;
  wire \ramloop[14].ram.r_n_4 ;
  wire \ramloop[14].ram.r_n_5 ;
  wire \ramloop[14].ram.r_n_6 ;
  wire \ramloop[14].ram.r_n_7 ;
  wire \ramloop[14].ram.r_n_8 ;
  wire \ramloop[15].ram.r_n_0 ;
  wire \ramloop[15].ram.r_n_1 ;
  wire \ramloop[15].ram.r_n_2 ;
  wire \ramloop[15].ram.r_n_3 ;
  wire \ramloop[15].ram.r_n_4 ;
  wire \ramloop[15].ram.r_n_5 ;
  wire \ramloop[15].ram.r_n_6 ;
  wire \ramloop[15].ram.r_n_7 ;
  wire \ramloop[15].ram.r_n_8 ;
  wire \ramloop[16].ram.r_n_0 ;
  wire \ramloop[16].ram.r_n_1 ;
  wire \ramloop[16].ram.r_n_2 ;
  wire \ramloop[16].ram.r_n_3 ;
  wire \ramloop[16].ram.r_n_4 ;
  wire \ramloop[16].ram.r_n_5 ;
  wire \ramloop[16].ram.r_n_6 ;
  wire \ramloop[16].ram.r_n_7 ;
  wire \ramloop[16].ram.r_n_8 ;
  wire \ramloop[17].ram.r_n_0 ;
  wire \ramloop[17].ram.r_n_1 ;
  wire \ramloop[17].ram.r_n_2 ;
  wire \ramloop[17].ram.r_n_3 ;
  wire \ramloop[17].ram.r_n_4 ;
  wire \ramloop[17].ram.r_n_5 ;
  wire \ramloop[17].ram.r_n_6 ;
  wire \ramloop[17].ram.r_n_7 ;
  wire \ramloop[17].ram.r_n_8 ;
  wire \ramloop[18].ram.r_n_0 ;
  wire \ramloop[18].ram.r_n_1 ;
  wire \ramloop[18].ram.r_n_2 ;
  wire \ramloop[18].ram.r_n_3 ;
  wire \ramloop[18].ram.r_n_4 ;
  wire \ramloop[18].ram.r_n_5 ;
  wire \ramloop[18].ram.r_n_6 ;
  wire \ramloop[18].ram.r_n_7 ;
  wire \ramloop[18].ram.r_n_8 ;
  wire \ramloop[19].ram.r_n_0 ;
  wire \ramloop[19].ram.r_n_1 ;
  wire \ramloop[19].ram.r_n_2 ;
  wire \ramloop[19].ram.r_n_3 ;
  wire \ramloop[19].ram.r_n_4 ;
  wire \ramloop[19].ram.r_n_5 ;
  wire \ramloop[19].ram.r_n_6 ;
  wire \ramloop[19].ram.r_n_7 ;
  wire \ramloop[19].ram.r_n_8 ;
  wire \ramloop[1].ram.r_n_0 ;
  wire \ramloop[20].ram.r_n_0 ;
  wire \ramloop[20].ram.r_n_1 ;
  wire \ramloop[20].ram.r_n_2 ;
  wire \ramloop[20].ram.r_n_3 ;
  wire \ramloop[20].ram.r_n_4 ;
  wire \ramloop[20].ram.r_n_5 ;
  wire \ramloop[20].ram.r_n_6 ;
  wire \ramloop[20].ram.r_n_7 ;
  wire \ramloop[20].ram.r_n_8 ;
  wire \ramloop[21].ram.r_n_0 ;
  wire \ramloop[21].ram.r_n_1 ;
  wire \ramloop[21].ram.r_n_2 ;
  wire \ramloop[21].ram.r_n_3 ;
  wire \ramloop[21].ram.r_n_4 ;
  wire \ramloop[21].ram.r_n_5 ;
  wire \ramloop[21].ram.r_n_6 ;
  wire \ramloop[21].ram.r_n_7 ;
  wire \ramloop[21].ram.r_n_8 ;
  wire \ramloop[22].ram.r_n_0 ;
  wire \ramloop[22].ram.r_n_1 ;
  wire \ramloop[22].ram.r_n_2 ;
  wire \ramloop[22].ram.r_n_3 ;
  wire \ramloop[22].ram.r_n_4 ;
  wire \ramloop[22].ram.r_n_5 ;
  wire \ramloop[22].ram.r_n_6 ;
  wire \ramloop[22].ram.r_n_7 ;
  wire \ramloop[22].ram.r_n_8 ;
  wire \ramloop[23].ram.r_n_0 ;
  wire \ramloop[23].ram.r_n_1 ;
  wire \ramloop[23].ram.r_n_2 ;
  wire \ramloop[23].ram.r_n_3 ;
  wire \ramloop[23].ram.r_n_4 ;
  wire \ramloop[23].ram.r_n_5 ;
  wire \ramloop[23].ram.r_n_6 ;
  wire \ramloop[23].ram.r_n_7 ;
  wire \ramloop[23].ram.r_n_8 ;
  wire \ramloop[2].ram.r_n_0 ;
  wire \ramloop[3].ram.r_n_0 ;
  wire \ramloop[3].ram.r_n_1 ;
  wire \ramloop[4].ram.r_n_0 ;
  wire \ramloop[5].ram.r_n_0 ;
  wire \ramloop[5].ram.r_n_1 ;
  wire \ramloop[5].ram.r_n_2 ;
  wire \ramloop[5].ram.r_n_3 ;
  wire \ramloop[5].ram.r_n_4 ;
  wire \ramloop[5].ram.r_n_5 ;
  wire \ramloop[5].ram.r_n_6 ;
  wire \ramloop[5].ram.r_n_7 ;
  wire \ramloop[5].ram.r_n_8 ;
  wire \ramloop[6].ram.r_n_0 ;
  wire \ramloop[6].ram.r_n_1 ;
  wire \ramloop[6].ram.r_n_2 ;
  wire \ramloop[6].ram.r_n_3 ;
  wire \ramloop[6].ram.r_n_4 ;
  wire \ramloop[6].ram.r_n_5 ;
  wire \ramloop[6].ram.r_n_6 ;
  wire \ramloop[6].ram.r_n_7 ;
  wire \ramloop[6].ram.r_n_8 ;
  wire \ramloop[7].ram.r_n_0 ;
  wire \ramloop[7].ram.r_n_1 ;
  wire \ramloop[7].ram.r_n_2 ;
  wire \ramloop[7].ram.r_n_3 ;
  wire \ramloop[7].ram.r_n_4 ;
  wire \ramloop[7].ram.r_n_5 ;
  wire \ramloop[7].ram.r_n_6 ;
  wire \ramloop[7].ram.r_n_7 ;
  wire \ramloop[7].ram.r_n_8 ;
  wire \ramloop[8].ram.r_n_0 ;
  wire \ramloop[8].ram.r_n_1 ;
  wire \ramloop[8].ram.r_n_2 ;
  wire \ramloop[8].ram.r_n_3 ;
  wire \ramloop[8].ram.r_n_4 ;
  wire \ramloop[8].ram.r_n_5 ;
  wire \ramloop[8].ram.r_n_6 ;
  wire \ramloop[8].ram.r_n_7 ;
  wire \ramloop[8].ram.r_n_8 ;
  wire \ramloop[9].ram.r_n_0 ;
  wire \ramloop[9].ram.r_n_1 ;
  wire \ramloop[9].ram.r_n_2 ;
  wire \ramloop[9].ram.r_n_3 ;
  wire \ramloop[9].ram.r_n_4 ;
  wire \ramloop[9].ram.r_n_5 ;
  wire \ramloop[9].ram.r_n_6 ;
  wire \ramloop[9].ram.r_n_7 ;
  wire \ramloop[9].ram.r_n_8 ;
  wire [0:0]wea;

  blk_mem_gen_0_bindec \bindec_a.bindec_inst_a 
       (.addra(addra[16:12]),
        .ena_array({ena_array[18:8],ena_array[6:0]}));
  blk_mem_gen_0_blk_mem_gen_mux \has_mux_a.A 
       (.DOADO({\ramloop[21].ram.r_n_0 ,\ramloop[21].ram.r_n_1 ,\ramloop[21].ram.r_n_2 ,\ramloop[21].ram.r_n_3 ,\ramloop[21].ram.r_n_4 ,\ramloop[21].ram.r_n_5 ,\ramloop[21].ram.r_n_6 ,\ramloop[21].ram.r_n_7 }),
        .DOPADOP(\ramloop[21].ram.r_n_8 ),
        .DOUTA(ram_douta),
        .addra(addra[16:12]),
        .clka(clka),
        .douta(douta),
        .\douta[0] (\ramloop[1].ram.r_n_0 ),
        .\douta[10] ({\ramloop[22].ram.r_n_0 ,\ramloop[22].ram.r_n_1 ,\ramloop[22].ram.r_n_2 ,\ramloop[22].ram.r_n_3 ,\ramloop[22].ram.r_n_4 ,\ramloop[22].ram.r_n_5 ,\ramloop[22].ram.r_n_6 ,\ramloop[22].ram.r_n_7 }),
        .\douta[10]_0 ({\ramloop[23].ram.r_n_0 ,\ramloop[23].ram.r_n_1 ,\ramloop[23].ram.r_n_2 ,\ramloop[23].ram.r_n_3 ,\ramloop[23].ram.r_n_4 ,\ramloop[23].ram.r_n_5 ,\ramloop[23].ram.r_n_6 ,\ramloop[23].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_0 ({\ramloop[16].ram.r_n_0 ,\ramloop[16].ram.r_n_1 ,\ramloop[16].ram.r_n_2 ,\ramloop[16].ram.r_n_3 ,\ramloop[16].ram.r_n_4 ,\ramloop[16].ram.r_n_5 ,\ramloop[16].ram.r_n_6 ,\ramloop[16].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_1 ({\ramloop[15].ram.r_n_0 ,\ramloop[15].ram.r_n_1 ,\ramloop[15].ram.r_n_2 ,\ramloop[15].ram.r_n_3 ,\ramloop[15].ram.r_n_4 ,\ramloop[15].ram.r_n_5 ,\ramloop[15].ram.r_n_6 ,\ramloop[15].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_2 ({\ramloop[14].ram.r_n_0 ,\ramloop[14].ram.r_n_1 ,\ramloop[14].ram.r_n_2 ,\ramloop[14].ram.r_n_3 ,\ramloop[14].ram.r_n_4 ,\ramloop[14].ram.r_n_5 ,\ramloop[14].ram.r_n_6 ,\ramloop[14].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_3 ({\ramloop[13].ram.r_n_0 ,\ramloop[13].ram.r_n_1 ,\ramloop[13].ram.r_n_2 ,\ramloop[13].ram.r_n_3 ,\ramloop[13].ram.r_n_4 ,\ramloop[13].ram.r_n_5 ,\ramloop[13].ram.r_n_6 ,\ramloop[13].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_4 ({\ramloop[20].ram.r_n_0 ,\ramloop[20].ram.r_n_1 ,\ramloop[20].ram.r_n_2 ,\ramloop[20].ram.r_n_3 ,\ramloop[20].ram.r_n_4 ,\ramloop[20].ram.r_n_5 ,\ramloop[20].ram.r_n_6 ,\ramloop[20].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_5 ({\ramloop[19].ram.r_n_0 ,\ramloop[19].ram.r_n_1 ,\ramloop[19].ram.r_n_2 ,\ramloop[19].ram.r_n_3 ,\ramloop[19].ram.r_n_4 ,\ramloop[19].ram.r_n_5 ,\ramloop[19].ram.r_n_6 ,\ramloop[19].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_6 ({\ramloop[18].ram.r_n_0 ,\ramloop[18].ram.r_n_1 ,\ramloop[18].ram.r_n_2 ,\ramloop[18].ram.r_n_3 ,\ramloop[18].ram.r_n_4 ,\ramloop[18].ram.r_n_5 ,\ramloop[18].ram.r_n_6 ,\ramloop[18].ram.r_n_7 }),
        .\douta[10]_INST_0_i_2_7 ({\ramloop[17].ram.r_n_0 ,\ramloop[17].ram.r_n_1 ,\ramloop[17].ram.r_n_2 ,\ramloop[17].ram.r_n_3 ,\ramloop[17].ram.r_n_4 ,\ramloop[17].ram.r_n_5 ,\ramloop[17].ram.r_n_6 ,\ramloop[17].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_0 ({\ramloop[8].ram.r_n_0 ,\ramloop[8].ram.r_n_1 ,\ramloop[8].ram.r_n_2 ,\ramloop[8].ram.r_n_3 ,\ramloop[8].ram.r_n_4 ,\ramloop[8].ram.r_n_5 ,\ramloop[8].ram.r_n_6 ,\ramloop[8].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_1 ({\ramloop[7].ram.r_n_0 ,\ramloop[7].ram.r_n_1 ,\ramloop[7].ram.r_n_2 ,\ramloop[7].ram.r_n_3 ,\ramloop[7].ram.r_n_4 ,\ramloop[7].ram.r_n_5 ,\ramloop[7].ram.r_n_6 ,\ramloop[7].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_2 ({\ramloop[6].ram.r_n_0 ,\ramloop[6].ram.r_n_1 ,\ramloop[6].ram.r_n_2 ,\ramloop[6].ram.r_n_3 ,\ramloop[6].ram.r_n_4 ,\ramloop[6].ram.r_n_5 ,\ramloop[6].ram.r_n_6 ,\ramloop[6].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_3 ({\ramloop[5].ram.r_n_0 ,\ramloop[5].ram.r_n_1 ,\ramloop[5].ram.r_n_2 ,\ramloop[5].ram.r_n_3 ,\ramloop[5].ram.r_n_4 ,\ramloop[5].ram.r_n_5 ,\ramloop[5].ram.r_n_6 ,\ramloop[5].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_4 ({\ramloop[12].ram.r_n_0 ,\ramloop[12].ram.r_n_1 ,\ramloop[12].ram.r_n_2 ,\ramloop[12].ram.r_n_3 ,\ramloop[12].ram.r_n_4 ,\ramloop[12].ram.r_n_5 ,\ramloop[12].ram.r_n_6 ,\ramloop[12].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_5 ({\ramloop[11].ram.r_n_0 ,\ramloop[11].ram.r_n_1 ,\ramloop[11].ram.r_n_2 ,\ramloop[11].ram.r_n_3 ,\ramloop[11].ram.r_n_4 ,\ramloop[11].ram.r_n_5 ,\ramloop[11].ram.r_n_6 ,\ramloop[11].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_6 ({\ramloop[10].ram.r_n_0 ,\ramloop[10].ram.r_n_1 ,\ramloop[10].ram.r_n_2 ,\ramloop[10].ram.r_n_3 ,\ramloop[10].ram.r_n_4 ,\ramloop[10].ram.r_n_5 ,\ramloop[10].ram.r_n_6 ,\ramloop[10].ram.r_n_7 }),
        .\douta[10]_INST_0_i_3_7 ({\ramloop[9].ram.r_n_0 ,\ramloop[9].ram.r_n_1 ,\ramloop[9].ram.r_n_2 ,\ramloop[9].ram.r_n_3 ,\ramloop[9].ram.r_n_4 ,\ramloop[9].ram.r_n_5 ,\ramloop[9].ram.r_n_6 ,\ramloop[9].ram.r_n_7 }),
        .\douta[11] (\ramloop[22].ram.r_n_8 ),
        .\douta[11]_0 (\ramloop[23].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_0 (\ramloop[16].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_1 (\ramloop[15].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_2 (\ramloop[14].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_3 (\ramloop[13].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_4 (\ramloop[20].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_5 (\ramloop[19].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_6 (\ramloop[18].ram.r_n_8 ),
        .\douta[11]_INST_0_i_2_7 (\ramloop[17].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_0 (\ramloop[8].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_1 (\ramloop[7].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_2 (\ramloop[6].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_3 (\ramloop[5].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_4 (\ramloop[12].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_5 (\ramloop[11].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_6 (\ramloop[10].ram.r_n_8 ),
        .\douta[11]_INST_0_i_3_7 (\ramloop[9].ram.r_n_8 ),
        .\douta[1] (\ramloop[2].ram.r_n_0 ),
        .\douta[2] ({\ramloop[3].ram.r_n_0 ,\ramloop[3].ram.r_n_1 }),
        .\douta[2]_0 (\ramloop[4].ram.r_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    ram_ena
       (.I0(addra[16]),
        .O(ram_ena_n_0));
  LUT3 #(
    .INIT(8'h10)) 
    ram_ena__0
       (.I0(addra[15]),
        .I1(addra[14]),
        .I2(addra[16]),
        .O(ram_ena__1));
  blk_mem_gen_0_blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.DOUTA(ram_douta),
        .ENA(ram_ena_n_0),
        .addra(addra[15:0]),
        .clka(clka),
        .dina(dina[0]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized9 \ramloop[10].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[10].ram.r_n_0 ,\ramloop[10].ram.r_n_1 ,\ramloop[10].ram.r_n_2 ,\ramloop[10].ram.r_n_3 ,\ramloop[10].ram.r_n_4 ,\ramloop[10].ram.r_n_5 ,\ramloop[10].ram.r_n_6 ,\ramloop[10].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[10].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[5]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized10 \ramloop[11].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[11].ram.r_n_0 ,\ramloop[11].ram.r_n_1 ,\ramloop[11].ram.r_n_2 ,\ramloop[11].ram.r_n_3 ,\ramloop[11].ram.r_n_4 ,\ramloop[11].ram.r_n_5 ,\ramloop[11].ram.r_n_6 ,\ramloop[11].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[11].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[6]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized11 \ramloop[12].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[12].ram.r_n_0 ,\ramloop[12].ram.r_n_1 ,\ramloop[12].ram.r_n_2 ,\ramloop[12].ram.r_n_3 ,\ramloop[12].ram.r_n_4 ,\ramloop[12].ram.r_n_5 ,\ramloop[12].ram.r_n_6 ,\ramloop[12].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[12].ram.r_n_8 ),
        .addra(addra),
        .clka(clka),
        .dina(dina[11:3]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized12 \ramloop[13].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[13].ram.r_n_0 ,\ramloop[13].ram.r_n_1 ,\ramloop[13].ram.r_n_2 ,\ramloop[13].ram.r_n_3 ,\ramloop[13].ram.r_n_4 ,\ramloop[13].ram.r_n_5 ,\ramloop[13].ram.r_n_6 ,\ramloop[13].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[13].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[8]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized13 \ramloop[14].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[14].ram.r_n_0 ,\ramloop[14].ram.r_n_1 ,\ramloop[14].ram.r_n_2 ,\ramloop[14].ram.r_n_3 ,\ramloop[14].ram.r_n_4 ,\ramloop[14].ram.r_n_5 ,\ramloop[14].ram.r_n_6 ,\ramloop[14].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[14].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[9]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized14 \ramloop[15].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[15].ram.r_n_0 ,\ramloop[15].ram.r_n_1 ,\ramloop[15].ram.r_n_2 ,\ramloop[15].ram.r_n_3 ,\ramloop[15].ram.r_n_4 ,\ramloop[15].ram.r_n_5 ,\ramloop[15].ram.r_n_6 ,\ramloop[15].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[15].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[10]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized15 \ramloop[16].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[16].ram.r_n_0 ,\ramloop[16].ram.r_n_1 ,\ramloop[16].ram.r_n_2 ,\ramloop[16].ram.r_n_3 ,\ramloop[16].ram.r_n_4 ,\ramloop[16].ram.r_n_5 ,\ramloop[16].ram.r_n_6 ,\ramloop[16].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[16].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[11]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized16 \ramloop[17].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[17].ram.r_n_0 ,\ramloop[17].ram.r_n_1 ,\ramloop[17].ram.r_n_2 ,\ramloop[17].ram.r_n_3 ,\ramloop[17].ram.r_n_4 ,\ramloop[17].ram.r_n_5 ,\ramloop[17].ram.r_n_6 ,\ramloop[17].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[17].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[12]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized17 \ramloop[18].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[18].ram.r_n_0 ,\ramloop[18].ram.r_n_1 ,\ramloop[18].ram.r_n_2 ,\ramloop[18].ram.r_n_3 ,\ramloop[18].ram.r_n_4 ,\ramloop[18].ram.r_n_5 ,\ramloop[18].ram.r_n_6 ,\ramloop[18].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[18].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[13]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized18 \ramloop[19].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[19].ram.r_n_0 ,\ramloop[19].ram.r_n_1 ,\ramloop[19].ram.r_n_2 ,\ramloop[19].ram.r_n_3 ,\ramloop[19].ram.r_n_4 ,\ramloop[19].ram.r_n_5 ,\ramloop[19].ram.r_n_6 ,\ramloop[19].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[19].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[14]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized0 \ramloop[1].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram (\ramloop[1].ram.r_n_0 ),
        .addra(addra[13:0]),
        .clka(clka),
        .dina(dina[0]),
        .ram_ena(ram_ena__1),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized19 \ramloop[20].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[20].ram.r_n_0 ,\ramloop[20].ram.r_n_1 ,\ramloop[20].ram.r_n_2 ,\ramloop[20].ram.r_n_3 ,\ramloop[20].ram.r_n_4 ,\ramloop[20].ram.r_n_5 ,\ramloop[20].ram.r_n_6 ,\ramloop[20].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[20].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[15]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized20 \ramloop[21].ram.r 
       (.DOADO({\ramloop[21].ram.r_n_0 ,\ramloop[21].ram.r_n_1 ,\ramloop[21].ram.r_n_2 ,\ramloop[21].ram.r_n_3 ,\ramloop[21].ram.r_n_4 ,\ramloop[21].ram.r_n_5 ,\ramloop[21].ram.r_n_6 ,\ramloop[21].ram.r_n_7 }),
        .DOPADOP(\ramloop[21].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[16]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized21 \ramloop[22].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[22].ram.r_n_0 ,\ramloop[22].ram.r_n_1 ,\ramloop[22].ram.r_n_2 ,\ramloop[22].ram.r_n_3 ,\ramloop[22].ram.r_n_4 ,\ramloop[22].ram.r_n_5 ,\ramloop[22].ram.r_n_6 ,\ramloop[22].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[22].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[17]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized22 \ramloop[23].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[23].ram.r_n_0 ,\ramloop[23].ram.r_n_1 ,\ramloop[23].ram.r_n_2 ,\ramloop[23].ram.r_n_3 ,\ramloop[23].ram.r_n_4 ,\ramloop[23].ram.r_n_5 ,\ramloop[23].ram.r_n_6 ,\ramloop[23].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[23].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[18]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized1 \ramloop[2].ram.r 
       (.DOUTA(\ramloop[2].ram.r_n_0 ),
        .ENA(ram_ena_n_0),
        .addra(addra[15:0]),
        .clka(clka),
        .dina(dina[1]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized2 \ramloop[3].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[3].ram.r_n_0 ,\ramloop[3].ram.r_n_1 }),
        .addra(addra[13:0]),
        .clka(clka),
        .dina(dina[2:1]),
        .ram_ena(ram_ena__1),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized3 \ramloop[4].ram.r 
       (.DOUTA(\ramloop[4].ram.r_n_0 ),
        .ENA(ram_ena_n_0),
        .addra(addra[15:0]),
        .clka(clka),
        .dina(dina[2]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized4 \ramloop[5].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[5].ram.r_n_0 ,\ramloop[5].ram.r_n_1 ,\ramloop[5].ram.r_n_2 ,\ramloop[5].ram.r_n_3 ,\ramloop[5].ram.r_n_4 ,\ramloop[5].ram.r_n_5 ,\ramloop[5].ram.r_n_6 ,\ramloop[5].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[5].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[0]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized5 \ramloop[6].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[6].ram.r_n_0 ,\ramloop[6].ram.r_n_1 ,\ramloop[6].ram.r_n_2 ,\ramloop[6].ram.r_n_3 ,\ramloop[6].ram.r_n_4 ,\ramloop[6].ram.r_n_5 ,\ramloop[6].ram.r_n_6 ,\ramloop[6].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[6].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[1]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized6 \ramloop[7].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[7].ram.r_n_0 ,\ramloop[7].ram.r_n_1 ,\ramloop[7].ram.r_n_2 ,\ramloop[7].ram.r_n_3 ,\ramloop[7].ram.r_n_4 ,\ramloop[7].ram.r_n_5 ,\ramloop[7].ram.r_n_6 ,\ramloop[7].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[7].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[2]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized7 \ramloop[8].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[8].ram.r_n_0 ,\ramloop[8].ram.r_n_1 ,\ramloop[8].ram.r_n_2 ,\ramloop[8].ram.r_n_3 ,\ramloop[8].ram.r_n_4 ,\ramloop[8].ram.r_n_5 ,\ramloop[8].ram.r_n_6 ,\ramloop[8].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[8].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[3]),
        .wea(wea));
  blk_mem_gen_0_blk_mem_gen_prim_width__parameterized8 \ramloop[9].ram.r 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ({\ramloop[9].ram.r_n_0 ,\ramloop[9].ram.r_n_1 ,\ramloop[9].ram.r_n_2 ,\ramloop[9].ram.r_n_3 ,\ramloop[9].ram.r_n_4 ,\ramloop[9].ram.r_n_5 ,\ramloop[9].ram.r_n_6 ,\ramloop[9].ram.r_n_7 }),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\ramloop[9].ram.r_n_8 ),
        .addra(addra[11:0]),
        .clka(clka),
        .dina(dina[11:3]),
        .ena_array(ena_array[4]),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_mux" *) 
module blk_mem_gen_0_blk_mem_gen_mux
   (douta,
    DOADO,
    \douta[10] ,
    \douta[10]_0 ,
    DOPADOP,
    \douta[11] ,
    \douta[11]_0 ,
    addra,
    clka,
    \douta[0] ,
    DOUTA,
    \douta[2] ,
    \douta[1] ,
    \douta[2]_0 ,
    \douta[10]_INST_0_i_3_0 ,
    \douta[10]_INST_0_i_3_1 ,
    \douta[10]_INST_0_i_3_2 ,
    \douta[10]_INST_0_i_3_3 ,
    \douta[10]_INST_0_i_3_4 ,
    \douta[10]_INST_0_i_3_5 ,
    \douta[10]_INST_0_i_3_6 ,
    \douta[10]_INST_0_i_3_7 ,
    \douta[10]_INST_0_i_2_0 ,
    \douta[10]_INST_0_i_2_1 ,
    \douta[10]_INST_0_i_2_2 ,
    \douta[10]_INST_0_i_2_3 ,
    \douta[10]_INST_0_i_2_4 ,
    \douta[10]_INST_0_i_2_5 ,
    \douta[10]_INST_0_i_2_6 ,
    \douta[10]_INST_0_i_2_7 ,
    \douta[11]_INST_0_i_3_0 ,
    \douta[11]_INST_0_i_3_1 ,
    \douta[11]_INST_0_i_3_2 ,
    \douta[11]_INST_0_i_3_3 ,
    \douta[11]_INST_0_i_3_4 ,
    \douta[11]_INST_0_i_3_5 ,
    \douta[11]_INST_0_i_3_6 ,
    \douta[11]_INST_0_i_3_7 ,
    \douta[11]_INST_0_i_2_0 ,
    \douta[11]_INST_0_i_2_1 ,
    \douta[11]_INST_0_i_2_2 ,
    \douta[11]_INST_0_i_2_3 ,
    \douta[11]_INST_0_i_2_4 ,
    \douta[11]_INST_0_i_2_5 ,
    \douta[11]_INST_0_i_2_6 ,
    \douta[11]_INST_0_i_2_7 );
  output [11:0]douta;
  input [7:0]DOADO;
  input [7:0]\douta[10] ;
  input [7:0]\douta[10]_0 ;
  input [0:0]DOPADOP;
  input [0:0]\douta[11] ;
  input [0:0]\douta[11]_0 ;
  input [4:0]addra;
  input clka;
  input [0:0]\douta[0] ;
  input [0:0]DOUTA;
  input [1:0]\douta[2] ;
  input [0:0]\douta[1] ;
  input [0:0]\douta[2]_0 ;
  input [7:0]\douta[10]_INST_0_i_3_0 ;
  input [7:0]\douta[10]_INST_0_i_3_1 ;
  input [7:0]\douta[10]_INST_0_i_3_2 ;
  input [7:0]\douta[10]_INST_0_i_3_3 ;
  input [7:0]\douta[10]_INST_0_i_3_4 ;
  input [7:0]\douta[10]_INST_0_i_3_5 ;
  input [7:0]\douta[10]_INST_0_i_3_6 ;
  input [7:0]\douta[10]_INST_0_i_3_7 ;
  input [7:0]\douta[10]_INST_0_i_2_0 ;
  input [7:0]\douta[10]_INST_0_i_2_1 ;
  input [7:0]\douta[10]_INST_0_i_2_2 ;
  input [7:0]\douta[10]_INST_0_i_2_3 ;
  input [7:0]\douta[10]_INST_0_i_2_4 ;
  input [7:0]\douta[10]_INST_0_i_2_5 ;
  input [7:0]\douta[10]_INST_0_i_2_6 ;
  input [7:0]\douta[10]_INST_0_i_2_7 ;
  input [0:0]\douta[11]_INST_0_i_3_0 ;
  input [0:0]\douta[11]_INST_0_i_3_1 ;
  input [0:0]\douta[11]_INST_0_i_3_2 ;
  input [0:0]\douta[11]_INST_0_i_3_3 ;
  input [0:0]\douta[11]_INST_0_i_3_4 ;
  input [0:0]\douta[11]_INST_0_i_3_5 ;
  input [0:0]\douta[11]_INST_0_i_3_6 ;
  input [0:0]\douta[11]_INST_0_i_3_7 ;
  input [0:0]\douta[11]_INST_0_i_2_0 ;
  input [0:0]\douta[11]_INST_0_i_2_1 ;
  input [0:0]\douta[11]_INST_0_i_2_2 ;
  input [0:0]\douta[11]_INST_0_i_2_3 ;
  input [0:0]\douta[11]_INST_0_i_2_4 ;
  input [0:0]\douta[11]_INST_0_i_2_5 ;
  input [0:0]\douta[11]_INST_0_i_2_6 ;
  input [0:0]\douta[11]_INST_0_i_2_7 ;

  wire [7:0]DOADO;
  wire [0:0]DOPADOP;
  wire [0:0]DOUTA;
  wire [4:0]addra;
  wire clka;
  wire [11:0]douta;
  wire [0:0]\douta[0] ;
  wire [7:0]\douta[10] ;
  wire [7:0]\douta[10]_0 ;
  wire \douta[10]_INST_0_i_1_n_0 ;
  wire [7:0]\douta[10]_INST_0_i_2_0 ;
  wire [7:0]\douta[10]_INST_0_i_2_1 ;
  wire [7:0]\douta[10]_INST_0_i_2_2 ;
  wire [7:0]\douta[10]_INST_0_i_2_3 ;
  wire [7:0]\douta[10]_INST_0_i_2_4 ;
  wire [7:0]\douta[10]_INST_0_i_2_5 ;
  wire [7:0]\douta[10]_INST_0_i_2_6 ;
  wire [7:0]\douta[10]_INST_0_i_2_7 ;
  wire \douta[10]_INST_0_i_2_n_0 ;
  wire [7:0]\douta[10]_INST_0_i_3_0 ;
  wire [7:0]\douta[10]_INST_0_i_3_1 ;
  wire [7:0]\douta[10]_INST_0_i_3_2 ;
  wire [7:0]\douta[10]_INST_0_i_3_3 ;
  wire [7:0]\douta[10]_INST_0_i_3_4 ;
  wire [7:0]\douta[10]_INST_0_i_3_5 ;
  wire [7:0]\douta[10]_INST_0_i_3_6 ;
  wire [7:0]\douta[10]_INST_0_i_3_7 ;
  wire \douta[10]_INST_0_i_3_n_0 ;
  wire \douta[10]_INST_0_i_4_n_0 ;
  wire \douta[10]_INST_0_i_5_n_0 ;
  wire \douta[10]_INST_0_i_6_n_0 ;
  wire \douta[10]_INST_0_i_7_n_0 ;
  wire [0:0]\douta[11] ;
  wire [0:0]\douta[11]_0 ;
  wire \douta[11]_INST_0_i_1_n_0 ;
  wire [0:0]\douta[11]_INST_0_i_2_0 ;
  wire [0:0]\douta[11]_INST_0_i_2_1 ;
  wire [0:0]\douta[11]_INST_0_i_2_2 ;
  wire [0:0]\douta[11]_INST_0_i_2_3 ;
  wire [0:0]\douta[11]_INST_0_i_2_4 ;
  wire [0:0]\douta[11]_INST_0_i_2_5 ;
  wire [0:0]\douta[11]_INST_0_i_2_6 ;
  wire [0:0]\douta[11]_INST_0_i_2_7 ;
  wire \douta[11]_INST_0_i_2_n_0 ;
  wire [0:0]\douta[11]_INST_0_i_3_0 ;
  wire [0:0]\douta[11]_INST_0_i_3_1 ;
  wire [0:0]\douta[11]_INST_0_i_3_2 ;
  wire [0:0]\douta[11]_INST_0_i_3_3 ;
  wire [0:0]\douta[11]_INST_0_i_3_4 ;
  wire [0:0]\douta[11]_INST_0_i_3_5 ;
  wire [0:0]\douta[11]_INST_0_i_3_6 ;
  wire [0:0]\douta[11]_INST_0_i_3_7 ;
  wire \douta[11]_INST_0_i_3_n_0 ;
  wire \douta[11]_INST_0_i_4_n_0 ;
  wire \douta[11]_INST_0_i_5_n_0 ;
  wire \douta[11]_INST_0_i_6_n_0 ;
  wire \douta[11]_INST_0_i_7_n_0 ;
  wire [0:0]\douta[1] ;
  wire [1:0]\douta[2] ;
  wire [0:0]\douta[2]_0 ;
  wire \douta[3]_INST_0_i_1_n_0 ;
  wire \douta[3]_INST_0_i_2_n_0 ;
  wire \douta[3]_INST_0_i_3_n_0 ;
  wire \douta[3]_INST_0_i_4_n_0 ;
  wire \douta[3]_INST_0_i_5_n_0 ;
  wire \douta[3]_INST_0_i_6_n_0 ;
  wire \douta[3]_INST_0_i_7_n_0 ;
  wire \douta[4]_INST_0_i_1_n_0 ;
  wire \douta[4]_INST_0_i_2_n_0 ;
  wire \douta[4]_INST_0_i_3_n_0 ;
  wire \douta[4]_INST_0_i_4_n_0 ;
  wire \douta[4]_INST_0_i_5_n_0 ;
  wire \douta[4]_INST_0_i_6_n_0 ;
  wire \douta[4]_INST_0_i_7_n_0 ;
  wire \douta[5]_INST_0_i_1_n_0 ;
  wire \douta[5]_INST_0_i_2_n_0 ;
  wire \douta[5]_INST_0_i_3_n_0 ;
  wire \douta[5]_INST_0_i_4_n_0 ;
  wire \douta[5]_INST_0_i_5_n_0 ;
  wire \douta[5]_INST_0_i_6_n_0 ;
  wire \douta[5]_INST_0_i_7_n_0 ;
  wire \douta[6]_INST_0_i_1_n_0 ;
  wire \douta[6]_INST_0_i_2_n_0 ;
  wire \douta[6]_INST_0_i_3_n_0 ;
  wire \douta[6]_INST_0_i_4_n_0 ;
  wire \douta[6]_INST_0_i_5_n_0 ;
  wire \douta[6]_INST_0_i_6_n_0 ;
  wire \douta[6]_INST_0_i_7_n_0 ;
  wire \douta[7]_INST_0_i_1_n_0 ;
  wire \douta[7]_INST_0_i_2_n_0 ;
  wire \douta[7]_INST_0_i_3_n_0 ;
  wire \douta[7]_INST_0_i_4_n_0 ;
  wire \douta[7]_INST_0_i_5_n_0 ;
  wire \douta[7]_INST_0_i_6_n_0 ;
  wire \douta[7]_INST_0_i_7_n_0 ;
  wire \douta[8]_INST_0_i_1_n_0 ;
  wire \douta[8]_INST_0_i_2_n_0 ;
  wire \douta[8]_INST_0_i_3_n_0 ;
  wire \douta[8]_INST_0_i_4_n_0 ;
  wire \douta[8]_INST_0_i_5_n_0 ;
  wire \douta[8]_INST_0_i_6_n_0 ;
  wire \douta[8]_INST_0_i_7_n_0 ;
  wire \douta[9]_INST_0_i_1_n_0 ;
  wire \douta[9]_INST_0_i_2_n_0 ;
  wire \douta[9]_INST_0_i_3_n_0 ;
  wire \douta[9]_INST_0_i_4_n_0 ;
  wire \douta[9]_INST_0_i_5_n_0 ;
  wire \douta[9]_INST_0_i_6_n_0 ;
  wire \douta[9]_INST_0_i_7_n_0 ;
  wire [4:0]sel_pipe;
  wire [4:0]sel_pipe_d1;

  LUT5 #(
    .INIT(32'h04FF0400)) 
    \douta[0]_INST_0 
       (.I0(sel_pipe_d1[2]),
        .I1(\douta[0] ),
        .I2(sel_pipe_d1[3]),
        .I3(sel_pipe_d1[4]),
        .I4(DOUTA),
        .O(douta[0]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[10]_INST_0 
       (.I0(\douta[10]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[10]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[10]_INST_0_i_3_n_0 ),
        .O(douta[10]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[10]_INST_0_i_1 
       (.I0(DOADO[7]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [7]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [7]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[10]_INST_0_i_1_n_0 ));
  MUXF7 \douta[10]_INST_0_i_2 
       (.I0(\douta[10]_INST_0_i_4_n_0 ),
        .I1(\douta[10]_INST_0_i_5_n_0 ),
        .O(\douta[10]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[10]_INST_0_i_3 
       (.I0(\douta[10]_INST_0_i_6_n_0 ),
        .I1(\douta[10]_INST_0_i_7_n_0 ),
        .O(\douta[10]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[10]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [7]),
        .I1(\douta[10]_INST_0_i_2_1 [7]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [7]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [7]),
        .O(\douta[10]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[10]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [7]),
        .I1(\douta[10]_INST_0_i_2_5 [7]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [7]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [7]),
        .O(\douta[10]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[10]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [7]),
        .I1(\douta[10]_INST_0_i_3_1 [7]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [7]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [7]),
        .O(\douta[10]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[10]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [7]),
        .I1(\douta[10]_INST_0_i_3_5 [7]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [7]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [7]),
        .O(\douta[10]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[11]_INST_0 
       (.I0(\douta[11]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[11]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[11]_INST_0_i_3_n_0 ),
        .O(douta[11]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[11]_INST_0_i_1 
       (.I0(DOPADOP),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[11] ),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[11]_0 ),
        .I5(sel_pipe_d1[2]),
        .O(\douta[11]_INST_0_i_1_n_0 ));
  MUXF7 \douta[11]_INST_0_i_2 
       (.I0(\douta[11]_INST_0_i_4_n_0 ),
        .I1(\douta[11]_INST_0_i_5_n_0 ),
        .O(\douta[11]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[11]_INST_0_i_3 
       (.I0(\douta[11]_INST_0_i_6_n_0 ),
        .I1(\douta[11]_INST_0_i_7_n_0 ),
        .O(\douta[11]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[11]_INST_0_i_4 
       (.I0(\douta[11]_INST_0_i_2_0 ),
        .I1(\douta[11]_INST_0_i_2_1 ),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[11]_INST_0_i_2_2 ),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[11]_INST_0_i_2_3 ),
        .O(\douta[11]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[11]_INST_0_i_5 
       (.I0(\douta[11]_INST_0_i_2_4 ),
        .I1(\douta[11]_INST_0_i_2_5 ),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[11]_INST_0_i_2_6 ),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[11]_INST_0_i_2_7 ),
        .O(\douta[11]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[11]_INST_0_i_6 
       (.I0(\douta[11]_INST_0_i_3_0 ),
        .I1(\douta[11]_INST_0_i_3_1 ),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[11]_INST_0_i_3_2 ),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[11]_INST_0_i_3_3 ),
        .O(\douta[11]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[11]_INST_0_i_7 
       (.I0(\douta[11]_INST_0_i_3_4 ),
        .I1(\douta[11]_INST_0_i_3_5 ),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[11]_INST_0_i_3_6 ),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[11]_INST_0_i_3_7 ),
        .O(\douta[11]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \douta[1]_INST_0 
       (.I0(sel_pipe_d1[2]),
        .I1(\douta[2] [0]),
        .I2(sel_pipe_d1[3]),
        .I3(sel_pipe_d1[4]),
        .I4(\douta[1] ),
        .O(douta[1]));
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \douta[2]_INST_0 
       (.I0(sel_pipe_d1[2]),
        .I1(\douta[2] [1]),
        .I2(sel_pipe_d1[3]),
        .I3(sel_pipe_d1[4]),
        .I4(\douta[2]_0 ),
        .O(douta[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[3]_INST_0 
       (.I0(\douta[3]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[3]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[3]_INST_0_i_3_n_0 ),
        .O(douta[3]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[3]_INST_0_i_1 
       (.I0(DOADO[0]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [0]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [0]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[3]_INST_0_i_1_n_0 ));
  MUXF7 \douta[3]_INST_0_i_2 
       (.I0(\douta[3]_INST_0_i_4_n_0 ),
        .I1(\douta[3]_INST_0_i_5_n_0 ),
        .O(\douta[3]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[3]_INST_0_i_3 
       (.I0(\douta[3]_INST_0_i_6_n_0 ),
        .I1(\douta[3]_INST_0_i_7_n_0 ),
        .O(\douta[3]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[3]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [0]),
        .I1(\douta[10]_INST_0_i_2_1 [0]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [0]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [0]),
        .O(\douta[3]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[3]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [0]),
        .I1(\douta[10]_INST_0_i_2_5 [0]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [0]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [0]),
        .O(\douta[3]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[3]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [0]),
        .I1(\douta[10]_INST_0_i_3_1 [0]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [0]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [0]),
        .O(\douta[3]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[3]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [0]),
        .I1(\douta[10]_INST_0_i_3_5 [0]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [0]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [0]),
        .O(\douta[3]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[4]_INST_0 
       (.I0(\douta[4]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[4]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[4]_INST_0_i_3_n_0 ),
        .O(douta[4]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[4]_INST_0_i_1 
       (.I0(DOADO[1]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [1]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [1]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[4]_INST_0_i_1_n_0 ));
  MUXF7 \douta[4]_INST_0_i_2 
       (.I0(\douta[4]_INST_0_i_4_n_0 ),
        .I1(\douta[4]_INST_0_i_5_n_0 ),
        .O(\douta[4]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[4]_INST_0_i_3 
       (.I0(\douta[4]_INST_0_i_6_n_0 ),
        .I1(\douta[4]_INST_0_i_7_n_0 ),
        .O(\douta[4]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[4]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [1]),
        .I1(\douta[10]_INST_0_i_2_1 [1]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [1]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [1]),
        .O(\douta[4]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[4]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [1]),
        .I1(\douta[10]_INST_0_i_2_5 [1]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [1]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [1]),
        .O(\douta[4]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[4]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [1]),
        .I1(\douta[10]_INST_0_i_3_1 [1]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [1]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [1]),
        .O(\douta[4]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[4]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [1]),
        .I1(\douta[10]_INST_0_i_3_5 [1]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [1]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [1]),
        .O(\douta[4]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[5]_INST_0 
       (.I0(\douta[5]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[5]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[5]_INST_0_i_3_n_0 ),
        .O(douta[5]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[5]_INST_0_i_1 
       (.I0(DOADO[2]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [2]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [2]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[5]_INST_0_i_1_n_0 ));
  MUXF7 \douta[5]_INST_0_i_2 
       (.I0(\douta[5]_INST_0_i_4_n_0 ),
        .I1(\douta[5]_INST_0_i_5_n_0 ),
        .O(\douta[5]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[5]_INST_0_i_3 
       (.I0(\douta[5]_INST_0_i_6_n_0 ),
        .I1(\douta[5]_INST_0_i_7_n_0 ),
        .O(\douta[5]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[5]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [2]),
        .I1(\douta[10]_INST_0_i_2_1 [2]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [2]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [2]),
        .O(\douta[5]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[5]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [2]),
        .I1(\douta[10]_INST_0_i_2_5 [2]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [2]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [2]),
        .O(\douta[5]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[5]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [2]),
        .I1(\douta[10]_INST_0_i_3_1 [2]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [2]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [2]),
        .O(\douta[5]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[5]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [2]),
        .I1(\douta[10]_INST_0_i_3_5 [2]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [2]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [2]),
        .O(\douta[5]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[6]_INST_0 
       (.I0(\douta[6]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[6]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[6]_INST_0_i_3_n_0 ),
        .O(douta[6]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[6]_INST_0_i_1 
       (.I0(DOADO[3]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [3]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [3]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[6]_INST_0_i_1_n_0 ));
  MUXF7 \douta[6]_INST_0_i_2 
       (.I0(\douta[6]_INST_0_i_4_n_0 ),
        .I1(\douta[6]_INST_0_i_5_n_0 ),
        .O(\douta[6]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[6]_INST_0_i_3 
       (.I0(\douta[6]_INST_0_i_6_n_0 ),
        .I1(\douta[6]_INST_0_i_7_n_0 ),
        .O(\douta[6]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[6]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [3]),
        .I1(\douta[10]_INST_0_i_2_1 [3]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [3]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [3]),
        .O(\douta[6]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[6]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [3]),
        .I1(\douta[10]_INST_0_i_2_5 [3]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [3]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [3]),
        .O(\douta[6]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[6]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [3]),
        .I1(\douta[10]_INST_0_i_3_1 [3]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [3]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [3]),
        .O(\douta[6]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[6]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [3]),
        .I1(\douta[10]_INST_0_i_3_5 [3]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [3]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [3]),
        .O(\douta[6]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[7]_INST_0 
       (.I0(\douta[7]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[7]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[7]_INST_0_i_3_n_0 ),
        .O(douta[7]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[7]_INST_0_i_1 
       (.I0(DOADO[4]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [4]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [4]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[7]_INST_0_i_1_n_0 ));
  MUXF7 \douta[7]_INST_0_i_2 
       (.I0(\douta[7]_INST_0_i_4_n_0 ),
        .I1(\douta[7]_INST_0_i_5_n_0 ),
        .O(\douta[7]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[7]_INST_0_i_3 
       (.I0(\douta[7]_INST_0_i_6_n_0 ),
        .I1(\douta[7]_INST_0_i_7_n_0 ),
        .O(\douta[7]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[7]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [4]),
        .I1(\douta[10]_INST_0_i_2_1 [4]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [4]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [4]),
        .O(\douta[7]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[7]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [4]),
        .I1(\douta[10]_INST_0_i_2_5 [4]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [4]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [4]),
        .O(\douta[7]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[7]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [4]),
        .I1(\douta[10]_INST_0_i_3_1 [4]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [4]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [4]),
        .O(\douta[7]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[7]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [4]),
        .I1(\douta[10]_INST_0_i_3_5 [4]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [4]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [4]),
        .O(\douta[7]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[8]_INST_0 
       (.I0(\douta[8]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[8]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[8]_INST_0_i_3_n_0 ),
        .O(douta[8]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[8]_INST_0_i_1 
       (.I0(DOADO[5]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [5]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [5]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[8]_INST_0_i_1_n_0 ));
  MUXF7 \douta[8]_INST_0_i_2 
       (.I0(\douta[8]_INST_0_i_4_n_0 ),
        .I1(\douta[8]_INST_0_i_5_n_0 ),
        .O(\douta[8]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[8]_INST_0_i_3 
       (.I0(\douta[8]_INST_0_i_6_n_0 ),
        .I1(\douta[8]_INST_0_i_7_n_0 ),
        .O(\douta[8]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[8]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [5]),
        .I1(\douta[10]_INST_0_i_2_1 [5]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [5]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [5]),
        .O(\douta[8]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[8]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [5]),
        .I1(\douta[10]_INST_0_i_2_5 [5]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [5]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [5]),
        .O(\douta[8]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[8]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [5]),
        .I1(\douta[10]_INST_0_i_3_1 [5]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [5]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [5]),
        .O(\douta[8]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[8]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [5]),
        .I1(\douta[10]_INST_0_i_3_5 [5]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [5]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [5]),
        .O(\douta[8]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \douta[9]_INST_0 
       (.I0(\douta[9]_INST_0_i_1_n_0 ),
        .I1(sel_pipe_d1[4]),
        .I2(\douta[9]_INST_0_i_2_n_0 ),
        .I3(sel_pipe_d1[3]),
        .I4(\douta[9]_INST_0_i_3_n_0 ),
        .O(douta[9]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \douta[9]_INST_0_i_1 
       (.I0(DOADO[6]),
        .I1(sel_pipe_d1[0]),
        .I2(\douta[10] [6]),
        .I3(sel_pipe_d1[1]),
        .I4(\douta[10]_0 [6]),
        .I5(sel_pipe_d1[2]),
        .O(\douta[9]_INST_0_i_1_n_0 ));
  MUXF7 \douta[9]_INST_0_i_2 
       (.I0(\douta[9]_INST_0_i_4_n_0 ),
        .I1(\douta[9]_INST_0_i_5_n_0 ),
        .O(\douta[9]_INST_0_i_2_n_0 ),
        .S(sel_pipe_d1[2]));
  MUXF7 \douta[9]_INST_0_i_3 
       (.I0(\douta[9]_INST_0_i_6_n_0 ),
        .I1(\douta[9]_INST_0_i_7_n_0 ),
        .O(\douta[9]_INST_0_i_3_n_0 ),
        .S(sel_pipe_d1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[9]_INST_0_i_4 
       (.I0(\douta[10]_INST_0_i_2_0 [6]),
        .I1(\douta[10]_INST_0_i_2_1 [6]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_2 [6]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_3 [6]),
        .O(\douta[9]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[9]_INST_0_i_5 
       (.I0(\douta[10]_INST_0_i_2_4 [6]),
        .I1(\douta[10]_INST_0_i_2_5 [6]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_2_6 [6]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_2_7 [6]),
        .O(\douta[9]_INST_0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[9]_INST_0_i_6 
       (.I0(\douta[10]_INST_0_i_3_0 [6]),
        .I1(\douta[10]_INST_0_i_3_1 [6]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_2 [6]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_3 [6]),
        .O(\douta[9]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \douta[9]_INST_0_i_7 
       (.I0(\douta[10]_INST_0_i_3_4 [6]),
        .I1(\douta[10]_INST_0_i_3_5 [6]),
        .I2(sel_pipe_d1[1]),
        .I3(\douta[10]_INST_0_i_3_6 [6]),
        .I4(sel_pipe_d1[0]),
        .I5(\douta[10]_INST_0_i_3_7 [6]),
        .O(\douta[9]_INST_0_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[0] 
       (.C(clka),
        .CE(1'b1),
        .D(sel_pipe[0]),
        .Q(sel_pipe_d1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[1] 
       (.C(clka),
        .CE(1'b1),
        .D(sel_pipe[1]),
        .Q(sel_pipe_d1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[2] 
       (.C(clka),
        .CE(1'b1),
        .D(sel_pipe[2]),
        .Q(sel_pipe_d1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[3] 
       (.C(clka),
        .CE(1'b1),
        .D(sel_pipe[3]),
        .Q(sel_pipe_d1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_norm_sel2.has_mem_regs.WITHOUT_ECC_PIPE.ce_pri.sel_pipe_d1_reg[4] 
       (.C(clka),
        .CE(1'b1),
        .D(sel_pipe[4]),
        .Q(sel_pipe_d1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_sel_reg.ce_pri.sel_pipe_reg[0] 
       (.C(clka),
        .CE(1'b1),
        .D(addra[0]),
        .Q(sel_pipe[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_sel_reg.ce_pri.sel_pipe_reg[1] 
       (.C(clka),
        .CE(1'b1),
        .D(addra[1]),
        .Q(sel_pipe[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_sel_reg.ce_pri.sel_pipe_reg[2] 
       (.C(clka),
        .CE(1'b1),
        .D(addra[2]),
        .Q(sel_pipe[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_sel_reg.ce_pri.sel_pipe_reg[3] 
       (.C(clka),
        .CE(1'b1),
        .D(addra[3]),
        .Q(sel_pipe[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \no_softecc_sel_reg.ce_pri.sel_pipe_reg[4] 
       (.C(clka),
        .CE(1'b1),
        .D(addra[4]),
        .Q(sel_pipe[4]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init \prim_init.ram 
       (.DOUTA(DOUTA),
        .ENA(ENA),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized0
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ,
    clka,
    ram_ena,
    addra,
    dina,
    wea);
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  input clka;
  input ram_ena;
  input [13:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ;
  wire [13:0]addra;
  wire clka;
  wire [0:0]dina;
  wire ram_ena;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized0 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ram_ena(ram_ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized1
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized1 \prim_init.ram 
       (.DOUTA(DOUTA),
        .ENA(ENA),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized10
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized10 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized11
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [16:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [16:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized11 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized12
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized12 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized13
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized13 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized14
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized14 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized15
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized15 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized16
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized16 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized17
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized17 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized18
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized18 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized19
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized19 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized2
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    clka,
    ram_ena,
    addra,
    dina,
    wea);
  output [1:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  input clka;
  input ram_ena;
  input [13:0]addra;
  input [1:0]dina;
  input [0:0]wea;

  wire [1:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [13:0]addra;
  wire clka;
  wire [1:0]dina;
  wire ram_ena;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized2 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ram_ena(ram_ena),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized20
   (DOADO,
    DOPADOP,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]DOADO;
  output [0:0]DOPADOP;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]DOADO;
  wire [0:0]DOPADOP;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized20 \prim_init.ram 
       (.DOADO(DOADO),
        .DOPADOP(DOPADOP),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized21
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized21 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized22
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized22 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized3
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized3 \prim_init.ram 
       (.DOUTA(DOUTA),
        .ENA(ENA),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized4
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized4 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized5
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized5 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized6
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized6 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized7
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized7 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized8
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized8 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_width" *) 
module blk_mem_gen_0_blk_mem_gen_prim_width__parameterized9
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized9 \prim_init.ram 
       (.\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram ),
        .\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ),
        .addra(addra),
        .clka(clka),
        .dina(dina),
        .ena_array(ena_array),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire CASCADEINA;
  wire CASCADEINB;
  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ;
  wire [31:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFB7CF5C7FE3FF817FCF9F87FC07C237FFF3F7FF1E3FFE06FFF07021FC270E3),
    .INIT_01(256'h3FF0E0003007FE037FFE0C3FF87FF0000FFF06021FC08FC4FC1F9F43FFFFE17F),
    .INIT_02(256'hF99800DF7FFFE00006380F030340FDDCFC0FFE7FFFFFF87FFF1B183E83FFFBF0),
    .INIT_03(256'h00301E0303C36E57FF0FFC7FFFFFF1FFF9FCAE9E79F3E3F038004000303FFE3F),
    .INIT_04(256'hFF3FF8FFFFFFFFFFC078336FFA6000FF0000000020FFFFFFFF2018DCFFF000FF),
    .INIT_05(256'h80613377F5E138FFC102000001FFFFFFFF0FF8387FE301FF00003F03000FB57F),
    .INIT_06(256'hFF00000003FF3FFFFF3FFFF97F0301FE00003E000001FEFEFF7FF8FFFFFFFFFF),
    .INIT_07(256'hFF3FFFFFFEB301FC00007E4C000DFFF9FF7FC0FFFFFFFFFFFFF1DFF3F5F8023F),
    .INIT_08(256'h01F02C5C0F17FFFEFF7F80FEC3FDFFFFFFEF1FBF83F8801FFC00000207F83E1F),
    .INIT_09(256'hFFFFCFFFE7FFFFFFFFEEFFFFBE00F31FFC0000003FE0FDDF5F9FC1FFFE0781F8),
    .INIT_0A(256'hFFDFFE3C1F81673FF00007EF3FF0F03C0702FCFFE387FFF003F0D8987FA7FFE8),
    .INIT_0B(256'hE063FFFF3E78E0F86003FFFF980FF00023E0000EF3CFFFE1FFFFDFF1C3FFFFFE),
    .INIT_0C(256'h00079F030000600037C0001E70BC07FFFFFF738001FFF83CFF3EF880080004FF),
    .INIT_0D(256'h000001FEF33907FBFFFE7F868FFFC1BFF57FF0E0C00009FF00FFFFF07F71FFF8),
    .INIT_0E(256'hFFFEFEC3FFFF003FE21F7000C00003FF00FFFD80FFE3FFF10F23303800000000),
    .INIT_0F(256'hE3CC1880000027FD401F7083FB07FC00F400700300F00000000000F36F783FF5),
    .INIT_10(256'h30381003E01FE000800007C4FFF0001C000C00E1DDF8FFC3FF387E63FFFF300F),
    .INIT_11(256'hFFC01FC08FE0C03F787E738FC7FFFFDFF833FF6BFFFF30F1ECB9CF8000000FE0),
    .INIT_12(256'h783E03DFFF3FFFF3F3FC001B3F7C003F0CE1070000001E0000783807FE7E030F),
    .INIT_13(256'hA7FFD138673CC30FC4E68260C000780680F07C1FFFFFF9FFFFCC7FC781FFC1FF),
    .INIT_14(256'h794FFC00E09C38310121FFFFC7FFFFFF03F07F1FFFFC7C12043C03C0FF81FF83),
    .INIT_15(256'h040FFF0001FFFFFFC741FFFFF0000000ECDF0F86FE01F787FFFF31816C5A200F),
    .INIT_16(256'h8300FCF8C00000006A30FF03E7FFC67CFFF8203FEC05FE0359EDF800C7F83800),
    .INIT_17(256'hF703FE030FFF817FFFF8139CF8233FFFFFF0080003C330003E9FF80173FC707F),
    .INIT_18(256'hFFFD9C719000BFFFBFFC5000000D40003EFF9007FFF000000340000000000000),
    .INIT_19(256'hBF41F00000D800713E7FDFFFFFE000001FC0000000000000CF6180E31FFDF0F9),
    .INIT_1A(256'hFF7FFFFFFF0001FFFFC0010000000000FFE061E3BFFA71D3FFE43F1F38DC5BFF),
    .INIT_1B(256'hFF8007C0000000027FFB07C7FFFB0667FC7C3F7F20DCFFF9A03FF80241C30073),
    .INIT_1C(256'h7E8E0707FB7174DFF89FF0F9001E0E81E3FDF004010201FFFFFFBFFC0FF80FFF),
    .INIT_1D(256'hF899E69B98843E00F9C1100F800001FFFEFCFFF81F1FFFE0C3003FF880000002),
    .INIT_1E(256'h7F03C027800000FFFF3CFF403D3FE000000000000000000000003CF7F71EFC00),
    .INIT_1F(256'hF938FFE038FFC0C00000000000000000000000C7C7DFFECFF07FD7E021003E00),
    .INIT_20(256'hC0000000000000000000100780003E0FFFFFFBFE7F001C007E03C000130200FF),
    .INIT_21(256'hE7FF7F0081001CFCFFFFC7E921F000000C0100043C0E03E1FA7DFFF3E00703FF),
    .INIT_22(256'hFF3FC7CFFEF800001F00000DFF061FC007FFFFF7F00F07FFFFF8000000000000),
    .INIT_23(256'h1F000003C001FF03CFFFFF43F33C07F8FFFA8E1078000000FFFC3F80010001FC),
    .INIT_24(256'hFFFF8703FFF00F0000030101E0000000400000804064F1F8FFFFE01FFF100000),
    .INIT_25(256'h00000000F000000000000380006480F3FFFFC1FFE310000001800063B001FC07),
    .INIT_26(256'h00000360006788607FFD38FBC33000000DC000412700F03F9FFE0603FFC0FF40),
    .INIT_27(256'h0FFCB308FFF000000DC0000700F0607F1F8E04F3FFC3FC0000000000E0000000),
    .INIT_28(256'hCE006300E0E0E3F07F1F8461FFFC40005C01000000000000000000000000E0F8),
    .INIT_29(256'hFFC4000FFFF8000FFF010000000000000000C0000000FFFF9DDF9F5A5F000000),
    .INIT_2A(256'h800060000000000000000000000000FFFFFFFFFFAF8000008401E80000001F00),
    .INIT_2B(256'h00FF00007000FFFF7FFFFFFFED8000000000DC0000081E03F08FFFFFC1637F3F),
    .INIT_2C(256'hFFFF7732830000001C003800003C1C0FC0CFFFFD01F0FFF00000000000000000),
    .INIT_2D(256'h40007000003C1C7FC08001FF07FFF080000000000000000000FF00004000FFFF),
    .INIT_2E(256'hC000FFFFFFF9C000000000000000000000FFF000FC00FFFFFFFFFF576E000000),
    .INIT_2F(256'h000000000000000000000000F00000FFC0FFFFD20E0000000000700000F801FF),
    .INIT_30(256'h00000000700000FFE07DF1A0010000000200000001C003FC809FFFE1F0000100),
    .INIT_31(256'hE0780A070000000022000000000301F8003FFE30F00000000000000000000000),
    .INIT_32(256'h00000000003FDFE080FE0380E0FFF0000000000000000000000000001C00FFFF),
    .INIT_33(256'hC7E3E07FF8FD80000000000000000000000000003C00FFFFFFF8F40F00000000),
    .INIT_34(256'h504000000000000000000021730000FFFFF800000000000000F00000007FFFC0),
    .INIT_35(256'h000000217F01FFFFFFE000000000000000F00000003F70210FDE07FFE0FC0004),
    .INIT_36(256'h59D0F01F0000000000000000000000203F83FFFFF1F800000000FFFF00FF00FE),
    .INIT_37(256'h00000000000000037FFFFFFFFFF000030000FFFF00FF00FE000040007C4CFFFF),
    .INIT_38(256'h7FF7FFFFF00000000003FFFFFFFFFFFE0000C4007FFCFFFFDA07FC1F00000000),
    .INIT_39(256'h071FFFFFFFFFFFFF00FF1C9C7FF3FFFFDBCE1807000000000000000000000003),
    .INIT_3A(256'h000F009C7FFFFFFFF9C070C0000000000000000000000007E007F80000000000),
    .INIT_3B(256'hFC00C1E000000000000000000000008F987FE000000000200517FF80F805F07F),
    .INIT_3C(256'h000000000000F00071FFF8000000000015F1F000C005E01CC00F00007FFFFFFC),
    .INIT_3D(256'hEFFCE0006000000000F0F0004001E000E00F03C07FFFFF7C0000000000000000),
    .INIT_3E(256'h00F9FC0000070070FC03FFFCFFFFFFFF0000000000000000000000000001FE0C),
    .INIT_3F(256'hFF00FFFFFFFFFFDF00000000000000000000000000703C80FFE0000000000000),
    .INIT_40(256'h8000000000000000000000003FF8001FFEFE00000000000000F9FC00701F00F0),
    .INIT_41(256'h00000000FFFF03FFF8080000000000000071F00003FF03F800021FF07FFFFF03),
    .INIT_42(256'hC000003E000000FF0031000003FF01FC00070000FFFFFFE78000000000000000),
    .INIT_43(256'h000000000FFF00FEE3E0000055FFFFF380000000000000000000000078070FFF),
    .INIT_44(256'hE7F0000000FFFFFF80000000000000000000000060033FE00001E03C80000000),
    .INIT_45(256'h8000000000000000000000004003FF9804FE9F10000000000031FC000FFE209E),
    .INIT_46(256'h00000000E307FE0000FFE000000000000000FF000FE97FD07FF171E60EFFFFFF),
    .INIT_47(256'h00780000000000000000FF000F407FD1FFB7F5E3E0FFFFF80000000000000000),
    .INIT_48(256'h00000C001F407C9FD83FF7FFF0FFFF80000000000000000000000000003FE000),
    .INIT_49(256'hE73FF3FFFBFFFFF000000000000000000000000000FC80070000000000FF0000),
    .INIT_4A(256'h0000000000000000000000001FF9001F0000000000FF000000000C003FC4E6FF),
    .INIT_4B(256'h000000007CE39EFF0000000000FF0000000030C03F1CF9FFE7F9FFE3737F7CC0),
    .INIT_4C(256'h000040F8FFFF00000000FFC0FF03F6E707F6FF8043FFFCC00000000000000000),
    .INIT_4D(256'h0000FFFFFF4FF80000E13F0FFFFFF0C000000000000000007FFF000038000600),
    .INIT_4E(256'h181FF03FFF7FF0C000000000000000007FFF00000000000000007FFFFFFF0000),
    .INIT_4F(256'h00000000000000000703000000000000000001FFFFFF00000000FFFFFE0FFD70),
    .INIT_50(256'hC2030000000100000F6700BFFFFFFFFE0001FD5EE11FF020E000001FF87FFCFC),
    .INIT_51(256'h0F1BE00FFFFFFFFE0000FDFF61FF7F00FFDE007EF83FFCFD00000000007E0000),
    .INIT_52(256'h00FFFFF1E1F000003FC01E7F83FFFF710000000000400000C20300000000F000),
    .INIT_53(256'h7A101FFF03FFFFF100000000048000000E0380030000C44007E7E20F00000000),
    .INIT_54(256'h00000080FF8000001E03000F00005CC00373C007FFFFFFFE00FFFFF06368E0FF),
    .INIT_55(256'h1E030000000011F00378000700FF00FF8000FD807F800000E078FFE7F8FFFFFE),
    .INIT_56(256'hE2F83F0700FF00FFFFFFFBC3FF8D00FFFC7DFCC0FEFFFFBF10000080FFC00000),
    .INIT_57(256'h8000F9C7FC1BF8FFFFFF70E00FFFFFBF300000C007F800001C030007000037F0),
    .INIT_58(256'hFFFE037CFFFFFFC0000003C003FF00001C03000000000404F0007F0100FF00FF),
    .INIT_59(256'h000003E000FE00000001000004C00D0EF0C07F0000FF00FE0000F1EFF07F1FFF),
    .INIT_5A(256'hCE010000F0001007F8000000003FFFFF8000FF00F01F80FFC7FC7FFFFFF7FEFF),
    .INIT_5B(256'hF0000000001FFFFE0000FF00007FBCFFC7127FFFFFFFFF0000000FE00803FF75),
    .INIT_5C(256'h0000F00041FFFFFFF0021FFFFFFFFFC0000007E06042F7BFB8000000F000307F),
    .INIT_5D(256'hF8803FFFFFFFFFF00000078047B7FFFF34000000C000007800020000000300FF),
    .INIT_5E(256'h000007001CBF3FE3FC000000FC001C70001F0000000300FE0000FF00FF7FFFFF),
    .INIT_5F(256'hFE400000FF00F00000071000000000000001FF01FF07FEFFFB073FFFFFC3DCC0),
    .INIT_60(256'h0001F100000000008000FFDFC707FFFFFF7E3FFFFFCDFF4300FF1F0F3FFEFFFF),
    .INIT_61(256'h80FFFFFFE007FFFFFFFF1CFFFFFFFFFFFFFF3F9F7FF9FFFFF4F00001FF10F000),
    .INIT_62(256'hFFFF1FFFFF00F800FFFF3F937FFF7FFC63F80001FFC3F88F0007FFC0000000FF),
    .INIT_63(256'hF8DF3E2797FF10FB69000007FF05F8FEE0000000008000F00055FFFFFE07FFFF),
    .INIT_64(256'hFC1CFC1F010000C0000000001B201CF00000FFFFFE1FFFFFFFFFFFFFF000C03F),
    .INIT_65(256'h00000080D10C87E00000FCFFFFFFFFFFFFFFFFFCE000FFFFF8007CDF0EFFFFFF),
    .INIT_66(256'hFF00FFFFFFFFFFFFFFFFFFC0307FFFFFF000E09F03FFFFFFFFF1000F00000800),
    .INIT_67(256'hFFFFFF007FFFFFFC0000C07EC3FFFFFF0FFC7C0000010B000001FF0FDFFFFC8F),
    .INIT_68(256'h1F61FF763FC03FFF01FEFC0000070000000FFEFF8FBF9E00FFFFFFE0FFFFFFFF),
    .INIT_69(256'h0FFFE1000000000000FFFF830000CF00FFFFFF00FFFFFFFFFFFFFE00FFFFFFF8),
    .INIT_6A(256'h01FF03C7C0C0A307FFFFFFC0FFFFFFFFFFFFFFF3FFFF83E01FE1FF67FFDC3FFC),
    .INIT_6B(256'h80FFFFC0FFFFFFFFFFFFFEFFFFFFBD001FFFFC0FFE3B3C7DCFF037E200000000),
    .INIT_6C(256'hFFFFFC3FFFFFFFFF1FFFE01C71E7303FFFF9EFE2000007C0FFFE87FA3FF37E83),
    .INIT_6D(256'h1FFE011BFE9FF066C010E3F000040FFFFFE017FFFFFFDFFF00FE0FC0FFFFFFFF),
    .INIT_6E(256'h0FE077F800000FFFFF61FFFFFFFFCFFF806E07F0FFFFFFFFFFFFF9F7F07FFFFF),
    .INIT_6F(256'hFC80FF80FF1F91FF00640CF07FFFFFFFFFFCFF07F11FFFFFFFFC00FBF60E74FE),
    .INIT_70(256'h000440F0001F1FFFFFC007F7FFFFE01F800403FF800114E10DF0F01F00001FFF),
    .INIT_71(256'hFFFC07F3FFFFF07FE17F7BE3FFFFDB8E010FF91F00001FFCDC55FF00003F80E7),
    .INIT_72(256'hC31530AFFFFFFFFFA81FFF3B0000007800F9FEFF00FFB8C30002BFFCFFFFFFFF),
    .INIT_73(256'hC83D9F1D0000107C60B1FFFF81FFD8C000037FFE800F7FFFFCCEC707FFFFF0F8),
    .INIT_74(256'h4C73FFFFFFF0BFE000B3FDFF803FFFFFFFFFFF07FFFF0160E3FFF8AF9FFFFFFF),
    .INIT_75(256'h0093C1FFFFFFFFFFFF7FFF003FFC07FBE7FFF93FBFFE003CC3FB5FF800003878),
    .INIT_76(256'hFFFF3FE380007FFFDF8FFFF87FF8DFFD03C1BC3A0000FFF0C3CBFFFFFFF79FF8),
    .INIT_77(256'hC60EC0F47FF0FFFC03337D380400F80007E3FFFFFFF8A6F87CFB70FFFFFFFFFF),
    .INIT_78(256'h860778614D60F7346FC3FFFFF8FB007CFFFF07FFFFFFFFFFFFFFFC80FFE0FFFF),
    .INIT_79(256'h0F81FFFFE3F00EFFFFFFFFFFFFFFFFFFFFFFF8C7FFE7FFFF0F87C07BFFFDFFFA),
    .INIT_7A(256'hFFFF1FFFFFFFFFFFFFFFFFFFE003FFFF0FFFE03FFFFFFFFF0717FC7FF9F803FF),
    .INIT_7B(256'hFFFFFFF0C0F7FFFFFFFFE0FFFFFFFF9FEF3FFE3FF43207FF7F013FE0FFC00F7F),
    .INIT_7C(256'hFEBFFFFFFFFF1FC0FE7FFEFFFC439FC0FF63037DFFB20E00FFFFFFFFFFFFFFFF),
    .INIT_7D(256'hFC3D3FFF3B9BBF00FF62BFFFFF67C0FFFFFF7FFFFFFDFFFFFFFFF3FFFFFF00FF),
    .INIT_7E(256'hFC0F3F7FFF038FFFFFFFFFFFFFFFFFFFFFFFE3F1FFFF3FFFF80FFFFFFFFF1FF9),
    .INIT_7F(256'hFFFC1FFFFFDFFFFFFFFF3FE0407FFFFFF007FFFFFFFF1FFFE1703FF79FDDFE01),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("LOWER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(CASCADEINA),
        .CASCADEOUTB(CASCADEINB),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED [31:0]),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFFF1F1FFFFFFFFFE07FFFFFFFF1FF9E5E91FFF7F8FF83FFC0F1C838CBF0301),
    .INIT_01(256'hFF87FFFBFFFE1FC175F03FFF3FBFF01FC0E023088CBFF1FFFFC01FFFFF9FFFFF),
    .INIT_02(256'h8DF1FFF97DB8F7C000007FF0E1FC00FF1F00FFFFFFF9FFFFFFFF07FFFFFFFFFF),
    .INIT_03(256'h0000FFFF0FF000FFFE207FFFFFF0557FFFFF07FFFFFFFFFFFFE7F381F81E1E00),
    .INIT_04(256'h0020001FFFF0555FFFFF1FFFFFFFFFFFFFFFF1E4F8017C0F85FFFFC00CFCE0BF),
    .INIT_05(256'hFFFFFFFFFFFFFFFF3FFFFFF0F807FC0F81F87FBB8FFCFFF8010FFFE1FFE000FF),
    .INIT_06(256'h1C1FFFFFF81FF10F80F77BB79FE33C71001EF001FFE07FFF00F87FFFFFF0003F),
    .INIT_07(256'h00E077220002478007FF8001FF80FFFFFFFC007FF000000FFFFFFFFFFFFFFFFF),
    .INIT_08(256'hFFFF0001F803FFFEFFF87FFFFFF000FFFFFFFFFFBFFFFFFF0003FFFFF83FE39C),
    .INIT_09(256'h0C00001FFCC000FFFFFFFFFF9FFFFFFFC0000F7FF9FFCFF8600107FCEFC3203E),
    .INIT_0A(256'hFFFFFFFF03FFFFFFE060000023FC1FE0F00007C0001FF3FFFFE007FFE00FFFFF),
    .INIT_0B(256'h0061003003F81FF0F8030FFFFFFFFFE61FC11FFF800FFFFFD0C0FFFFFFE050FF),
    .INIT_0C(256'hF0603FC0FFFFFF1F8007FFFF0007FFFE0001FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0D(256'hF03F80000000FCC00001FFFFFFF0FFF0FFFF003FFFFF800099E0E07103F07FF8),
    .INIT_0E(256'h00FE000700000000FFFF000055FFFFFF9FFFF17E01F0FFF800CFFFC7E000C0C0),
    .INIT_0F(256'hFFFF00007FFFFFFF83FFF3DC000047001FFFFFFFE003C371FCFF0000555480C0),
    .INIT_10(256'hFFFFE3C0000000003FFC400FE0C407FFFFFE0000FFFF0000FFFE001F00000000),
    .INIT_11(256'hFC03F007C000E0FFFFF00000FFFF0000FFFE007F0000FFFF0055FFFFFFFFFFFF),
    .INIT_12(256'hFFF00005FFFF0001FFFE00FF81FFFFFF80FFFFFFFFFFFFF8FFFC11E3F3FF4003),
    .INIT_13(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FEC00B77E3FFF03FF01800038000FC1F),
    .INIT_14(256'hFFFFD57FFFFFFFF0FE000FFB03FFF8FFFFF8FC008000E002B0C005FFFFFF0017),
    .INIT_15(256'hF8078FF8789FFC300FFF7E01C000E3E000007FFFD554007FFFFFFFFFFFFF87FF),
    .INIT_16(256'hE0CC3803FF981F5200707FFF0000C7FF51F7FF0900FF033F00060000FFFFFFE0),
    .INIT_17(256'hA000FFE20000FEFFF00FF190F0F87F0000000001FFFFFFE0C007BFFFFE01F000),
    .INIT_18(256'h00FE807FFF00FC3000E1FFFFFFFF00FF0000FFFEC080FE8000300003FF6007FF),
    .INIT_19(256'hFFFFFFFF0007013F003FFFCFFB00FFF00431FCF90000FF8F80C7FF0000F83CC4),
    .INIT_1A(256'h001FF8067001FFFFF000F0FF03FFFFF7FFFFF003E7F8F80007000FFFFFFFFFFC),
    .INIT_1B(256'hE58E70FEE7FFFF8BFF8C000FC7FFF0F0000FDFFFFFFDFFFCFFFFFFFF00000000),
    .INIT_1C(256'h5C007BFF07FFF0FFFE7FFFCC003F9E00FF000000FF01003FF07FE720D000FFFF),
    .INIT_1D(256'hFF071FFC7F1E7FF0FFF007FE7FFF803FF00FC77D00C0000003FEFFFFFC83FCD3),
    .INIT_1E(256'hFF0007FE7807C387F000C3EF0C8000004FFEFF3F7CFF9C391C007BFC07E100FF),
    .INIT_1F(256'h00C007FD63F09806BF6822F3E7FFF95C780031FFF17F008E2007FF30C3CF7FE0),
    .INIT_20(256'h9FE31FF787FFBFFFF8007F82F81F01FFC738C71F3FFF000310FF66FFFFFFC007),
    .INIT_21(256'h0003707F800033CCCE7C8000000000070780880C00000000DFFAF7FFF3E1073F),
    .INIT_22(256'hFCC0000000000000E0B9881CF0000000FFFFFFFE6CE600379FE07E18FFC71CFF),
    .INIT_23(256'h00000000B9FFFF7FDD07E0FE63C603ECFF08FA6FE0FCD000C1F800C07C2618FF),
    .INIT_24(256'hC101013F0FE4E3DE0703AD1FFFF80000C1040019C7E1F000007F003FF0000000),
    .INIT_25(256'hF80FFFDFFFFFE003600F33667CFF017F003FFF7D000000000000000000000EFF),
    .INIT_26(256'h79998FFF000700FA005000000000000FFFFFC00000000007800FFFFFFFFFEB63),
    .INIT_27(256'h0000000007FFFFFFFEFFFECFC0000000F6007821FFE1C3FF00E7E377FFFFE783),
    .INIT_28(256'h7FFF6ECFF00F00C009E01C06F20027FFC4FBE713FF7FCCC3E43F001F007C0003),
    .INIT_29(256'hFFF0040E60C3E3FFFFDB9F01001AD39F0000FFFFFFFF008000000007FFFFFFFF),
    .INIT_2A(256'hFF8002836B26000200FFFFFF000780F8FFF7000011CF821F800019600000000C),
    .INIT_2B(256'hFFFF00000059FFFCFFFF00219D0FBFFE000000C6630F0000C09FC630E3C7FBFF),
    .INIT_2C(256'h1FFFFF0F9C00FC007F000084001F00803607800FFF8FDD8120F9E75B311000E0),
    .INIT_2D(256'h00050000000000C03E7FFFE07E2703F09FC38A80703F00C8FF7F000000007F80),
    .INIT_2E(256'hB8FDFFFE3803BC38D42B000000FF810000000003C000000000E003030000F73F),
    .INIT_2F(256'hC331FFE000FF8000000007FF000000000010008004C03F200000008000008001),
    .INIT_30(256'h0000FF0000C0000E001800000010088280001000801F0000CCF87BF01F0FFC2A),
    .INIT_31(256'hFFF000000070393BFF00010E007F308ED133CFF8FFC64AC010666000F0030000),
    .INIT_32(256'h0F00017F0FE000028C00360037AA08C10000F8FFE0C003FFE000FF00C7F807FF),
    .INIT_33(256'h06FD3F92DC08F80E00007FFFFC001FFFFF010060E700FFFFFEFE0FFC88C0C8F8),
    .INIT_34(256'h00000FFFE0F8073FFF70CC0080CFE000017FC1006006B90107FF023FE37F07E1),
    .INIT_35(256'hC401000CFF0600000F7FFF6E00EFFFFFE00387003CFFFF0FE4A28FD80000000E),
    .INIT_36(256'hFF7EFFFF19FFFFFFC20FFFE33FFFFFFFDCECE4070000000E000000E8003B1FFF),
    .INIT_37(256'h03FE9F7FFFFFFC210479C1000100013E00080020000CFFF000800008000000E7),
    .INIT_38(256'hFFD0030F0000FF3F000E0000013F0F00003FC00103333FEFFFFEFFE0F8000001),
    .INIT_39(256'hFF3F001FFFFA00001F38000007303800FFE000000000000000000000803FF8FF),
    .INIT_3A(256'h08C0000000003C7EFFC220FFFFFE008E06004000003FFFFF900304FF8001FFFF),
    .INIT_3B(256'hFEEFDFFF99BFFFFE800300000C7FEFFEE43E8C3F0001FFFFE07F831FFC200003),
    .INIT_3C(256'hCFFFFFFFCFFFFFFEFFFFC7FE0001FFFF007FFFFF00000000001B000FFFFF7FFF),
    .INIT_3D(256'hF9FF7FFF811FFF7FFFFFFFFE000007FCFE20E3A3FF8FFFFFF3B887FFE6EF6FFE),
    .INIT_3E(256'hFFFFFFEFFA9FCEC004000100001EFF7E1CE680100000477F9FFFDFFFFFFFFFFE),
    .INIT_3F(256'h400000FFFEF9FEF3FE3EE80F1C0040F2FFFFFFFFFFFF9FFFFFFEE40000000038),
    .INIT_40(256'hFF7E1F3FFFFF9BBFF9FEFBFFFFFE8F7F000000008F0F0000F0FD4FFFFFC02100),
    .INIT_41(256'hFFFFFFFF3FFFFFFF00000003003FFF3C03C01FFFEF001E3E8F00FFFFFFFE8203),
    .INIT_42(256'h0000FFFFFFFC0000FF007EF0003FBF30216000008C00E10707001F20813F9F78),
    .INIT_43(256'hF000FFFFFFFF0106200F000000031FFE0042FF30001FFE803FFFFFF89F3FEFFB),
    .INIT_44(256'hF020FFFFCFFF78FD7EFE037E1FFFFE017E07C6FFFFFF39FF0000FFFFFFFF03FF),
    .INIT_45(256'hFF20007FFE81F7FE03FCC1E87F8F3FFFFFFDFFFF000FFFF000FFFFFFFFFE87FF),
    .INIT_46(256'hF9FF9FFFFF7F6000FFFFFFFFFF03FF000FFFFFFFEFE000FFFFDF0000FFF81F1F),
    .INIT_47(256'hF4E7FFFFF7FFFFFCFFFFC30000000000000000000087E0F87887C00C7FC00006),
    .INIT_48(256'hFFFF8FFFC6FB8007C607C0F8007000F80080403FFFFFC2800000FF3067C20000),
    .INIT_49(256'h400080000770FFFFCE3F0008083FFFFFFEFFF8F966FFF1DCFFFFFFFE03FFFFE7),
    .INIT_4A(256'hFE1FDE0EFFFF80C079033FCFFFFFFFFCFFFFB9FFFFFF63FFFFFF833FEE0F0000),
    .INIT_4B(256'hFD73FCE09800FFFFFFFFFB03D15E0080001E030F0F8E1FFE60007000F8730CDC),
    .INIT_4C(256'hFF0800C0007F63FE07FF7F800400000031383300F8613F1C9C09DF9F7EFF8FEE),
    .INIT_4D(256'h8FBF7FF0C00300003B1030180F00000B9F19FFFE7FFFFFFFFF9FFFFEFF3E3FFF),
    .INIT_4E(256'h9018B01F060080019FF9FEFBFFFFFFFFFF0F00FFFFC3FFFCE00018000000003E),
    .INIT_4F(256'h9962E3F0FFFF07DC00E01FF0FFF7CFFE0070F80F0102063F80003FCF03FFFF7F),
    .INIT_50(256'h7807FFFEFF70FFFF0070001FFFFFFFFF000006000300000299FFC7FF800823F0),
    .INIT_51(256'h003BFF7EF80FFFFF00E00200000E1BFF9FFFE1FF860F0003B9061FFFFFF8E7E8),
    .INIT_52(256'hC7FF8F0C00300087DFFF8FC13FFFFF01001CFF83FC7FFFE77F7F63CFFC1F807E),
    .INIT_53(256'h200007FF000003FEFFE3FF0FFF7FDFFEDFFFFFFFE7FFDFFE87D100F799F3E7FF),
    .INIT_54(256'hFFE1E3FEFF7FFE3EFF7FFFFFFFE0FFF33FFEFC00010367F1FFFF86FF00000000),
    .INIT_55(256'hFFFFFFFE83FFFFF17FF9FFFF600000E01FFF8FFFFFFFFE31001C80F8009FFEFF),
    .INIT_56(256'hC7F8FFFF6680468E3000073F33C7FFFF008000FF80003FFFFFFFFF3FFFFFFE7F),
    .INIT_57(256'h80001C007FDF7ECFFFFE87CE00E2003FFFFFFFFFFFFFFFC3FFFFFFCF79E1FFFF),
    .INIT_58(256'hFFFEF08F00007E1E8EFFFEFFFFFFFE00FFFFFFFFE3FFFF5F82FFFE0077F68000),
    .INIT_59(256'hFFAFFFFFFFFFFF7F3E1F8EFFC1FFFC7F7F0000837FBE81F908810F0FCF202087),
    .INIT_5A(256'hFFE77E783FFFFF0FFFFF0701139603F00EFFE10000FB0000FF089F073FF9FF7F),
    .INIT_5B(256'h0FFF7FFFB9967FE1C7FFFEFFFFC2C700003CFFFFF7FFFF8FC1CFFFFFFFFF3FFF),
    .INIT_5C(256'hFFFF7FFFFFFF21F8889EDFFBFEDFE32799FFFFFEFFFFFFFFFFF0CCF77E03FF8F),
    .INIT_5D(256'h9FA39D4FE003C37FFFFFF89FFF30FFC17FFFFE073F03FFFFFFFFFFFFFF7C068F),
    .INIT_5E(256'hFEFFF89FFFFFFFFFC7FFFFFFFFFFFFFCFF3F7FFFFFFF6EC000FF7FFF67E6FBFF),
    .INIT_5F(256'hFF07FF0063F000FF00007EFFFEFFFFFCFF7EE7FFFCEFFFFEBFFEEFFC2FF00E9F),
    .INIT_60(256'h0000FE01008FFFFFFFFFFFFFFFFC87E6FFFFC213FFFE079D7FDFFFFFFFFFFFFF),
    .INIT_61(256'hFFFFFFF1FFFFFFE3FEE3F7F3FEFB0EFFFFFFFEFFFF7FFFF1FFFF71E1FFFC003E),
    .INIT_62(256'hFCE38E7F11FF8EE7FFFFFFFFFFFF7FFFFEFF1FE0FFFFFFE7000000010807FFFF),
    .INIT_63(256'hFFFFFFFFFFFFFF7FF0029FC007FFFFC10000209F3800EFFFFFFFE6F1FFFC3FBF),
    .INIT_64(256'h3FDF9FFFF1FFFFFFFFFFFFFF8E01023FFF3EFFFF9BFFFF7DFDFE8D91FEFF7FFF),
    .INIT_65(256'h0FFFFFFFFF7FFFFFFF7EFF7FFF7FFFF0F91EE17BFFFFFFBFFF7FE3EFFFFFFFFF),
    .INIT_66(256'h000FFFFFFFFF3FFEFFFF8BFF00BFFFBFF7FFFFFFF17F017F1F070001FFFFFFCF),
    .INIT_67(256'h00FFFFFFFCFF0007F0000000E0F3FFFC0FFFFFFFF03FFFFFF3F700FFFEF3000F),
    .INIT_68(256'hFF000F01F8FFFFFE0FFFFFFFF01FFFFFFFFFBF7F00020FFF1F8000001F0F3FF0),
    .INIT_69(256'h9FFFFFFFFFFFFFFFFFFE000000001FFFFFFFFFDFFFFFFFFFFFF10021F5F40027),
    .INIT_6A(256'h00FF037F040C0FFFF00FFFCFFFFFC01FFFFFFFFFFFFCFFF67F8E1001F010387F),
    .INIT_6B(256'h07ECFFFFFFFFFFFFFFFFFF38FFFFE7FFFFFFFFFFFFF07F1FFFFF3EFFBFA7FF0F),
    .INIT_6C(256'hFF7FFFEFFFFDFFFFFFFFFFFFFFFFFF3CFFFF3E7F1F03030000000FFF8FCF000C),
    .INIT_6D(256'hCFFF1C000003FFFCFFFFFFBE0AE307FF010001FF06C7803800FFFFFFFFFFE37F),
    .INIT_6E(256'hFF000292FFC3C3C487FF1AFF8FF3FFFF000E0300000000878FFFB8DFBFFDFF01),
    .INIT_6F(256'hFF300A1FFFF003630FFF8FF80000000004F8F870C000000000DE00000FFF7FFF),
    .INIT_70(256'h0117E9F0FFE7FF87F3FFFFCE79FFFC0FEE808E67F7E0FFFF1C00038F3ECF9BFF),
    .INIT_71(256'hF000000000000000000033F800FFFFFF2000788FFFC7F1FF000027D9710E6606),
    .INIT_72(256'hF92FFFFF6718FFF80E1FF803FFC1805E011936008E9F9879FF001037F0036330),
    .INIT_73(256'hFF9FC303FFFFFF1EFF81FEFD67FC0000001663FD6FFE6757CFFF71CC18FFFFFF),
    .INIT_74(256'hFEE3FB37FF7FE6FFC3B7FFDFE7FFFFF1E7F88031E7FFFEFF000020008F82000F),
    .INIT_75(256'h809291FFBFFFFFF19F1FFFFFFFFFFFFFFFFFFFF0470000800C9F036307FFFFDC),
    .INIT_76(256'h7FFFFFFFFBF9FFFFFFFFFFFFFFFFFFFFFFFF0CFE000FC7FFFCF8FF54FEFEB9FF),
    .INIT_77(256'hFFFFFFFFFFFF80E071764181E10FEEFF3CCF3A2EF37C38FFFFEFFFDFEF7FFFFF),
    .INIT_78(256'hFE7600007F7FFE80FEC43D5EFBFF7FFFCF77FFF7FF0F3FFB3FFFFEFBFE7EFFFF),
    .INIT_79(256'hFE7EF71DF7F3FFFFFFB2FFFCFFFFE0FF46FFFFFFFE771EFF4BDFFFFFFFFFFC5F),
    .INIT_7A(256'hFFC633F5FFFFFCBEF9FF9F1EFFFFF0FEF1FFFBFFFEFFEB7FEFFF9FE7E40193FF),
    .INIT_7B(256'hFFFFDFFFFFFEFFFF53FF1BFFF6BF0100E103E7FFE0F932FFFFFFBDFF7BFFFF7F),
    .INIT_7C(256'h99901AEFFEFF3FFFFFFF9303F7CED6FFFFFCBCFFFFFCFF86C7FFFFFEFFFFFFFF),
    .INIT_7D(256'hFE79FF67FFF9DDFFFEFEECFE9FFEFF8786DFFFFFFFFFFFFFFFFFFEF83FFFC100),
    .INIT_7E(256'hFFFFFF3EDFFFFE73FFCFFFF7FFFBFFFFFFFFF4F83FFFFF0011FFFFFFFFFFFFFF),
    .INIT_7F(256'hFBFFFF7FFFFFFCFFFFFFD9FFFFFFFFFF1FFEFC00873ABFDF32F0FFF9F1F1FAFF),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("UPPER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(CASCADEINA),
        .CASCADEINB(CASCADEINB),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED [31:1],DOUTA}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized0
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ,
    clka,
    ram_ena,
    addra,
    dina,
    wea);
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  input clka;
  input ram_ena;
  input [13:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 ;
  wire [13:0]addra;
  wire clka;
  wire [0:0]dina;
  wire ram_ena;
  wire [0:0]wea;
  wire [15:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED ;
  wire [15:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h8FFFDBFFFFF7FFFFDF01F67E0C7BFFFFFFFBFBFFB1FFF97D3FFD37FFFFF7DEED),
    .INIT_01(256'hEFFFE501F17FFFC6CDC0FEFF9807FFF17FFF33FFEF93DFFDBF8333FFFFFFFFFE),
    .INIT_02(256'hE803DC99DEEFEFFDFFFFF7FFC61C7E4CFF7CFFFFFF7EFFFFCFFFFFFFF167FFFF),
    .INIT_03(256'h7FFFFFFFC0F00000FFFEFFFFFF7FFFFFFFFFFFFFFFFFFCCFE1FFFBFF30606000),
    .INIT_04(256'h667FFFBBFFFEFF7FFFFFFFFFFFFFDF9FF07087FFCBF0F99801071BC0DA8F8FFF),
    .INIT_05(256'hFFFFFFFFFFFFFFFFFFF0C0837BFBFFF8E747907FF3F8CFFF3EFEFFFFF5708000),
    .INIT_06(256'hFFFFE0C3FFFFFFFFFFFF90003C87FFFF0137EFFFFF7CFE8FE0100090FFFEFFFF),
    .INIT_07(256'hDFFFFFF504001FFF011C12FFFFFEFFCFE30000100C1FFFFF77FFFFFFFFFFFFFF),
    .INIT_08(256'h300000E1FFFEFFFFFF7F001008000631037FFFFFFFFBFFFBFFFFFFEFFFFFFFEE),
    .INIT_09(256'hFFFFFFB28C002000000003FF7FFFFFFEFFFFFFFFCFFFFFFFFFFFFEFFEDDC1E1F),
    .INIT_0A(256'h913000203BC3E7FFFFFFFFFFFFFFFFFFF7FFFEFEEDFFFE3F7BC30600FFFBF6F3),
    .INIT_0B(256'hFF7FFFFF7FFFFFFFE3FFFFFF241DFFFFF7FFCE01FFF1C17FFFF3FFF7FC3FF020),
    .INIT_0C(256'h71F1FEFE61FF778CEF7EF333FFF0801FFFE1FFF7FFFFFFFB997FFC000000C0FD),
    .INIT_0D(256'hFFF3F7F1FFFEC0003F01FEF6FFFFFFFF797FFFF603800018717BFFFFC5FCCFFF),
    .INIT_0E(256'h7E19E6C3DEFFFFFCFF7FFFF71FEFC1800060006780F9E673F080F27BCFF1E7FF),
    .INIT_0F(256'hF0671CE7F9FFFFC000000002800BFFFE3181F2FB0003E3FFE7E40E63E73CFF7F),
    .INIT_10(256'h3060010000015FFC2119F3FF0F87E17FFF538700629CFFFFF8FFE7F3FFFFFC00),
    .INIT_11(256'h7010F3FF1FF7E1FFFF3DC70063AEFFCFD9BBFF7FFB77FC08C0000006F83FFFFF),
    .INIT_12(256'hFF84E78073BC7F8E8901EF3F7137FE7EC0800000001FFF7FFFF70FE000000000),
    .INIT_13(256'h9991030F0007067EFF980000000E0E338EFFFFFF10800000301073FF1CF7CEFF),
    .INIT_14(256'h00DE042000030C008FFFFF3FF88C30613118F01F00208CC7B031FCFF001C3FCF),
    .INIT_15(256'h03F80001008F39637FFDE018C000008090E5FBFF801B7F8FF99B818380030F00),
    .INIT_16(256'h7FFF8000000000009FEAFFFFFFE3FF07FFFF9980F1230F0000260F7EF9E7CE00),
    .INIT_17(256'h9FD7FFFFDF1FF0837EFBF98C61000FE0F8623F7F7CFFCF0001C00080008C300F),
    .INIT_18(256'h780E038CC000FFE69C40112039F3FF8F181CE0900004203151FFC60000000000),
    .INIT_19(256'h8000702031C180077C000098000400000031C000010100003FF7FFFFFFFD7800),
    .INIT_1A(256'h3000003C7B0600C000000001C10380083F77FFFFFEFFFC007C0C0F38DE00FFE2),
    .INIT_1B(256'h00000000C1800C183F7F7FFFFEFFE11F7FFF1E79FE11FFA00000F02200800000),
    .INIT_1C(256'h01070003C77E413F7FF11E7BFF997F201F01386380000000200000043FFE7FF0),
    .INIT_1D(256'h3FF187F8CFBB00307F3108638000000000000000007473F00000000000000C00),
    .INIT_1E(256'hF110F8673830000000100000000021000003E00000000C00800300000018011C),
    .INIT_1F(256'h083C0180E00000000030CF000CE00800C0020000000000003FFE87FC8F7F0010),
    .INIT_20(256'h000000000EE17880C340000030000000001E068004762012F1F8FC2630300000),
    .INIT_21(256'hC7E080203D000000000C00000C267117FFF89F3601618C1800FC3189F0002000),
    .INIT_22(256'h000000010100F104F7D9FC7021F31880003E1981800404260000800064030080),
    .INIT_23(256'hE098FE7301FF08FC007E18C1004600050000800000000000E780606267C0E030),
    .INIT_24(256'h0F3E9DC131E300030000800000000000E7C07F677FE7FFFCC13B000000010026),
    .INIT_25(256'h0E6000000000000043C77F77FFFFFEFFE3FF1800000380B48018FFFF0FFFF1FE),
    .INIT_26(256'h000F5F77EFC7FFFFEFF8BCE0C093F0A00000FFFF9FFFFBFF1FFFBFE133F10080),
    .INIT_27(256'hFBF8FFFFFEBFF120818000C19C3FFFFFFFFFFFF71FF01C021EFF804000000000),
    .INIT_28(256'h81E00098881C7C1F8FFF9DC10FF03C961ECFDCE7E0013C00001F8FE7C7C37EE3),
    .INIT_29(256'h03C6000004103990BE0CDF77E0037EC0007F89C4C7C31EE3FFFFFF7EFFFFF1B1),
    .INIT_2A(256'hFF1C1F73FCFFFCE011F98181E0079FE7E79FFE3F7FBFBBB5E7FC00B80000C000),
    .INIT_2B(256'h11F001016004C1C6CD8CFFF733BF0FB7E6FE83F9091CC0000700000000011B97),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOADO_UNCONNECTED [15:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOBDO_UNCONNECTED [15:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM18.ram_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(ram_ena),
        .ENBWREN(1'b0),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized1
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire CASCADEINA;
  wire CASCADEINB;
  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ;
  wire [31:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFCFFF9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFF),
    .INIT_01(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFFFFFFF),
    .INIT_02(256'hFFFFFF3FFFFFFFFFFFFFFFFFFFFFFE03FFFFFFFFFFFFFFFFFFFCFFFC7FFFFFFF),
    .INIT_03(256'hFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFF517FFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_04(256'hFFFFFFFFFFFFFFFFFFFFC09FFDFFFFFFFFFFFFFFFFFFFFFFFFDFFF3FFFFFFFFF),
    .INIT_05(256'hFFFFC08FF81EFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFF),
    .INIT_06(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFF),
    .INIT_07(256'hFFFFFFFFFF4FFFFFFFFFFFBFFFF3FFFEFFFFFFFFFFFFFFFFFFFFE00FF807FFFF),
    .INIT_08(256'hFFFFFFBFFFE3FFF9FFFFFFFFFFFFFFFFFFF0E0407C07FFFFFFFFFFFFFFFFFFFF),
    .INIT_09(256'hFFFFFFFFFFFFFFFFFFF0000041FFFCFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFF),
    .INIT_0A(256'hFFE001C3E07FF8FFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFFFFF7FFFCFFFF7),
    .INIT_0B(256'hFFFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFFFFFFFFFFF9FFFFEFFFFFFFFFFFFFFFF),
    .INIT_0C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFC107FFF7FFFBFF),
    .INIT_0D(256'hFFFFFFFF7C7EFFFFFFFFFFFFFFFFFE7FFE000FFF3FFFF7FFFFFFFFFFFFFFFFFF),
    .INIT_0E(256'hFFFFFFFFFFFFFFFFFC008FFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC7FFFFFFFF),
    .INIT_0F(256'hFC03E77FFFFFDFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFCF0FFFFFB),
    .INIT_10(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEE3FFFFFFFFFFFFFFFFFFCFFF),
    .INIT_11(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7FF7FFFFCF0FFFC7F07FFFFFFFFF),
    .INIT_12(256'hFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFC0FFFEF8FFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_13(256'hFFFF8EC79FFFFFF00FF87D9F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_14(256'h063003FF1F63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFFFFC),
    .INIT_15(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF73FFFFFFFFFFFFF9FFFFCE7E83BDDFF0),
    .INIT_16(256'hFFFFFFFFFFFFFFFFF7CFFFFFFFFFFFFFFFFC1FC003F801FC3F1207FF3807FFFF),
    .INIT_17(256'hEFFFFFFFFFFFFEFEFFFC000307FCC000FFFFF7FFFC3FFFFFFFFFFFFFFFFFFFFF),
    .INIT_18(256'hFFFE03807FFFC000FFFFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_19(256'h7FBE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7FFF),
    .INIT_1A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FFFFFFCFFFFFFFF1FE0FFE38400),
    .INIT_1B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFFFFFFFFFFE300067E0007FDFFFCFFFF),
    .INIT_1C(256'hFFFFFFFFFCFEFFFFFF7FFFFEFFE1F17E00020FFBFFFDFFFFFFFFFFFFFFFFFFFF),
    .INIT_1D(256'hFF7FFFFC7FFBC1FF003EEFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1E(256'h80FC3FD87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8FFFFFF),
    .INIT_1F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FF83FFF3FFFFFEFFFDFFFC1FF),
    .INIT_20(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7FF80FFE3FF81FC3FFFFCFFFFFF),
    .INIT_21(256'hFFFFFFFFFFFFFFFFFFFFFFFE1E0FFFFFF3FEFFFBFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_22(256'hFFFFFFFFFF07FFFFE0FFFFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_23(256'hE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_24(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFF),
    .INIT_25(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFFF81FEFFFFFFE7FFF9FCFFFFFFF),
    .INIT_26(256'hFFFFFFFFFFFFFFFFFFFFC7FC3CCFFFFFFE3FFFBFDFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_27(256'hFFFFCFFF000FFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_28(256'hFFFF9CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_29(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFBD8EFFFFFF),
    .INIT_2A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFDF7FFFFFFFFE1FFFFFFFFFFF),
    .INIT_2B(256'hFFFFFFFFFFFFFFFFFFFFFFFF1E7FFFFFFFFF3FFFFFF7FFFFFFFFFFFFFFFCFFFF),
    .INIT_2C(256'hFFFFFFCD7CFFFFFFE3FFFFFFFFC3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2D(256'hBFFFFFFFFFC3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88F1FFFFFF),
    .INIT_2F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8DF1FFFFFFFFFFFFFFFF07FFFF),
    .INIT_30(256'hFFFFFFFFFFFFFFFFFFFFFFDFFEFFFFFFFDFFFFFFFE3FFFFFFF7FFFFFFFFFFFFF),
    .INIT_31(256'hFFFFF1FFFFFFFFFFDDFFFFFFFFFFFFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_32(256'hFFFFFFFFFFFFFFFF7FFFFC7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_33(256'h3FFC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFF),
    .INIT_34(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_35(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEFFE1FFFFFFFFFFFF),
    .INIT_36(256'hBFEFFFE0FFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_37(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_38(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3DFFF3E0FFFFFFFF),
    .INIT_39(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3C3FE7F8FFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3A(256'hFFFFFFFFFFFFFFFFFE3F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3B(256'hFFFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3C(256'hFFFFFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3D(256'h1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3),
    .INIT_3F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_40(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF01FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_41(256'hFFFFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFF),
    .INIT_42(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_43(256'hFFFFFFFFFFFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_44(256'h1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_45(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFDF7F),
    .INIT_46(256'hFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8FFFFFFFFFFF),
    .INIT_47(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFCFF8FFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_48(256'hFFFFFFFFFFFFFFFFFF1FF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_49(256'hF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFF),
    .INIT_4B(256'hFFFFFFFFFFFC7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFCFFFFFFFF),
    .INIT_4C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F8FFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4E(256'hFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF),
    .INIT_50(256'h3FFFFFFFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_51(256'hFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_52(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFFFFFFFFF),
    .INIT_53(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_54(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFF),
    .INIT_55(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_56(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_57(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_58(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_59(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5A(256'h37FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFC008E),
    .INIT_5C(256'hFFFFFFFFFFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFF81F87E07FFFFFFFFFFFFFF),
    .INIT_5D(256'hFFFFFFFFFFFFFFFFFFFFFFFFF88FFFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5E(256'hFFFFFFFFF87FFFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_60(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07CFFFFFF),
    .INIT_61(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE07FFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_62(256'hFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_63(256'hFFFFFFDFEFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_64(256'hFE63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_65(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFFF),
    .INIT_66(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFFFFFFEFFFFFFFFFFFF),
    .INIT_67(256'hFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_68(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_69(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6A(256'hFFFFFC383F3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFF),
    .INIT_6B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7FFFFFFFFCFFDFFFFFFFF),
    .INIT_6C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFE1FFFFFFFFE1FFDFFFFFFFFFFFF00FCFF0CFF7F),
    .INIT_6D(256'hFFFFFEFC017FFFF9FFEF1FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6E(256'hF01FFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0FFFFFFFFF),
    .INIT_6F(256'hFFFFFFFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC09FFFB01),
    .INIT_70(256'hFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFF067FEE31EF3FFFFFFFFFFFFFF),
    .INIT_71(256'hFFFFFFFFFFFFFFFFFE8007FFFFFFE7FFFFFFFEFFFFFFFFFFFFBBFFFFFFFFCFFF),
    .INIT_72(256'hFCF8FFDFFFFFFFFFDFFFE007FFFFFFFFFF3BFFFFFFFFCFFFFFFF7FFFFFFFFFFF),
    .INIT_73(256'hFFFEFFE3FFFFFFFFFF7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_74(256'hF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFF),
    .INIT_75(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFF),
    .INIT_76(256'hFFFFFFFC7FFFFFFFE7FFFFFFFFFFFFFEFFFF7FFDFFFFFFFFFFF7FFFFFFF8FFFF),
    .INIT_77(256'hFFFFFFFFFFFFFFF8FFFC7EF9FFFFFFFFFFFFFFFFFFFFDFFFFFFF8FFFFFFFFFFF),
    .INIT_78(256'h7FF8FFFEFFFFF87B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_79(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD),
    .INIT_7A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFF7FFFFFFF),
    .INIT_7B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFCFFDFFFFFFFFFFFFFFFFFFFF),
    .INIT_7C(256'hFFFFFFFFFFFFFFFFFF9FFFFF03BFFFFFFF9FFFFFFFCFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7D(256'hFFFFFFFFC7E7FFFFFF9FFFFFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFE3FFFF),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("LOWER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(CASCADEINA),
        .CASCADEOUTB(CASCADEINB),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED [31:0]),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF6FFFF3E7FFFFFFFFFFF7FFFFFFFFF),
    .INIT_01(256'hFFFFFFFFFFFFFFFFFBFFFFFFFE7FFFFFFFFFDCF703FFFFFFFFFFFFFFFFFFFFFF),
    .INIT_02(256'hF3FFFFFEFE7FF83FFFFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_03(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFFF7F),
    .INIT_05(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7FFFFFFFFFEFFFFFFFFFFFFFF),
    .INIT_06(256'hFFFFFFFFFFFFFFFFFFF8FFCFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_07(256'hFFFFF8DFFFFC387FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_08(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_09(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF803103CFFFF),
    .INIT_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0B(256'hFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFF9E03FFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0C(256'hFF9FFFFFFFFFFFE07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFFF3FFF),
    .INIT_0F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFFFFFFFFFFFFFFFFFF),
    .INIT_10(256'hFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_11(256'hFFFC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_12(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_13(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_14(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_15(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_16(256'h1FFFFFFFFFFFE0EDFFFFFFFFFFFFFFFFFFFFFFF6FFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_17(256'hDFFFFFFFFFFFFFFFFFFFFE7F0F0780FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_18(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FFFFF),
    .INIT_19(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF06FFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1B(256'hFBFFFFFFFFFFFFF7FFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1C(256'hBFFFFFFFFFFFFFFFFFFFFFFFFFC061FFFFFFFFFF00FEFFFFFFFFF81FFFFFFFFF),
    .INIT_1D(256'hFFFFFFFF801F800F000FFFFE00007FFFFFFFF883FF3FFFFFFFFFFFFFFFFFFF3F),
    .INIT_1E(256'hFF000001F8003F80FFFFFFF0F3FFFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1F(256'hFFFFFFFEFFFF7FFFFFF7DFFC1FFFFEFFFFFFFFFFFE80FF70DFF8003F03F07FE0),
    .INIT_20(256'hFFFFFFF87FFFFFFFFFFFFFFDFFE0FE0007C0F81FFFFFFFFFFFFFFFFFFFFFC000),
    .INIT_21(256'hFFFFFF807FFFC3F0F07FFFFFFFFFFFF8F87F77F3FFFFFFFF3FFC7FFFFFFEFFFF),
    .INIT_22(256'hFFFFFFFFFFFFFFFF1F4677E30FFFFFFFFFFFFFFFF3FFFFFFFFFFFFE0003FFFFF),
    .INIT_23(256'hFFFFFFFF46000080FFFFFFFFFFFFFFF3FFF7041FFF003FFFFFFFFF3F8038E0FF),
    .INIT_24(256'hFFFFFFFFFFFFFFE1FFFFDEFFFFFFFFFFFFFFFFE10701FFFFFF80FFC00FFFFFFF),
    .INIT_25(256'h07FFFFFFFFFFFFFF9FF03C787FFFFE80FFC00082FFFFFFFFFFFFFFFFFFFFF100),
    .INIT_26(256'h81E10FFFFFF8FF05FFAFFFFFFFFFFFF000003FFFFFFFFFF8FFFFFFFFFFFFF780),
    .INIT_27(256'hFFFFFFFFF8000000010001303FFFFFFF09FFFFFFFFFFFFFFFF1FFF8FFFFFF87C),
    .INIT_28(256'h800091300FF0FF3FFFFFFFF9FFFFDFFFFF07FFEFFFFFF0FCF83FFFE0FF83FFFC),
    .INIT_29(256'hFFFFFFF1FF3FFFFFFFE7FFFFFFE31C1FFFFF00000000FF7FFFFFFFF800000000),
    .INIT_2A(256'hFFFFFFFC73C7FFFFFF000000FFF87F070008FFFFEE307DE07FFFE69FFFFFFFF3),
    .INIT_2B(256'h0000FFFFFFA600030000FFDE62F04001FFFFFF399CF0FFFFFFE039CFFF3FE7FF),
    .INIT_2C(256'hE00000F063FF03FFFFFFFF7BFFE0FF7FCFFFFFFFFFFFE3FFFF061B9C3EEFFF1F),
    .INIT_2D(256'hFFFAFFFFFFFFFF3FFFFFFFFFFFDFFF007FE2F3FFFFC0FF370080FFFFFFFF807F),
    .INIT_2E(256'h7FFFFFFFFFFFFFFF18CAFFFFFF007EFFFFFFFFFC3FFFFFFFFF1FFCFCFFFFFFC0),
    .INIT_2F(256'hFCCFFFFFFF007FFFFFFFF800FFFFFFFFFFFFFF7FFFFFC0DFFFFFFFFFFFFFFFFF),
    .INIT_30(256'hFFFF00FFFF3FFFF1FFFFFFFFFFEFF7FFFFFFEFFF7FE0FFFFF3FFFFFFFFFFFF33),
    .INIT_31(256'h000FFFFFFF8FC7C400FFFEF1FF80CF71EFFFFFFFFFF88CFFEF99FFFF0FFCFFFF),
    .INIT_32(256'hF0FFFE80F01FFFFDFFFFCFFFC733FFFFFFFF07001F3FFC001FFF00FF3807F800),
    .INIT_33(256'hF902001CE7F7FFF1FFFF800003FFE00000FEFF9F18FF00000101F00377FFF707),
    .INIT_34(256'hFFFFF0001F07F8C0008F33FFFF301FFFFE803EFFFFF946FEF800FDC01C80F81E),
    .INIT_35(256'h3BFEFFF300F9FFFFF0800091FF1000001FFC78FFC30000F0FE64B71FFFFFFFF1),
    .INIT_36(256'h00810000E60000003DF0001CC0000000E110FBFFFFFFFFF1FFFFFF17FFC4E000),
    .INIT_37(256'hFC01608000000000007FFFFFFEFFFEC1FFF7FFDFFFF3000FFFFFFFF7FFFFFF18),
    .INIT_38(256'hFFFFFCF0FFFF00C0FFF1FFFFFEC0F0FFFFC03FFFFCCCC0100001001F07FFFFFE),
    .INIT_39(256'h00C0FFE00005FFFFE0C7FFFFF8CFC7FF001FFFFFFFFFFFFFFFFFFFFF7FC00000),
    .INIT_3A(256'hF7FFFFFFFFFFC381003DDF000001FF71FFFFFFFFFFC00000FFFCFB007FFE0000),
    .INIT_3B(256'h01100000000000017FFCFFFFF38010001BC173C0FFFE00001F807CE003DFFFFC),
    .INIT_3C(256'h300000003000000000003801FFFE0000FF800000FFFFFFFFFFE4FFF000008000),
    .INIT_3D(256'h060080007EE0008000000001FFFFF80301DF1C00007000000000000019109001),
    .INIT_3E(256'h000000100560313FFFFFFEFFFFE10080E3197FFFFFFFB8806000200000000000),
    .INIT_3F(256'hFFFFFF000100000C01C117F0E3FFBF0D000000000000600000011BFFFFFFFFC7),
    .INIT_40(256'h0081E0C0000000000601040000017080FFFFFFFF70F0FFFF0F02B000003FFFFF),
    .INIT_41(256'h0000000000000000FFFFFFFCFFC000C3FC3FE00010FFE1C170FF000000017FFC),
    .INIT_42(256'hFFFF00000003FFFF00FF810FFFC040CFFFFFFFFF73FFFEF8F8FFE0DF7EC00000),
    .INIT_43(256'h0FFF00000000FEFFFFF0FFFFFFFCE001FFBD00CFFFE0017FC000000000000000),
    .INIT_44(256'h00000000300007000101FC81E00001FE81F839000000C600FFFF00000000FC00),
    .INIT_45(256'h00DFFF800000080000033E178070C00000020000FFF0000FFF00000000017800),
    .INIT_46(256'h00000000000000000000000000FC00FFF0000000101FFF000020FFFF0007E0E0),
    .INIT_47(256'h0B1800000800000000003CFFFFFFFFFFFFFFFFFFFFFFFF07FFF83FF380000000),
    .INIT_48(256'h0000700039047FF839F83F07FF8FFF07FFFFFFC0000000000000000000000000),
    .INIT_49(256'hBFFF7FFFF88F000031C0FFF7F7C00000000000000000000000000001FC000000),
    .INIT_4A(256'h01E021F100007F3F86FC0030000000000000460000009C0000007CC011F0FFFF),
    .INIT_4B(256'h028C031F67FF0000000004FC2EA1FF7FFFE1FCF0F071E0019FFF8FFF078CF323),
    .INIT_4C(256'h00F7FF3FFF809C01F800807FFBFFFFFFCEC7CCFF079EC0E363F6206000007011),
    .INIT_4D(256'h7040800F3FFCFFFFC4EFFFE7F0FFFFF460E60001000000000000000100C1C000),
    .INIT_4E(256'h6FE77FE0F9FFFFFE600600000000000000000000000000001FFFE7FFFFFFFFC1),
    .INIT_4F(256'h669D1C00000000000000000000000000FF8F07F0FEFDF9C07FFFC030FC000080),
    .INIT_50(256'h0000000000000000FF8FFFE000000000FFFFF9FFFCFFFFFD660038007FF7DC0F),
    .INIT_51(256'hFFC4008107F00000FF1FFDFFFFF1E40060001E0079F0FFFC46F9E00000000000),
    .INIT_52(256'h380070F3FFCFFF782000703EC00000FEFFE300000000001800809C3000000000),
    .INIT_53(256'hFFFFF800FFFFFC0000000000000000012000000000000000782EFF08660C1800),
    .INIT_54(256'h00000000000001C1008000000000000CC00003FFFEFC980E00007900FFFFFFFF),
    .INIT_55(256'h000000017C00000E800600009FFFFF1FE0007000000001CEFFFF7FFFFF600000),
    .INIT_56(256'h38070000997FFFFFFFFFF8C0CC380000FF7FFF007FFFC000000000C000000180),
    .INIT_57(256'h7FFFFFFF8000813000017831FF1DFFC0000000000000003C00000000801E0000),
    .INIT_58(256'h00010F70FFFF81E1710000000000000000000000000000007D0001FF88097FFF),
    .INIT_59(256'h0050000000000080000000003E00000080FFFF7C80417E06F7FFFFFFFFDFDF78),
    .INIT_5A(256'h00188000000000F00000F8FEEC69FC0FF1001EFFFF04FFFF00F760F8C0000000),
    .INIT_5B(256'hF00000004669800038000000003D38FFFFC30000000000703E3000000000C000),
    .INIT_5C(256'h000080000000DE077761200001001CD86600000000000000000F330881FC0070),
    .INIT_5D(256'h605C62B01FFC3C800000000000000000000001F8C0FC0000000000000083F970),
    .INIT_5E(256'h0000000000000000380000000000000300C080000000913FFF00800098190400),
    .INIT_5F(256'h0000000000000000FFFF810001000003008118000010000140000003D0000000),
    .INIT_60(256'hFFFF01FEFF700000000000000000000100000000000000000020000000000000),
    .INIT_61(256'h0000000000000000000000000104F10000000100000000000000000000000000),
    .INIT_62(256'h00007180E000711800000000000000000100000000000000FFFFFFFEF7F80000),
    .INIT_63(256'h00000000000000000000000000000000FFFFDF60C7FF1000000000000003C040),
    .INIT_64(256'h00000000000000000000000071FEFDC000C10000640000000001720001000000),
    .INIT_65(256'hF000000000800000008100000000000000001E800000000000001C1000000000),
    .INIT_66(256'hFFF000000000C00000007400FF40004000000000000000000000000000000000),
    .INIT_67(256'hFF00000000000000000000000000000000000000000000000C08FF00010CFFF0),
    .INIT_68(256'h0000000000000000000000000000000000004080FFFDF000E07FFFFFE0F0C00F),
    .INIT_69(256'h00000000000000000001FFFFFFFFE00000000020000000000000000000000000),
    .INIT_6A(256'hFF00FC80FBF3F0000FF0003000003FE000000000000000000000000000000000),
    .INIT_6B(256'hF813000000000000000000000000180000000000000000000000C100405800F0),
    .INIT_6C(256'h008000100000000000000000000000000000C180E0FCFCFFFFFFF0007030FFF3),
    .INIT_6D(256'h000000000000000000000041F51CF800FFFFFE00F9387FC7FF00000000000000),
    .INIT_6E(256'h00FFFD61003C3C3B00000400700C0000FFF1FCFFFFFFFF787000470040000000),
    .INIT_6F(256'h00C00400000FFC9CF0007007FFFFFFFFFB07078F3FFFFFFFFF21FFFFF0008000),
    .INIT_70(256'h00001000000000000C000031800003F0117F7198001F0000E3FFFC70C1306400),
    .INIT_71(256'h00000000000000000000000000000000DFFF877000380E00FFFFD80000000000),
    .INIT_72(256'h000000000000000000E007FC003E7FA1FEE6C9FF716067860000000000008000),
    .INIT_73(256'h00603CFC000000E1007E01029803FFFFFFE99C02900180383000000000000000),
    .INIT_74(256'h011C0198000019003C4800201800000E18077FCE18000100FFFFDFFF707DFFF0),
    .INIT_75(256'h7F6D6E000000000000000000000000000000000FB8FFFF7FF360FC9CF8000023),
    .INIT_76(256'h800000000406000000000000000000000000F301FFF03800030001BB01000000),
    .INIT_77(256'h0000000000000000000000001EF01100C330C110008000000000002010800000),
    .INIT_78(256'h000000000000017F013BC020000000003080000000F0C004C000010000000000),
    .INIT_79(256'h018108E0080000000000000300001F00B9000000000000003400000000000000),
    .INIT_7A(256'h0000000300000341060060E10000000000000000010014801000000000000000),
    .INIT_7B(256'h0000200000010000200000000140FEFF1EFC1800000000000000020084000000),
    .INIT_7C(256'h660000100100C00000007CFC0000200000030300000300793800000100000000),
    .INIT_7D(256'h0000009800062200010113016001007879200000000000000000000000003EFF),
    .INIT_7E(256'h000000C12000000C003000080000000000000000000000FFEE00000000000000),
    .INIT_7F(256'h00000000000000000000200000000000600103FF78C40000000000000E0E0700),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("UPPER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(CASCADEINA),
        .CASCADEINB(CASCADEINB),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED [31:1],DOUTA}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized10
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h000000000000000000000000000000000000000000000000000000003CFFFFFF),
    .INITP_01(256'h00000000000000000000000000000000000000007FFFFFFFF800000000000000),
    .INITP_02(256'h000000000000001F00000001FFFFFFFFF8000000000000000000000000000000),
    .INITP_03(256'h00000007FFFFFFFFF80000000000000000000000000000000000000000000000),
    .INITP_04(256'hFE0000000000000000000000000000000000000000000000000000000001FFFF),
    .INITP_05(256'h000000000000000000000000000000000000000000FFFFFF0000001FFFFFFFFF),
    .INITP_06(256'h0000000000000000000000007FFFFFFF0000001FFFFFFFFFFFE0000000000000),
    .INITP_07(256'h00000003FFFFFFFF0000003FFFFFFFFFFFFC0000000000000000000000000000),
    .INITP_08(256'h0000007FFFFFFFFFFFFF00000000000000000000000000000000000000000000),
    .INITP_09(256'hFFFF800000000000000000000000070000000000000000000000000FFFFFFFFF),
    .INITP_0A(256'h00000000000027000000000000000000000000FFFFFFFFFF0000007FFFE7FFFF),
    .INITP_0B(256'h0000000000000000000000FFFFFFFFFF000000FFFC03FFFFFFFC000000000000),
    .INITP_0C(256'h000001FFFFFFFFFF000000FF0003FFFF8FF0000000000000000001FEFFFC7F00),
    .INITP_0D(256'h000000FC0000FFF0000000000000000000003FFFFFFFFFF00000000000000000),
    .INITP_0E(256'h00006000000000000000FFFFFFFFFFEC00000000000000000000007FFFFFFFFF),
    .INITP_0F(256'h0000FFFFFFFFCFE00000000000000000000007FFFFFFFFFF000003FC00007000),
    .INIT_00(256'h8DB115595B15F3F37B9DDFDFDFDFDFDDFFFFFFFFFFFFFFFFFDFDFDFFFFFFFDFD),
    .INIT_01(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6D6D8D),
    .INIT_02(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D5D5F5F5),
    .INIT_03(256'hD3D3B3B3B3B3D3D3B3D3D3D3D3D3D3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_04(256'hB1B1B1B1B1D3F5F5B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_05(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_06(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_07(256'h8F8F8F8F8F8F91918F8F8F8F8F8F8F8F8F8F8F918F8F8F918F8F8F8F8F8F8F8F),
    .INIT_08(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_09(256'hFFDDBD7915F3B18F6F6F6F6F6F6F6F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_0A(256'hD1379DBFBD9B7B9B9BBDBDDFDFDFFFFFFFFFFDFDFDFDFDFDFFFFFFFFFFFFFFFF),
    .INIT_0B(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6D6D6D8F8F),
    .INIT_0C(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_0D(256'hD3D3D3D3B3B3D3D3B3D3D3D3D3D3D3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_0E(256'hB1B1B1B1B1B1B1D3D1D1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3D3D3),
    .INIT_0F(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_10(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_11(256'h8F8F8F8F8F8F91919191918F8F8F8F8F8F9191919191919191918F8F8F8F8F8F),
    .INIT_12(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_13(256'hFFFFDF7915D18F8F6F6F6F6F6F6F8F8F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_14(256'h379DBF9D9DBDBFDFBDBDDDDFDFDFDFFFDFFFDFFFFFFFFFFFFFFFDFFFFFDFDFFF),
    .INIT_15(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8FB1F517),
    .INIT_16(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F5F5F5F5F5F5F5F5F5F5F51515151515),
    .INIT_17(256'hB3D3D3D3B3B3B3D3D3D3D3D3D3D3D3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_18(256'hB1B1B3B3B3B1D3D3D3D3D3B3B3B3B3B3B3B3B3D3D3B3B3B3B3B3B3B3B3D3D3D3),
    .INIT_19(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_1A(256'h919191919191919191919191919191919191919191919191B1B1B19191919191),
    .INIT_1B(256'h919191918F8F8F91918F8F8F8F8F8F8F8F919191919191919191919191918F8F),
    .INIT_1C(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191918F),
    .INIT_1D(256'hFFFFBD9959F38D8F8F8F6F6F6F6F6F8F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_1E(256'h9D9D9B7B9BBDBDBDBDBDBDBDDDDDDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFDFFFFF),
    .INIT_1F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8FAFD3377BBF),
    .INIT_20(256'hF5F5F5F5F3F3F5F5F5F5F5F5F5F5F5151515151515353737575779797979797B),
    .INIT_21(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F3D3),
    .INIT_22(256'hB3B3B3B3B3B3B3B3B1B1B1B3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_23(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_24(256'h918F8F91919191B19191B1B1B1B1B1B1B19191B1B1B1B1AFB1B1B1B1919191B1),
    .INIT_25(256'h8F8F8F8F8F8F8F8F91919191B1B1B19191919191919191918F91919191919191),
    .INIT_26(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F918F918F),
    .INIT_27(256'hDFBDBDBD9B7915D1B1D1B18F6D6F6F6F8F8F8F8F8F8F8F6F8F8F8F8F8F8F6F6F),
    .INIT_28(256'h797B7B7B9B9B9B9B9B9DBDBDBDBDBFDFDFDFDFDFFFFFFFFFFFFFFFFFFFDFDFFF),
    .INIT_29(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8FD117597B797B),
    .INIT_2A(256'hF5F5F5F5F5F5F5F515151515151535373737575979799B9BBDBDDDDDDDDDDDDD),
    .INIT_2B(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F5F5F5),
    .INIT_2C(256'hB3B3B3B3B3B3B3B3B1B1B3B3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_2D(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_2E(256'hB1919191918F91919191B1B1B1B1B1B1B3B1B1B1B1B1B1D1B1B1B1B191919191),
    .INIT_2F(256'h9191919191918F8FB1B1B1B1B1B1B1918F8F8F919191B1B1B1B1B19191919191),
    .INIT_30(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_31(256'hBDBDBDBDBDBD9B7B595937F5D3B3916F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_32(256'h59597979797B7B9B9B9B9D9DBDBDBDBDDFDDDDDDDFFFFFFFFFFFDDDFDDDDDDDD),
    .INIT_33(256'h6F6F6F6F6F6F6F6F6F6F6F8F6F6F6F8F8F8F6F6F6F6F6F8F8FB1F33759597979),
    .INIT_34(256'hF5151517171737373737595959595959597B7B9D9DBDBDBDDFDFDFDFDFDFDFDF),
    .INIT_35(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F5F5F5F5),
    .INIT_36(256'hB3B3B3B3B3B3B3B3B3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_37(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B1B1),
    .INIT_38(256'hB1B1B1B1B1B1B1B1B1B1B1D3D3D3D3D3D3D3D3D3D3D3B1B1B1B1B1B1B1B3B3B3),
    .INIT_39(256'h91919191919191919191919191919191919191919191B1B18F8F8F8F91919191),
    .INIT_3A(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91918F8F8F8F918F918F),
    .INIT_3B(256'hBDDDDDDDDDDDDFDFBDBDBD9B5937F5B18FB1B1D3B1B18F8F8F8F8F8F8F8F8F8F),
    .INIT_3C(256'h5759597979797B9B9B9B9B9BBDBDBDBDBDBDDDDDDDDDDDDDDDDDDDDDDDDDDDDD),
    .INIT_3D(256'h6F6F6F6F6F6F6F6F6F6F8F8F6F6F6F8F8F8F8F8F8F8FB1B1B1F3173737575757),
    .INIT_3E(256'h17373759595959797B7B7B7B7B7B9B9B9D9D9DBDBFBFDFDFDFDFDFDFBFBDBD9D),
    .INIT_3F(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F51515),
    .INIT_40(256'hD3D3D3D3D3D3D3D3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_41(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3),
    .INIT_42(256'hB1B1B1B1B1B1B1D3B1B1B1D3D3D3D3D3D3D3D3F3F5F5F3D1B1B1B1B1B1B1B1B1),
    .INIT_43(256'h9191919191919191B1B1B1B1B1B1B1B1919191919191AFAFB1B1B1B1B1B1B1B1),
    .INIT_44(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191),
    .INIT_45(256'hBDBDBDBDBDBDBBBDBDDDBD9B9B7B3715F3F5F5F5D3B18F8F8F8F8F8F8F8F8F8F),
    .INIT_46(256'h59595979797B797979797979797979799B9BBDBDBDDDDDDDDDBDBDBDBDBDBDBD),
    .INIT_47(256'h6F6F6F6F6F6F6F6F6F6F8F8F6F6F8F8F8F8F8F8FB1B1D3D3D315373737575757),
    .INIT_48(256'h59597B7B7B9B9B9B9B9B9B9B9B9B9BBDBFBFBFBFBFDFDFDFDFDFBFBDBD9D9B7B),
    .INIT_49(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F5F5F515151537),
    .INIT_4A(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_4B(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3),
    .INIT_4C(256'hB1B1B1B1B1B1B1B1D1D3D3D3D3D1D3D3F3F3F3F3F5153737D3D3D1B1B1B1B1B1),
    .INIT_4D(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1AFB1B1D1),
    .INIT_4E(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F918F8F8F8F8F91B191B191B191B1),
    .INIT_4F(256'h99999999BBBBBBBBBBBB99575757573715F5D3D1B1B18F8F8F8F8F8F8F8FB191),
    .INIT_50(256'h5959597979795737351513F3F3131535597B9BBDBDBDBDBDBDBDBDBDBD9D7B79),
    .INIT_51(256'h8F8F8F8F8F8F8F8F8F8F8F6F6F6F8F918F8F8FB1B1D3F3F3F315353537575757),
    .INIT_52(256'h9B9B9B9B9B9B9DBD9DBDBDBDBDBDBDBDDFDFBFBFBDBDBDBDBD9D9D9B7B7B5959),
    .INIT_53(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F5F5F5F5F5F51517373737595979),
    .INIT_54(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_55(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_56(256'hD1D1F3F3F3F3F3F3D1D3F3F3D3D3D3F3D1F315F5F3151737F3F3D3D3D3D3D1B1),
    .INIT_57(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D1D1B1D1D3F3F3D1D1D1),
    .INIT_58(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_59(256'h57573557799B9B9B997977351313F3F3F3D1B1AFB1B1B1918F91918F8F91918F),
    .INIT_5A(256'h373737373717F5F3D1D1D1CFD1F3153537597B9B9B9B7B7B7B9B9D9D9B7B7B59),
    .INIT_5B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F91918F8FB1B1D1D3F3F31515353537575737),
    .INIT_5C(256'hBBBDBDBDBDBDBDBDBDDDDDDDDDDDDDDFDDDDBDBDBDBD9B9B7B79595959593737),
    .INIT_5D(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D5D3F3F3F5F5F5F5F5F517375959797B9B9D),
    .INIT_5E(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_5F(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1B1B1D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_60(256'h1537575979797959151313131515F3F3F315353537373715F3F3D3D3D1D1B1B1),
    .INIT_61(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D3F3F313597B9B9D7B3715F3),
    .INIT_62(256'h8F8F8F8F8F8F8F8F8F8F8F8F9191918FB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_63(256'h15F3F3F31537373735351513F3D1AFAFAFAFB1D1B1B1B19191918F8F8F8F8F8F),
    .INIT_64(256'hF5F5F5F5D3D3B1B1B1B1B1D1D3F5173737375959595737573759595959373737),
    .INIT_65(256'h8F8F8F8F8F8F8F8F8F8F8F8F919191919191B1B1D1D1F3F31515353737371515),
    .INIT_66(256'hBDBDBDBDBDDDDDDDDFDFDFDFDDDDDDDDDDDDDDBDBD9B9B7B7979595959595939),
    .INIT_67(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D5D5D3D3D3D3F3F5F5F515173759797B9B9DBD),
    .INIT_68(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_69(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_6A(256'h9DBDDFDFDFDFBD9B9B7B79797B7B595759373537799D7B3737371715F3D1D1D1),
    .INIT_6B(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1F3F31515597B9DBDDFDFDFDFBFBDBDBD),
    .INIT_6C(256'h8F8F8F8F8F8F8F919191919191919191B1B1B1B1B1B1D1D1D1D1D1D1D1D1D1D1),
    .INIT_6D(256'hD1B1B1B1B1D3D3D3F3D3D1B1AFAFAFAFAFB1D1B1B18F8F9191918F8F8F8F8F8F),
    .INIT_6E(256'hB1B3B3B1918FB1B1B1B1D1D3F3F5F5F5373737371515151515353715F3F3F3D3),
    .INIT_6F(256'h716F6F8F8F8F8F8F8F8F8F91B1B191919191B1B1D1D3F3F3153537573715F3F3),
    .INIT_70(256'hBDDDDDDDDDDDDDDDDDDFDFFFFFFFDFDFDFDFDDBDBD9B9B7B7959593737371715),
    .INIT_71(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F5F5F5F5F5F5151537599B9DBD),
    .INIT_72(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_73(256'hD1B3B3B3B3B3B3B3B1D3D3B3D3D3D3B1D3D1B1B1D1D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_74(256'hDFDFDFDFDFDFDDDDDDBBBBBDDFBDBDBDBD7B35355959597B593715F51515F3D1),
    .INIT_75(256'hB1B1B1B1B1B1D1D3B1D3D3D1B1D3D3D1599DBFBFBFDFDFDFDFDFDFDFDFDFDFDF),
    .INIT_76(256'h91919191919191919191919191919191B1B1B1B1B1D1D3D3D3D3D3D3D1D1D1D1),
    .INIT_77(256'h9191919191B1B1B1B1B1B1B1B1B1B1B1D11515D3B1B1B18F8F8F9191B1918F91),
    .INIT_78(256'hB1B1B1D1D1AFAFD1D1D1D1D1D3D3D3D3F3153513F1F1F1F1D1D1D1D1CFAFAFB1),
    .INIT_79(256'h71919191918F8F8F8F91B1B1B1B1B1919191B1B1D3F31515575959373715F5D1),
    .INIT_7A(256'hBDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDBDBB9B7979595737373737171515),
    .INIT_7B(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F5F5F515171737373759799BBDBD),
    .INIT_7C(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_7D(256'hD1B3B3B3B3B3B3B3B1D3D3B3B3D3D3B1D3D3D3D3D3D3D3D1D3D3D3D3D3D3D3D3),
    .INIT_7E(256'hFFFFFFFFFFFFFDDDDDBDBDDDDFBDBDBD7937F3F337371537371715F3F3F3F3D3),
    .INIT_7F(256'hD1D1B1B1D1D1D1D1D3D1D1D3D3D1D3F59DDFDFDFDFFFFFFFDFFFFFFFFFFFDFFF),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized11
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [16:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [16:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [7:7]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000007FFFFFFFFFF000007F9660061000000600000000000),
    .INITP_01(256'h000007FFFFFFFFFF000007FFFFFFFFFE0007F000000000000019FFFFFFFFCEC0),
    .INITP_02(256'h00F8FFFFFFFFFFFF8003E00000000000003BFFFFFFFFFE800000100000000000),
    .INITP_03(256'h8000FFE000000000007BFFFFFFFFFC8000007C000000000000000FFFFFFFFFFF),
    .INITP_04(256'h00FFFFFFFFFFFC000000F800000000000000FFFFFFFFFFFF07FFFFFFFFFFFFFF),
    .INITP_05(256'h0000E000000000000000FFFFFFFFFFFF6FFFFFFFFFFFFFFF80007FF800000000),
    .INITP_06(256'h00007FFFFFFFFFFF1FFFFFFFFFFFFFFF80017FF80000000003FFFFFFFFFFFE00),
    .INITP_07(256'hFFFFFFFFFFFFFFFFC0007FF80000000007FFFFFFFFFFD0000000000000000000),
    .INITP_08(256'hC000FFFE000000319FFFFFFFFFFF0000000000000000000000003FFFFFFFFFFF),
    .INITP_09(256'hFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFBFFFFFFFFFFFFFFF),
    .INITP_0A(256'h000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFC00FFFFF000003FF),
    .INITP_0B(256'h00007FFFFFFFFFFFFFFFFFFFFFFFFFFFE01FFFFF80001FFFFFFFFFFFFFFE0000),
    .INITP_0C(256'hFFFFFFFFFFFFFFFFFEBFFFFF0001FFFFFFBFFFFFFFFC00000000000000000000),
    .INITP_0D(256'hFFFFFFFFC303FFFFFF1FFFFFFFF0000000000000000000000000DFFFFFFFFFFF),
    .INITP_0E(256'hFC0FFFFFFFE000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'h000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFCFC3FFFF),
    .INIT_00(256'h91919191919191919191919191919191B1B1B1B1B1D1D1D3D3D3D3D3D1D1D1D1),
    .INIT_01(256'hB191919191B1D3F5F5F5F5F3D3D1D1D3F31515F5D1B18F8F8F8F8F8F9191B1B1),
    .INIT_02(256'hF31515D1D11315F3F3F3F3F3F3F3F3F3F31313F1CFD1F313D1D1D1D1F1D1D1AF),
    .INIT_03(256'h91916F8F8F8F8F8F8F8F8F91919191B1B1B1B1D3F315375959797B5715F3F515),
    .INIT_04(256'hBDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDBDBD9B7B7959573737371717171717),
    .INIT_05(256'hD3D3D3D3D3D3D3D3D3D3D5D5D5D5D5F3F5F5F5F5F517373959597979797B9BBD),
    .INIT_06(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_07(256'hD1D3D3D3D3D3D3D3D1D3D3D1D3D3D3D3D3D1D1D1D1D1D1D3D3D3D3D3D3D3D3D3),
    .INIT_08(256'hFFFFFFFFFFFFFFFFFFDDDDDDDDBDBDBD7935F1F3373715F51515F3F3D3D3F3F3),
    .INIT_09(256'hB1B1B1B1D1D1D1B1D3B1D31515D1F559DFFFFFDFDDFFFFFDFFFFFFFFFFFFFFFF),
    .INIT_0A(256'h9191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D1D1D1D1D1D1B1B1),
    .INIT_0B(256'hD3B1B1B1B1B1D3F5F3D3D3F3F315373939593915F3D1D1B1B1B1B1B1B1B1918F),
    .INIT_0C(256'hDFFFFFBB9BDFFFDD9B9B9B9B9B9B9B9B797957351535597979371535595935F3),
    .INIT_0D(256'hB3919191B1B1B1B1B1B1B1D1D1B1B1B1B1B1D3D3F3153759797979593737599B),
    .INIT_0E(256'hBDDDDDDDBDBDBDDDDDDDDDDDDDDDDDDDBDBD9D9B7B5959393937373717171717),
    .INIT_0F(256'hD3D3D3D3D3D3D3D3F5F5F5F5F5F5F5F5F5F5F5F5F51537393959597979799BBD),
    .INIT_10(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_11(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F515F5F3D3D3D3D3D3D3D3D3D3D3),
    .INIT_12(256'hFFFFFFFFFFFFFFFFFFFFFFDDBDBDBDBD9B371315373515F315F5F3D3D1D1D3F3),
    .INIT_13(256'hB1B1B1B1B1D1D1D1D1D3375937F115BDDFFFDDDDDDFDFDFDFDFDFDFFFFFFFFFF),
    .INIT_14(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D1D1D1B1B1),
    .INIT_15(256'h15D1AFD1D3D1D1D1D1D1D1F3F3F51515151515F3F1F3F3F1D1D1B1AF8F91B191),
    .INIT_16(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFBDBDBDBDDFDFFFBD9B9BBDBD9B59),
    .INIT_17(256'hB1B1B1B1B1D1D3F31515153715F3F3F31537595937375759795957597B9DBFDF),
    .INIT_18(256'hBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBB9B9B9B7B795959393939373717171717),
    .INIT_19(256'hF3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5151517373737375959799B9B),
    .INIT_1A(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_1B(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5375B7B3915F3D3D3D3D3D3D3D3D3D3),
    .INIT_1C(256'hFFFFFFFFFFFFFFFFFFFFDFDDBD9D9D9B795735373715F3F315F3D3D1D1D1D3D3),
    .INIT_1D(256'hB1B1B1B1B1B1D1D3F315597B37F337BDDFDFDDDDFDFFFFFDFFFFFFFFFFFFFFFF),
    .INIT_1E(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1B1B1B1),
    .INIT_1F(256'h37F3D1D1D1D1AFAFAFD1F3F3F3F3D1D115153557799BBDBD795735F3B1AFAFB1),
    .INIT_20(256'hFFFDFDFFFFFDFDFFFFFFFFFFFFFDFFDFDFDFDFDFDFFFFFDFDFDFDFDFBD9D9D9B),
    .INIT_21(256'hD3D3D3D3F3155979BFBDDFDFDFBD9B9DBDBFDFBF9B7979797B79599BBDBDDFDF),
    .INIT_22(256'h9B9B9BBDBDBDBDBDBDBDBDBDBD9B9B7B79795959593737373717171717171717),
    .INIT_23(256'hF3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F51717171717171717173737575979797B),
    .INIT_24(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3),
    .INIT_25(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F539595B5917F5D3D3D3D3D3D3D3D3D3D3),
    .INIT_26(256'hFFFFFFFFFFFFFFFFFFDFDDBD9B795959795757593715F5F3F5F3D3D1D3D3D3D1),
    .INIT_27(256'hB1B1B1B1B1B1D3F315375757373779BDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_28(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_29(256'h35F3D1D1D1AFD1F3D1D1D1D1D1D3F3F5F31335BDDFDFFFFFFFFFDF9D37D3AFB1),
    .INIT_2A(256'hFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFDFDFDFDDDDDDDFDFDFBF9B7B),
    .INIT_2B(256'hF51515F51579BDDFDFDFFFFFDFDDDDDFDFDFDFDFBD9B9B9B9B9BBDBDDDDDDDDD),
    .INIT_2C(256'h597979799B9B9B9B9B9B9B9B7B79795937373737371717171717151515151515),
    .INIT_2D(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515171717171717373737375759595957),
    .INIT_2E(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3),
    .INIT_2F(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5373715F5D3D1D3D3D3D3D3D3D3D3D3D3),
    .INIT_30(256'hFFFFFFFFFFFFFFFFFFDFDDBD7937153579373757371515F3F3D3D3D3D3D3D3D3),
    .INIT_31(256'hB1B1B1B1D1F315373737373757799BBDDDDDDDDFFFFFFFFFFFFFFFFFFFDFDFDF),
    .INIT_32(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_33(256'h15F5F3D3B1B1D3F5D3D1B1B1B1D3D315F3359BDFFFFFFFFFFDDDFFDF7BF3B1B1),
    .INIT_34(256'hFFFFFFFFDFFFFFFFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFFFFFDDDDDFFFDD9957),
    .INIT_35(256'hF3F3F313359BDDFFFFFFFFFFFFFFFFFFFDDFDDDDBDBDBDBDBDDDDFDFDDDDFDFF),
    .INIT_36(256'h3757575959597979595959575737373737371717171715151717171515151515),
    .INIT_37(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5151517171717373737595757373737),
    .INIT_38(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_39(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_3A(256'hFFFFFFFFFFFFFFFFFFDFDDBD7B3737377915F315F3F3F3D1D3D3D3D3D3D3D3D3),
    .INIT_3B(256'hD3D1D1D1F315597B375959575757799BBDDDDDDFDFFFFFFFFFFFFFFFDFDFDFDF),
    .INIT_3C(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D1D1D1D1B1D1D1B1D1D1D1D1D1D1),
    .INIT_3D(256'h3715F5D3D3D3D3D3B1B1D3D3D3D3D3D3F379DFFFDDFFFFFDFFFFFFDF79D1AFD1),
    .INIT_3E(256'hFFDFFFFFFFDFDFDFDFDFDFDFDFDFDFDFDFFDFFFFFFFDFDFFFDFFFFDDBB775535),
    .INIT_3F(256'h1515153559BDDDDDFFFFFFFFFFFFFFFDFFFFDDDDDDBDBDBDDFDFDFDFDFFFFFFD),
    .INIT_40(256'h7979797979595959595939373717171517171717171515151515151517151515),
    .INIT_41(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515151515373737373737375759),
    .INIT_42(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_43(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_44(256'hFFFFFFFFFFFFFFFFDFDFDFBD79595937F5F3D3D3D3D3D3D1D3D3D3F3F3D3D3D3),
    .INIT_45(256'h15F3F31537595959595737575757799BBBBDDDDDDDDFFFFFFFFFFFFFFFFFFFFF),
    .INIT_46(256'hD1B1B1B1B1B1B1B3B1B3D3B1B1B1B1B1D1D1D3D3B1B1D1D1AFF31515D1D1F313),
    .INIT_47(256'h1515F5F3F3F3D3D3D1D1D3F3D3D1D1F357BBDFFFFFFFFFFFFFFFFFBB595715F3),
    .INIT_48(256'hFFFFFFFFFFFFFFDFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFFFFFDDBB795735),
    .INIT_49(256'h15F31537799BBDDDDFFFFFFFFFFFFFFFDFDFDDBDBDBDBDBDDFDFDFDFFFFFFFFF),
    .INIT_4A(256'h7B7B7B7959595959373737171717171717171717171717151717171717171717),
    .INIT_4B(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515173737375959597B7B),
    .INIT_4C(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_4D(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_4E(256'hFDFFFFFDFFFFFFFFFFDFBD9B57373715F3D3D3D3D3D3D3D3D3D3F3F5F5F3D3D3),
    .INIT_4F(256'h3535353757595737593735373757577999BBDDDDDDDFDFDDFFFFFFFFFFFFFFFF),
    .INIT_50(256'hD1D3D3D3D1B1B1B1B1D3D3D3D3D3D3D3D1B1D1D3D3F3151737597B5937353757),
    .INIT_51(256'h3515F3F3F3F3F3F3D3D3D3F113375957BBDDFFFFFFFFFFFFFFFFFFDFDDDFBD9B),
    .INIT_52(256'hFFFFFFFFFFFFFFFFDFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFDFDFDFBD9B7979),
    .INIT_53(256'h15151537799BBDBDDFDFDFFFFFFFFFDFDFBFBDBDBDBDBDDDDFDFDFFFDFFFFFFF),
    .INIT_54(256'h7959593737373737371717171717171717171717171515151717171717171717),
    .INIT_55(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5151515153737393959595959595979),
    .INIT_56(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_57(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3),
    .INIT_58(256'hFFFFFFFFFFFFFFFFDFBD7957351515D3D3D3D3D3D3F3D3D3D3D3D3D3D3D3D3D3),
    .INIT_59(256'h35375757373737373515153537375779799BBDBDBDDDDDDDDDDFFFFFFFFFFFFF),
    .INIT_5A(256'h37F3B1B1D3D3D1B1D1D3D3D3D1B1B1D1F3F3F31515377B7B7B7B593735353735),
    .INIT_5B(256'h371515F3F3F5F5F3F3F3F313579BBDBDDFDFFFFFFFFFFFFDFDFDFFFFDFFFFFBD),
    .INIT_5C(256'hFFFFFFFFFFFFFFFFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFDDBDBD9B9B7957),
    .INIT_5D(256'h171737597B9B9DBDDFDFDFDFDFDFDDDDBDBDBDBDBDBDDDDFDFDFFFFFFFFFFFFF),
    .INIT_5E(256'h3737373737371717371717171717171717171717171515151717171717171717),
    .INIT_5F(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5151515173739593959595937373737),
    .INIT_60(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_61(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3),
    .INIT_62(256'hDFDFDFFFFFFFDDBD9B7935151315F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_63(256'h353737373515153513F31315353757799B9B9B9B9BBDBDDDDDDFDFDFFFFFFFFF),
    .INIT_64(256'hF3D3D1D1D1D1D3D3D1B1D1D3D3F3F53737353757597B9D9D7959373515353515),
    .INIT_65(256'h35151515151515F515F315599B9BBDDFDFDFDFDFFFFFFFFDFFFFFFFFFFFFFFBD),
    .INIT_66(256'hFFFFFFFFFFFFFFDFDFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFDDBDBB99573535),
    .INIT_67(256'h37375959797B9B9DBDBDBDDDDDDDDDDDBDBDBDBDBDBDDDDFDFDFFFFFFFFFFFFF),
    .INIT_68(256'h3737373737373737371717171717171717171717171717171717171717171717),
    .INIT_69(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515F515151517173737373737373737),
    .INIT_6A(256'hF3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_6B(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3),
    .INIT_6C(256'hDFBDBDDFDFDFBD9B57351313F3F3F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_6D(256'h3515151515151515F3F3F315153537577B7B9B9B9B9DBDBDBFDFDFDFDFDFDFDF),
    .INIT_6E(256'h5715F3D3D3F53739F5F3D3D1D1F315575757799B9B9D9D9B5957373535353515),
    .INIT_6F(256'h35151515151515151535599DBD9DBDDDDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFDD),
    .INIT_70(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFFFFFFFFFFFFFFFDFDFDFBD9B795735),
    .INIT_71(256'h3737595959797B9B9B9BBBBDBDBDBDBDBDBDBDBDBDBDDDDDDFFFFFFFFFFFFFFF),
    .INIT_72(256'h1717373737171717151717171717171717171717171717171717171717171717),
    .INIT_73(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515171717173717173737),
    .INIT_74(256'hF3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_75(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_76(256'hBFBDBDBDBDBD7957351313F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_77(256'h151515151515F3F3D1F3F3F3153737375959797B9B9B9B7B7B9B9B9B9DBDBDBD),
    .INIT_78(256'h9D15F1F3377B7B593715F5F3F1F315379B9B9BBDBD9B9B795757373735151515),
    .INIT_79(256'h35151313F3F3153737799D9D9BBDBDDDDDDFFFFFFFFFFFFFFDFDFFFFFDFFFFDD),
    .INIT_7A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFBD9B795757),
    .INIT_7B(256'h373759595979797B7B9B9B9B9B9B9B9BBDBDBDBDBDBDDDDDDFDFFFFFFFFFFFFF),
    .INIT_7C(256'h1515151515151515151517171717171717171717171717171717171717171717),
    .INIT_7D(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151517171717171515151515),
    .INIT_7E(256'hF3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_7F(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT5 #(
    .INIT(32'h10000000)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_i_1 
       (.I0(addra[15]),
        .I1(addra[16]),
        .I2(addra[14]),
        .I3(addra[12]),
        .I4(addra[13]),
        .O(ena_array));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized12
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000F3FFFFFFFFFFFFFFFFFFFFFFFFFFF3FFFFFFFFFFFFFFC00FFFFFFF800000),
    .INITP_01(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000833800000000000000000000000),
    .INITP_02(256'hFFFFFFFFFFFFFFC000000000000000000000000000000000000007FFFFFFFFFF),
    .INITP_03(256'h00000000000000000000000000000000000017FFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_04(256'h00000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF78),
    .INITP_05(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0FFFFFFF00000000000000000),
    .INITP_06(256'hFFFFFFFFFFFFFFFFFFFFFFE0FFFFFFF8000000000000000000F0000000000000),
    .INITP_07(256'hFFFBFFC0FFFFFFFF7FFE00000000000003FC000000000000FFFFFFFFFFFFFFFF),
    .INITP_08(256'hFFFE00000000000007F0000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_09(256'h0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00007000FFFFFFFF),
    .INITP_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000017FFFFFFFFFFFF0000000000000),
    .INITP_0B(256'hFFFFFFFFFFFFFFFF000007FFFFFFFFFFFC0000000000000000000000000000FF),
    .INITP_0C(256'h0000FFFFFFFFFF800000000000000000000000030000FFFFFFFFFFFFFFFFFFFF),
    .INITP_0D(256'h00000000000000000000000F0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0E(256'h00000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1FFFFFFFFFFFE000),
    .INITP_0F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000),
    .INIT_00(256'h7B7B59595937351515F3F3F3D3D3D3D3D3D3D3D3D3D3D3F3D3D3D3D3D3D3D3D3),
    .INIT_01(256'h1515F5F5F3F3D3D1D3F3F3F3151537371515153759573735153537375757797B),
    .INIT_02(256'h1513577B595937151535373757597B9BBDBBBDBD9B7979575957373737371515),
    .INIT_03(256'h35151513F3F33559799DBD9B9BBDDDBDDDDFFFFFFFFFFDFDFFFFFFFFFFFFFFBD),
    .INIT_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDBDBD9B79573735),
    .INIT_05(256'h3737595959797979797B7B7B7B9B9B9B9B9BBBBBBDBDDDDDDFDFFFFFFFFFFFFF),
    .INIT_06(256'h1515151515151517171717171717171717171717171717171717171717171717),
    .INIT_07(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515F5F515151717171515151515),
    .INIT_08(256'hF3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_09(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_0A(256'h15F5F3F3F3F3F3F3F3D3D3F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_0B(256'hF3F3D3D1D1D1D3F3F3F3F3F3F3F3F3F3F3F3D1F313F3F3D1F3F31313F3F31515),
    .INIT_0C(256'h3735799D7959571513375979797B9B9B9B9B9B9B7959595737373517151515F5),
    .INIT_0D(256'h1535353515133779BDBDBDBDBBBDBDBDDFDFDFFFFFFFFFFFFFDDDDDFDFDFBD9B),
    .INIT_0E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDDDDBB797957373535),
    .INIT_0F(256'h3737575759797B797B7B79797B7B7B7B9B9B9BBBBDBDDDDDDFDFFFFFFFFFFFFF),
    .INIT_10(256'h1515151515151515171717171717171717171717171717171717171717171717),
    .INIT_11(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515151515151515151515),
    .INIT_12(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_13(256'hD3D3D3F3F3F3F3F3F3F3F3D3D3F3F3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5),
    .INIT_14(256'hF3F3F3F3F3F3F3F3F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3),
    .INIT_15(256'hD3D3D3D1D1D1D1D1F3F3F3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_16(256'h353757797959373535579B9B9B79797979997979573735151515F3F3F3F3D3D3),
    .INIT_17(256'h3735353515133779BDBD9B9B9B9BBDBDDDDDDDDDFFFFDFDFDFDFDFBD7B573515),
    .INIT_18(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFDFDDBD9B9B79795757373735),
    .INIT_19(256'h373737575959797B7B7B7B79597979797B9B9BBBBBBDBDDDDDDDDFDFFFFFFFFF),
    .INIT_1A(256'h1515151515151515171717171717171717171717171717171717171717171717),
    .INIT_1B(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515F51515151515151515151515),
    .INIT_1C(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_1D(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5),
    .INIT_1E(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3),
    .INIT_1F(256'hD3D3D3F3F3F3F3F3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_20(256'h15153537575757579B9DBDBDBD9B9B9B9B79795757373515F315151515F3F3F3),
    .INIT_21(256'h37351535153537797B7B9B9B9B9B9BBBBBBBDDDDDFDFDFDFBD9B79371513F3F3),
    .INIT_22(256'hFFFFFFFFFFFFFFFFFFFFFFFFDFDFFFFFDFFFFFFFDDBD9B997979575759595737),
    .INIT_23(256'h373737575959797979797B7979797B79799B9B9B9BBBBDDDDFDFDFDFFFFFFFFF),
    .INIT_24(256'h1515151515151717171717171717171717171717171717171717171717171717),
    .INIT_25(256'hF5F5F51515151515151515151515151515151515151515151515151515151515),
    .INIT_26(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_27(256'hF3F3F3F3F3F3F3F3D3F3F5F3F3F3F5F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5),
    .INIT_28(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_29(256'hD3F3D3F3D1D1D1D1F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_2A(256'h151515355759799BBDBDBDBDBD9B9B7B9B9B79797959573735373937F5D3D1D1),
    .INIT_2B(256'h373715353737375759797B79795757577799BBBDBDBDBFDF9D7B15F3D3F3F3F5),
    .INIT_2C(256'hDFDFFFFFFFFFFFDFFFFFFFDFDFDFFFFFDFDFFFDFBB9979797957575959595937),
    .INIT_2D(256'h1537375759595959595979797B797B7B7B9B9B9B9BBDBDBDDFDFDFDFDFDFDFDF),
    .INIT_2E(256'h1515151517171717171717171717171717171717171717171717171717171717),
    .INIT_2F(256'h1515151515151515151515151515151515151515151515151515151515151515),
    .INIT_30(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_31(256'hF3F3F3F3F3F3F3F315171715F5F5F5F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_32(256'hD3D3D3D3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_33(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_34(256'h15153537577979799B9B9B9B9B79795757575779595735131537373715F3F3F5),
    .INIT_35(256'h371515153535373759595957351515153557999B79797B9B7B3915F3F3F3F5F5),
    .INIT_36(256'hFFFFFFFFDFDFDDDDDFDFDFDFDFDFDFDFDFDFDDBB797979795757575759593735),
    .INIT_37(256'h1535373757575757575759797959797B7B7B7B9B9BBDBDDFDFDFDFDFDFDFDFFF),
    .INIT_38(256'h1717171717171717171717171717171717171717171717171717171717171717),
    .INIT_39(256'h1515151515151515151515151515151515151515151515151515151515151515),
    .INIT_3A(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_3B(256'hF5F5F5F5F5F51515395959371515F5F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_3C(256'hD3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_3D(256'hF31315151535373737373737151515F5F5F5F5F5F3F3F3F3F3F3D3D3D3D3F3F3),
    .INIT_3E(256'h1315353737575757775757797957353513355757353513131313151515151515),
    .INIT_3F(256'h15151515151515153737371515F3151535375737351515151515F3F3F3F3F3F3),
    .INIT_40(256'hDFFFDFDFDFDDDDDDDDDDDFDFDDDDDDDDDDBD9B79575757595757575757373515),
    .INIT_41(256'h35353737373737375757575757575959797979799B9BBDBDBDBDBDDDDDDDDFDF),
    .INIT_42(256'h1717171717171717171717171717171717171717171717171717171717171717),
    .INIT_43(256'h1515151515151515151515151515151515151515151515151515151515151517),
    .INIT_44(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_45(256'hF5F5F5F5F515151515171715F5F5F5F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_46(256'hD3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_47(256'h373757595959595979795957371515F5F3F3F3F3F3F3D3D3F3F3D3D3D3D3D3F3),
    .INIT_48(256'h1515153535353535555757575757353535575757553535575757575757575737),
    .INIT_49(256'hF3F3F5F3F3F3F3F3F3F3F5F5F3F3F3F5F3131513F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_4A(256'hBDBDDDDDBDBDDDDDDDDFDFDFDDBDBDBB9B9B7979575757575757373737371515),
    .INIT_4B(256'h37373737373737575757573737375757375757597979799B9B9B9BBBBBBBBDBD),
    .INIT_4C(256'h1717171717171717171717171737171737373737373737373737373737373737),
    .INIT_4D(256'h1515151515151515151515151515151515151515151515151717171717171717),
    .INIT_4E(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_4F(256'hF3F3F3F3F5F5F5F5D3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_50(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_51(256'h7B7B79797959595937373515F3F3D3D1F3F3F3F3F3F3F3F3F3F3D3D3D3D3D3D3),
    .INIT_52(256'h3737373757575757575757575957597979797979575777797979799B9B9B9B9B),
    .INIT_53(256'hF3F3F3F3F3F3F3F3D3F3F3F3F3D3D3D3F3F3F3F3F3F3F315F313131515353535),
    .INIT_54(256'h999B9BBBBBBBBDDDDFDFDFDDBDBB9B9979595757575737375737373737151515),
    .INIT_55(256'h3737373737375757577959373757573535353757575757577979799979799999),
    .INIT_56(256'h1717171717171717171717173737373737373737373737373737373737373737),
    .INIT_57(256'h1515151515151515151515151515151515151515151515151515171517151717),
    .INIT_58(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515151515151515),
    .INIT_59(256'hF5F5F5F5F5F5F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_5A(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_5B(256'h151515131313F3F3F3F3F3F3F3F3F3F3D3D3D3D3F3F3F3F3F3F3F3D3D3D3D3F3),
    .INIT_5C(256'h59597B7B7B797979799B9B797979799B9B9B9B9B9B9B9B9B9B79795735353535),
    .INIT_5D(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F31315353735375757797979),
    .INIT_5E(256'h797999999B9BBBBDDFDDBDBB9B79775757575757573737375757373715151515),
    .INIT_5F(256'h3737375757575757797979575757573535375757373737575979797979797777),
    .INIT_60(256'h1717171717171717171717373737373737373737373737373737373737373737),
    .INIT_61(256'h1515151515151515151515151515151515151515151515151717171717171717),
    .INIT_62(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515151515151515151515),
    .INIT_63(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F51515),
    .INIT_64(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_65(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_66(256'h797979797B9B9B9B9B9B9B9B9B7B7979797979573735351513F3F3F3F3F3F3F3),
    .INIT_67(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F313131335353535575959797979797979),
    .INIT_68(256'h7979797979797979797979595757575757373737373737373737353535151313),
    .INIT_69(256'h7959797B9B79799B9B7979797977575779797757373757595979797979597979),
    .INIT_6A(256'h1717171717171717171737373737373737373737373737373737373737373737),
    .INIT_6B(256'h1515151515151515151515151515151515171717171717171717171717171717),
    .INIT_6C(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515151515151515151515),
    .INIT_6D(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515),
    .INIT_6E(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_6F(256'hF3F3F3F3F3F3F3F3F3F3F3D3D3D3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_70(256'h79595757575757573735353535353535151513F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_71(256'hF3F3F31515151515151515355757575757777979797979797979797979797979),
    .INIT_72(256'h7979795959575757575757373737373537373735353535353735353515151313),
    .INIT_73(256'h9B9B9B9B9B9B9B9B9B9B9B9B9B99797979797957575759595959595979595959),
    .INIT_74(256'h1717171717171717373737373737373737373737373737373737373737373737),
    .INIT_75(256'h1515151515151515151515151515151517171717171717171717171717373737),
    .INIT_76(256'h1515151515151515151515151515151515151515151515151515151515151515),
    .INIT_77(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5151515),
    .INIT_78(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_79(256'hF5F3F3F3F3F3D3D3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_7A(256'h3535351515131313F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_7B(256'h15153737595959595959797B9B9B9B9B7B7B7979797979795959575757575757),
    .INIT_7C(256'h5757575757373737353535353535353535353535353535351515151515131313),
    .INIT_7D(256'h9B9B799B9B9B9B9BBBBB9B9B9B9B9B9B79797957575759795959595959595957),
    .INIT_7E(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_7F(256'h1515151515151515171717171717171717171717171717171717373737373737),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized13
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000001FFFFFFFF),
    .INITP_01(256'hFFFFCFFC0000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_02(256'h00000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_03(256'h000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000),
    .INITP_05(256'hFFFFE0806007FCFF808000000000000000000000000000000000FFFFFFFFFFFF),
    .INITP_06(256'h00000000000803FC8000000071FFC3B931FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_07(256'hDC0000007FFFFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000008000),
    .INITP_08(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000003C0701FFFF),
    .INITP_09(256'hFFFFFFFFFFFFFFFF00000000210000000000C07FFFFFFFFFFCFF8003FFFFFFFF),
    .INITP_0A(256'h000000E00000000000C077FFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0B(256'h03FF7FFFFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'hFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003FFC100000),
    .INITP_0D(256'hFFFFFFFFFFFFFF0117FFFFFFFFFFFFFF00010FFFFC0000003FFFFFFFFFFFFFFF),
    .INITP_0E(256'h000000000007FFFF0701FFFEE3F11C01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_0F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000),
    .INIT_00(256'h1515151515151515151515151515151515151515151515151715171517151715),
    .INIT_01(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515),
    .INIT_02(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_03(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_04(256'h1515F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5),
    .INIT_05(256'h373759597B7B7B59795979797959573737373737373737353535353515151313),
    .INIT_06(256'h5757373737373737353535353535353535351515151515151515131315151515),
    .INIT_07(256'hBDBB9B9BBBBBBBBDBBBB9B9B9B99997979795757575757575757575757575757),
    .INIT_08(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_09(256'h1717171717171717171717171717171717171717171717171717171737373737),
    .INIT_0A(256'h1515151515151515151515151515151515151515171717171717171717171717),
    .INIT_0B(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515),
    .INIT_0C(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_0D(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_0E(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3D3D3D3D3D3F3F3F3F3F3F3F3F3),
    .INIT_0F(256'h375959595737373535153537371515131515F3F315151515151515151313F3F3),
    .INIT_10(256'h5959573737353537373737373717171715151515151515151515151515153737),
    .INIT_11(256'hBDBBBBBBBBBB9B9B9B9B9B797979795757575757373737373737373737375759),
    .INIT_12(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_13(256'h1717171717171717171717171717171717171717171717173737373737373737),
    .INIT_14(256'h1515151515151515151515151515151515151515171717171717171717171717),
    .INIT_15(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515),
    .INIT_16(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_17(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_18(256'hF3F5F3F3F3F3F5F5F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F3F3F3F3),
    .INIT_19(256'h37373737151515131313131515151515F5F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_1A(256'h3737371515151515151515151515151515151515151515151515173737373737),
    .INIT_1B(256'h9B9B9B9B9B9B9979797979575737353535353737353515151515353535375759),
    .INIT_1C(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_1D(256'h1717171717171717171717171717171737373737373737373737373737373737),
    .INIT_1E(256'h1515151515151515151515151515151515151515151517171717171717171717),
    .INIT_1F(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515151515),
    .INIT_20(256'hF5F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_21(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5),
    .INIT_22(256'hF5F5F5F5D5F5F5F5F5F5F5F5F5F5F5F5D3F3F3F3F3F3F5F5F3F5F3F5F3F3F3F3),
    .INIT_23(256'h1715151515151515F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_24(256'h1515151515151515151515151515151515151515151515151515171717171737),
    .INIT_25(256'h9B9B9B9B9B797959575737373535151515151515151515151515151515151537),
    .INIT_26(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_27(256'h3737173717371737173717371737171737373737373737373737373737373737),
    .INIT_28(256'h1515151515151515151515151515151515153535353535353737171717171717),
    .INIT_29(256'hF5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F515151515151515151515151515151515),
    .INIT_2A(256'hF3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_2B(256'hF3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5),
    .INIT_2C(256'hF3F5F5F5F5F5F5F5F5F5F5F3F3F3F3F3F5F5F5F5F5F5F5F5F3F3F3F3F3F3F3F3),
    .INIT_2D(256'h15F5F3F3F3F3F3F315F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5F3F3F3F3F3),
    .INIT_2E(256'hF51515F5F5F5F5F3F5F5F5F5F5151515151515151515F5F51515151515151515),
    .INIT_2F(256'h79797959575737373737373715151517151515F5F5F5F5F515F5F5F5F5F5F3F3),
    .INIT_30(256'h3937373737373737373737373737373737373737373737373737373737373737),
    .INIT_31(256'h1717173737373737373737373737373737373737373737373737373737373737),
    .INIT_32(256'h3515151515151515171717171717171735353535353537373535353737373737),
    .INIT_33(256'hF5F51515F5F5F515151515151515151515151515151515151515151515353515),
    .INIT_34(256'hF3151515F3F3F31515151515151515151515F5F5F5F5151515F5151515F5F515),
    .INIT_35(256'h15F3F3F3F3F3F3F3F3F3F5F5F5F3F3F3F3F3F3F3F3F5F5F5F3F5F5F5F5F5F5F5),
    .INIT_36(256'hF3F3F3F3F3F3F3F3F3F3F3F515F5F3F3F3F3F3F3F3F31515375937131515F3F3),
    .INIT_37(256'hF3F5F5F3F3F3F3F3F5F3D3F3F5F5F5D3F3F5F5F5F3F3F3F3F5F3F3F3F3F3F3F5),
    .INIT_38(256'hF5F3F3F5F5F5F5F3F3F3F5F5F5F5F5F515F5F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_39(256'h573737353515151515151515F5F5F5F5F5F5F5F5F5F5F5F5F3F3F5F5F5F5F5F5),
    .INIT_3A(256'h3737373737373737595959373737373759595959593939373939393737373737),
    .INIT_3B(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_3C(256'h3535353537575737353535153535373757573737373535353737375737373535),
    .INIT_3D(256'h1515151515151515F5F5F5151515151535151515151515151537597979797957),
    .INIT_3E(256'hF313131515151515151515151515151515151515151515151515151515151515),
    .INIT_3F(256'h3715F3151515F3F3F3F3F3F3F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5),
    .INIT_40(256'hF3F3F3F3F3151515F3F3F3F3F3F3F3133535373757597B9BBDBD9D7979575757),
    .INIT_41(256'hF5F5F5F3F5F5F3F3F3F3F5F5F5F5D5F5F3F3F3F3F3F3F3F3F5F515151515F3F3),
    .INIT_42(256'hF3F3F3F3F3F3F3F5F5F5F5F5F5F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_43(256'h151515151515F5F5F5F5F5F5F5F3D3D3F5F5F5F5F5F3F3F3F5F5F5F5F5F5F3F3),
    .INIT_44(256'h5959595959595959373737373737373737373737373737375959595959595959),
    .INIT_45(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_46(256'hBDBBBBBBDDDDDDBD999979797979999BDFBDBDBDBDBD9B9B9B9D9D9D9B7B7959),
    .INIT_47(256'h151515151515151515151717171515151515153535575759799DDFFFDFDFDFDF),
    .INIT_48(256'h3535353535373737575757573737151515151515151515151515151515151515),
    .INIT_49(256'h573715353515F3F3151515151515151515F5F5F5F5F5F5F5F5F5F5F3F3F51515),
    .INIT_4A(256'h151513131315151535353535353537579B9B9B9B9B9B9B9B9B797979799BBDBD),
    .INIT_4B(256'hF5F5F3F5F5F5F3F3F3F3F5F5F3F3F5F51515F5F5F3F5F5F3F315151515151513),
    .INIT_4C(256'hF3F315F5F3F5F515F5F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_4D(256'hF3F3F3F3F3F3F5F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F3F3F3F5F5F5),
    .INIT_4E(256'h3737373737373737373737375959595939395959595959595959595959595959),
    .INIT_4F(256'h5959595959595959373737373737373737373737373737373757375737373737),
    .INIT_50(256'hFFFFDDDDFFFFFFDDFFFFDFDDDDDFFFFFDFDFDFDFDFDFBFBDDFDFBFBDBD9D7B7B),
    .INIT_51(256'h1515151515151515151515151515151535353537597B9B9B9BBDDFDFDFDFDFDF),
    .INIT_52(256'h5959575757575757575959575737351537373515151515151515151515151515),
    .INIT_53(256'h9B7B7B7B593735373715151515373717F5F5F5F5F51515151515151515373737),
    .INIT_54(256'h57373535353757599B9B7B9B9B9B9BBBBDBDBBBDBDBDBDBDBB79575779BBDFBD),
    .INIT_55(256'hF5F5F5F5F3F3F3F31313F3F3F3F3F5F5F3131313F31315157B9B9D9D9D9D9B7B),
    .INIT_56(256'hF3F3F3F3F3F3F3F3F5F5F5F5F5F5F5F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F5),
    .INIT_57(256'hF3F3F3F5F5F5F5F5F3F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5151515F5F5F5F5F3),
    .INIT_58(256'h5959595959595959373737373737373737375959595959595957575959595959),
    .INIT_59(256'h3737373737373737373737373737373737373737373737373737373737373737),
    .INIT_5A(256'hFFFFFFFFFFFFDFDFDFDDDDDDBDBDBDDDBDBDBDBDBDBDBDBD9B9B7B7979595757),
    .INIT_5B(256'h1515151515151515151515153737375959575759797B7B9BBDBFDFDFDFDFDFDF),
    .INIT_5C(256'h79795757597979799B9B9D9D9D9D9B9B7B795937151515151515151515373515),
    .INIT_5D(256'h9B7B9B9B7B5937373515151515151515F5151515151515153737575757797979),
    .INIT_5E(256'h79797979797B9B9BBD9D9B9D9BBDBDDDDDDDDDDDDFDFDDBDBD997979799BBD9B),
    .INIT_5F(256'hF5F5F5F3F3F313355737351535371513F3131313131535579BBDBFBDBDBD9D9B),
    .INIT_60(256'h151515151515F5F3F5F5F515F5F5F3F3F5F5F5F5F3F5F5F5F3F3F3F3F3F3F3F3),
    .INIT_61(256'hF3F3F5F5F5F5F5F5F5F5F5F5F5F5F3F3F5F5F5F5F5F3F3F3F3F3151515151515),
    .INIT_62(256'h5939393939393939595959595959595959595959595959595959595959595959),
    .INIT_63(256'h5959595959595959575757575757575957575959595959595959595959595959),
    .INIT_64(256'hBBBBBBBBBBBBBBBD9B9B9B9B7979797979797957575757575757575757375757),
    .INIT_65(256'h5737373737373735373737373759597B57375759797B7B7B9B9B9B9BBDBDBDBD),
    .INIT_66(256'h79795979799B9B9B9B9B9B9B9B9B9B9B7B79573735353737575979797B7B7B7B),
    .INIT_67(256'h795757797979351315151515151515F5151537373737575757797979799B9B99),
    .INIT_68(256'h7B7B9B9B9B9B9B7B9B9B9BBBBDBBBDDFFFFFDFDDDDBD9B9979799B9B9B9B9DBD),
    .INIT_69(256'hF3F31315153579BD9B9B9B9B9B9B593537575757575779799BBDBDBDBDBDBDBD),
    .INIT_6A(256'h151313131515F5F3F3F3F3F3F3F3F5F5F3F5F5F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_6B(256'hF5F5F5F5F3F3F3F3F5F5F5F5F5F5F515F5F3F3F3151515355737151513131515),
    .INIT_6C(256'h5959595959595959595959595959595959595959595959595959595959595959),
    .INIT_6D(256'hF3F3F513F5131515151515151515151537373737373737373737373739593939),
    .INIT_6E(256'h795959595959595937373735151515151515151313131313F3F3F3F3F3F5F515),
    .INIT_6F(256'h7B795959595959593735353535373737373737375959797B79797B7B9B9B9B79),
    .INIT_70(256'h7979797979799B9B9B9B9B797979797957373735353737577B9DBDBD9DBD9D9D),
    .INIT_71(256'h99797999997935351315151515151515353757575757797979999B9BBBBB9B99),
    .INIT_72(256'h57597979797979799B9B9BBDBDBBBDDDDDDDDDDDBDBB9979799BBBBDBD9B9BBD),
    .INIT_73(256'h15153559799BBDDFDDDFDFBDBDBD9B799B7B797979799B9B9B9B9BBBBB9B9999),
    .INIT_74(256'h131313F3F3F3151537151515F3F3F515F3F5F5151515F5F5F5F5F5F5F5F5F515),
    .INIT_75(256'hF5F5F5F5F5151515F3F3F3F3F3F3F313131313153557799BDFDF9B79351513F3),
    .INIT_76(256'hD0D0D0D0D0D1F3F3F3F3F3F3F315151515151515153737373737575959595979),
    .INIT_77(256'h8C8C8C8C8C8C8C8C8E8C8EAEAEAEAEAEAEAEAEAEAEAEAEAEB0B0B0D0D0D0D0D0),
    .INIT_78(256'hF3D1D1D1D1D1AEAEAEAE8C8C6C6A6A6A8C8A6A6A8A8A6A6A6A6A6C6C8C8C6C6C),
    .INIT_79(256'h3737353737373737373737373737373737373737373737373535351513131313),
    .INIT_7A(256'h999B9B7979575757575737353535373737373737373737373759595937373715),
    .INIT_7B(256'h99999B9B7937353513151515151515151537575737575779799BBDBDBDBDBB9B),
    .INIT_7C(256'h3535355757799B9B9B9B9BBDBB9BBBBDDDDDDDDDBD9B7977999B9B9BBD9B799B),
    .INIT_7D(256'h3757799DBFBDBDBDBDDDDFBB9B9B9B9B7979575779799B9B9B9B9B9B99775535),
    .INIT_7E(256'h575759573737599B9D9B59371513131315153737373737151515151515151515),
    .INIT_7F(256'h1313131515353737575737353515151513131535799BDFFFDFDFFFFFDD9B5735),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized14
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000001),
    .INITP_01(256'hFFFFFFFFFFFFFE0000000000000000000000000000000000FFFFFFFFFFFFFFFF),
    .INITP_02(256'h00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_03(256'h0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000),
    .INITP_05(256'hFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000001F7FF036000),
    .INITP_06(256'hFF0000000000000000000000000000000000013BFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_07(256'h0000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INITP_08(256'h000000000700FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000),
    .INITP_09(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000),
    .INITP_0A(256'hFFFFFFFFFE00000000000000000000000000000000000000000000000F1FFF73),
    .INITP_0B(256'h000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'h00000000000000000000000000000810FFFFFFFFFFFFFFFFFFFFFFC000000000),
    .INITP_0D(256'h0000000000000000FFFFFFFFFFFFFFFFFFFE0000000000000000000000000000),
    .INITP_0E(256'hFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000),
    .INITP_0F(256'h000000000000000000000000000000000000000000000000000000000000C07F),
    .INIT_00(256'hB0AEB0AEB0D0B0D0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0F0F2F2F2F2F2F2F212),
    .INIT_01(256'h8C8C8CAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0AEAEAEAEAEAEAEB0B0B0B0B0D0),
    .INIT_02(256'h6A6A6A6A8A8A6A6A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_03(256'h37373735353515151313F3F3F1D1D1D1CEAEAEAEACAC8C8C8A8A8A8A8A8A6A6A),
    .INIT_04(256'h5737373737353515353515151515151515151515353535353535373737373737),
    .INIT_05(256'h5959595937371515151515151515151535375959575759795959595959595757),
    .INIT_06(256'h3535375757799B9B9B9B9B9B9B7B7B7B9B9B9BBBBDBDBDBDBD9B9B9B9B9B7B79),
    .INIT_07(256'h5959797B7B9B9B9B9B9B9B7B7B7B9B9B7B79799DBFBD9B9B9B9B9B7B59573535),
    .INIT_08(256'hFFFFFFBD9979999BBDBD9957353535355757575959797B7B5957373737153537),
    .INIT_09(256'h35355799BDDDDDDFDFDDDFDFBB7735335599BD9B99BBDDDDFFFFFFFFFFFFFDDD),
    .INIT_0A(256'hD0D0D0D0D0D0D0D0D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_0B(256'hAEAEAEAEAEAEAE8CAEAEAEAEAEAEAEAEAED0D0B0AED0D0D0D0D0D0D0D0D0D0D0),
    .INIT_0C(256'h8C8C8C8C8C8C8C8C8C8CACACACACACACACACAEAEAEAEAEAEACACACAC8C8C8C8C),
    .INIT_0D(256'hACACAC8C8A8A8A6A68686868686848686A6A6A6A6A6A6A6A8A8C8C8C8C8C8C8C),
    .INIT_0E(256'h1515151515151515373737373737373757373737151515F3F3F3F3D1D1CFAFAF),
    .INIT_0F(256'h1515151515151515151515151515151515373737151515151515151515151515),
    .INIT_10(256'h353737375757775779797979797B7B7B59577979797959595959797979795957),
    .INIT_11(256'h5959595959595757595959575757595959797B797B9B7B797979595735351535),
    .INIT_12(256'hDDDDDDBB99999BBBBDBD9B797979797979777779797979797959595737353757),
    .INIT_13(256'hBBBBBBDDDFFFFFFFFFFFFFFFDDBBBBBBBDDFDFDDBBDDDDDDFFFDFDFFFFFFFDDD),
    .INIT_14(256'hD0D0D0D0D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_15(256'hAEAEAEAEAEB0B0B0AEAEAEB0B0B0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_16(256'hACACACACACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_17(256'h68686868686868688A8A8A8A8A8C8C8C6A6A6A6A8A8A8C8C8C8C8C8C8C8C8C8C),
    .INIT_18(256'h3737373735351515151513F3F1D1D1D1ACAC8C8C6A6A6A6A4848484848484868),
    .INIT_19(256'h1515151515151515353515151515151313131515151515151313151535353535),
    .INIT_1A(256'h9B9B9B9999999999797977575735151335353557573735353535353535351513),
    .INIT_1B(256'h59595959575757353535353535353535355757355757353333353557799B9BBD),
    .INIT_1C(256'hDDDFDFDDDDDDDDDFDDBDBD9B9BBDBD9B797979797979797B9B7B795735353537),
    .INIT_1D(256'hBDBD9BBDBDBDBDDDDDBDBBBBBB9BBBBDBBBB9B99999B9BBBBDDDDDDDDDDDDDDD),
    .INIT_1E(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4),
    .INIT_1F(256'hD0D0D0D0D2D2D2D2D2D2D2D2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_20(256'hAEAEAEAEAEAEAEAEAEAECECECECED0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_21(256'h6A6A6A6A6A8A8A8A6A6A6A6A6A6A6A6A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_22(256'hD1D1D1CFAFAEACAC6A6A6A68684848486A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_23(256'h151515151515151515151515151515353535353535353535573737351513F3F3),
    .INIT_24(256'hDFDFDFDDDDDDDDDDBDBDBBBB9B79575735353535151535353535151515131315),
    .INIT_25(256'h57373535353535353535353535575757997757799B9977577779999BBDDFDFDF),
    .INIT_26(256'hBDBDDDDDDDDFDFDDDDDDBDBDBDBDBD9B9B9B9B7957579BBD9B9B795735353557),
    .INIT_27(256'hBD9D9B9DBDBDBDBDBB9B9B9B9B9B9B9B9B79797979799B9B9BBBBBBBBDDDBDBD),
    .INIT_28(256'h1414141414141414F4F4F4F4F4F41414F41414F4F4F4F4F4F2F2F2F2F2F2F2F2),
    .INIT_29(256'hF2F2F2F2F2F2F2D2F2F2F2F2F2F2F2F2F4F4F4F4F4F4F41414141414F4141414),
    .INIT_2A(256'hCECECED0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D2D2F2F2F2F2),
    .INIT_2B(256'h6A6A6A6A6A6A6A6A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8CAEAEAEAEAEAEAEAE),
    .INIT_2C(256'h6868684848484846686868686A6A6A6A686868686A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_2D(256'h1515151515353535353535353515151535151313F3F1D1D1ADAC8C8A6A686848),
    .INIT_2E(256'hFFFFFFDDDDFFFFFDFFFDDDDDDDBB9B9979595735151535351515151515153537),
    .INIT_2F(256'h57373535353535353557575757799BBDFFDDDDDFFFDFDDDDBDBDBDBDDDDDDDBD),
    .INIT_30(256'hBBBBBBBBBDDDDDBDBDBDBDBDBDBDBDBDBDBB9B795757799B5757573535575779),
    .INIT_31(256'h9D9B9B9B9B9B9B9B797B9B9B9B9B9B9B9B7B7B9B9B9B9B9B9B9B9BBBBBBBBDBD),
    .INIT_32(256'h1414141414141414141414141414141414141614141414161414141414141414),
    .INIT_33(256'hD2D2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F414F4F4141414F41414),
    .INIT_34(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D2D2),
    .INIT_35(256'h8A8A8A8A8A8A8C8C8C8C8C8C8C8C8C8CAEAEAEAEAEAEAEAEAEAEAED0D0D0D0D0),
    .INIT_36(256'h686868686A8A6A8A8A8A6A6A686868686A8A8A8A8A8A8A8A6A8A8A8A8A8A8C8A),
    .INIT_37(256'h3737373735151313F3F3F1D1AFAFAD8C6A6A6868484846464848484868686868),
    .INIT_38(256'hDFFFFFDDDDFFFFFDDDDDDDDDDDBBBB9979795957373535153537373737351535),
    .INIT_39(256'h7B7959575757575735575757599BBDDFDDFFDFBDBBBDDDDFBBBB9BBBBDDDDFFF),
    .INIT_3A(256'hBBBBBBBBBBBDBDBBBBBB9B9B9B9B9B9B9B797757577779799B9B9B9BBDBDDFDF),
    .INIT_3B(256'h795959797959577979575959797979797979797B79797979999B9B9B9B9B9B9B),
    .INIT_3C(256'hF4F4F4F414141414141414141414141414141414141414161616363636161616),
    .INIT_3D(256'hD0D0D0D0D0D2F2F2D0D0D2D2D2D2F2F2D2D0D2F2F2F2F2F2F2F2F2F4F2F2F2F2),
    .INIT_3E(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_3F(256'h6A6A8A8C8C8C8C8C8CACAEAEAEAEAEAEAEAEAEAEAEAEAED0B0D0D0D0D0D0D0D0),
    .INIT_40(256'h6A6A6A6A6A6A6A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A6A),
    .INIT_41(256'hAEAEAC8C8A6A68484848484848484848686868686A6A6A6A6A6A6A6A6A6A6A68),
    .INIT_42(256'hFFFFFFFFFFFFFFFFDDDDDDFFDFDDDDBD9B9B7B79595937371313F3F3F3D1D1D1),
    .INIT_43(256'h9B9B7B797B7979795757575757799DBDBDDDDDBBBBBDDDDDBDBB9B9BBBBDDDFF),
    .INIT_44(256'h9999999999999999999999999999999B79795757777979799B9BBBBBBDBDBDBD),
    .INIT_45(256'h7959597979595779797957575757595959797979797979797979997979797999),
    .INIT_46(256'hF2F2F2F2F4141414F4F4F4F4F4F4F4F414141414141414141414141414141414),
    .INIT_47(256'hD2D2D0D0D0D0D0D0D0D0D0D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_48(256'hAEAED0D0D0D0D0B0D0D0B0AEAEB0D0D0AEAEB0B0B0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_49(256'h8C8C8C8C8C8C8CACAEAEAEAEAEAEAEAEAEAEB0AEAEAEAED0D0D0D0D0D0B0AEAE),
    .INIT_4A(256'h6A6A6A6A6A6A6A6A8A8A8A8A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A8A8A8A8C8C),
    .INIT_4B(256'h46464648484848484848486868686A6A686868686868686A6A6A6A6A6A6A6A6A),
    .INIT_4C(256'hDDDFDFDDDDFFFFFFFFDDDDBB9B79573513F3F1AEACAC8C6A6A6A484846464646),
    .INIT_4D(256'hDDBDBDBDBFDDBDBD9B9B797779799BBDDDBBBBDDFFDFDDDDBBBB9B9B9BBBBDBD),
    .INIT_4E(256'h999B9999777979797779797999999BBDBDBB9B7977777999999BBBBDDDDFDFBD),
    .INIT_4F(256'h9B7B797979575757777757575757575779797979799B797979999B7979797999),
    .INIT_50(256'hF4F4F4F414141414F4F4F414141414141414141414141414F4141414F4F41414),
    .INIT_51(256'hD0D0D0D2F2F2F2D2D0F2F2F2F2F2F2F2F2F2F2D2D2F2F2F2F2F2D2F2F4F4F2F2),
    .INIT_52(256'hD0D0D0D0D0D0D0AED0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_53(256'hAEAEAEAEAEAEAEAEAEAEAEB0B0B0B0AEAEAEAEAEAEAEAEAED0B0AEAED0D0D0B0),
    .INIT_54(256'h6A6A8A6A6A6A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8C8C8A8C8C8C8C8C8CAC),
    .INIT_55(256'h68686868686868686868686868686868686868686868686A6A6A6A6A6A8A6A8A),
    .INIT_56(256'h79797755331310F0EECEACAA8A68684846464648484848484848484848484848),
    .INIT_57(256'hBDBDBDBD9B9B9B99797979775779799B9B9B9BBBBBBDBDBDBDBDBB9B9B9B9B99),
    .INIT_58(256'hDDDDDDBDBDBDBDBD99999B9BBBBDDFDFDFDFBB795777999B9B9B9BBBBDDDDDBD),
    .INIT_59(256'hBB9B7979575757575757553535353535575779797977777979797979999B9BBB),
    .INIT_5A(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F414141414141414141414141414),
    .INIT_5B(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2F2F2F2D2F2F2F2D0D2F2F2F2D2F2F2),
    .INIT_5C(256'hAEAEAEAEAEAEAEAED0D0D0D0D0AEAEAED0D0D0D0D0D0D0D0AEB0D0D0D0D0D0D0),
    .INIT_5D(256'hAEAEAEAEAEAEAEAEB0B0B0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0B0B0AE),
    .INIT_5E(256'h6A6A6A6A6A6A6A8A6A6A6A6A6A8A6A6A8C8C8C8C8C8C8C8C8C8C8C8E8EAEAEAE),
    .INIT_5F(256'h48686868686868686A6A6A6A6A6A6A6A6868686A8A8A8A8A8A8A8A8A8A8A8A8A),
    .INIT_60(256'h6868686866464646464646464868686868686868686868684848486868686868),
    .INIT_61(256'h999B9B9B79797979575757577799797979999BBD9B7957571513F1CECEAC8A8A),
    .INIT_62(256'hDFFFFFFFFFFFFFFFDDBDBBBBBDDDDDDDDDBDBB797777799999999999999BBBBB),
    .INIT_63(256'h9B797777797977575555577779999B9B9B997999999999999999999BBBBDDDDD),
    .INIT_64(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F414F4F2F2F2F2F414F4F4F4F4),
    .INIT_65(256'hD0D0D0F0F0F0D0D0D0D0D0D0D0D0D0D0D0F2F2F2D0D0F2F2D0F2F2F2F2D2F2F2),
    .INIT_66(256'hAEAEAEAEAEAEAECECED0D0D0D0CECED0AEAEAEAEAECED0D0D0D0D0D0D0D0D0D0),
    .INIT_67(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8C8C8C8CAEAE8C8CAEAEAEAEAEAEAEAE),
    .INIT_68(256'h6A6A6A6A6A6A6A6A6A6A6A8A8A8A8C8C8C8C8C8C8E8E8E8EAEAEAEAEAEAEAEAE),
    .INIT_69(256'h68686868686868686A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A8A8A8A6A6A6A6A6A),
    .INIT_6A(256'h4846464868686868686868484848484868686868686868686868686868686868),
    .INIT_6B(256'h9B9B9BBDBDDDDFDDBDBD9B79577735F1CECECEAEAC8A68484848464648484646),
    .INIT_6C(256'hBDDDDFDFDFDDDDBDBDBB9B9BBBDDDDDFBDBDBDBB9B9B9B9B7777777757575777),
    .INIT_6D(256'h9B9BBBBDDDBD9B99999BBDBDDDDFDFDFDFDDDDDDDDBDBBBBBBBBBBBBBBBBBBBB),
    .INIT_6E(256'hF2F2F2F2F2F2F2F2F4F2F2F2F2F2F2F2F2F2F2F4F4F4F2F2D2F2F2F2F2F2F2F2),
    .INIT_6F(256'hAECED0D0D0D0D0D0D0D0D0D0D0CED0D0D0F2F2D0D0F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_70(256'hAECED0AEAECED0D0D0D0D0D0D0D0D0D0F0F0F0F0D0F0F2F2D0D0D0CECECECECE),
    .INIT_71(256'hD0CECEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8CAEAEAEAEAEAEAEAE),
    .INIT_72(256'h6A6A6A6A6A6A6A8A8C8C8C8C8C8C8C8CAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_73(256'h68686A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_74(256'h4848464848486868484848484848484868686868686868686868686868686868),
    .INIT_75(256'hBD9B7977573513F1CEAC8A686868462446464626264646484848484848484848),
    .INIT_76(256'hBDBDBDBDBDBDBBBB9B9B9B9B9BBBBDDFDFDFDFDFDDBD9B999B9BBDBDBDBDBDBD),
    .INIT_77(256'h99BBDDDDBD9B9B99BDBDDFDFDDBDBDBDBDDDDFDDBB9B9BBBBBBBBBBBBBBDBDBD),
    .INIT_78(256'hF2F2F2F2F2F2F2F2F4F4F2F2F2F2F2F21414F2F2F4F4F4F4F414141414141414),
    .INIT_79(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0F2F2F2D0F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_7A(256'hD0D0D0D0D0D0D0AED0D0D0D0D0D0D0D0D0CEAEAEAEAEAEAED0CECED0D0D0D0D0),
    .INIT_7B(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAED0D0AEAEAEAEAEB0D0D0D0D0),
    .INIT_7C(256'h8A8C8C8C8C8C8C8C8C8CACACACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_7D(256'h6A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A6A8A6A6A8A8A8A8A8A8A8A8A),
    .INIT_7E(256'h48484848486868684848484848484A6868686868686868688A8A6A6A6A6A6A6A),
    .INIT_7F(256'h8A68686868464646684646466868686A686868686868686A6868686848484846),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized15
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h000000000000000000000000000000000000000000000021FFFFFFFFFFFFFFF8),
    .INITP_01(256'h00000000000000000000000000000000FFFFFFFFFFFFF0000000000000000000),
    .INITP_02(256'h000000000003CE78FFFFFFFFFF80000000000000000000000000000000000000),
    .INITP_03(256'hFFFFFFFC00000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h000000000000000000000000000000000000000000000000000000000004101E),
    .INITP_05(256'h0000000000000000000000000000000000000000000008FCFF3C380000000000),
    .INITP_06(256'h00000000000000000000000000018C60C0000000000000000000000000000000),
    .INITP_07(256'h0000000000018801000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h000000000000000000000000000000000000000000001F79FC0700803884103F),
    .INITP_0A(256'h000000000000000000000000000001313FFFF11CF7C479FF0000000000000000),
    .INITP_0B(256'h00000000000000011FE0003F6180FF0800000000000000000000000000000000),
    .INITP_0C(256'h0000000000010000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000003),
    .INITP_0E(256'h0000000000000000000000000000000000001E796000188F6000000000000000),
    .INITP_0F(256'h0000000000000000000000000000BF1F00000000000000000000000000000000),
    .INIT_00(256'h999B9BBBBBBBBBBB9B9BBBBBBBBBDDDDFFDFDFFFDFDFBDBB7977573513F1CECE),
    .INIT_01(256'h9B9B9B795757799B9BBDBDBDBB9B9BBB9BBBBDBB9B999BBBBBBB9B9B9B9B9B9B),
    .INIT_02(256'hD2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4F2F2F2F2F2F4F4F4F414F4F2F2F414),
    .INIT_03(256'hF2F2F2F2F0F0F2F0D0D0D0D0F0F2F2F2F2F2D0D0F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_04(256'hCECECECED0D0D0AED0AEAEAEAEAECED0D0D0CEAEAECED0D0F0D0D0D0D0F2F2F2),
    .INIT_05(256'hAEAEAEAEAEAEAEAEAEAEAECECECEAEAEAEAEAEAEAEAEAEAEAED0D0D0D0D0D0D0),
    .INIT_06(256'h8CACACAEACACACAC8C8C8CACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_07(256'h8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8C8C),
    .INIT_08(256'h48484848686868688A6A6A6A6A6A6A6A6A6A6A8A6A8A8A8A8A8A8A8A6A6A6A8A),
    .INIT_09(256'h6868686868686868686868686846466868686868464646464646464848686868),
    .INIT_0A(256'hBBBBBBBBBBBBBBBBDDBD9B9B9B9B997955351313F0CC8A8A6846464646464646),
    .INIT_0B(256'h9B9977575779799B7B9B9B9B9B9B9BBBBB9B9B999BBBBBBB9B9B9B9B9B9B9B9B),
    .INIT_0C(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_0D(256'hD0D0D0D0D0D0D0D0F0F0F2F2F2F2F2F2F2F2F0F0F2F2F2D0F2F2F2F2F2D0F2F2),
    .INIT_0E(256'hAEAEAEAEAED0D0CEAEAEAEAEAED0D0D0D0CECEAECED0F0F2D0D0CECED0D0D0D0),
    .INIT_0F(256'hAEAEAEAEAEAEAEAEAEAEAEAECED0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0AEB0B0),
    .INIT_10(256'hACAEAEAEAEAEACACACACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_11(256'h6A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_12(256'h68686868686A8A8A6868686A6A6A6A6A6A8A8A8A8A8A6A6A6A6A6A6A6A6A6A6A),
    .INIT_13(256'h68686868686868686868686868464646464646466868688A6868686868686868),
    .INIT_14(256'h9B9979797755555513F1CECCAC8A684646464666666648686A68686868686A6A),
    .INIT_15(256'h57555557575957575757575757577777797977777999997977777979799B9B9B),
    .INIT_16(256'hF2F2F2F2F2F2F2F2F2F2F2F4F4F414141414F4F4141414F4F414141414F4F4F4),
    .INIT_17(256'hF2F2F2F0D0D0D0D0F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D0D0F2F2D2D0),
    .INIT_18(256'hAED0CEAEAECED0D0F2D0D0D0D0D0D0D0F2F2D0D0D0D0D0F2D0CEAECED0D0D0D0),
    .INIT_19(256'hD0D0D0D0D0D0D0D0D0D0AEAEAEAED0D0D0D0D0B0AEAEAEAEAEAEAEAEAEAEB0D0),
    .INIT_1A(256'hACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEACACACACAEAEAEAEAEAEAECE),
    .INIT_1B(256'h6A8A8A8A8A8A8A8A8C8A8C8A8C8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_1C(256'h6A6A6A68686868686A6A6A6A6A6A6A6A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_1D(256'h466868686868686868686846686868686868686868686868686A6A6868686868),
    .INIT_1E(256'hCEACACAA8A664646464646464646686868686868686868684848686848484848),
    .INIT_1F(256'h355557575535375757375757575757575757797979795757575735351313F1D1),
    .INIT_20(256'hD2D2F2F2F4F4F2F2F4F4F2F4F414F4F4F4F4F414F4F4F4F4F4F4F414141414F4),
    .INIT_21(256'hF2F2F2F2F2F2D0D0D0D0D2F2F2F2F2D2D2F2F2F2F2F2F2F2F2F2D2D0D0D0D0D2),
    .INIT_22(256'hD0D2F2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0F2F2D0F2F2F2F2F2F2D2D0),
    .INIT_23(256'hD0D0D0D0D0D0D0D0D0D0B0D0D0D0AEAED0D0D0D0D0D0D0D0D0D0D0D0D0D0D2F2),
    .INIT_24(256'hD0D0AEAEAECEAEAEAEAEAEAEAEAEAEAEAEAEAEAECECECEAED0D0D0D0D0D0D0D0),
    .INIT_25(256'h6A6A6A8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAEAEACAEAEAEAEAEAEAE),
    .INIT_26(256'h6A6A6A6A6A6A6A6A686868686A6A6A6A8A8A8A8A8A6A6A6A8A8A8A6A6A6A8A8A),
    .INIT_27(256'h48486868686868686868686868686868686868686868686A6A6A6A6A6A6A6A6A),
    .INIT_28(256'h4646464648686868686868686868686848686868686848486868684848484848),
    .INIT_29(256'h9B9B997779997935F1F113575513EFCFCEF1353513CECECE8A8A686846462626),
    .INIT_2A(256'hF4F4F2F2F2F4F4F4F4F4F4F4F4F4F4F4F4F4F4F414F4F4F4141414141414F4F4),
    .INIT_2B(256'hD2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D0D2D2F2D2D2D2D2F2F2F4F4F4F4F4F2),
    .INIT_2C(256'hD2F2F2D0D0D2D0D0D0D0D0D0D0D0D0D0D0D2D2D0D0D2D2D0D0D2D2F2F2D2D0D0),
    .INIT_2D(256'hD0D0D0D0D0D0B0AED0D0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_2E(256'h8C8C8C8EAEAEAEAEAEAEAEAEAEAEAEAED0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_2F(256'h6A8A8C8C8C8C8C8CACACACACAEAE8C8CAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_30(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A6A6A6A6A6A6A6A6A6A6A6A6A8A8A),
    .INIT_31(256'h686868686868686868686868686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_32(256'h4848484848686868686868686868686868686868686868686868686868686868),
    .INIT_33(256'h1313F1CFCFEFCD8A6868888A8A68686846686848462646484646464648686868),
    .INIT_34(256'hF4F2F2F2F2F2F2F4F2F4F4F4F4F4F41414F4F4F41414F4F4F41414F4F4F4F4F4),
    .INIT_35(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2D2D2D2D0D2D2D2D2D2D2D2D0D2F2F2F2F2F2F2),
    .INIT_36(256'hD0D2D2D0D0D0D0D0D0D0D0D0D0D0D0D2D0D2D2D0D2D2D2D2F2F2F2F2F2F2D2D0),
    .INIT_37(256'hB0B0B0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_38(256'hAEAEAEAEAEB0D0D0D0D0D0D0D0D0D0D0D0D0B0B0D0D0D0D0AEAED0D0D0B0B0B0),
    .INIT_39(256'h8C8C8CACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAED0),
    .INIT_3A(256'h6A8A8A8A8A8A6A6A8A8A8A8A6A6A8A8A6A6A6A6A6A6A6A6A6A6A8A8A8A8C8C8C),
    .INIT_3B(256'h68686868686868686A686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A),
    .INIT_3C(256'h6868686848484848686868686868686868686868686868686868686868686868),
    .INIT_3D(256'h4646466668686868684646464646464668686848486868686868484848464648),
    .INIT_3E(256'hF2F2F2F2F2F2F2F2F4F4F4F4F4F4F41414F4F4F414F4F4F4F4F4F4F4F4F4F414),
    .INIT_3F(256'hF2F2F4F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_40(256'hF2F2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4F4F4F4F2F2F2),
    .INIT_41(256'hB0B0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D2D2D2D0D0D0D0D2D2D0D2F2F2D2D2F2),
    .INIT_42(256'hD0D0D0D0D0AEAEAED0D0AEAED0D0D0B0D0D0AEAEAEB0D0D0AEAEB0D0D0AEAEB0),
    .INIT_43(256'h8CACACACACACAEAEAC8C8C8C8CAC8C8CAEAEAEAEAEAEAEAEAEAEAEAEAED0D0D0),
    .INIT_44(256'h6A6A6A6A6A6A6A6A8A8A6A6A6A6A6A8A8A8A8A8A8A8A8A8A8C8C8C8C8C8CACAC),
    .INIT_45(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A),
    .INIT_46(256'h4848484848484848686868686868686868686868686868686A6A6A6A6A6A6A6A),
    .INIT_47(256'h4646464648464648684848686846486846464868684846464848484848484848),
    .INIT_48(256'hF2F4141414F4F4F414F4F4F4F414F4F4F4F4F414F4F4F4F4F4F4141414141616),
    .INIT_49(256'h141414141414F4F2F4F4F4F4F4141414F4F4F4F4F2F2F2F414F4F4F4F2F4F4F4),
    .INIT_4A(256'hF4F4F2F2F2F2F4F4F4F4F4F4F4F2F2F4F2F2F41414141414F414141414F4F414),
    .INIT_4B(256'hD0D0D0D0D0D0D0D0D0D0D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_4C(256'hD0D0D0D0B0AEAEAED0D0B0B0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D0D2),
    .INIT_4D(256'h8C8C8C8C8C8C8C8CAEAEAC8CAEAEAEAEAEAEAEAED0D0D0B0D0D0D0D0D0D0D0D0),
    .INIT_4E(256'h6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_4F(256'h68686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A6A6A6A6A6A),
    .INIT_50(256'h4848484868686868686868686868686868686868686868686A6A6A6A6A6A6A6A),
    .INIT_51(256'h6868686868464648484848484848484868686868684868684848484868686868),
    .INIT_52(256'h14141414F41414161414F4F4F414F4F4F414141614F4F4161414141414141416),
    .INIT_53(256'hF4F4141414141414141414141414141414141414F4F4F414F4F4F4141414F4F4),
    .INIT_54(256'hF2F2F2F2F2D2D2D2F2D2F2F2F2F2F2F4F2F2F2F4F4F4F414F2F41414F4F4F414),
    .INIT_55(256'hD0D0D0D0D0D0D2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D2D2F2F2D2D2D2),
    .INIT_56(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2),
    .INIT_57(256'h8C8C8C8C8C8C8CAED0D0AEAEAEAED0D0AEB0D0D0D0D0D0D0D0D0D2D0D0D0D0D0),
    .INIT_58(256'h6A6A6A6A6A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8CACACACACACACACAC8C8C8C8C),
    .INIT_59(256'h6A6A6A6A8A8A8A8A8A6A6A6A6A6A6A8A6A6A6A6A6A6A6A6A8A8A8A8A6A6A6A6A),
    .INIT_5A(256'h686868686868686868686868686868686868686A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_5B(256'h4848486868686868686868686868686848686868484868686868686848484848),
    .INIT_5C(256'hF41414F4F4F4F41414F4F4F4F4F4F4F41414141614141416F4F4F4F414F4F4F4),
    .INIT_5D(256'hF2F4F41414141414141414F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4141616161414),
    .INIT_5E(256'hD2D2D2D2D2D2D0D2D2D2D2D2F2D2F2F2F2F2F2F2F2F2F2F2F2F2F4F4F4F4F414),
    .INIT_5F(256'hD0D0D0D0D0D0D2F2D0D2D2F2F2D2D2D2D2D0D0D0D2F2D2D2D2D0D0D2D2D0D0D2),
    .INIT_60(256'hAEAEAEAEB0D0D0D0B0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_61(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0D0D0D0D0D0D0D0D0D0B0AEAEAE),
    .INIT_62(256'h8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAC8C8C8C8C8C8C8C),
    .INIT_63(256'h8A8A8A8A8A8A8A8A8A8A6A6A6A6A8A8A6A6A6A6A6A6A6A6A8A8A8A8A8A8A8A8A),
    .INIT_64(256'h6868686868686868686868686868686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_65(256'h6868684848484848686848484848686848486868686868484848484868686868),
    .INIT_66(256'hF2F4F4F4F4F2F2F2F4F4F4F4F4F4F414F4F4F4F4F4F4F2F4F4F4F4F4F4F4F4F2),
    .INIT_67(256'hF4F4F4F4F4F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4F2F2F2F4F4F4F4F4),
    .INIT_68(256'hF2F2F2F4F4F4F4F4F2F2F2F2F2F2F2F2F4F2F2F4F4F2F2F2F2F4F4F4F4F41414),
    .INIT_69(256'hD2D2D2D0D0D0D0D0D0D0D0D2D0D0D0D0D2D0B0B0D0D2D2D0F2D2D2D2F2D2F2F2),
    .INIT_6A(256'hD0D0B0B0D0D0D0D0D0D0D2D2D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D2D2D0D0D0),
    .INIT_6B(256'h8C8C8C8C8C8CAEAEAEAEAEAEB0D0B0AEB0B0B0D0D0D0D0AEAEAEAEAEAEAEAEAE),
    .INIT_6C(256'h8C8C8C8C8C8CACACACACAC8C8C8C8CAC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAE),
    .INIT_6D(256'h6A6A6A6A6A6A6A6A8A8A8A6A6A8A8A8A8A8A8A8A8A8A8A8A6A6A8A8A8A8C8C8C),
    .INIT_6E(256'h68686868686868686868686868686868686868686A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_6F(256'h4868684868686868486868686868484868684846484848486868684848484848),
    .INIT_70(256'hF2F2F2F4F4F2F2F2F4F4F4F4F2F2F2F2F4F4F2D2F2F2F4F2F4F4F2F2F4F4F2F2),
    .INIT_71(256'hF41414F2F2F2F4F4F2F2F2F2F2F2F4F4F2F2F4F4F2D2F2F2F2F2F2F2F2F2F4F4),
    .INIT_72(256'hF41414F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F41414F4F4F414F4F2F414141414),
    .INIT_73(256'hF2F2F2F2F2F2F2F4F2F2F2F4F2F2F2F4F2F2F414141414F4F414141614F4F414),
    .INIT_74(256'hD0D2D2D2D0D0D2D2D2D2D2D2D0D0D2D2D0D0D0D0D0D0D0D0F2D2D0D2F2D2D0D0),
    .INIT_75(256'hAEAEAEAED0D0D0D0D0D0D0D0D0D2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2F2),
    .INIT_76(256'h8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAEAEAEAEAEAE),
    .INIT_77(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A8A8A8A8C8C8C8C8C8C8C),
    .INIT_78(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A686A6A6A6A6A6A6A6A6A6A6A6A6A8A8A),
    .INIT_79(256'h48484848484848486848484848484868484848686868686868686A6A68686868),
    .INIT_7A(256'hF2F2F2F4F4F2F2F2F2F2F2F4F4F4F4F4F4F4F4F2F2F2F2F2F4F2F2F2F2F2F2F2),
    .INIT_7B(256'hF2F4F4F2F2F2F2F2F4F4F4F2F2F2F2F4F2F2F4F4F2F2F2F2F4F4F2F2F2F2F2F2),
    .INIT_7C(256'hF4F4F2D2D0D2F2F2F2F2F2F2F2F2F2F214F4141414141414F4F2F21414141414),
    .INIT_7D(256'hF2F2F2F2F2F2F2F2F4F4F2F2F2D2F2F4F2F2F2F2F2F2F4F4F4F4F4F4F2F2D2D2),
    .INIT_7E(256'hD0D0D0D0D0D0D0D2D0D0D0D0D0B0B0D0D0D0D0D0D0D0D0B0D2D2D2D2F2F2F2D2),
    .INIT_7F(256'hD0D0D0D0D0D0D0AED0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized16
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000700),
    .INITP_02(256'h0000000000000000000000000000000000000000000060000001000000000000),
    .INITP_03(256'h00000000000000000000000000000000000000000300017F8000000000000000),
    .INITP_04(256'h0000000000000000000000000001FFFC81700000000002000000000000000000),
    .INITP_05(256'h000100000000FF00000100018000400000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h00000000000000000000000000000000000000000007E6007880000000000000),
    .INITP_08(256'h0000000000000000000000000000000010810000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000600000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h8C8C8C8C8C8C8C8CACACAEAEAEAEAEAEAEAED0AEAEAED0D0D0D0D0D0B0D0D0D0),
    .INIT_01(256'h6A6A6A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_02(256'h68686868686868686A6A6A6A6A6868686A6A6A8A8A8A8A8A8A6A6A6A8A8A8A8C),
    .INIT_03(256'h68686868686868686868686868686868686868686868686868686A6A68686868),
    .INIT_04(256'hF2F2F2F4F4F2F2F2F4F4F4F4F4F4F4F4F2F4F4F4F2F2F2F2F4F4F4F2F2F2F2F2),
    .INIT_05(256'hF2F2F2F2F4F4F4F4F2F2F2F2F2F2F4F4F2F2F2F2F4F4F4F4F2F4F4F4F4F4F4F4),
    .INIT_06(256'hD2F2F2D2D2F2D2D2F2F2F2F2F2F2F2D2F2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_07(256'hD2F2F2F2D2D2D2D2F2F2D2D2D2D2F2F4F4F4F2F2D2D2D2D2F4F2F2F2F2D2D2D2),
    .INIT_08(256'hD0D0D0D0D0D0D0D2D0D0D2D2D2D2D2D2D2F2F2F2F4141414F2F2F2F2F2F2F2F2),
    .INIT_09(256'hAEB0D0AEAEAEAEAED0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0),
    .INIT_0A(256'hAEAEAEAEAEAEAEAEAEAEAEAEAECEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_0B(256'h8A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8CACAC8C8C8C8C8C8C8C8CACACAC8C8C8C),
    .INIT_0C(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A68686A6A6A6A6A6A6A6A6A6A6A6A8A8A),
    .INIT_0D(256'h6868686868686868486868686868684868686868686868686868686868686868),
    .INIT_0E(256'hD2D2D2F2F2F2F2F4F4F4F4F2F2F2F2F2D2F2F2D2D2D2F2F2F2F2F2F2F2D2F2F2),
    .INIT_0F(256'hF2F2F2F2F2F2F2F2F2F2F2F2D2F2F2F2D2D2F2F2F2F4F2F2D2F2F2F4F4F4F4F4),
    .INIT_10(256'hD2F2F2F4F4F4F2F2F2F2F2F2F2F2F2D2F2F2F2F2F2F2F2F4F2F2F2F2F2F2D2D2),
    .INIT_11(256'hF2F2F2F2F2F2F2F2F2F2F2F4F4F4F414F4F4F2F2D2D2D2D2F4F2F2F2F2F2F2F2),
    .INIT_12(256'hD2D2D2D2D2D2D2F2F2F2F2F4F4F4F4F4F41414F4F4F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_13(256'hD0D0F2D0D0D0F2F2F2F2F2F2D2F2F2F2D2D0D0D0D0D2D2D2D2F2F2F2F2F2F2D2),
    .INIT_14(256'hAEAEAEAEAEAEAEAED0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0AEAEAEB0D0D0D0),
    .INIT_15(256'h8C8C8C8C8C8C8C8C8C8C8C8C8CACACACACAC8C8CACACAEAEAEAEAEAEAEAEAEAE),
    .INIT_16(256'h68686868686868686A6A6A6A6A6A8A8A8A8A8A8A8A8A8A8A8C8A8A8A8A8A8A8A),
    .INIT_17(256'h6868686868686868686868686868686868686868686868686868686868686868),
    .INIT_18(256'hF2D2D2F2F2F2F2F2D2D2D2D2D2D2D2F2F2F2D2D2D2F2F2F2D2F2F2F2D2D2D2F2),
    .INIT_19(256'h14F4F2F2F2F2F2F2F4F4F4F2F2F2D2D2D2D2F2F2F2F2F2F2D2F2F2F2F2D2D2D2),
    .INIT_1A(256'hF2F2F2F2F2F41414F4F4F4F4F4F4F4F4F4F4F4F4F4F4F414F414141414141414),
    .INIT_1B(256'hF2F2F2F2F2F2F2F4F2F2F2F2F2F2F2F2D2D2D2D2D2D2F2F2F4F2D2F2F2F2F2F2),
    .INIT_1C(256'hD2F2F2F2F2F2F2F2F2F2F2F2F2F2D2D2D2F2F2F2D2D2D2D0D2D2F2F2D2D2F2F2),
    .INIT_1D(256'hD0F2F2D0D0D0D0D0D0D2D2D0D0D0D0D0D0D0D0D0D2D2F2F2D0D0D0D2D2D2D2D2),
    .INIT_1E(256'hAEAEAEAEAEAEAEAED0AEAEAEAEAEAEAEAEAEB0B0AEAEAEAED0D0D0D0D0D2D2D0),
    .INIT_1F(256'h8C8C8C8CACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_20(256'h686868686A6A6A6A6A6A6A6A6A8A8A8A8A8C8C8C8C8C8C8C8A8A8A8C8C8C8C8C),
    .INIT_21(256'h686868686868686868686868686868686868686868686868686868686868686A),
    .INIT_22(256'hF4F4F4F4F4F2F2F2F2F2F2F2F2F2F2F2F4F4F2F2F4F414F4F4F4F4F4F4F4F4F4),
    .INIT_23(256'h14F4F2F2F2F2F414F4141414F4F4F2F2F2F4F4F4F4F4F4F4F2F2F4F4F4F2F2F2),
    .INIT_24(256'hF2F2F2D2D2F2F2F2F2F2F2F4F4F4F4141414141414141414161614141414F4F4),
    .INIT_25(256'hB0B0D0D0D0D0D2F2D0D0D0D0D0D2F2F2F2F2F2F2F2F2D2D2F2D2D0D0D0D0D2D2),
    .INIT_26(256'hD0D0D2D2D2D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D2F2F4F2D2F2F2D2D0D0D0D0),
    .INIT_27(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0B0AEB0D0D0D0D0B0B0B0B0B0D0D0D0),
    .INIT_28(256'hAEAEAEAEAEAEAEAEAEAEAEAEAED0D0D0AEAEAEB0AEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_29(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_2A(256'h8A8A8A8A8A8A8A8A8C8C8A8A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8C8CACACAEAE),
    .INIT_2B(256'h68686868686868686868686868686A6A68686A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_2C(256'h14F4F4F4F4F2F2F4F4F4F4F4F4F4F4F4F414F4F4F4F4F4F4F4F4F4F4F4F4F4F4),
    .INIT_2D(256'hD0D0D0D2D2D2F2F2F2F2F2F2F2F4F414F2F2F2F4F4F4F4F4F2F2F4F4F4F4F414),
    .INIT_2E(256'hD2D2F2F2F2F2D2D0F2D2D2F2F2F2F4F41414141414141414F4F4F2D2D2D2D0D0),
    .INIT_2F(256'hB0D0D0D0D0D0D0D0F2F2D2F2F2F2F414F2F2F2F2F2F2F2D2D2D2D0D0D0D0D0D2),
    .INIT_30(256'hD0D0D0D0D0D0D0D2D0D0D0D0D2F2F2F2F2F2F2D2D2D2D2D2F2F2F2F2D2D0D0D0),
    .INIT_31(256'hB0D0D0B0D0D0D0D0D0D0D2D2D0D2D2D2D0D0D0D0D2D2D0D0D2D2D2D2D2D2F2F2),
    .INIT_32(256'hD0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D2D0D0D0D0D0D0D0D0B0B0B0B0AEAEAEAE),
    .INIT_33(256'hCECEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_34(256'h6A8A8A8A8A8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAEAEAEAECECECECE),
    .INIT_35(256'h68686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A8A8A8A8A),
    .INIT_36(256'hF2F2F2F2F2F2F2F2D2D2D2F2F2F2F2F2D2F2F2F2D2D2D2D2D0D0D2D2D2D2D2D0),
    .INIT_37(256'hD2D2F2F2F2D0D2F2D2D2D2D2D2D2F2F2D2D0D0D2F2F2F2D2D2D2D2D2D2D2D2D2),
    .INIT_38(256'hF2F2D2D0D2D2F2F2F2F2F2F2F2F2F2F4D2D2D2F2F2D2D0D2D0D0B0AED0D0D0D0),
    .INIT_39(256'hF414141414F4F2F214F4F2F2F2D2D0D0D2D2D2D2F2F2F2F2F2F2F2F2D2D2F2F2),
    .INIT_3A(256'hF2F2F2D2D2F2F2F2F4F4F4F4F4141414141414F4F41414F4F2F2F2F2F2F2F2F2),
    .INIT_3B(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D2F2F2F2F2F2F2F2F2),
    .INIT_3C(256'hD0D0D0D0D0D0D0D0D0F2F2F2F2D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D2F2),
    .INIT_3D(256'h8EAEAEAEAEAEAEAEAEAEAEAEB0B0B0B0AEAEAEB0B0AEAEAED0D0D0D0D0D0D0D0),
    .INIT_3E(256'h8C8A8C8C8C8C8C8C8C8C8C8C8C8A8A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_3F(256'h6A6A6A8A8A8A8A8A6A8A8A8A8C8A8A8A8A8A8A8A8A8A8A8A6A8A8A8C8A8A8C8C),
    .INIT_40(256'hD0D2F2D0D0D2F2D2D2D0D0D0D0D0D0D0D0D0D0D0D0D0D2F2D2D2D2D2F2F2D2D2),
    .INIT_41(256'hD0B0B0D0D2D2D0D0D0D0D0B0B0AEB0B0D2D2D2D2D2D2D2F2D0D0D0D0D0B0D0D0),
    .INIT_42(256'hF2F2F2F2F2F4F4F4F4F4F2F2F2F2F2F2D2D2D2D0D0D2D2D0D2D0D0D0D0D0D0B0),
    .INIT_43(256'hF2F2F414F4F4F2F214F4F2F4F4F2F414F4F4F2F2D2D2D2F2F2F2F2D2D2D2D2D2),
    .INIT_44(256'hF2F2F2F2F2F2F2F2F2D2D2F2F2F2F2F4F2F2F2F2F2F4F4F4D0F2F2F2F2F2F4F4),
    .INIT_45(256'hD2D2D2F2D2D0D0D2D2D2D2D2D2D2D0D0D0D0F2F2F2D2D2D2D2D0D0D0D2D2F2F2),
    .INIT_46(256'hAEB0D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D0D2D2D0D2D0D0D2D2D0D0D0),
    .INIT_47(256'hAEAEAEAEAEAEAEB0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_48(256'hAEAEAEAEAC8C8C8C8C8C8C8C8C8C8C8CAE8C8C8EAE8E8C8CAEAE8E8C8C8EAEAE),
    .INIT_49(256'h6A6A6A8A8A8C8C8C8A8C8C8C8C8A8C8C8C8C8C6A6A8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_4A(256'hD0D2D2D0D0D0D0D0D0D0D0D0D0D2D2D2F2F2F2F2D2D2D2D2F2F2F2F2F2F2D2D0),
    .INIT_4B(256'hF4F4F2F2F2F2D2D0D2D2D2D2D0D0D2D2D0D0D2D2D0D0B0AED0D0D2D2D0D0D0D0),
    .INIT_4C(256'hF4F4F4F4F41414F4F4F4F4F2F2D2D2D2D2D2D2D2D2D2D2D2F2F2F2F2F2F2F2F2),
    .INIT_4D(256'hD2F2F2F2F2F2F2F2F2F2F2F2D2D2D2D2F2F4F2F2F2F2F2F4F2F2F2F2F2F2F2F4),
    .INIT_4E(256'hF2F2F2F2F2F2D2D2F2D2D2F2F2F2F2F2F2F2F2F2D2D2D2F2D2F2F2F2D2F2F2F2),
    .INIT_4F(256'hD2D2D2F2F2D2F2F2F2F2F2F2F2F2F2F2D2D2F2F2F2F2F2F2F2F2D2F2F2F2F2F2),
    .INIT_50(256'hAEB0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D0D0D2D2D2F2D2D2F2F2D2D2F2),
    .INIT_51(256'hB0B0B0B0B0B0B0D0B0B0B0B0B0B0B0B0AEAEB0B0AEAEAEAEB0AEAEAEAEAEAEAE),
    .INIT_52(256'h8C8C8C8C8C8C8E8EAEAEAEAEAEAEAEAED0AEAEB0D0B0AEAEAEAEAEAEAEB0B0D0),
    .INIT_53(256'h6A6A8A8A8C8C8C8C8C8C8C8C8A8A8C8C8C8C8C8C8CACAE8C8C8C8C8C8C8C8C8C),
    .INIT_54(256'hD2F2F2D2D0D2D2D0D0D0D0D2D2F2D2D2D2F2F2F2F2F2F2F2F2F2D2F2F2D2D2D0),
    .INIT_55(256'hF4F4F4F4F4F4F2F2F4F4F2F2F2F2F2F2D0D2D2F2F2D2D2D0D0F2F2F2F2F2D2D2),
    .INIT_56(256'hF2F2F2F2F2F2F2F2F4F2F2F2F2F2D2D2F2F4F4F4F2F2F2F2F2F2F2F2F4F4F4F4),
    .INIT_57(256'hD2F2F2D2D2D2F2F2F2F2F2D2D0D0D0D0D0D2D2D0D0D0D2F2F2F2F2F2F2D2F2F2),
    .INIT_58(256'hD2D2D2D2D2F2F2F2F2D2D2F2F2F2F2F2F4F4F2F2F2F2F2F2D2F2F2F2D2F2F2F2),
    .INIT_59(256'hF2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2D2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_5A(256'hD0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D2D2D2),
    .INIT_5B(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0B0D0D0B0B0B0B0B0B0B0AED0D0D0D0D0B0B0B0),
    .INIT_5C(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEB0B0B0D0B0B0D0D0D0B0D0D0D0D0D0D0D0D0D0),
    .INIT_5D(256'h8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8CAEAC8CAEAEAEAEAEAEAEAE),
    .INIT_5E(256'hF2F2F2F2F2F2F2F2F4F4F4F2F2F2F2F2D2D2F2F2F2F2F2F2D0D0D0D0D0D0D0D0),
    .INIT_5F(256'hF2D2D2F2F2F2F2F2F4F2F2F2F2F2F2F2F2F2F2F2F2F2F4F4D0F2F2F2F2F2F2F2),
    .INIT_60(256'hD0D0D2D2D2D2D2D0F2F2F2F2F2F2F2F2D2F2F4F2D2D2D2D0D2F2F2F2F2F2F2F2),
    .INIT_61(256'hD2F2F2D2D0D0F2F2F2F4F2F2D2F2F2D0D0D2F2D2D2D0D2F2F2F2F2F2D2D2D2F2),
    .INIT_62(256'hD0D0D2D2D2D2F2F2F2F2D2F2F2F2F2D2F2F2D2D2D2D2F2F2F2F2F2F2D2F2F2F2),
    .INIT_63(256'hF2F2D2D2F2F2D2D2F2D2D2D2D2F2F2F2F2D2D2D2D2D2D2D2F2F2F2F2F2F2F2D2),
    .INIT_64(256'hD2D2D2D2D0D0D0D0D2F2F2F2F2F2F2F2F2F2F2D2D2D2D2D2F2D2D2F2F2F2F2F2),
    .INIT_65(256'hD0D0D0D0D0B0B0B0B0B0B0B0B0B0B0B0D0B0AEAEB0B0B0B0D0D0D0D0D0D0D0D0),
    .INIT_66(256'hAEAEAEAEB0D0D0D0AEAEAEAEB0B0B0B0D0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_67(256'h8C8C8C8C8C8C8C8C8C8C8C8C8C8CAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_68(256'hD2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D0D0D0D0D0D0D0D0D0D0D0D0D0B0),
    .INIT_69(256'hF2F2D2D2D2D2D2D2D0D0D0D0D2F2F2F2F2F2D2D0D0D0D2D2D2D2D2D2F2F2F2F2),
    .INIT_6A(256'hD0D2D2D2F2F2F2D2F2F2F2D2D2D2D2D2D0D2F2D2D0D0D0D0D2D2F2F2D2D2D2F2),
    .INIT_6B(256'hD0D2F2D0D0D0D0D0D2F2F2D2D2F2F2D2D2D2D2F2F2F2D2D2D0D2F2F2D2D2F2F4),
    .INIT_6C(256'hF2F2F2F2D2D2D2D2F2F2F2F2F2F4F2F2F2F2D2D2D2F2F2D2F2F2F2F2D2D2D2D0),
    .INIT_6D(256'hF2F2D2D2D2F2F2D2F2F2F2F2F2F2F2F2F2F4F4F4F2F2F2F2F2F2D2D2D2D2D2D2),
    .INIT_6E(256'hD0D0D2F2F2F2F2F2F2F2F2F2F2D2D2D2D2F2F2F2F2F2F2D2F2F2F2F2F2F2F2F2),
    .INIT_6F(256'hB0D0D0D0B0B0B0B0D0D0D0D0D0B0D0D0D0D0B0B0D0D0D0D0D0D0D0D2D2D2D2D2),
    .INIT_70(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D0D0D0D0D0D0D0),
    .INIT_71(256'h8C8C8C8C8C8C8C8CACACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_72(256'hD2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0AEAEAEAEAEAED0D0D0B0B0AEAEAE),
    .INIT_73(256'hD2D2D2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D0D0D0D0D0D0D0),
    .INIT_74(256'hD0D2D2D2D2F2F2D2D2D2D2D2D2D0D0D0D0D2D2D2D0D2D2D2D0D0D0D0D0D0D2D2),
    .INIT_75(256'hD0D2F2D0D0D2D0D0D0D2D2D0D0D2D2D2D0D0D0D2F2F2D2D0D0D2D2D2D2D0D2D2),
    .INIT_76(256'hF2F2F2F2D2D2D2F2F2F2F2F2F2F2F2F2F4F2F2F2F2F2F2F2D0D2D2D0D0D2D2D2),
    .INIT_77(256'hF2F2F2D2F2F2F2F2F2F2F2F2F2F2F2F2D2F2F4F4F2F2D2D2F2F2D2D2D2D2D2D2),
    .INIT_78(256'hF2F2F2F2F2F2F2D2F2D2D2D2D2D2D2D0D0D2D2D2D2D2D2D2F2F2F2F2F2D2D2F2),
    .INIT_79(256'hD0D0D0D0D0D0D0D2D2D2D2D2D2D2D2F2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2D2),
    .INIT_7A(256'hAEAEAEB0B0B0B0B0D0D0D0D0D0D0D0D0D0D0D0D2D0D0D0D2D2D0D0D0D0D0D0D0),
    .INIT_7B(256'hACACACAEAEAEAEAEAEACAEAEAEAEAEAEAEAEAE8EAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_7C(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0AEAEAEAEAEAEB0AED0D0B0B0AEAEAEAE),
    .INIT_7D(256'hB0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D0D0D0D0D0D0D0),
    .INIT_7E(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0B0B0AEB0D0D0D0D0D0B0D0D0D0D0B0),
    .INIT_7F(256'hD2F2F2D2D2F2F2D0D2D2D2F2F2D2D2F2F2F2F2F2F4F2F2F2D2D2D2D2D0D0D0D0),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized17
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000800000000000000000000000),
    .INITP_01(256'h00000000000000000000001FC000000000000000000000000000000000000000),
    .INITP_02(256'h000000C100009800000000000000000000000000000000000000000000000000),
    .INITP_03(256'h2000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000007000000000),
    .INITP_06(256'h000000000000000CF8FF00000000000000000000000000000000000000000000),
    .INITP_07(256'h00001C0000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h00000000000000000000000000000000000000000000000000030E0F8E000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hD0D2F2F2D2D2F2F2F2F2F2F2D2F2F2F2F2D2D0D2D2D2D0D0D0D0D0D0D2F2F2D2),
    .INIT_01(256'hD2F2F2F2F2F2F2F2F2F2D2D2D2D2D2D2D0D0D2F2D2D2D0D0D2D2D2D2D2D0D0D0),
    .INIT_02(256'h14F4F4F2F2F2F2D2F2F2F2F2F4F4F4F4F2F2F2F2F2F2F2F2F4F2F2F4F4F2F2F2),
    .INIT_03(256'hD2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4F2F2F2F2F2F2F2F2F2F2F2F2F4F2F2F2),
    .INIT_04(256'hAEAEAEAEB0D0D0D0D0D0D0D0D0D0D0D0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_05(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAED0D0AED0D0D0D0D0D0B0AEAEAEAEB0),
    .INIT_06(256'hD0D0D0D0D0D0D0D0B0D0D0D0B0AEAEAED0D0D0D0D0D0D0AEB0AEAEB0D0B0AEAE),
    .INIT_07(256'hB0B0D0D0D0B0B0B0AEB0B0D0D0D0D2D2D2D2D0D0D0D2F2F2D0D0D0D0D0D0D0D0),
    .INIT_08(256'hD0D2D0D0D0D0D0D0D2D0D0D0B0B0AEAED0D0B0AEAED0D0D0D0D0B0B0B0B0AEAE),
    .INIT_09(256'hD2F2D2D0D2F4F2D2F2F2F2F2F2D2D2F2F2F2F2F2D2D2D2F2D2D2D2D2D2D2D2D2),
    .INIT_0A(256'hD2F2F2F2D2D0D2D2F2F2F2F2D0D2D2D2D2D2D2F2F2F2F2D2D2F2F2D2D2D2D0D0),
    .INIT_0B(256'hD2F2F2F2D2D2D2D2F2F2F2D2D2D2D0D0D0D0D0D2F2F2D2D2D0D0D0D2D2D0D0B0),
    .INIT_0C(256'h1414F4F4F4F4F4F4F2F2F4F2F2D2F2F2F2F2F2D2D2F2F2F2F2D2D2F2F2D2D2F2),
    .INIT_0D(256'hF2F2F2F2F2F2F2F2D2D2F2F2F2F2F2F4F2F2F4F4F2F2F2F2F4F4F41414141414),
    .INIT_0E(256'hAEAEB0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D0D0D0D0D0D2D2D2),
    .INIT_0F(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAED0B0B0AEAEB0D0D0),
    .INIT_10(256'hD0D0D0D0D0D0AEAEAEAEAEAEAEAEAEAEB0AE8C8CAEAEAE8CAEAEAEAEAEAEAEAE),
    .INIT_11(256'hB0D0D0D0D0D0D0D0F2D2D2D2D2D2D0D0F2F2D2F2F2F2D0D0D0D0F2F2D0D0D0D0),
    .INIT_12(256'hD0D0D0D0D0B0B0AEAEB0D0D0D0B0D0D2D0B0B0D0D0B0D0D2B0D0D0D2D2D0D0B0),
    .INIT_13(256'hF2F2F2F2F2F2F2F2F4F2F2F2F2F2F2F2D2D2D2D2F2F2D2D2D0D0B0B0D0D0D0D0),
    .INIT_14(256'hF2D2D0D0D0D0D0B0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D2D2D2D2F2F2F2F2D2),
    .INIT_15(256'hD2D2F2D2D0D0D0D0D0D2D2D2D2D2D0D0D0D2D2D2D2D0D0D0D0D0F2F2F2F2D2D2),
    .INIT_16(256'hF4F4F4F4F4F2F2F2F4F2F2F2F4F4F4F214F4F41414F4F4F4F2F4F4F4F2F2F2F2),
    .INIT_17(256'hD0D2F2F2F2D2D2D2F2F2D2D2D2D2D2D2F2F2F2F2F2F2F2F21414F4F4F2F2F414),
    .INIT_18(256'hD0D0D0D0D0D0D0B0D0D0D0D0D2D0D0D0D0B0D0F2F2D0B0D0D0D0D0D2D2D0D0D0),
    .INIT_19(256'hAEAEAEAEAEAE8C8CAEAEAEAEAEAEAEAEB0D0D0D0D0D0D0D0B0B0D0D0D0D0D0D0),
    .INIT_1A(256'hD0AEAEAEAEAEAED0D0D0D0D0D0D0B0AEAEAEAEAEAED0D0AED0D0D0D0AEAEAEAE),
    .INIT_1B(256'hD0D0D0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0D0D0D0D0AEAED0D0D0B0AEAEAE),
    .INIT_1C(256'hB0B0D0D0D0D0B0B0AEAED0D0B0B0D0D0AEAEAEAEAEAEB0D0D0D0B0D0D0D0D0D2),
    .INIT_1D(256'hD2D2D0D0D0D0D2D2D2D2D0D0D0D0D0D0D2D2D2D0D0D0D2D2B0B0B0B0D0D0D0D0),
    .INIT_1E(256'hF2F2F2F2F2D2D2D2D2F2F2D2D2F2F2F2F2F2F2F2F2F2F2F2D2D2D2D0D0D0D0D0),
    .INIT_1F(256'hF4F414F4F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D2D0D0D0D0D0D0D2D0D0B0B0),
    .INIT_20(256'hF4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F2F4F4F4F4F4F4F4F2F2F2),
    .INIT_21(256'hD0D0D0D0D0D2D2D2F2F2F2F2F2F2F2F2F2F2F4F2F2F2F2F2F2F2F2F2F2F2F2F4),
    .INIT_22(256'hD0D0D0D0D0D2D0D0D0D2D0D0F2D2D0D0D0D0D0D2D2D0D0D0D0D0D2D2F2F2F2F2),
    .INIT_23(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAE8C8CAEAEAEAEB0B0D0D0D0D0D0D2D2D2D0D0),
    .INIT_24(256'hAEAEAEAEAEAEAEAEAEAEAE8C8C8C8C8CAEAEAEAEAEAEAEAED0D0AED0D0D0AEAE),
    .INIT_25(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0AEAEAEAEAEAEAEAE),
    .INIT_26(256'hB0B0B0D0D0D0D0D0AEAEB0B0B0B0B0D0B0B0B0D0D0B0D0D0F2D0D0B0B0B0D0D0),
    .INIT_27(256'hD2D2D2D2D2D2D2D2D2D0D0D0D0D0D0D0D0D0D0B0B0B0D0D0D0D0D0D0D0D0B0AE),
    .INIT_28(256'hD0F2F2F2F2F2F2F2D0D2D2D0D0D0D0D0D2D2D2D2D0D0D0D0F2F2D2D2D2D2D2D2),
    .INIT_29(256'hF2F2F2F2F2D2F2F2F4F4F4F4F4F4F4F4D2D2F2F2F2F2F2F2F4141414F4F2F2F2),
    .INIT_2A(256'hF2D2D2D2D2F2D2D2D2D2F2F2F2D2D2D2F2F2F2F2D2D2F2F4F2F2F2D2F2F2F2D2),
    .INIT_2B(256'hF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2),
    .INIT_2C(256'hD0D0D0D0D2D0D0D0D2F2D2D2F2F2D2F2F2F2F2D2D2F2F2F2D2D0D2F2F2F2F2F2),
    .INIT_2D(256'hAEAEAEAEAEAEAEAEAEAEB0AEB0D0D0AEAEAEAEAEAEB0B0D0AEAEAEB0B0B0AEAE),
    .INIT_2E(256'hAEAED0D0D0AEAE8CAEAEACACACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAECEAEAE),
    .INIT_2F(256'hD0D0D0AEAEB0D0B0D0B0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_30(256'hB0B0B0B0B0B0B0B0B0AEB0B0B0D0B0B0D0D0D0D0D0D0D0D0D0D0D0D0B0AEAEAE),
    .INIT_31(256'hD0D0D2D0D0D0D0D2D2D2D2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0AEAE),
    .INIT_32(256'hD0D0D2D2D2D2D2D2F2F2F2F2F2F2D2D2F2F2F2D2D2D2D0D0D0D0D0D0D0D0D2D2),
    .INIT_33(256'hF2F2F2F2F2F2F2F2D2D2F2F2F2F2F2F4F2F2F2F2F2D2D2D2D0D0D0D0D0D0D0D0),
    .INIT_34(256'hF4F2F2F2F2F2F2D2D2F2F2F2F2F2F2F2D2D2D2D2D0D0D0D2D2D2D2D0D0D0D0D0),
    .INIT_35(256'h1414141414F4F2F21414141414141414F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D2),
    .INIT_36(256'hD0D0D0D0D2D0D0D0D0D2D0D0F2F2D2F2F2F2F2F2F2F2F2F2F4F2F2F41414F4F4),
    .INIT_37(256'hAEAEB0B0B0AEAEAEAEAEAEAEB0D0D0D0B0D0D0D0B0AEB0B0AEAEB0B0B0B0B0B0),
    .INIT_38(256'hAEAE8C8C8C8C8C8CACACAEAEAEAEAEAE8C8CAEAEAEAEAEAE8C8C8C8C8CAC8C8C),
    .INIT_39(256'hD0D0B0AEAEB0B0AEAEAEAEAEAEAE8E8EAEAEAEAC8C8CACAE8C8C8C8C8C8C8CAC),
    .INIT_3A(256'hAEAEAEAEAEAEAEAEB0AEAEAEB0B0B0B0AEAEAEAEAEAEAEAEB0D0D0D2D2D0D0D0),
    .INIT_3B(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0B0B0B0D0D0D0D0D0D0D0B0B0AEAEAEAEB0B0AE),
    .INIT_3C(256'hD0D0D0D0D2D2D0D0D0D0D0D0D0D0D2D2F2F2F2F2F2F2F2F2D2D2D2D0D0D0D0D0),
    .INIT_3D(256'hB0D0D0D0D0D0D0D0D0D0D0D0D0D0D2D2D0D2D2D2D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_3E(256'hF2D2D2D2D2D2D0D0D0D0D0D0D0D0D0D0F2D2D0D0D0D0D0D2D0D2D2D2D0D0D0D0),
    .INIT_3F(256'hD2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F4F4F4141414F4F4F2F2F2F2F2F2F2F2),
    .INIT_40(256'hD0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0D2D2D0D2F2F2D2F2D2D2D2F2F2F2F2),
    .INIT_41(256'hAEB0D0D0D0D0D0D0B0D0D0B0B0D0D0B0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_42(256'h8C8C8A6A6A6A6A8A8A8C8C8C8C8C8A8A8A8C8C8C8C8C8C8C8A8A8A8A8C8C8C8C),
    .INIT_43(256'hAEAEAEAEAEAEAEAEAE8E8EAEAEAEAEAE8C8C8C8C8CACACAE8C8C8C8C8C8C8C8C),
    .INIT_44(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8C8C8EAEAE8E8E8E),
    .INIT_45(256'hD0D0D0D0D0D0D0D0D0D0D0B0B0B0AEAEB0AEAEAEB0AEAEAEAEAEAEAEAEAEAEAE),
    .INIT_46(256'hF2D0D0D0D2D2D2D0F2F2F2F2F2D2D2F2D0D0D0D0D0D0D0D0D0D2D2D0D0D0D0D0),
    .INIT_47(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D0D2D2F2D0D0D0D0D0D0D0D0),
    .INIT_48(256'h14F4F4F4141414F4F4F2D2F2F2F2F2F2F2D2D2D2F2F2F2F2D0D2F2D2D0D0D0D0),
    .INIT_49(256'hD0D0D0D0D0D0F2F2F4F2F2F2F2F21414F2F2F2F2141414F4F2F2F2F414141414),
    .INIT_4A(256'hD2D0D0D0D0D0D0D0D0D0D0D0D0D0B0D0B0D0D0D0D0D2D0D0D0D0B0D0D0D0D0D0),
    .INIT_4B(256'hB0B0AEAEAEAEB0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_4C(256'h8C8C8CACACAC8C8C8C8C8C8A8A8A6A6A8A8A8A8A8A6A6A6A6A6A8A8A8A8A8C8C),
    .INIT_4D(256'hAEAEAEAEAEAEAE8C8C8C8C8C8C8C8C8C8C8C8A8A8C8C8C8A8C8CAC8C8C8C8A8C),
    .INIT_4E(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEB0D0B0AEAEB0AEAED0D0B0B0AEAEAEAE),
    .INIT_4F(256'hAEAEAEAEAEAEAEAED0D0D0D0D0B0AEAED0D0D0D0D0D0B0B0B0AEAEAEAEAEAEAE),
    .INIT_50(256'hD0D0D0D0D0D0D0D0D0D0D0D0B0AEAEAED0D0D0D0D0D0D0D0AEAEAEAEAEAEAEB0),
    .INIT_51(256'hD0D0D0D0D0D0B0B0F2D2D0D0D0D0D0D2D0D0D0D0D0D0B0B0D0D2D2D2D0D0D0D0),
    .INIT_52(256'hD2F2F2F2F2F2F2F2D2D2D0D2D2D2D2D0D2D2D2F2F2D2D2D2D0D0D2D2D0D0D0D0),
    .INIT_53(256'hD2D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0F2F2D0D0F2F2F2F2F2F2F2F4),
    .INIT_54(256'hD0D0D0D0D0D0D0D0D0D2D0D0D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D0D0D0D0D0),
    .INIT_55(256'hD0B0AEAEAEAEB0D0AEB0B0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_56(256'h6A6A6A8A8C8C8A8A8C8A8A8A6A6A8A8A6A6A6A6A8A8C8C8C8C8C8C8C8A6A8A8A),
    .INIT_57(256'h8EAEAEAEAEAEAE8C8C8C8CAEAEAE8C8CAEAC8C8C8C8C8C6A6A8A8C8C8A6A6A6A),
    .INIT_58(256'hAEAEAEAEAE8E8E8EAEAEAEAEAEAEAEAEB0B0AEAEAEAEAE8EAEAEAEAEAEAEAEAE),
    .INIT_59(256'hAEAEAEAEB0B0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8EAEAEAEAEAE),
    .INIT_5A(256'hAEAEAEAEAEAEAEB0AEAEAEAEAEAEAED0AEAEAEAEAEAEAEAEAEAED0AEAEAEAEAE),
    .INIT_5B(256'hD2D2D2D2D2D2D0D0D0D0D0D0D0D0D0D0B0B0D0D0D0D0D0D0AEAEB0B0AEAEAEAE),
    .INIT_5C(256'hD0D0D2D0D0D0D0D2D0D2D2F2D2D2D2D2D0D0D2D2D0D0B0D0D2D2D0D0D2D2D2D2),
    .INIT_5D(256'hAEAED0D0D0AEAEAED0D0D0B0AEAEAEAED0D0D0D0D0D0AEAED0D0D0D0AEAED0D0),
    .INIT_5E(256'hD0D0D0D0D0D2D0D0D0D0D0D0D0D0D0D0D0D0B0B0B0AEAEAEAEAEAEAE8E8EAEAE),
    .INIT_5F(256'hD0D0D0D0D0D0D0D0D0D0D0B0AEB0B0AEAEAEAEAEAEB0D0D0D0D0D0D0D0D0D0D0),
    .INIT_60(256'h8C8C8A6A6A8A8C8C8C8C8C8C8C8C8A8A6A6A6A6A6A6A6A6A8A8A8A8A8A8A6A6A),
    .INIT_61(256'h8C8C8C8C8C8C8C8C8C8C8C8CACACAEAC8C8CACAC8C8CACAE8C8C8C8CAC8C8C8C),
    .INIT_62(256'hACAC8C8C8CACAEAEAEAEAEAEAEAEACAE8C8CAEAEAC8C8C8CAC8C8C8C8C8C8C8C),
    .INIT_63(256'hAEAEAEAEAEAEAEAEAEAE8C8C8C8CAEAE8C8CAE8E8C8C8EAEAE8E8C8C8E8C8C8C),
    .INIT_64(256'hAEAEAEAEAE8CAEAE8E8C8C8C8EAEAEAE8C8CAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_65(256'hB0D0D0D0D0D0D0D0D0D0D0D0B0B0D0D0AED0D0B0AEAEAEAEAEAEAEAEAE8EAEAE),
    .INIT_66(256'hAEAEAED0D0B0AEB0B0B0B0D0B0B0B0D0D0D0D0D0D0D0D0B0D0D0D0B0B0D0D0D0),
    .INIT_67(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEB0B0D0D0B0AEAEAEAEAEB0D0D0D0D0B0AEAEB0),
    .INIT_68(256'hD0D0D0D0B0B0B0B0D0D0D0B0B0D0D0D0D0D0D0D0D0B0AEB0B0B0B0B0AEAEAEAE),
    .INIT_69(256'hD0D0D0D0D0D0B0B0D0D0D0D0D0D0D0D0D0D0D0D0D0B0B0D0B0D0B0AEAEB0D0D0),
    .INIT_6A(256'h8C8C8C8A6A8A8C8C8C8C8C8C8C8C8C8A6A6A6A6A6A6A6A6A6A6A68686868686A),
    .INIT_6B(256'h6A6A6A8A8A8A8C8C8C8C8A8A8A8A8A8A8C8C8C8C8C8C8CACACACACACAC8C8C8C),
    .INIT_6C(256'h8C8C8C8C8C8C8C8C8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A8A6A6A6A6A8A),
    .INIT_6D(256'h8CACAE8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_6E(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEACACAEAEAEAE8C8CAE8C8C8CAC8C8C8C),
    .INIT_6F(256'hAED0D0AEAEAEAEAEB0B0D0D0D0D0B0AEAEB0B0AEAEAEAEAED0AEAEAEAE8EAEAE),
    .INIT_70(256'hD0B0B0D0D0B0AEAEAEB0D0D0D0B0B0D0B0AEAEAED0D0B0AEAEB0B0B0AEAEAEAE),
    .INIT_71(256'hB0B0B0B0D0D0B0B0AEAEAEAEAEB0B0D0D0AEAEB0B0AEAEAEAEAEB0AEAEAEAEAE),
    .INIT_72(256'hD0D0D0D0D0B0B0B0B0AEAEAEAEAEB0D0D0D0D0D0D0B0B0D0AEAEB0B0AEAEB0D0),
    .INIT_73(256'hD0D0D0D0D0D0D0D0D2D2D2D2D2D2D2D2D2D0D0D2D0D0D0D0D0D0D0B0B0D0D0D0),
    .INIT_74(256'h6A6A6A6A6A6A8A8A8C8C8C8C8A6A6A6A8A8A8A8A8A8A6A6A8A6A6A6A6A8A8A8A),
    .INIT_75(256'h8A8C8C8C8C8C8C8C8A8A6A6A8A8A8A8A8A8A8C8C8A8A8A8C8C8C8C8A8A8A6A6A),
    .INIT_76(256'h8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A8A8A8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_77(256'h8A8C8C8C8C8C8C6A8C8C8C8C8C8C8C8C8C8C8C8C8C8C6A6A8C8C8C8C8C8C8C8C),
    .INIT_78(256'hAEAEAEAEAE8C8C8C8C8C8C8C8C8C6A6A6A6A6A6A8C8C8C6A8C6A6A8C8C8C8A8C),
    .INIT_79(256'hAEAEAEAEAEAEAEAEAE8EAEAEAEAEAE8CAEAE8C8C8CAEAE8CAE8EAEAEAEAEAEAE),
    .INIT_7A(256'hAEAEAEAEAE8E8C8CAEAEAEB0AEAEAEAEAEAEAEAEAEB0AEAEAEAEB0B0B0AEB0D0),
    .INIT_7B(256'hAEAEAEAEAEAEAEAED0B0B0AEAEB0D0D0B0AEAED0D0B0AEAEB0B0B0AEAEAEAEB0),
    .INIT_7C(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0AEAEAEAEAEAEAEAEB0B0AEAEAEAEAEB0),
    .INIT_7D(256'hD2D2D2F2F2F2F2F2D2D2D2D2D2D2D2D2D2D0D0D2D2D0D0D2D0D0D0D0D0D0D0D0),
    .INIT_7E(256'h68686868686A6A6A6A6A6A6A68684848686868686868686868686A6A6A6A6A6A),
    .INIT_7F(256'h8A8A8A8A8A6A6A6A6A6A686A6A6A6A6A6A8A8A8A6A6A6A8A6A6A6A6A6A686868),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized18
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h8C8C8C8C8C8C8C8C8A8A8C8C8C8C8C8C8C8C8C8A8A8A8C8C8C8C8C8A8A8A8A8A),
    .INIT_01(256'h6A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_02(256'h8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C6A6A6A6A6A6A6A8C8C6A6A6A8A8C6A6A6A),
    .INIT_03(256'hAEAEAEAEAEAEAE8EAEAEAEAEAEAEAEAE8C8C8C6C6C8C8C8C6C6A8C8C6C8C8C8C),
    .INIT_04(256'hAEAEAEAEAE8E8E8EAEAEAEAE8E8C8C8C8E8C8C8C8C8E8E8EAEAE8E8C8CAEAEB0),
    .INIT_05(256'hAEAEAEAEAEAEAEAEB0B0AEAEAEB0B0B0B0AEB0D0D0B0AEAEB0B0AEAEAEAEAEAE),
    .INIT_06(256'hF2F2F2F2F2D2D2D2D0D0D0D0D0D0D0AED0D0B0B0AEAEAEAEB0AEAEAEAEAEAEAE),
    .INIT_07(256'hD0D0D0D0D0D0D0D0D2D2D2D2D2D2D2D2D2D0D0D2D2D0D0F2F2F2F2F2D2F2D2D0),
    .INIT_08(256'h8A8A6A6A6A6A6A6A6A6A6A686868484848484848484868684868686868686868),
    .INIT_09(256'h6A6A6A6A6A8A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A),
    .INIT_0A(256'h8A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A6A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_0B(256'h6A8A8A6A8A8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A8A8C8A6A6A6A),
    .INIT_0C(256'h8C8C8C8C8A6A8C8C8C8C8C8C8C8C8C8C8CACAC8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_0D(256'hAE8C8CAEAEAE8C8C8C8C8C8C8C8C8EAE8CAE8C8C8C8C8C8C8C8C8C8C6A6A8C8C),
    .INIT_0E(256'hAEAEAEAEAEAE8E8CAEAEAEAE8E8C8E8EAEAEAE8E8EAEAEAEAEAEAE8C8C8CAEAE),
    .INIT_0F(256'hAEB0B0B0B0B0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8E8E8EAE),
    .INIT_10(256'hF2D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0D0D0D0D0D0AEAEAEAEAEAEAEAEAEAEB0),
    .INIT_11(256'hD0D0D0D0D0D0D0D0D2D2D2D2D2D2D2D2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2D0),
    .INIT_12(256'h686868686A6A6A6A6A6A6A6868686868686868686868686A6868686868686868),
    .INIT_13(256'h8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A6868686A6A6A6A6A8A6A6A6868686868),
    .INIT_14(256'h6A6A6A6A8A8A8A8A8A8A8A8A8A6A6A6A6A6A6A8A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_15(256'h8C8C8A6A6A8A8C8C8A8A8A6A6A6A6A8A6A6A6A6A6A8A8A8A6A6A6A6A6A6A6A6A),
    .INIT_16(256'hAC8C8C8C6A6A6A6A6A6A6A6A8A8A6A6A8C8C8C8C8C8A8C8C8C8C8C8C8C8C8A8A),
    .INIT_17(256'hAE8C8CAE8E8C8C8C8C8C8C8C8C8C8CAE8CAEAE8C8C8C8CACAC8C8C8C8C8C8C8C),
    .INIT_18(256'hAEAEAE8C8C8C8C8C8E8E8E8E8CAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_19(256'hB0D0D0D0D0D0B0D0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8C8E8C),
    .INIT_1A(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0B0AEAED0D0B0AEB0D0B0AE),
    .INIT_1B(256'hD0D0D0D2D2F2F2F2D2D0D2D2D2D2D2D2F2D0D0F2F2F2F2F2D0F2F2D0D0D0D0D0),
    .INIT_1C(256'h4646464648686868686868686868686868686868686868686868484848486868),
    .INIT_1D(256'h6A6A6A6868686868484848486868686868686868686868686868686868486868),
    .INIT_1E(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A68686A6A6A6A8A6A6A6A6A686868686A6A6868),
    .INIT_1F(256'h8A8A8A6A6A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_20(256'h6A6A6A6A6A6A8A8C8C8A8A6A8A6A6A6A686A6A8A6A6A6A8A8A6A6A8A8A6A6A6A),
    .INIT_21(256'hAC8C8CAC8C8C8C8CACACAEAEAEAEAEAE8C8CACAC8C8C8C8C8A6A6A8A6A6A6A6A),
    .INIT_22(256'hAEAEAEAEAEAEAEAE8C8C8C8C8C8CAEAE8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_23(256'hAEAEAEAEAEAEAEAED0D0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_24(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0F2D0D0D0D0D0D0D0D0D0D0AEAE),
    .INIT_25(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_26(256'h6868484848486868686868686868686868686868686868686868686868686868),
    .INIT_27(256'h6868686A6A6A6868686868686868686868686868686868686868686868686868),
    .INIT_28(256'h68686A686868686868686868686868686A6A6A6A6A6868686A6A6A6A6A6A6A6A),
    .INIT_29(256'h6A6A6A6A6A8A8A8A686A6A8A8A8A6A6A6A6A6A8A8A8A6A6A8A8A8A8A6A6A6A6A),
    .INIT_2A(256'h8A6A6A6A8A8A8A8A8A8A6A6A6A6A6A6A6A8A8C8C8C8A6A6A6A6A6A6A8A6A6A6A),
    .INIT_2B(256'h8C8C8C8C8C8A8C8C8A8A8C8C8C8C8C8A8A8C8C8CACAC8C8C8C8C8C8C6A6A8A8A),
    .INIT_2C(256'h8CAC8E8C8CAEAE8C8C8C8C8C8C8C8C8C8C8CACAEACAC8C8CAEAC8C8C8C8C8C8C),
    .INIT_2D(256'hD0CED0CECECEAEAED0AEAEAEAEAEAEAEAEAEAEAEAEAEAEAE8CAEAEAEAEAEAEAE),
    .INIT_2E(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0CEAEAEAECEAEAEAEAECEAEAEAED0D0D0AE),
    .INIT_2F(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_30(256'h6868684846464848686848686868464648484868686868686868464668686868),
    .INIT_31(256'h6868686868686868486868686868686846464646464648686848464646484848),
    .INIT_32(256'h6868686868686868686868686868686A48684848484868686868686868686868),
    .INIT_33(256'h6A6A6A6A68686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6868686A6A6A6A6A6A6A6A),
    .INIT_34(256'h8C8C8C8C8C8C8C8C8C8C8A6A6A8A8A8A8C8C8C8A8A8A8A8A8A8C8C8A6A8A8A6A),
    .INIT_35(256'h8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A8A8C8C8C8C8C8C),
    .INIT_36(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEACACACAEAEAEACAC8C8C8C8C8C),
    .INIT_37(256'hD0D0D0D0D0D0D0D0D0D0CEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_38(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_39(256'hD0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0),
    .INIT_3A(256'h6846464646464646464646466868686868686848484848486868686868686868),
    .INIT_3B(256'h6868686868686868686868686868686868686868686868686868686868686868),
    .INIT_3C(256'h6868684848464646686868686868686868686868686868686868686868686868),
    .INIT_3D(256'h6A6A6A6848484848484848484848484848484848484848484848484848484868),
    .INIT_3E(256'h6A6A6A6A6A6A6A6A8A8A8A6A6A6A6A6A686868686868686868686868686A6A6A),
    .INIT_3F(256'hAEAEAEAEAEACAEAE8C8C8C8C8C8C8C8C8A8A8A6A8A8A8A8A8A8C8C8C8A8A8C8C),
    .INIT_40(256'hCECEAEAEAEAEAEAECECECEAEAEAEAEAEAEAEAEAEACAC8C8CACACAEAEAEAEAEAE),
    .INIT_41(256'hD0D0D0CECEAEAEAECECECEAEAEAECECECECECECECECECECECED0D0D0CECECECE),
    .INIT_42(256'hD0D0D0D0D0D0D0D0F0D0D0D0D0D0D0D0CECECECECECECECECECECECECECECECE),
    .INIT_43(256'hCECED0D0D0D0D0D0CECECECECED0D0D0CECEAECECECECECECED0D0D0D0D0D0D0),
    .INIT_44(256'h6866464646464646464646464666664646464646464646464646464646464646),
    .INIT_45(256'h6868686868686868686868686868686848686868686868686868686868686868),
    .INIT_46(256'h6A6A686868686868686868686868686A68686868686868686868686868686868),
    .INIT_47(256'h6A6A6A6A6A686868686848486868684848484868686868684848484848686868),
    .INIT_48(256'h8A8A6A6A6A6A6A6A6A8A6A6A6A6A6A686868686868686848686A6A686A6A6A6A),
    .INIT_49(256'h8C8C8C8C8C8C8C8C8C8C8C8A8A8A8A8A6A6A6A68686A6A6A6A6A6A6A6A6A6A6A),
    .INIT_4A(256'hAEAEAEAEAEAEAEACAEAEAEAEAEAEACACACAEAEAEAEAC8C8C8C8C8C8C8C8C8C8C),
    .INIT_4B(256'hCECECEAEAEAEAEAEAEAEAEAEACACACACACACAEAEAEAEAEAEAEAECEAEAEAEAEAE),
    .INIT_4C(256'hCECED0CECECECED0D0D0D0CECECECECECECECEAEAEAEAEAEACAEAEAEAEAEAEAE),
    .INIT_4D(256'hAEAEAEAEAEAEAEAECEAEAEAEAECECECECECECECECECECECECECECECECECECECE),
    .INIT_4E(256'h6666464646464646464646464646464646464646464646464646464646464646),
    .INIT_4F(256'h6868686868686868686868686868686846464868686848466868686868464646),
    .INIT_50(256'h6868686868686868486868686868686848686868684848466868686868686868),
    .INIT_51(256'h686868686A6A6A6A68686868686A686868686868686868484848484848486868),
    .INIT_52(256'h8C8A8A8A8A8A8C8C8A8A8A8A8A6A6A6A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A6A),
    .INIT_53(256'h8C8C8C8C8C8C8C8A8C8C8A8A8A8A8A8A8A8C8C8A8A8C8C8A8C8C8C8C8C8C8C8C),
    .INIT_54(256'hAEAEAEAEAEAEAEAEACAEAEAEAEAEACACAEAEAEAEAEAEAEAEAEAEAEAEAC8C8C8C),
    .INIT_55(256'hAEAEAEAEAEAEAEAEAECECEAEAEAEACACAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_56(256'hAEAEAEAEAEAEAEAEAECECECEAEAECECECEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE),
    .INIT_57(256'hAEAEAEAEAEAEAEAEACACACACACACACACACACACACACACACACAEAEAEAEAEAEAEAE),
    .INIT_58(256'h4666464646666666686866666666464646464646664646466868666666666666),
    .INIT_59(256'h6868686868686868686868686868686868686868686868686866464666664646),
    .INIT_5A(256'h686868686868686868686A686868484868686868686868684848686868686868),
    .INIT_5B(256'h8A6A6A6A686A6A6A6A6A68686A6A6A686A6A6A6A6A6A68686A68686868686A6A),
    .INIT_5C(256'h8A8A8A8A8A8A8A8C8C8C8C8C8C8A8A8A8A8C8C8C8C8C8A8A8A8A6A6A6A6A6A6A),
    .INIT_5D(256'hACAC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A6A6A8A8A8A6A6A6A),
    .INIT_5E(256'hAEAEAEAEAEAEACACACACACACACACACAC8C8C8C8CACACACACACACAEAEAEAEACAC),
    .INIT_5F(256'hCECEAEAEACACACACACACAEAEAEAEAEAEAEAEAEACACACACACACACACACACACACAC),
    .INIT_60(256'hACACACACACACACACACACAEAEAEACACAEAEAEACACAEAEAEAECECECECEAEAECECE),
    .INIT_61(256'h8A8A8A8A8C8CACACACACACACACACACACACACACACACACACACACACACACACACACAC),
    .INIT_62(256'h6666666666666666686666666666464646466666464646464666666646464646),
    .INIT_63(256'h6868686868686868686868686868686868686866466668686868464646464646),
    .INIT_64(256'h6A6A6A6A6A6A6A6A686A6A6A6A686A6A68686868686868686868686868686868),
    .INIT_65(256'h8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_66(256'h8C8C8C8C8C8C8C8C8C8A8C8C8C8A6A6A6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A),
    .INIT_67(256'h8C8C8C8C8C8C8C8CACAC8C8C8C8C8C8C8C8C8C8C8C8C8C8A8C8C8CACACAC8C8C),
    .INIT_68(256'hACACACACACAC8C8C8CACACACACACAC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C),
    .INIT_69(256'hAEAEAEAEAEAEACACACACACACACACACACACACACACACAC8C8CACACACACACACACAC),
    .INIT_6A(256'hACACACACACACACACACACACACACACACACACACACACACACACACACACACAEAEAEAECE),
    .INIT_6B(256'h8A8A8A8A8A8AACAC8A8AACAC8C8A8A8C8A8AACACACACACAC8A8A8AACACACACAC),
    .INIT_6C(256'h6666666666666666464666666646464446464646464646464646464646464646),
    .INIT_6D(256'h6868686868686868686868686868686866664646464646466868684646464666),
    .INIT_6E(256'h6868686868686868486868686868686A68686868686868686868686868686868),
    .INIT_6F(256'h6A686868686A6A6A68686868686868686868686A6A6A68686868686868686868),
    .INIT_70(256'hACACACACACACACAC8C8A8A8C8C8A8A8A8A8A8A8A8A8A8A8A6868686A6A68686A),
    .INIT_71(256'hACAEAEACACACACACAC8C8C8C8CAC8C8CACACAEACACAEAEAC8C8A8A8C8C8C8C8C),
    .INIT_72(256'hAEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAECECEAEAEAEAEAEAEAEAEAEAEAEAEACAC),
    .INIT_73(256'hACACACACACACACACAEAEAEAEACACACAEAEAEAEAEACACACACACAEAECECECECECE),
    .INIT_74(256'hACACACACAA8A8AACACACACACACACACAC8C8CACACACACACACAC8C8CACACACACAC),
    .INIT_75(256'h46464646464646466868686846464668686666688A8A8A688A8A8A8AACACACAC),
    .INIT_76(256'h4646464646466666666666666666464446464646464646464646464646464646),
    .INIT_77(256'h6868686868686868686868686868686868686868686868684668686644444668),
    .INIT_78(256'h8A8A8A8A8A8A6A6A8A8A8A6A6868686A8A6A6868686868686868686868686868),
    .INIT_79(256'hAC8C8C8CACAC8C8C8A8A8A8A8A8A8A8A8A8A8C8C8C8C8A8A8A8A8A8C8C8A8A8A),
    .INIT_7A(256'h8CACACACACACACACACACACACACACACACACAC8C8C8C8CACACAC8C8C8CAC8C8CAC),
    .INIT_7B(256'hACAEAEACACACACAC8C8C8A8CACACACAC8C8C8C8C8CACAEAEACAC8C8CACAEAEAC),
    .INIT_7C(256'hACACACACACACAEAEACACACACACACACACACAEAEAEAEAEAEAEAEAECECECECEAEAE),
    .INIT_7D(256'h8C8A8A8A8A8A8A8A8A8A8A8A8A8A8CACACACACACAC8C8C8C8A8A8C8C8CACACAC),
    .INIT_7E(256'h8A8A8A8A8A8A8A8A8A8A8A8AACACACAC8C8C8C8C8C8C8C8CAC8C8CACAEAC8C8A),
    .INIT_7F(256'h4646464646464646686868464624464624242446688A68666868688A8A8A8A8A),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized19
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h6666664646666666466666666666664646464646464646444646464646464646),
    .INIT_01(256'h8868666666666668688868666666686868686868686868666668686866464646),
    .INIT_02(256'h6868686868686868686868686868686868686868686868686868686868686868),
    .INIT_03(256'h6A6A8A8A6A68688A8A8A8A8A8A8C8C8A6A8A8A8A6868688A8A68688A8A686868),
    .INIT_04(256'h8A8A8A8A6A6868688A8A68686A8A8A6A6A6A8A6A6A6A8A8C8A8A8A8A8A6A6A6A),
    .INIT_05(256'h8A8A8A8A8C8A8A6A8A8A8A6A686868688A6A6A6A6A8A8A8C8A8A8C8C8A8A8A8C),
    .INIT_06(256'h8A8A8A8A6A8A8A8A8A8A6A68686A8A8A8A8A8A8A8A8A8A8A8A684668688A8A8A),
    .INIT_07(256'h26464646464648684846686868688A6A6A686A8A6A46466A686A6A6868484646),
    .INIT_08(256'h48686A684846486A48686868686A684848686848486868484848464648686848),
    .INIT_09(256'h8A8A8A8A8A8A6A68688A8A68688A8A8A8A8A8A8A8A8A68886868686868684668),
    .INIT_0A(256'h4444444444444646444666664646444444444444464646464446464646464646),
    .INIT_0B(256'h6666464666666666666666664666666666464646666666666666666646444444),
    .INIT_0C(256'h4646464646464646464666686846464646464646464646464646464646464646),
    .INIT_0D(256'h2426464626242426464648464848484846466868464646686846466868464646),
    .INIT_0E(256'h4646464646262646244646242446462646464646464646484848484848464646),
    .INIT_0F(256'h6868686868464646686846464646464646464626262426264646262646462626),
    .INIT_10(256'h68686868484848484646464648686A6A8A8A8A686868888A46688A8A66464668),
    .INIT_11(256'h4868684848686868464646464646464648464868482626464648686868686868),
    .INIT_12(256'h6A6A8A8A6A686A8A6A8A8A6A6A6A6A68686A6A686A8A8A6A6A6A68686A8A8A6A),
    .INIT_13(256'h6A6A686A6A6A6A686A6A6A68686868688A8A68688A6A688A8A8A8A8A8A8A8A8A),
    .INIT_14(256'h2222222222242424444444444444442424242222444444444444444444444444),
    .INIT_15(256'h2202002222222222444444444444444424222222244444444444464444242222),
    .INIT_16(256'h4668686846466846464646464646462446464646444444444646464644242422),
    .INIT_17(256'h6868686868686868486868686868686868686868686868686868686868464668),
    .INIT_18(256'h686868686868686868686A686868686A686A6A6A6868686A686A6A6A68686868),
    .INIT_19(256'h8A8A8A8A8A6868688A8A8A686868686868686A68684868686A68484868686848),
    .INIT_1A(256'h8A6A8A8A6A68686868686A6A6A8A8A8A8A8A886868688A8A688AACACAC8A8A8A),
    .INIT_1B(256'h8A8A8A8A8A8A8A8A8C8C8A6A8A6A6A8A8A6A6A8A8A8A6A6A686A6A6A6A6A8A6A),
    .INIT_1C(256'h8A6A6A8A8A6A6A6A686A6A6A6A6A8A8A6A6A8A8A8A8A8A8A8A8A8A6A6A8A8A8A),
    .INIT_1D(256'h6868686868686868686868686A6A6A68688A68686868688A6868688A68688A68),
    .INIT_1E(256'h2222222222222222222222222222222222222222222222222222222222222242),
    .INIT_1F(256'h2424222224242422444444442444444422222224242222224444444444242222),
    .INIT_20(256'h8A8A8A8A6868686868688A8A8868686868686666464646666666664646462424),
    .INIT_21(256'h8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A6A6A8A8A8A8A8A),
    .INIT_22(256'h8A8A8A8A8A8A8A68688A8A8A688A8A8A8A8A8A8A6A6A8A8A6A6A8A8A6A6A6A6A),
    .INIT_23(256'h8A6A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8C8C8A8A8A8C8C8A6A6A8A8A8A8A),
    .INIT_24(256'h6A686A8A8A6A686A6A8A8A8A6A6A6A6A6A6A6A6A6A8A8A8A8A68688AACAC8A8A),
    .INIT_25(256'h6868686A6A6A6A68688A6A688A6A4868686868686A6A6A686A8A8A6A6A6A6868),
    .INIT_26(256'h6A6868686868686848686868686A6A6A6868686A6A68686A6A6A6A6868686868),
    .INIT_27(256'h484848464868686A686868686868686868686868686868AC8A4668AC8A686A6A),
    .INIT_28(256'h4444444444242222222222222222222222222242222222224242222222222242),
    .INIT_29(256'h6868686866666868686868666666666646464646464644444444444646464444),
    .INIT_2A(256'h8A8A8A8A8A6A68688A8A8A8A8A8A8A8A8A6A6868686868686868686868686868),
    .INIT_2B(256'h6A6A6868686A6A686A6A6A6A6A6A8A8A8A6A686A6A6A6A686A68686A6A6A6A8A),
    .INIT_2C(256'h6A6A6A6A8A8A6A686868686868686868686868686868686868686A6A6868686A),
    .INIT_2D(256'h686868686A6A686868686868686868686A6A6A6A6A6A68686A6A6A68686A6A6A),
    .INIT_2E(256'h6848686A6A68686A6868686868686868686868686A6868686868686868686868),
    .INIT_2F(256'h6A6A8A8A6A6A8A8A688A6A6A8A8A688A6A8A8A6A8A8A8A6A686A8A6A6A6A6A68),
    .INIT_30(256'h686868686868686A6A6868686A6848464848686A684848684848684848484868),
    .INIT_31(256'h686848484868686A48686868484646466868686868686AAC8C688AAE8A688A8A),
    .INIT_32(256'h6666666666664646464646464644444444444444444444444444444242424244),
    .INIT_33(256'h686868686868686868686A686868686868686868686868686868686868686868),
    .INIT_34(256'h6868686A6868686868686868686868686A6A6A6A68686A6A68686A6A8A8A8A8A),
    .INIT_35(256'h6A6A6868686A6A68686868686868686A8A6A6A686A6868686868686868686868),
    .INIT_36(256'h686868688A8A8A688A8A6A6A8A8A8A6A8A6A6A6A6A6A68686A6A6A6A6A6A6A6A),
    .INIT_37(256'h8A8A6A8A8A8A686868686868686868686868686A6A6A6A6A68686A6868686A6A),
    .INIT_38(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6868686868686A6A8A6A68686868),
    .INIT_39(256'h48686868484848686A6A68486868686A686A6A6A686A6A6A6A8A8A8A6A6A6A6A),
    .INIT_3A(256'h686868684848686A8A6A68686A6848464848686A684846486848486868486868),
    .INIT_3B(256'h6A6A68686868686A48686A6A684648688A8A6A686848688A4848688A68686846),
    .INIT_3C(256'h6868686868686868686868686868686866666668666666666666666646466666),
    .INIT_3D(256'h68686A686868686A686A6A6868686A68686868686868686A6868686868686868),
    .INIT_3E(256'h6868686A6868688A68686868686A6A8A68686A6A686868686A6A6A6A68686868),
    .INIT_3F(256'h686A8A6A6A8A6A688A6A6A6A6A6A6A8A8A6A6A6A6A6A6A6A6A6A6A6868686A68),
    .INIT_40(256'h68686868686A686868686868686868686A6A6A6A6A6A6A6868686A6A68686868),
    .INIT_41(256'h8A8A8A8A8A8A68686A8A8A6A6A68686868686A6A6A6A8A8A68686A6A68686868),
    .INIT_42(256'h686A6A68686868686A6A6A6A6A6A68688A8A6A6A6A6A6A6A6A6A686A6A8A6A68),
    .INIT_43(256'h68686A6A6A6868686A6A684848486A6A6A6A6A6A6A68686A686A686868686868),
    .INIT_44(256'h4848486848484868684848686848486848686868686868686868686868686868),
    .INIT_45(256'h686868686868686A68686A6A68686A8A8A8A6A686868466848688A6868686846),
    .INIT_46(256'h686868686868686868686868686868688A888888686868888888888868886888),
    .INIT_47(256'h6A6A6A6A6A6A6A68686A6A6A686A6A6A68686A6A686868686A68686868686868),
    .INIT_48(256'h8A6A6A6A6868688A6868686868686868686868686868686868686868686A6A6A),
    .INIT_49(256'h686A8A8A8A8A6A686868686868686868686868686868686868686868686A6A6A),
    .INIT_4A(256'h6A6A686868686848486868686868686868686868686868686868686868686868),
    .INIT_4B(256'h8A8A8A8A8A8A8A8A6A6A6A6A6A6A68686A6A8A6A6A686A6A6A6A6A6A6A68686A),
    .INIT_4C(256'h686A6A6868686A686868686A6A6A6A686A6A6A6A8A8A8A8A6A6A6A6A68686A6A),
    .INIT_4D(256'h484848686A68484868484868464868486A6868686868686868686868686A8A8A),
    .INIT_4E(256'h6868686A6A6A6868684848484848686A686868686868686A6868686868686868),
    .INIT_4F(256'h484868686868686A6A6868686848686A686868688A6A686A8A8A8A6A6A684868),
    .INIT_50(256'h686A6A68686A6868686868686868686A6A68688A686868686868686868686868),
    .INIT_51(256'h68686A6A68686A686A6868686A6A6A68686868686868686A686A6A68686A8A6A),
    .INIT_52(256'h686A6A6A6A6A8A8A8A8A6A6A6A8A8A6A686868686A6868686A68686868686868),
    .INIT_53(256'h68686A68684868686868686A6A686868686A6A8A6A486A6A686A6A6A6A686868),
    .INIT_54(256'h6A8A8A8A6A686868686868686868686A68686848484848486868686868686848),
    .INIT_55(256'h6A6A6A8A8A8A8A8A6A6A6A6A6A6A6A6A6A8C8C8A6A6A8A6A8A8A8A8A6A6A6A8A),
    .INIT_56(256'h6A6868686868686A68686868686868686A686868686A6A8A6A6A6A6868686A8A),
    .INIT_57(256'h4848486868684848684848464648484848686868684868686868686848688CAC),
    .INIT_58(256'h6A686868686A6848686868686868686A68686868686868684868686868686868),
    .INIT_59(256'h484848484848484848484648484868686846486868686A6A6868686868686868),
    .INIT_5A(256'h686A6A68686868686A6A686A6A68686A68686A6A686868686A68686A68686868),
    .INIT_5B(256'h6848686A6A6A8A688A6A6A6A6A6A68686868688A8A6868686868686868686868),
    .INIT_5C(256'h6A6A686868686A8A8A8A8A6A686868686A686868686868688A6A686868686868),
    .INIT_5D(256'h486A8A6A4846688A68686868686868684868486A6A486A6A6A6A6A6A68684868),
    .INIT_5E(256'h68686A8A8A6A8A8A8A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A6A68686868686868),
    .INIT_5F(256'h686868686A6A6A68686868686868686A6A8A8A6A686A6A68686A6A6A6A6A6A6A),
    .INIT_60(256'h6A6868686A6868686868686868686868686868686868686A6A6A6A6868686A6A),
    .INIT_61(256'h68686A6A6A6A6A6A6A6A6A68686A6A6A68686868686868686868686868686A8A),
    .INIT_62(256'h68686868686868484868686868686A8A6A6A6A68686A6A6A6A8A8A8A8A6A686A),
    .INIT_63(256'h68686868686868686848486868686A6A6846686868688A6A6868686868486868),
    .INIT_64(256'h68686868486868686868684868686848686A8A6A484868484848686868484868),
    .INIT_65(256'h6A68686A6A6A6A6868686A6A6A68686868688A8A8A6868688A6A6A6A6A686868),
    .INIT_66(256'h6A6A6A6868686A8A6A6A6A6A68686A688A6A6A686A6868686868686868686868),
    .INIT_67(256'h8A8A8A686A8A8A686868686868484848486848686868686868686A6A6A686868),
    .INIT_68(256'h484868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A8A6A6A8A8A8A8A8A8A8A6A),
    .INIT_69(256'h68686868686868686A6A6A6868686868686A6A6868686848686A6A6A68684848),
    .INIT_6A(256'h6868686A6A686868686868684848686868686868686868686A68686A6A6A6A6A),
    .INIT_6B(256'h686A6A6A6A686A6A686A6A6A686868686A686868686A6868686868686A6A6868),
    .INIT_6C(256'h68686A6868486868486868686868686A6A6A6868686868686A8A8A6A8A6A686A),
    .INIT_6D(256'h6868686868686868684848686868686A6848686868686A6A686868686868686A),
    .INIT_6E(256'h686868486868686868686868688A6A6848686A6848686A484846484848484848),
    .INIT_6F(256'h6A686A6A6A6A684868686A6A6868686868686868686868686A68688A6A686868),
    .INIT_70(256'h48686A6A6868686868686A686A6A6A686A6A68686A6A686868686A6A68686A6A),
    .INIT_71(256'h8A8A8A6A8AAC8A688A6A6A8A8A6A6A6A6A6A6868686868686868686A68686868),
    .INIT_72(256'h4848686868686868686A6868686868686A6A6A6A6A6A68688A8A6A8A8A8A8A8A),
    .INIT_73(256'h6A6A68686A6A6A6A6A6A6A6A6A6A6A6A686A6A68686868486A6A6A6A68684848),
    .INIT_74(256'h6868686A6A68686868686868484868686868686868686868686868686A6A6A6A),
    .INIT_75(256'h6868686848486868686A6A6A6A6A6A6A6A6868686A6A686868686A6A8A8A6A68),
    .INIT_76(256'h48686A686868686868686868684848686868686868684848486868688A6A6868),
    .INIT_77(256'h484848486868686868484848484848484846686848686868686868686868686A),
    .INIT_78(256'h686868686868684868686868688A6A6846464846486A8A686848484848484646),
    .INIT_79(256'h68688A8A6A6A8A6A8A8A8A6A686868686A6A68686A6A6A686A68686A6A684868),
    .INIT_7A(256'h686A8A8A6A6A68688A8A6A6A8A8A6A6868686A6A8A8A6A6A6A6A6A6A6A6A6A6A),
    .INIT_7B(256'h688AAC8A686A8A688A6A6A8A8A6A6A8A68688A68688A8A8C8A8A8A8A8A686A6A),
    .INIT_7C(256'h6868686868686868686A6A6868686A6868686868686868686A686868686A6A6A),
    .INIT_7D(256'h686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A8A6A6868686868686868686A6A6A),
    .INIT_7E(256'h8A6A6A6A6A6A686A686A6A686868686868686868686868686868686868686A8A),
    .INIT_7F(256'h68686868484648686868486868686868686868686868686868686A6A6A6A6A6A),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized2
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    clka,
    ram_ena,
    addra,
    dina,
    wea);
  output [1:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  input clka;
  input ram_ena;
  input [13:0]addra;
  input [1:0]dina;
  input [0:0]wea;

  wire [1:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [13:0]addra;
  wire clka;
  wire [1:0]dina;
  wire ram_ena;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:2]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000100000105400000014400000000000000000000000004004000154),
    .INIT_01(256'h0000000004000000000000000000000004000000000100015505401000000000),
    .INIT_02(256'h0000000000000000000000500400005410000000000000000000000000000001),
    .INIT_03(256'h0000000000000000000000000000054105040555000000001415554000000000),
    .INIT_04(256'h0000400500000000000000010000000000000000000000000000000000000000),
    .INIT_05(256'h0115555004051414040101000100000500000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000001000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000001000000000000000000000000),
    .INIT_08(256'h0000000000000000050000000000000000000000000005550411150015000000),
    .INIT_09(256'h0000000000000000000000010000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000005000150500000000000001000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000004001000000000000000000000000000000000000000000000100000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000001),
    .INIT_14(256'h0000000000000000000000000000000000000000054000000000000000010050),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000005000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000400000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000040000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000500000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000050000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000010000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000040000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000004),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000010000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0010000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0001000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(2),
    .READ_WIDTH_B(2),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(2),
    .WRITE_WIDTH_B(2)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:2],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ram_ena),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized20
   (DOADO,
    DOPADOP,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]DOADO;
  output [0:0]DOPADOP;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]DOADO;
  wire [0:0]DOPADOP;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h4868686868686868686868686A686868686A6A6A6A6868484868686A8C8C8A68),
    .INIT_01(256'h4646464848486868684848484848464846464848464868486868484848486868),
    .INIT_02(256'h6A6868686A8A6A68686868686868684868486848486A8A686A68686868484848),
    .INIT_03(256'h6A6A8A8A6A6A8A6A6A8A8A6A6A686A8A8A8A6A6A8A8A8A688A6A8A8A8A6A6A6A),
    .INIT_04(256'h8A8A8A8A6A6A6A8A8A6A6A6A8A8A6A6868686A6A6A6A6A6A6A68686A6A6A6A6A),
    .INIT_05(256'h688A8A6A68686A68484848686868686868686A684868688A4648686868686868),
    .INIT_06(256'h6868686868686868686A6A686868686868686868686868686868686868686868),
    .INIT_07(256'h6868486868686A6A6A6A6A6A686868686A8A8A8A686868686A686868686A6A6A),
    .INIT_08(256'h6A6A6A6A6A6A6A6A6A6A6A6A6A686A8A8A6A6A686868686868686A6868686A8A),
    .INIT_09(256'h686868686868686868484646486868684868686868486868686868686868686A),
    .INIT_0A(256'h484846464868684848464668686A6868686A6A6A6A6A68486868686A8C8C6A68),
    .INIT_0B(256'h4646484848484848484848686848484846464848464648464848484646484848),
    .INIT_0C(256'h6A6A6A6A6A6A6A6A8A6A6A8A6A6A68686A68686868686868686868686868688A),
    .INIT_0D(256'h68686A8A686868686868686868686A8A68686A68686A68686A6A6A6A6A6A6A68),
    .INIT_0E(256'h4848484646464868486868686868686848686868484848686868686868686868),
    .INIT_0F(256'h8A6A6868686A6A68686868686868686A68686846466846682424242424464646),
    .INIT_10(256'h6868686868486868486868484868686868686868686868686868686868686A6A),
    .INIT_11(256'h6868686868686A6A6A6A6A6A6A6868686A6A8A8A6A68686A6A6A686868686868),
    .INIT_12(256'h6A68686A6A6A686A686A6A6A6A686A8A8A8A8A6A6A6A6A6A68686A6A68686A8A),
    .INIT_13(256'h68686868686868688A6868686A6A8A8A68686868686868686868686868686868),
    .INIT_14(256'h4848464646484846464646484848464648486848486848484848484868684848),
    .INIT_15(256'h4848686868686868484648686848484846486848464868484848484848484848),
    .INIT_16(256'h686A8A6A68686A8A6A686A8A6A68686A68688A8A68688A8A686A8A8A68688AAE),
    .INIT_17(256'h4646486868688A68686A8A8A6A686868686A8A6A686A8A8A6A8A8A6A6A8A8A6A),
    .INIT_18(256'h4646484846464646464868484646462646464646242426462426464626242626),
    .INIT_19(256'h686868484646686A6A6868686868686A68688A68688A68686868686848686868),
    .INIT_1A(256'h6A6A6A6868686868686A6A68686A8A6A68686868684848486868484868686868),
    .INIT_1B(256'h6A6A6A686A6A6A6A6A6A6A6A6A6A6A6A6A686A6A6A6A68686868686A6A6A6A68),
    .INIT_1C(256'h6868686A6A6A6A6A48686A6A68686A8A6A6868686868686868686A8A8A8A6A6A),
    .INIT_1D(256'h4846484848684848686868686868688A8A6A6A6A6A6A6A686A6868686A8A6A68),
    .INIT_1E(256'h4848484646464646484848484626242446464646464648464648464646464646),
    .INIT_1F(256'h4848686868686848684868686848484848686868486868684848484848484848),
    .INIT_20(256'h6A6A8A686A8A6A8C6A8A6A8A6A6A6A8A6A8A8A8A6A68686A6A8A6A6A68686A8A),
    .INIT_21(256'h464646242646464848464646464868684646466848486868686A8A8A8A8A8A8A),
    .INIT_22(256'h686A8A6A6848486868686A684848484868686A68484646484846484848462626),
    .INIT_23(256'h686A6A68686868686868686868484868686868686868686A6868686A6A6A6868),
    .INIT_24(256'h686868686868686A68686868686A6A8A6A6868686A6A6A686868686868686868),
    .INIT_25(256'h6A6A6A8A6A68686A6A6A8A6A6A6A68686A6A6A6A6A6A6A6A8A6A6A8A6A68686A),
    .INIT_26(256'h6A6A6A6A6A6A6A6A68686A6A6A6A6A8A686A6A686868686848686A8A6A686A6A),
    .INIT_27(256'h4668684646484846464868684848486868686868686868686A48686A68486868),
    .INIT_28(256'h6868684848484848486868684846264648484646464848484646264646484646),
    .INIT_29(256'h4848686868686848686868686848686A6A686868486868686868686868686868),
    .INIT_2A(256'h48686A6A8A8A68686A6A686A6A8A8A8A6A8A8A8A6A68686A6A6A6A6A68686A6A),
    .INIT_2B(256'h6868684848686868684848484848686848464648462426264648686868486868),
    .INIT_2C(256'h68686A6A6848686868686A6868686868686A6868686868686A68686868484848),
    .INIT_2D(256'h8A8A8A8A6A6A6A6A686A6A6A686868686A686868486868686868686868686A6A),
    .INIT_2E(256'h8A8A8A6A6A6A6A6A6A8A8A8A8A6A8A8A8A6A6A6A6A6A6A6A8A8A8A8A6A6A6A6A),
    .INIT_2F(256'h686A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A68686A8A6A6A8A8A6A6A8A),
    .INIT_30(256'h68484868686868686868686868686A6A68686868686A6A6A68686A6A6A6A6A6A),
    .INIT_31(256'h6868482626464646264646484846464848484848484848486846486848464848),
    .INIT_32(256'h6868686848686868484868686848484868686868686848484848464648686848),
    .INIT_33(256'h68686848484868684848686A6A68486848486A8A6A6868486868686868686A6A),
    .INIT_34(256'h4646686868684648684846464868686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_35(256'h68686868686A6A68686868686868686868486868484646464648484846464648),
    .INIT_36(256'h686868686868686A686A6A68686868686A686868686A68686A68686868486868),
    .INIT_37(256'h8A8A8A8A8A8A8A8A8A8A6A6A6A6A6A6A6A6A6A68686868686A68686868686A6A),
    .INIT_38(256'h8A8A8A8A6A6A6A6A6A6A8A8A6A6A6A6A8A8A6A6A8A8A8A6A8A8A8A8A6A68686A),
    .INIT_39(256'h486A6A68686A6A6A6A6A6A8A6A6A6A6A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_3A(256'h4646464648484846484848484848686868684848686A6A68686868686868686A),
    .INIT_3B(256'h6A6A684646464868264648484848484846464848484848464646486848464646),
    .INIT_3C(256'h6868686868686868686868686868484868686A6A6A6868686868484868686868),
    .INIT_3D(256'h6868684848484868484648686848464846464848464848466868686868686868),
    .INIT_3E(256'h48484846686846686848464646484646484868686A6A686868686A6A6A6A6A6A),
    .INIT_3F(256'h68686868686A6A6868686868686868686A68688A686868686868686868686868),
    .INIT_40(256'h68686868686A6A6A6A6A6A686868686868686868686868686A68684848486868),
    .INIT_41(256'h8A8A6A8A8A8A8A8A8A8A8A8A8A8A8A6A8A8A6A6A6A6A6A6A6A6A6868686A6A8A),
    .INIT_42(256'h6A6A6A6A6A6A6A6A686A8A6A6A68686A6A6A686A6A8A8A8A6A6A8A8A6A686A8A),
    .INIT_43(256'h486868484848686868686A6A6A6A6A6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A6A),
    .INIT_44(256'h4646464648484626264646484846464848484846486848484848684848484868),
    .INIT_45(256'h686A6A6868686868484868686868686848484868684848484646486848464846),
    .INIT_46(256'h6868686868686868686868686848484868686A6A6868686A6A68686868686868),
    .INIT_47(256'h4648686848484848484646464846464846464848464648464848484848484848),
    .INIT_48(256'h6A686848686A686A6A6868686868684848486868686868684868686868686868),
    .INIT_49(256'h68686868686A6A6A68686A6A6A6A6A6A8A68686A68686868686A6A6868686868),
    .INIT_4A(256'h6A686868686A68686A6A6A6A6868686868686868686868686A68686868686868),
    .INIT_4B(256'h6A6A6A6A8A8A8A8A8A8A6A8A8A8A8A6A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_4C(256'h68686A6A6A6A6A6A6A6A8A6A6A6A6A8A6A6868686A6A8A8A6A6A8A8A8A6A8A8A),
    .INIT_4D(256'h48464848484646684848686868484848686868686868686A686A6A6A6A6A6A6A),
    .INIT_4E(256'h6848484868684626264646484848464848686848464646464848484846464848),
    .INIT_4F(256'h68686A6868686868686868686868686A68686868686868686848686848486868),
    .INIT_50(256'h68686848484868684868686848484868686868484848686A68686A6A6A6A6A6A),
    .INIT_51(256'h4648686848484868684846464848686846486A8A6A6848464848484846464648),
    .INIT_52(256'h68686A686A6A68686A6A6A6A8A6A6A6868686868686868686868686868686868),
    .INIT_53(256'h6A6A6A68686A6A6A68686A6A6A6A6A6A6A68686A686868686868686868686868),
    .INIT_54(256'h8A6A68686A6A6A686A6A6A6A6A6A6A6A6A6A6A6A6A686A6A6A6A6A6A6A6A6A68),
    .INIT_55(256'h6A6A6A6A6A8A8A6A6A6A6A6A6A6A6A6A8A8A8A6A6A6A6A686A6A6A6A6A6A6A6A),
    .INIT_56(256'h68686868686868686A6868686868686A6A6A686868686A6A8A8A8A8A8A6A8A8A),
    .INIT_57(256'h6848486868464668486868686868686868686868686848686868686868686868),
    .INIT_58(256'h686868686A6848464848686A68686868686A6A68484848484868684848484848),
    .INIT_59(256'h6868686868686868686868684848686868686868686868686A68686868686A68),
    .INIT_5A(256'h686848484848486848686868484648486868484626464868484868686A6A6A68),
    .INIT_5B(256'h4848484646486868686848686868686848686A8A6A6868464648484848484868),
    .INIT_5C(256'h68686A68686A4868686868686A6A68686868686868686868686868686A6A6868),
    .INIT_5D(256'h686A6A6868686A6A6A6A6A6A6A6A6A6A6A686A6A6A8A8A68686A6A6A6A6A6868),
    .INIT_5E(256'h8A6A6A686A6A6A6A68686A6A6A6A6A6A6A6A6A686A6A6A6A686A6A686A6A6A68),
    .INIT_5F(256'h6A6A6A6A6A6A6A6A686A6A6A6A6A6A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A68),
    .INIT_60(256'h486868686868686868484846484848486A6A6868686868686A6A6A6A6A68686A),
    .INIT_61(256'h6868686A6848486868686A6A6A6A6A6A6A6A6A6A6A6868686868686848686848),
    .INIT_62(256'h6A686A6A6A6868486868686A6A6868686A6A6A68686868686868686868486868),
    .INIT_63(256'h6868686868686868686868484848486868686868686868686A686A6868686A68),
    .INIT_64(256'h6868684848686868686868684846464668484626264646464648484868686868),
    .INIT_65(256'h4848484646486868484868686868484868486848464868484848684848486868),
    .INIT_66(256'h686A8A686868486A6A6A6A6A6A6A6A6A686868686868686A6868686868686868),
    .INIT_67(256'h686A8A6A6868686A6A6A6A6A6868686A6A68686A686A6A6868686A8A8A8A6A68),
    .INIT_68(256'h6A6A68686868686868686A6A6A6A68686A684848686A6A6A68686868686A6A68),
    .INIT_69(256'h686A6A6A6868686A686A6A6A6A686A8A6A6A6A8A8A8A8A6A6A6A6A6A6A6A6A6A),
    .INIT_6A(256'h686868686868686A6A6848486868684868686848484848686868686868686868),
    .INIT_6B(256'h68686A6A6868686868686A6A6A6A6A6A6A6A6A8A8A6A6A6A686A6A68686A6A68),
    .INIT_6C(256'h6868686A6A686868686868686868686868686868686A6868686A6A6A68686868),
    .INIT_6D(256'h4848686868686868484868484848486868686868686868686868686848686868),
    .INIT_6E(256'h686868686868686A686868686868484868684848484848484848484848484848),
    .INIT_6F(256'h4848484848484848486868684848686868486868484868686868686848484868),
    .INIT_70(256'h6A6A68686A6A6A6A6A8A8A6A6868686A8A6A6868686868686868686A8A8A6A6A),
    .INIT_71(256'h6A6A6A6A686868686A686868686868686A686A6A6868686868686A8A6A68688A),
    .INIT_72(256'h6A686848484848686A6A6A6A6A68686A6A6A6A686868684868686A686868686A),
    .INIT_73(256'h4848484848686868686A6A48486868686868686868686868686A6A6A686A6A6A),
    .INIT_74(256'h68686868686A6A8A6A6A686868686A6A6A6A6868686868686A68484848684846),
    .INIT_75(256'h6868686868686868486868686A8A8A6A6A6A6A8A8A6A6A6A8A8A6A68686A6A6A),
    .INIT_76(256'h686848686868686868686868686868686A686A6A6A6A68686868686868686848),
    .INIT_77(256'h486868686868684848484868684848686868684848686868686868484848686A),
    .INIT_78(256'h6868684848686868484868686848484868686868686868684868686848486868),
    .INIT_79(256'h464846464648484848686A464648484846464848486868464648684846486868),
    .INIT_7A(256'h68686868686868686868686868686A6A6A6A6868686868686868686A6A6A6A6A),
    .INIT_7B(256'h68686A6A6868686A6A6868686868686A8A6A6A6A68686868686A8A8A6A68686A),
    .INIT_7C(256'h6A686868684868686868686A6A686A6A686A6A686A6A6A6A686A6A6A6A686868),
    .INIT_7D(256'h4848686868484868486868484648686848686868686868686868686848686A6A),
    .INIT_7E(256'h686A6A6A686868686868686A6A6A6A6A6A6A6868686A6A6A6A68686868686868),
    .INIT_7F(256'h6A686868686868686868686868686A6A6868686A6A6A68686A6A6A68686A6A6A),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],DOADO}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],DOPADOP}),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized21
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h6868686868686868686868686868686868686868686868484848686868484848),
    .INIT_01(256'h6868686868684848686868686868686868686868686868686868686848686868),
    .INIT_02(256'h4848484848484848684848686868484868686868686868686868686868686868),
    .INIT_03(256'h464848464648686846688A684648486A48484646466868684648484646464848),
    .INIT_04(256'h686868686A6A6A6A6A686848686A6A6A68686868686868686868686868686A6A),
    .INIT_05(256'h68686A686868686A6868686A6A68686A6A6A8A8A68686A6A6A6A6A8A6A68686A),
    .INIT_06(256'h68686868686868686848486868686A6A68686A686A6A6A6A6A6A6A6A6A6A6848),
    .INIT_07(256'h68686A6A6A686868686A6A68686868684848686868686A6A6868684848686868),
    .INIT_08(256'h6A6A6A6A686868686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_09(256'h6868686848484848686848484646486848484848686848486868686868686868),
    .INIT_0A(256'h68686A686868686A68686868686868686A686868686868484848686868684848),
    .INIT_0B(256'h6848486868484868684868686848686868686868686868684868686868686868),
    .INIT_0C(256'h4868684848484868AC8A48484848484848686868484848686848484868686848),
    .INIT_0D(256'h484848484868686848486A684868486868684846464868684646464646464646),
    .INIT_0E(256'h6868686A8A8A6A6A8A8A6A68686A6A6A686868686868686868686A6A68686A8A),
    .INIT_0F(256'h68686A6868686868484868686868686868686A6A68686A6A6A686A6A8A6A6868),
    .INIT_10(256'h68686868686868686868486868686868686868686868686868686A6A68684846),
    .INIT_11(256'h6A6A6A6A8A6A6A6A8A8A8A6A6A6A8A6A6868686A6A6A6A6A6A6A6A686868686A),
    .INIT_12(256'h68686868484868684868686A6A6A6A6A6868686A6A8A6A6868686A6A6A6A6A6A),
    .INIT_13(256'h6868686848486868684848484626464848464646486848484848684848686848),
    .INIT_14(256'h6868686848486868486868686868686868686868686868686868686A6A6A6868),
    .INIT_15(256'h6846464848464868484848484846484868686868686868684868686868686868),
    .INIT_16(256'h4848684848484868AC8A68484848484648484848484848684848464648684848),
    .INIT_17(256'h4848484848686868484648484868464868684848484848484848484848484848),
    .INIT_18(256'h686868686A6868686A8A8A6A686868686A686868686868686868686868688A8A),
    .INIT_19(256'h6868686868686868484868686868484868686868484868686868686A6A6A6868),
    .INIT_1A(256'h6A6A6A6A6A6868686A6A6868686868686868686868484868686A6A6848484848),
    .INIT_1B(256'h8A6A68686A8A6A6A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_1C(256'h48484848464848684848686868686868686868686A6A68686868686A6868686A),
    .INIT_1D(256'h6A6A6868686868686A6868686848486848464646486868484848686848684848),
    .INIT_1E(256'h4848484848484848486868686868686848484868686868686868686A6A686868),
    .INIT_1F(256'h4846464848464868484848484848486868484848484868684848486868686868),
    .INIT_20(256'h4848484846464648464648686868464648484848486868686848484868686868),
    .INIT_21(256'h6868484848484848484868684646466A68686868684848486868484868484848),
    .INIT_22(256'h6868686868686848466868684848686868686868686868686A68686868686A6A),
    .INIT_23(256'h486868686868686868686868686848486868686846464848484868686A686868),
    .INIT_24(256'h6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A686A6A6A6A68686A686A6A6A6848686A),
    .INIT_25(256'h6A6868686A8A8A6A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A68686A6A6A6A6A6A),
    .INIT_26(256'h6868684848484868686868686868686868686868686A68684868686868686868),
    .INIT_27(256'h68686868686868686A68686A6A68686868484848686868686868686868686848),
    .INIT_28(256'h686848484848484848686868686868684848486868686A684868686868484848),
    .INIT_29(256'h6848486868484868484848484848486868484848484848686848484848686868),
    .INIT_2A(256'h6848484848484848464648484848486848484868686868686848484848686868),
    .INIT_2B(256'h6868686868484848686868484848486868686868484848686868686868686868),
    .INIT_2C(256'h6868686868686848464648464648484868686868686868686868464668686868),
    .INIT_2D(256'h486868686868686868686868686868686A686868464648464646486868684868),
    .INIT_2E(256'h6A6A6A6A6A6A6A6A8A6A686A6A6A6A6A686A6A6A6A68686A6A6A6A6A686A6A6A),
    .INIT_2F(256'h68686868686A6A8A8A8A6A6A6A6A6A6A68686868686868686868686A6A68686A),
    .INIT_30(256'h6A6A6A6A686868686A6A686868686A6A6A686868686A68686868686868686868),
    .INIT_31(256'h4868686868484848486868686868686868686868686A686868686A6A6A6A6A68),
    .INIT_32(256'h6868686848486868686868686868686868686868686A6A6A6868484848484848),
    .INIT_33(256'h6868686868484848484848484848486868684868684848686848484848684848),
    .INIT_34(256'h6A684868686868686A6848484848686A48486868484868684848484848486868),
    .INIT_35(256'h68686868686868686A686846686A48466A6868686848686A6868686868686868),
    .INIT_36(256'h6868484848684848464646464648684846466868484648686848464648686868),
    .INIT_37(256'h68686A6A686868686A6868686868686868686868464868484646486868484648),
    .INIT_38(256'h686848686868686A6A6A686A6A6A6A6868686A6A684848686A6A686A6A6A6A6A),
    .INIT_39(256'h464868686868686A8A6A6A6A6A68686868686868686868686A6A686A68486868),
    .INIT_3A(256'h68686A6A6A6868688A8A6A68686A6A6A8A6A6868686A6A686868686868686868),
    .INIT_3B(256'h686868686868484848686A6848686868686868686A6A6A6A68686A6A6A6A6A6A),
    .INIT_3C(256'h4868686846464868686868684848686868684868686868686868684848484848),
    .INIT_3D(256'h486868484868684868686868684868686A686868686868686868484868684848),
    .INIT_3E(256'h6A68486868686848484848686868686868686868484848484848484848686868),
    .INIT_3F(256'h6868686A6A6A6A6A686A8A68486A686868686A6A6A686868686868686868686A),
    .INIT_40(256'h4848686868484868484646464648686868686848484848686848464868686848),
    .INIT_41(256'h686868686868686868686A6A6A6868686A686868686868686868686868684848),
    .INIT_42(256'h484848484848486848686868686A6A68686A6A686868686868686A6A68686A6A),
    .INIT_43(256'h6868686A6A6A68486868686A6A6A6A6A6A6A6A686868686A6A68686A68484848),
    .INIT_44(256'h48686A6A6A68686868686A68686868686A6A6A6A6A6A6A6A6A6868686A6A6A6A),
    .INIT_45(256'h6868686A6A6A68686868686848686868486868686868686A4868686868686868),
    .INIT_46(256'h6868484848686868486868484848686868484868686868686868684846686A48),
    .INIT_47(256'h6868686868686848684868686A6A686868484848686868686868486868684846),
    .INIT_48(256'h6A68686868686868686868686868686868686868686848486868686868686868),
    .INIT_49(256'h68686A6A6A686868686A6A686868686868686A6A6A686A6A6A68686868686868),
    .INIT_4A(256'h6868686848686868684846464868686868686868686868486848486868686868),
    .INIT_4B(256'h686868684848686868686868686868686A686868686868686868686868686868),
    .INIT_4C(256'h6868686868486868486868486868684868686868486868486868686868686868),
    .INIT_4D(256'h68686A6A6A686868686868686868686868686A6A6868686A6A686A6A6A686868),
    .INIT_4E(256'h686868686868686868686868686868686A6A6868686A68686A686868686A6A6A),
    .INIT_4F(256'h686868686A686848486868484868686868686A68686868684868686868686868),
    .INIT_50(256'h6868686868686868686868686848686A68684868686868686868684846686848),
    .INIT_51(256'h6868686A6A6868686868686A6A6A6A68686868686A6A6A686868686868684848),
    .INIT_52(256'h8A6A68686868686868686A6A6A6A6A6868686A68686868686868686868686868),
    .INIT_53(256'h68686A6A68686868686A6A6868686A6868686A6A6A68686A6A68686868686868),
    .INIT_54(256'h6A6868684868686868686848686868686868686868686848686868686A686868),
    .INIT_55(256'h6868684848464848486868684868686868686868686868686868686868686A6A),
    .INIT_56(256'h68686868686868686868686868686848686A6868686868486868686868484848),
    .INIT_57(256'h68686A6A6868686868686868484848684848686868686868686A6A6A6A6A6868),
    .INIT_58(256'h6868686868686868686868484848486868686868486868686868686868686A6A),
    .INIT_59(256'h6A68686A6A6868684868686848686868686A6A6A686868686868686868686868),
    .INIT_5A(256'h686868686A6A68686868686A6A68686A68686868686868686868686848686848),
    .INIT_5B(256'h68686A6A6A6A6A686A6A6A6A6A6A6A6A68686A6A6A6A6A6848686A6A68486868),
    .INIT_5C(256'h6A6A686868686868686A6A8A8A6A6A6A686868686868686A6868686A6A6A6A6A),
    .INIT_5D(256'h686868686868686868686868686868686868686868686868686868686868686A),
    .INIT_5E(256'h6A68686848686868686868686868686868686868686868486868686A6A686868),
    .INIT_5F(256'h6868684848464646484848484848686868686848484648486868484848686868),
    .INIT_60(256'h686868686868686A68686A68686A6868686A6A6A686868686A6A6A6868686868),
    .INIT_61(256'h48686868484848684848484846464868464848686868686848686A6868686848),
    .INIT_62(256'h6868684868686868686868484848484868686868484868686868484868686868),
    .INIT_63(256'h68686A6A6A6A6A6A68686A6868686A6A68686868686868686868686868686868),
    .INIT_64(256'h686868686A6A6A6A6868686A6A68686A686868686868686868686A6848686848),
    .INIT_65(256'h4868686A6A6A6A686A6A6A6A6868686A68686A6A6A6A686868686A6A68686868),
    .INIT_66(256'h686868686868686868686A6A6A6A686868686868686868686868686A6A6A6A6A),
    .INIT_67(256'h4848484848486868464868484868686848686868686868686868686868686868),
    .INIT_68(256'h6868686868686868686868686868686868484868686868484868686868686868),
    .INIT_69(256'h6868686848484848486868684868686868686868484848686868686868686868),
    .INIT_6A(256'h48486868686868686868686868686868686A6A6A686868686868686868686868),
    .INIT_6B(256'h4868686848484868484848484646466848484848484848684868684848684846),
    .INIT_6C(256'h6868686868686868686868686848484868686868484868686868686868686868),
    .INIT_6D(256'h6A6A6A6A6A6A6A6A68686A6A6A68686A68686868686868686868686868686A6A),
    .INIT_6E(256'h68686868686A6A6A68686A6A6A686A8A68686A6A686A6A6A6A686A6A686A6A48),
    .INIT_6F(256'h48686868686A68686A6A6A6A686868686A6A68686A6A6A686A6A6A6A68686868),
    .INIT_70(256'h6868484848486868686868686868686868686868686868686868686868686868),
    .INIT_71(256'h4848464646464646464646464648686846464646484848484848484848686868),
    .INIT_72(256'h6868686868684848686868686848486868486868686868486868686868686868),
    .INIT_73(256'h6868686868686868686868686868686A68686868686868686868686868686868),
    .INIT_74(256'h6868684848484848464848484848484846686A68484868686848486868686868),
    .INIT_75(256'h68686A6868686868484848484846464848484848464848686868686848484848),
    .INIT_76(256'h6868686A68686868686868686868684868686848484868686868686868686848),
    .INIT_77(256'h6A6A6A6A68686A6A6A686A6A6A6868686A6A68686868686868686A6868686A6A),
    .INIT_78(256'h6868686868686A6A686A6A6A6A6A6A6A686A6A6A68686A6A6A686A6A6A6A6A68),
    .INIT_79(256'h484868686868686868686A68686868686A686868686A6A6A6A6A686868686848),
    .INIT_7A(256'h6868684848484848486868686868686848686868686868684868686868684848),
    .INIT_7B(256'h6848464646464646464646464646484846464646464648464848484848484848),
    .INIT_7C(256'h6868686868686868686868686848686868686868686868686868686868686868),
    .INIT_7D(256'h6868686868686868686868686868686868686868686868686868686868686868),
    .INIT_7E(256'h6868686848484848464848484848484848686868484848464848464868686868),
    .INIT_7F(256'h6868686A68686868684868686848464648486868484868686868686868484868),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized22
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h68686A6A68686868686A6A6A686868686868686868686868686868686A686848),
    .INIT_01(256'h6A6A6A6A68686A6A6A6A6A6A6A6868686A6A6A6A6A6A6868686A6A6A686A6A6A),
    .INIT_02(256'h686868686868686848686A68686A6A68686A6A6A6868686A6848686A6A6A6A6A),
    .INIT_03(256'h4848484848484868686868686868686868686868686868686A68686868686848),
    .INIT_04(256'h6868686868484868486868686868686848686868684848484848686868484646),
    .INIT_05(256'h6848464646486868484848464646484868484646464646464868684848684846),
    .INIT_06(256'h6868686868686868686868686868686868686868686868686A68686868686868),
    .INIT_07(256'h6868686848484848686868686868686868484848686868686868686868686868),
    .INIT_08(256'h6868686868486868464868484848484868686868686848464848484868686868),
    .INIT_09(256'h686868686A68684868686868684846464648686868486868684848686848686A),
    .INIT_0A(256'h48686A6A6868686A6A6A6A686868686A6A6868686A6A6A686868686A6A686848),
    .INIT_0B(256'h6A6A6A6A6A6A6A8A6A6A6A6A6A6868686A6A6A6A6A6A6A6A686A6A6A6A6A6A68),
    .INIT_0C(256'h484868686A68686846686A484868684868686A686868686A6848686A686A6A6A),
    .INIT_0D(256'h4646464646464848484848486868684848484868686868486868686868686868),
    .INIT_0E(256'h4868686868686868484848484868686868686868686868684848686848464646),
    .INIT_0F(256'h6868464648686868686868484648686868684848464646464868686868686846),
    .INIT_10(256'h6868686868686868686868686868686868686868686868686868686868686868),
    .INIT_11(256'h6848484848484826486868484648684868484848484846464648686848486868),
    .INIT_12(256'h6A6868686868684868686848686A684868686868686868464648686848686A68),
    .INIT_13(256'h6868686868686848466868686868482648484868686868686868484848486868),
    .INIT_14(256'h48686A686868686A6A6A6A6A6A6A6A6A6848686A6A6A68686A68686868686868),
    .INIT_15(256'h6A8A8A6A686A6A6A8A6A6A6A6A68686A6A6A8A8A6A6A6848686A8A6A68686868),
    .INIT_16(256'h48686868686868686848486848486868686868686848486868466868686A6868),
    .INIT_17(256'h464646464646464846484848484848484848484848486868486868684848686A),
    .INIT_18(256'h4868686868686A8A6A686868686868686A8A8A6A686A68684848686868464646),
    .INIT_19(256'h6868686868686A6A68686848484848484868686848686848686A6A6848686848),
    .INIT_1A(256'h6868686868686868686868686868686848484848486868686848486868686868),
    .INIT_1B(256'h6848484848684846486868484648484668684848484646464648684846486868),
    .INIT_1C(256'h68686868686868686A6A684868686A68686868686868484646486868688A8C8A),
    .INIT_1D(256'h68686868686A6868486868686868484668486868686868686A68684848486868),
    .INIT_1E(256'h4868686868686A6A6A6A6A6A6A6A6A6A6A68686A6A68686A6A6A6A6A6A6A6868),
    .INIT_1F(256'h6A6A6A68686868686A68686A6A68686A6A6A6A6A6A6A6A68686A8A6A68686A6A),
    .INIT_20(256'h68686868484848484846484848486868684848484848484868466868686A6868),
    .INIT_21(256'h6868462626464868486868686868686848484868684848684848686848484848),
    .INIT_22(256'h6868686868686A6A8A6A686868686A6A6A8A8A6A6A6A8A8A6A68686A6A686868),
    .INIT_23(256'h6A6A6A68686A6A6A6A6868686868686868686A6868686868686A6A6868686868),
    .INIT_24(256'h4848684848486868484848484848484846464646464648484848686868686868),
    .INIT_25(256'h4846486868686848486868684868684668484648484646464648484646464848),
    .INIT_26(256'h686A6A6A68686A6A6A6A6A6868686A6A686868686868484668686868486A8A6A),
    .INIT_27(256'h686868686A6A6A6A6868686A6A6868486868686A6A6A686A6A6A68686868686A),
    .INIT_28(256'h6868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A68686A8A6A6A6A6A6A6A6A),
    .INIT_29(256'h68686868686868686868686A6A6868686A6A6A6A6A6A6A6A6A6A8A6A6A6A6A6A),
    .INIT_2A(256'h6868686868484846684848484848686848484848484646486846686848686868),
    .INIT_2B(256'h6A6A68464648686868686A6A6868686868486868686848686868686868484848),
    .INIT_2C(256'h68686868686868688A6A6A686A6A6A6A6A8A8A8A6A6A8A8C8A6A686A8A6A6A68),
    .INIT_2D(256'h6A6A6A6868686A6A6A6A6A686A6A6A6A686868686A8A8A6A686A8A6868686A8A),
    .INIT_2E(256'h4648684846464868484848484648484848462624242646484648684848484848),
    .INIT_2F(256'h48464868686A6A6868686A686868686868484648484846464868484846486868),
    .INIT_30(256'h686A6A6A6A6A6A8A6A8A8A6A68686A6A68686868686868686868686848686848),
    .INIT_31(256'h6A6A6A6A6A8A8A8A6A6A686A8A6A686A6A6A6A8A8A8A8A8A8A6A6A6868686A6A),
    .INIT_32(256'h6A68686A6A6A6A6A6A6A6A8A8A8A8A8A6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A),
    .INIT_33(256'h6A484868686868686868686A686848686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_34(256'h6A6A686A6A6848466A48686848686A6A68686868484646486848686848686868),
    .INIT_35(256'h686A6A68686A6A686A6A6A6A686868686868686A6A6868686A6A6A6868686868),
    .INIT_36(256'h6A686868686A6A688A6A686868686A6A686A8A8A6A6A6A8A8A6A6A686A6A6A6A),
    .INIT_37(256'h6868686868686868686868686A6A6A8A686868686A8A6A6A686A6A6868686A8A),
    .INIT_38(256'h4868684846464848464648484848686848464646464648484648484646464846),
    .INIT_39(256'h48466868686A6A686A6A6A68686A6A6868484868686848486868686868686868),
    .INIT_3A(256'h6A6A6A6A6A8A8A8A6A8A8C8A6A686A6A68686A8A8A6A686868686868686A8A68),
    .INIT_3B(256'h6A6A6A6A6A8A8A8A8A6A6A8A8A6A6A8A6A6A6A8A8A8A8A8A8A8A6A6A6A6A6A6A),
    .INIT_3C(256'h6A68686A6A6A6A6A686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A8A6A6A),
    .INIT_3D(256'h6A4848686868686A6A6868686848484868686A6A6A6A68686A6A6A6A6A6A6A6A),
    .INIT_3E(256'h6A6A6A6A6A6A68688A68686A68686A686A6A6A6A6848486A68486A6848686868),
    .INIT_3F(256'h686A6A686A8A8A686A6A6A6A6A6868686868686A6A6A6A686A6A6A6A8A8A6A6A),
    .INIT_40(256'h6A68684868686A686A68686868686A6A6868686A6A6A68686A6A6A6868686A8A),
    .INIT_41(256'h68686868484848686868686868686A6A68686868686868686868686868686A68),
    .INIT_42(256'h686868684848484848484848686868684648686A6A6A68684868684846484846),
    .INIT_43(256'h6848686A6A6A6A686A68686868686868686868686868686868686A68686A6A6A),
    .INIT_44(256'h6A6A6A6A8A8A8A6A6A8A8A8A6A6A6A6A6A6A8A8A8A6A68686A686A6A6A8A8A6A),
    .INIT_45(256'h6A6A686A6A6A6A6A8A6A6A6A8A6A6A8A6A6A6A6A6A6A6A6A8A6A6A6A6A6A6A6A),
    .INIT_46(256'h6A68686A6A6868686868686A6A6A6A68686A6A6A6A6A6A686868686A6A6A6A6A),
    .INIT_47(256'h6A68486868686A6A6A6A6A68686848686868686A6A6868686A6A6A6A6A68686A),
    .INIT_48(256'h6A6A6A6A8A8A6A6A6A68686A686868686A6A8A8A6A68688A6A686A6A686A6A6A),
    .INIT_49(256'h6A8A6A68688A8A6A6A6A6A6A6A6A6A6A6868686A6A6A686868686A8A8C8A6A6A),
    .INIT_4A(256'h68686848486868686868684868686A6A684868686A6A6868686A6A6868686A8A),
    .INIT_4B(256'h4848686848464868686868686868686868686868686868686A68684868686868),
    .INIT_4C(256'h6A6A6A6868686848686868686868686848686A8A8A6A68686A6A684848686848),
    .INIT_4D(256'h6A686A6A6A6A6A68686868686A686868686868686A68686868686A6868686A6A),
    .INIT_4E(256'h6A6A686A6A8A6A6A6A6A6A8A6A6A6A6A6A6A8A8A8A6A6A6A8A6A6A8A6A8A8A6A),
    .INIT_4F(256'h686868686A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A),
    .INIT_50(256'h6A48486A6A68686868686A6A6A6A68686A6A686868686846484868686A6A6868),
    .INIT_51(256'h6A6A6A68686A6A8A8A8A6A6A6A6A68686A6A6A6A6A6A6A6A6A6A6A6A6A68686A),
    .INIT_52(256'h6A6A6A6A6A6A6A6A6A68686A686868686A6A6A6A6A6A6A8A6A6A8A8A6A6A6A8A),
    .INIT_53(256'h8A6A6868686A6A6A6A6A6A6A6A6A6A6A6A6868686A6A6868686A6A8A8A8A6A6A),
    .INIT_54(256'h68686848484868686868486868686A6868484868686868686A6A6A68686A6A6A),
    .INIT_55(256'h4848484846464668486868684848486868686868686868686868484848686868),
    .INIT_56(256'h8A8A6A6868686868686868686868686A6A6A6A6A686868688A8A6A4848686848),
    .INIT_57(256'h8A6A8A8A6A6A6A686868686A6A6A6868684868686A6A6A6A4868686868686868),
    .INIT_58(256'h6A68686A6A6A6A6A6A6A6A6A6A6A6A8A6A6A6A8A6A6A6A8A8A6A6A8A6A8A8C8A),
    .INIT_59(256'h686868686A6A6A6A6A6A6A686A6A6A686A6A6A6A6A6A6A6A6A6A6868686A6A6A),
    .INIT_5A(256'h6868686A6A68686A68686A6A6A6A6A686A6A4848486848264848486868684848),
    .INIT_5B(256'h8A8A8A68686A8C8C6A8A8A8A8A6A6A6A6A6A6A6A6A6A6A6A6A6A8A8A6A68686A),
    .INIT_5C(256'h686A6A6A6A686A6A6848686A6A686A6A6A68686A8A8A8A8A8A6A8C8A6A8A8A8C),
    .INIT_5D(256'h8A6868686A6A6A6A6A6A686A6A6A6A686A68686A6A6A68686A6A6A6A6A6A8A8A),
    .INIT_5E(256'h6868686848484846464648686868686868686868484868686A6A6868686A6A6A),
    .INIT_5F(256'h46464848464646484868686846464646484848686848686A6868684848486868),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized3
   (DOUTA,
    clka,
    ENA,
    addra,
    dina,
    wea);
  output [0:0]DOUTA;
  input clka;
  input ENA;
  input [15:0]addra;
  input [0:0]dina;
  input [0:0]wea;

  wire CASCADEINA;
  wire CASCADEINB;
  wire [0:0]DOUTA;
  wire ENA;
  wire [15:0]addra;
  wire clka;
  wire [0:0]dina;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ;
  wire [31:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFFFFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_01(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_02(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_03(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_05(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_06(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_08(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_09(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_10(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_11(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_12(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_13(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_14(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_15(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_16(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_17(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_18(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_19(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_20(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_21(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_22(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_23(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_24(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_25(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_26(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_27(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_28(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_29(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFF),
    .INIT_2A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_2F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_30(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_31(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_32(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_33(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_34(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_35(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_36(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_37(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_38(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_39(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_3F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_40(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_41(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_42(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_43(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_44(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_45(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_46(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_47(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_48(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_49(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_4F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_50(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_51(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_52(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_53(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_54(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_55(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_56(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_57(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_58(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_59(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_5F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_60(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_61(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_62(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_63(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_64(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_65(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_66(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_67(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_68(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_69(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_6F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_70(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_71(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_72(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_73(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_74(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_75(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_76(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_77(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_78(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_79(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_7F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("LOWER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(CASCADEINA),
        .CASCADEOUTB(CASCADEINB),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOADO_UNCONNECTED [31:0]),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_B_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_01(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_02(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_03(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_04(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_05(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_06(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_07(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_08(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_09(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0D(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0E(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_0F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_10(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_11(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_12(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_13(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_14(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_15(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_16(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_17(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_18(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_19(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1A(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1B(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1C(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1D(256'hFFFFFFFFFFE0000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1E(256'h00FFFFFFF800007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_1F(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC003FF801F),
    .INIT_20(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800FFE0000000000000000000003FFF),
    .INIT_21(256'hFFFFFFFFFFFFFC00FF800000000000000000000000000000FFFFFFFFFFFFFFFF),
    .INIT_22(256'h00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_23(256'h0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0FF00),
    .INIT_24(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE07FE00000000000000000000),
    .INIT_25(256'hFFFFFFFFFFFFFFFFFFFFC07F8000000000000000000000000000000000000000),
    .INIT_26(256'hFE01F0000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF),
    .INIT_27(256'h00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_28(256'h0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFC0000000000000),
    .INIT_29(256'hFFFFFFFFFFFFFFFFFFFFFFFFFFFC1FE000000000000000000000000000000000),
    .INIT_2A(256'hFFFFFFFF83F80000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF),
    .INIT_2C(256'h00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFC1FC0000000),
    .INIT_2D(256'h0000000000000000FFFFFFFFFFFFFFFFFFFCFC00000000000000000000000000),
    .INIT_2E(256'hFFFFFFFFFFFFFFFFE0F400000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFC3),
    .INIT_31(256'h00000000000000000000000000000000FFFFFFFFFFFF0F000000000000000000),
    .INIT_32(256'h0000000000000000FFFFFFFFF83C000000000000000000000000000000000000),
    .INIT_33(256'hFFFFFFE0F8000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h000000000000000000000000000000000000000000000000FF1887E000000000),
    .INIT_36(256'h0000000000000000000000000000000001FF0000000000000000000000000000),
    .INIT_37(256'h0000000000000000FF8000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("UPPER"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T 
       (.ADDRARDADDR(addra),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(CASCADEINA),
        .CASCADEINB(CASCADEINB),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOADO_UNCONNECTED [31:1],DOUTA}),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPADOP_UNCONNECTED [3:0]),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ENA),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.CASCADED_PRIM36.ram_T_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized4
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'hFFFC10387FFFFFFFE3000000000FFFFFFFFFFFFFFFFFFF000000000000000000),
    .INITP_01(256'h78000000001FFFFFFFFFFFFFFFFFFC000000000000000600FFFFFFFFFFFFFFFF),
    .INITP_02(256'hFFFFFFFFFFFFF0000000000000000C00FFFFFFFFFFFFFFFFFFFC001E7FFFFFFF),
    .INITP_03(256'h0000000000000000FFFFFFFFFFFFFFFFFFFC000FFFF7F37FF8000000003FFFFF),
    .INITP_04(256'hFFFFFFFFFFFFFFFFFFFC0007FCE0007FF000000000FFFFFFFFFFFFFFFFFFC000),
    .INITP_05(256'hFFF80007E000007FFC00000000FFFFFFFFFFFFFFFFFF00000000000000001FFF),
    .INITP_06(256'hFE00000003FFFFFFFFFFFFFFFFE700000000000000007FFFFFFFFFFFFFFFFFFF),
    .INITP_07(256'hFFFFFFFFFFC00000000000000001FFFEFFFFFFFFFFFFFFFFFFF000010000003F),
    .INITP_08(256'h000000000003FFFFFFFFFFFFFFFFFFFFFFE000000000003FFC00000007FFFFFF),
    .INITP_09(256'hFFFFFFFFFFFFFFFFFFE000000000003FF80000003FFFFFFFFFFFFFFFFF800000),
    .INITP_0A(256'hFFC000000000003FF0000003FFFFFFFFFFEFFFFFFF00000000000000000FFFFF),
    .INITP_0B(256'hC000007FFFFFFFFFFF87FFFFE00000000000000C001FFFFFFFFFFFFFFFFFFFFF),
    .INITP_0C(256'hFF03FFFFC00000000000000E003FFFFFFFFFFFFFFFFFFFFFFF000000000000FF),
    .INITP_0D(256'h0000007E007FFFFFFFFFFFFFFFFFFFFFFE000000000001FF000011FFFFFFFFFF),
    .INITP_0E(256'hFFFFFFFFFFFFFE7FF8000000000003FF0001FFFFFFFFFFFFFF01808000000000),
    .INITP_0F(256'hF0000000000007FE0003FFFFFFFFFFFFE0000000000000000000007800FFFFFF),
    .INIT_00(256'h9191918F8FAFAFAFAFAF8F8F8F91916F8F8FB1B1B1AFB1D1F3F5D3D3B1AFB1B1),
    .INIT_01(256'hB1D1B1B1D1D3D3D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B19191B1B191),
    .INIT_02(256'hDDDDBBBBBB9B9B9B9B9B9B9B9B9B9B9D9D9D7B5937351515F3F3D3D3D3D1D3D1),
    .INIT_03(256'h799B9B9B9B9B9B9B9B9B9B9B9B797979595959373737373715599BBDBDBDDDDD),
    .INIT_04(256'hAFAFB1D1D1D1D3F3F3F3F3F31313131335353737575757373757575757597979),
    .INIT_05(256'h131313F3F3F31313F3F3F3F3F3F3F1D1D3D3D1D1B1B1B1B1B1B1B1AFAFAF8F8F),
    .INIT_06(256'hCF13353537575757575979575757351313353537353515153535151515131313),
    .INIT_07(256'hFFFDFDFFFFFFDFDFDDBD9B9B7935F1D1F1F1F315F5D3D1D1F3F5153515F3CFAD),
    .INIT_08(256'hDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFDFDF),
    .INIT_09(256'hDFFFFFFFFFDFDFDFDFDFDFDFDFDFDFDFDFDDDDDFDFDDBDBDBBBDBDBBBDBDDFDF),
    .INIT_0A(256'h9191918F8F8F8F8F8F8F8F8F8F919191B18F8FAFD31515F5F3F3D3D3B1AFB1B1),
    .INIT_0B(256'hB1B1B1B1B1D3D3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B19191B1B191),
    .INIT_0C(256'hDDDDDDBDBB9B9B9B799B9B9B9D9D9D9D7B7B59371515F3F3D1D1D1D1B1B1B1B1),
    .INIT_0D(256'h799B9B9B9B9B9B9B9B9B9B9B9B795757573737351515151515377B9DBDDDDDDD),
    .INIT_0E(256'hAFB1B1D1D1D1D1D1D1F1F3131315353537373757575757573757595959597979),
    .INIT_0F(256'hF313151513F3F3F3F3F3F3F3F3F1D1D1D1D1B1B1B1B1AFAFAFAFAFAFAFAFAFAF),
    .INIT_10(256'hF135351313353557575957353535351535353535151313151515151513131313),
    .INIT_11(256'hFFFFFFFFFFFFFFDFBD9B9B795713F1CFD1D1D1F3D1D1B1B1B1D1F335373513F1),
    .INIT_12(256'hDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFDFDF),
    .INIT_13(256'hDFDFFFFFFFDFDFDFDFDFDFDFDFDFDFDFDFDFDDDFDFDDBDBB9BBDDDDFDFDFDFDF),
    .INIT_14(256'h9191918F8F8F8F8F8F8F8F8F8F8F8F8FB1D1D3F31515F3D1D1D1AFB1B1AFB1B1),
    .INIT_15(256'hB1B1B1B1B1D1B1B1B1B1B1B1B1AFAFB1B1B1B1B1B1B1B1B1B1B1B1B191919191),
    .INIT_16(256'hDDDDDDDDBD9B9B9B9B9B9B9B7B7B797959593715F3F3D1D1D1D1D1D1B1B1B1B1),
    .INIT_17(256'h9B9B9B9B9B7B7B7B7B797979595737353535151515151515153779BDBDDFDDDD),
    .INIT_18(256'hAFB1D1D1D1D1D1D1D1F313151535353757575757575757575759797979797979),
    .INIT_19(256'h1313151513F3F1F1F3F3F1F1D1D1D1D1D1B1B1B1AFAFAFAFAFAFAFAFAFAFAFAF),
    .INIT_1A(256'h153535353515153557573715F313131535351513F3F31315F313151513131313),
    .INIT_1B(256'hFFFFFFFFFFFFDFDFDFBD9B7B5735F3F1CFAFAFAF8F8F8FAFAFAFD1F335573513),
    .INIT_1C(256'hDFDFDFFFFFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFDFDF),
    .INIT_1D(256'hDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDDDDDDDDDDBDBB9B9B9BBDDFDFDFDFDFDF),
    .INIT_1E(256'h8F8F8F919191B1B1B1B18F8F8F8F8FB1D1F3F3F3F3F3F3D1D1D1B1D1D3F3F5F3),
    .INIT_1F(256'hB1B1B1B1B1B1B1B1B1B1B1B1AFAFAFAFB1B1B1AFAFAF8F8F8FB1B1B1918F9191),
    .INIT_20(256'hDFDFDDDDBDBDBDBDBD9B7B59373737351515F3F3D1D1D1D1D1D1D1D1B1B1B1B1),
    .INIT_21(256'h9B9B9B9B7B7B79797957373737373735351515151515153535577BBDDFDFDDDD),
    .INIT_22(256'hB1D1D1D1D1D1D1F113153737373737575757575757595959797979797979797B),
    .INIT_23(256'h15151515F3F3F3F3F3F3F3D1D1D1D1D1B1B1AFAFAFAFAFAFAFAFAFAFAFAFAFAF),
    .INIT_24(256'h373737373513F1F3131515F3F1F1F3F3F3F3F3F3F3F3F3F3F315151515153535),
    .INIT_25(256'hFFFFFFFFFFDFDDDDBD9B9B593713F3F1AFAF8D6D6B6B6D8DAFAFAFD1F3355757),
    .INIT_26(256'hDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_27(256'hDFDFDFDFDFDFDFDFDDDDDDDDDDDDDDDDBDBDBDBDBB9B99999BBDDDDDDDDFDFDF),
    .INIT_28(256'h8F8F8F8F919191B1B1918F8F8F8FAFB1CFF1F313355757153759373759595917),
    .INIT_29(256'hB1B1B1B1B1B1B1B1AFAFB1AFAFAFAFAF8F8FAFAF8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_2A(256'hDFDFDFDDBDBDBD9D7979371515153535F3F3D1D1D1D1D1D1D1B1B1B1B1B1B1B1),
    .INIT_2B(256'h9B9B9B9B9B9B9B9B59573757575757573735151515151537597BBDDFDFDFDFDF),
    .INIT_2C(256'hD1D1D1D1D1D1F3F3153757575757575957575779797979797B7B7B7B7B7B9B9B),
    .INIT_2D(256'h373515151513F3F3F3F3D3D1D1D1B1B1AFAFAFAFAFAFAFAFAFAFAFAFAFB1B1B1),
    .INIT_2E(256'h373715F3F3AFAFAFAFD1D1D1D1D1D1AFF1F1F3F3F3F3F3F3F315151515353537),
    .INIT_2F(256'hFFFFFFFFDFDFDDBD9B9B793713F1D1D1D1AF8D6D4B6B6D6D8F8F8FAFD1153759),
    .INIT_30(256'hDFDFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF),
    .INIT_31(256'hDDDFDFDFDFDFDFDDDDDDDDDDDDDDBDBDBDBDBBBB9B99999BBBBDDDDDDDDFDFDF),
    .INIT_32(256'h8F8F8F8F9191919191918F8F8F8D8DAFF335799B9D9B79577BBD9D9B7B595915),
    .INIT_33(256'hB1B1B1B1B1AFAFB18F8F8F8F8F8FAFAF8F8F8F8F8F8F8F8F8F8F8FAFB1AF8F8F),
    .INIT_34(256'hDFDFDFDDBDBD9B79353515F3F3131313F1D1D1D1D1D1D1D1B1B1B1B1B1B1B1B1),
    .INIT_35(256'h9B9B9B7B7B7B7979575757597959575737353535151537597B9DDFDFDFDFDFFF),
    .INIT_36(256'hD1D1D1D1F3F315353757595757575757575759797B9B9B9B7B7B7B7B7B9B9B9B),
    .INIT_37(256'h37373735151513F3F3F3D3D1D1B1B1B1AFAFAFAFAFAFAFAFAFAFAFB1B1D1D1D1),
    .INIT_38(256'hF3F3D3D1D1AF8DAF8F8F8FAFAFAFAFAFD1D1D1D1D1F3F3F3F3F3151515353737),
    .INIT_39(256'hFFDFDFDFDFDFBDBD9B9B7B37F3D1D1D1B1AF8F6D6B6B6D6D6D8D8F8FAFD1F313),
    .INIT_3A(256'hDFDFDFDDDDDFDFDFDDDDDFDFDFDFDFDFDFDFDFDFDFDFDFDFFFFFFFFFFFFFFFFF),
    .INIT_3B(256'hDDDDDDDDDDDDDDDDDDDDDDBDBDBDBDBD9B9B79797979999BBBBDDDDDDFDFDFDF),
    .INIT_3C(256'h8F8F8F8F8F919191919191AF8FAFD115BBDDFFDFBD9BBBBDBDBDBD9D7B3715F3),
    .INIT_3D(256'hB1B1B1B1B1AFAFB1B1B1AFAF8F8F8FAFAF8F8F8F8F8F8F8F8F8F8FB1D1B1AF8F),
    .INIT_3E(256'hDDDFDDBDBD9B79351513F3F1F3F3F3F3D3D1D1D1D1AFAFB1B1B1B1B1B1B1B1B1),
    .INIT_3F(256'h9B9B7B79797959573737375757573757351515151535597B9DBDDFDFDFDDDDDD),
    .INIT_40(256'hF3F3F3F3F31537575959575737373737577979799B9B79797979797B9B9B9B9B),
    .INIT_41(256'h373737351515F3F3D1D1D1D1B1B1B1AFAFAFAFAFAFAFB1D1B1D1D1D1D1D1D3D3),
    .INIT_42(256'h8DAFAFB1B18F8D8F8F8F8F8F8FAFAFAFD1D1D1D1D1D3F3F3F3F3151535353537),
    .INIT_43(256'hDDDDDDDDDDDDBD9B7B7957F3B1AF8D6D8D8D6D6B4B4B4B4B6D6D6D6D6D8D8F8F),
    .INIT_44(256'hDDBDBDBDBDBDDDDDDDDDDFDFDFDFDFDDDDDDDFDFDDDFDFDFDFDFDFFFFFFFFFFF),
    .INIT_45(256'hDDDDDDDDDDDDDDBDBDBDBDBDBDBDBB9B9B797979797979999BBDDDDDDDDDDDDD),
    .INIT_46(256'h8F8F8F8F8F9191919191B1B1AFD157BDDFFFDFFFFFDFDFDFFFDFBDBD79353537),
    .INIT_47(256'hB1B1B1B1B1AFAFB1F3F3D3D1B1AFAFB1AF8F8F8F8F8F8F8F8F8FB1D3F3D3AF8F),
    .INIT_48(256'hDDBDBD9B9B7B591515F3F3D1D3F3F3F3D1D1D1D1D1AFAFB1B1B1B1B1B1B1B1B1),
    .INIT_49(256'h5959595959595957373535351515353735151313131537799D9DBDBDDDDDDDDD),
    .INIT_4A(256'hF3F315151537575957573737375757577979799B79795757575779799B9B9B7B),
    .INIT_4B(256'h3735151515F3F3D1D1D1B1B1AFAFAFAFAFAFAFD1D1D1D1D1D1D1D1D3F3F3F3F3),
    .INIT_4C(256'h6D8D6D6D6D6D6D8F8D8F8F8F8D8D8FAFB1D1D1D1D1D1D1D1D1F3153537373535),
    .INIT_4D(256'hDDDDDDDDBDBD9B9B7B7937D1AF8D6D4B4B4B4B4B4B4B4B4B4B6D6D6B6B6D6D6D),
    .INIT_4E(256'hBDBDBDBDBDBDBDBDDDDDDDDFDDDDDDDDDDDDDDDDDDDDDDDDDFDFDFDFDFFFFFFF),
    .INIT_4F(256'hDDDDDDDDDDBDBDBDBDBDBDBDBDBB9B9B9B797979799B9B9BBBBDDDDDDDDDDFDF),
    .INIT_50(256'hD1B1B1B191919191B1B1B1B1159BDFDFFFFFFFFFFFFFFFFFDDDFBD7957575757),
    .INIT_51(256'hB1B1B1AF8FAFB1D1F3D3D1B1B1B1B1B1B1B18F8F8F8F8F8FAFB1D3F3F3D1D1D1),
    .INIT_52(256'h9B9B795735353515F3D3D3F3F3F3F3F5F3F3D3D1D1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_53(256'h3737373737373737151513F31315151515353537373737577B9B9DBDBDBDBBBB),
    .INIT_54(256'h15151535373737373757575757575757799B9B9B7957575759797B9B7B795959),
    .INIT_55(256'h35151513F3F1D1D1AFB1B1B1AFAFAFAFD1D1D1D1D1F1F3F3F3F3F3F3F3F31515),
    .INIT_56(256'h6D6D6D6D6D4B6D6D6D6D6D8D8F8F8FAFAFB1B1B1B1AFB1D1F3F3153737373535),
    .INIT_57(256'hBBBBDDDD9B9B9B795735F3AF6D4B4B4B4B4B4B4B292B4B4B6D6D4B4B6B6D6D6D),
    .INIT_58(256'hBD9B9B9B9B9BBDBDBDBDDDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDDDBDDDDDDDDD),
    .INIT_59(256'hBDBDBDBDBDBDBDBDBDBDBDBD9B9B9B9B9B7959797B9B9B9B9B9BBDBDBDBDBDBD),
    .INIT_5A(256'hD3D3D1B18F8F8F91B1AFB1379BBFDFFFFFFFFFFFFFFFFFFFFFDDBB7979795957),
    .INIT_5B(256'hB1B1B1B1AFAFB1D1D3D1B1B1B1B1AFAFB1918F8F8F8F8FAFB1D1D1F31515F3D1),
    .INIT_5C(256'h353515F3F3F3F3D3D3D3D3D3F3F3F3F3D3D3D3D1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_5D(256'h373737373515151313F3F3F3F313151537373737375759595979797979999999),
    .INIT_5E(256'h151535373737373737575979595757577999797959575979797B7B7B79595757),
    .INIT_5F(256'h1513F3F3D1D1D1D1D1D1D1D1B1B1D1D1D1F1F1F3F3F3F3F3F313131515151515),
    .INIT_60(256'h4D4D4D4D4B4B4D6D6D6D6D8F8F8F8F8F8FAFB1AFAFB1D1F31515373737353535),
    .INIT_61(256'hBB9B9B9B79797937F1D1AF8D6B4B4B4B4B4B4B4B4B4B4B4D6D6D6D6D6D6D6D6D),
    .INIT_62(256'h9B7979799B9B9B9BBDBDBDBDBDBDBDBD9B9B9B9B7979595759799B9BBDBDBDBB),
    .INIT_63(256'hBDBDBDBDBDBDBDBD9B9B9B9B9B997979595959595979799B9B999B9B9BBBBB9B),
    .INIT_64(256'hD1D3D3B18F8F8F8FAFAF159BDFDFDFFFFFFFFFFFFFFFFFFFFFDDBDBB9B9B9B9B),
    .INIT_65(256'hB1B1B1B1B1B1B1B1D1B1B1AFAFAFB1B1B191918F8F8FB1D1D3D3F3F5173715F3),
    .INIT_66(256'h1313F3F1F1F3F3D3D1D1D1D1D1D1D1D1D1D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_67(256'h3535151515151313F3F3F3F3F3F3151517151515153737373737353535353535),
    .INIT_68(256'h153537575757375757597979595759797979797979797B9B7B7B795957373737),
    .INIT_69(256'hF3F3D1D1D1D1D1D1D1D1D1D1D1D1F3F3F3F3F313F3F3F3131515151515151515),
    .INIT_6A(256'h4B4B4B4B4B4B4D6D6D6D6D8D8F8F8F8F8F8FAFAFAFD1F3153737373737351515),
    .INIT_6B(256'h9B795757353513F1AD8D8D6D4B4B4B294B4B4B4B4B4D4D6D6D6D6D6D4D4B4B4D),
    .INIT_6C(256'h79795979797B7B9B9B9BBDBDBD9B9B9B7B795957573735153557799B9BBDBD9B),
    .INIT_6D(256'hBDBDBDBDBDBD9D9B9B9B9B9B997979575759595959597979797979799B9B9B9B),
    .INIT_6E(256'hAFD3F3D1AFAFAFAFAF157BBDDFDFDFFFFFFFFFFFFFFFFFFFFFDFDFDDBB9B9DBF),
    .INIT_6F(256'hAFAFAFAFAFB1B1B1B1AFAF8FAFAFAFB1918F8F8F8F8FB1D3F3153717151515F3),
    .INIT_70(256'hF1F1D1D1D1D1D1D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1AFB1B1B1B1B1B191B1),
    .INIT_71(256'h1515151515151515F3F3F3F3F3F3F51515F3F3F3F3F3F3F315F3F3F3F1F1F1F1),
    .INIT_72(256'h3757575757575759575959595759799B7B7B9B9B9B9B7B7B7959573735353535),
    .INIT_73(256'hF3F1D1D1D1D1D1D1D1D1D1D1F1F3F31313131313131313131515151315151515),
    .INIT_74(256'h4B4B4B4B4B4B4D6D6D6D6D8D8D8F8F8F8F8F8FAFD1F315153737373735151515),
    .INIT_75(256'h7957351313F3CFAD8D8D6B4B4B4B4B2B4B4B2B4B4B4B4B4B4B4D6D4D4B4B4B4B),
    .INIT_76(256'h7B7959597979797B7B9B9B9B9B9B7B7957373515151515F3F31537597B9B9B9B),
    .INIT_77(256'hBDBDBDBDBD9D9B9B9B9B79797979575757575957595959375759595959797979),
    .INIT_78(256'hD1F3F3D1AFAFD1D1379BDFDFDDFFFFFFFFFFFFFFFFFFFFFFFFDFDDBD9B799BBD),
    .INIT_79(256'hAFAFAF8F8FAFAFAF8F8F8F8FAFAFAFAF8F8F8F8F8F8FAFB1F537393715F3F3D1),
    .INIT_7A(256'hD1D1D1D1D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1918F8F91919191918F),
    .INIT_7B(256'h151515F3F3F3F3F3F3F3D3D1D1D1F3F3F3F3D1D1D1F3F1D1D3D3F3F3F3F3F3F3),
    .INIT_7C(256'h57575757575757595757575759797B9B9B9B9B7B797957373737351515151515),
    .INIT_7D(256'hF1F1D1D1D1D1D1D1D1D1F1F3F3F3131313131515131315151515151315153537),
    .INIT_7E(256'h4B4B4B4B4B4B4B6D6D6D6D6D8D8D8F8F8F8FAFB1D315151537373515151313F3),
    .INIT_7F(256'h37351513F3F3CFAF8D6D6B4B4B4B6D6D6D4D4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized5
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h001FFFFFFFFFFFF800000000000000000000007C03FFFFFFFFFFFFFFFFFFC01F),
    .INITP_01(256'h00000000000000000000000F0FFFFFFFFFFF7FF7FFFF8003E8000000000007F8),
    .INITP_02(256'h000000BFBFFFFFFFFFFF1843FFFF80000000000000000FF8003FFFFFFFFFFFF8),
    .INITP_03(256'hFFFF00000FFE00000000000000001F8000FFFFFFFFFFFFFEC000000000000000),
    .INITP_04(256'h060000000000000003FFFFFFFFFFFFF80000000000000000000001FFFFFFFFFF),
    .INITP_05(256'h0FFFFFFFFFFFFCF00000000000000000000003FFFFFFFFFFFFFF000000000000),
    .INITP_06(256'h000000000000000060000F3FFFFFFFFFFFFC0000000000000F00000000000000),
    .INITP_07(256'hC20004FFFFFFFFFEFFFC0000000000001FC00000000000001FFFFFFFFFFFF000),
    .INITP_08(256'hFFFE0000000000001FC00000000000003FFFFFFFFFFF80000000000000000000),
    .INITP_09(256'h0E000000000000007FFFFFFFFFFF80000000000000000000C70000FFFFFFFFFB),
    .INITP_0A(256'hFFFFFFFFFFFF000000000000000000005F0001FFFFFEFFC3FFFE080000000000),
    .INITP_0B(256'h00000000000000001A0001FFFFFCFFC7FFFF3FF0000000000000000000000001),
    .INITP_0C(256'h0000000FFCF0FFCFFFFFFFF800000000000000000000000FFFFFFFFFFFFE0400),
    .INITP_0D(256'hFFFFFFF800000000000000000000001FFFFFFFFFFF3FFC000000000000000000),
    .INITP_0E(256'h000000000000003FFFFFFFFFFF000000000000000000000000000007E000FE00),
    .INITP_0F(256'hFFFFFFFFF800000000000000000000000000000300007C00FFFFFFFC00000000),
    .INIT_00(256'h59595959575757597979799B9B7979793513F1F1F3F3D1D1D1D1F31535375959),
    .INIT_01(256'hBDBDBD9D9B9B9B9B797979795957353515375757575737353557573735355759),
    .INIT_02(256'hF3F3D1D1D1F33779BDDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDBDBB9B9B9DBD),
    .INIT_03(256'h8FAFAFAFAF8F8F8FAFAFB1B1B1B1B1B1918F8F8F8F8FAFB1F51515151515F3D1),
    .INIT_04(256'hD1D1D1D1D3D1D1D1B1B1B1B1B1AFAFAFB1B1B1B1B1B1B1B18F8F8F8F8F8F8F8F),
    .INIT_05(256'hF3F3F3F3F3F1F1D1D1D1D1D1D1D1D1D3F3F3D1D1D1D1D1D1D3D3D3D3D3D3D1D1),
    .INIT_06(256'h575757373737575757575779797B79799B7B7959575757373515151513F3F3F3),
    .INIT_07(256'hD1D1D1F1D1D1D1D1D1F1F3131313131313131515151515151515151515353757),
    .INIT_08(256'h4B4B4B4B4B4B4B4B6B6D6D6D8D8D8F8F8FAFAFD1F31535153535151513F3F3F1),
    .INIT_09(256'h151513F313F3D1AFD1AF8D6D6D6D6F6F6D6D4D4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_0A(256'h37575779595757575779797B7B79795713F1CFCFAFAFAF8F8D8D8FAFD1F31315),
    .INIT_0B(256'h9B9B9B9B9B9B7B7B7979795957353513F31537373535353515373715F3133557),
    .INIT_0C(256'hF3F3D3F315599BBFDFDFDDFFFFFDFDFFFFFFFFFFFFFFFFFFDFDDBDBDBDBDBFBF),
    .INIT_0D(256'h8FB1B1B1B1AF8F8FB1B1D1D3D3D3D1B1918F8F8F8F8FB1D3F3F3F3F315151513),
    .INIT_0E(256'hD1B1B1B1B1B1B1B1B1AFAFAFAFAFAFAFB1B18F8F8F8F8F918F8F8F8F8F8F8F8F),
    .INIT_0F(256'hF3F3F3F3F3F3F3F3F3D3D3D3D3D3D3D3D3D3D3D1D1D1D1D1D3D3D1D1B1B1B1B1),
    .INIT_10(256'h5757575757575757575979797B7B797979795959575757571515151313F3F3F3),
    .INIT_11(256'hD1D1D1D1F1F1F1F1F3F313151515151515353535353535351515151535353757),
    .INIT_12(256'h4B4B4B4B4B4B4B4B6B6D6D8D8D8F8FAFAFB1D1F3151515151515131313F3F3D1),
    .INIT_13(256'hD1F3D1D1F3F3D1D1F5D3B18F8F6F6D6D6D4D4D4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_14(256'h1515353757373737375757797959573513F1CFAFAF8F8D8D6D6D6D8D8FAFAFD1),
    .INIT_15(256'h9B9B9B9B9B9B9B9B7979797959573535F3F1F31313F3F3F3F313F3F1D1F11313),
    .INIT_16(256'h13F315597B9B9DBDBDDFFFFFFFFFFFFFFFDFDFDFDFDFDFDFDDDFBDBD9B9B9DBD),
    .INIT_17(256'hAFB1D3D3D1B1AF8FAFB1D3D3D3D3B1B18F8F8F8F8FAFD1F515F5151515151515),
    .INIT_18(256'hD1B1B1B1B1AF8FB1B1B1AFAFAFAFAFAFB1AF8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_19(256'h1515F3F3F3F3F3F3F3F3D3D3F3F3F3F3D3F3D3D3D1D1D1D1D1B1B1B1B1B1D3D3),
    .INIT_1A(256'h595759797979795979797B7B79797979595979595737353515151313151513F3),
    .INIT_1B(256'hF3D3D1D3F3F3F3F3131515353535353535353737353535353535353737575757),
    .INIT_1C(256'h2B4B4B4B4B4B4B6B6B6D6D8D8D8F8FAFD1D3F3151513131313F3F3F3F3F3F3F3),
    .INIT_1D(256'h8DAF8DAFF3F3D1D1F3F3D1B18F6D4D2B4B4B4B4B4B4B4B4B4B4B2B2B2B4B4B4B),
    .INIT_1E(256'hF1D1D1F31315153513153537373515F3F1D1AF8D8D8D8D8D6D6D6D8D8D6D6D8D),
    .INIT_1F(256'h9B9B9B9B9B9B9B9B9B7B797B9B7B5937F3CF8DAFD1D1CFAFD1D1AF8DAFD1D1D1),
    .INIT_20(256'h15577B9B9BBDBDBDDFDFFFFFFFFFFFFFDFDFDFDFDFDFDFDF9D9B7B7B7B7B7B7B),
    .INIT_21(256'hB1D1B1B1AF8FAFAFB1B1B1D3D3B3B18FAFAFAFAFAFB1F3171515151515151313),
    .INIT_22(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1AFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_23(256'hF3F3F3F3D3F3F3F3F3F3F3F3D3D3D3D3D1D1D1D1D1D1B1B1AFB1B1B1B1D1D3D3),
    .INIT_24(256'h7979797979797979797979797979797979595957371515151515151515F3F3F3),
    .INIT_25(256'hD3F3F3F3F3F31535353535353535353757353537373737573537575757595979),
    .INIT_26(256'h2B4B4B4B4B4B4D6D6B6D8D8D8D8FAFD1D3F3F3F3F3F3F3F3F3F3F5F3F3D3D3F3),
    .INIT_27(256'h6B8DAFAFF11513F3CFD1D1D18D6B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_28(256'hAFAFAF8D8DAFD1D1D1D1F3F5F3D1AFB18F8F6D6D6D6D6D6D6D6D6D4D4B4B4B4B),
    .INIT_29(256'hBDBDBDBDBDBB9B9B9B7B9B9B9B795737D18F6D8D8D8F8D6B6B8D8D6D6D8F8F8D),
    .INIT_2A(256'h57797979799BBDBDDDDFDFDFDFDFDFDFDFDDBDBDBDBDBDBFBD7957595937597B),
    .INIT_2B(256'hAFF3F3D18F8DB1B1D1B1B1B1B1B1B1B18FAFAFD1D3F515371515151515151515),
    .INIT_2C(256'hB1B1B1B1AFAFAFAF8FAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_2D(256'hF3F3F3F3F3F3F3F3F3F3D3D3D3D3D3D3D1D1D1D1D1D1B1B1B1B1B1B1B1B1B1B1),
    .INIT_2E(256'h79797979797979797979797979797979575737371513F3F313131313F3F3F3F3),
    .INIT_2F(256'hF3F3F3F315153535353737373757375757575757575737373537575757575979),
    .INIT_30(256'h2B4B4B4B4D4B4B6D6B6D8D8F8FAFD1D1D3F3F3F3F3F3D3D3D3D3F3F3D3D3D3D3),
    .INIT_31(256'h498DD1F315353537F1D1D1AF6D6D6D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_32(256'h8F8F8D6D8D8DAFD1D1D1D1D1B18D6B6B6D6D6D6D4B4B4D6D4D4D4D4B4B4B4B4B),
    .INIT_33(256'hBDBDBDBDBDBDBB9B9B9B9B9B7937F1AF6D4B6B6D6D6D6D6B6D6B4B6B6D6D6D6D),
    .INIT_34(256'h79797979999BBDDDDFDFDFDFFFDFDFDFBD9B7979799B79595779795757595937),
    .INIT_35(256'hD11515D1AFB1F3D1B1AF8F8F8FB1B1B1D1D3F3F315373715F3F5151515153737),
    .INIT_36(256'hB1B1AFAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_37(256'hF3F3D3D3D3F3F3F3D3D3D3D3D3D1D1D1D1D1D1B1B1B1B1B1B1B1B1B1B1AFAFAF),
    .INIT_38(256'h797979797979797B7B7B7B795957573715151515F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_39(256'hF3F3F31537373737575757575759595757575757573735353537375757595979),
    .INIT_3A(256'h4B4B4B4B4B4B6D6D6D8D8FAFB1D1D3D3D3D3F3D3D1D1D1D1D1D1D1D1D3D3D3D1),
    .INIT_3B(256'h8FAFD315353537373715F3D18F8F6D4D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_3C(256'h8D8D8D8D8DAFAFAFD1D1D3D1B18F8D6D6D6D6D4B4B4B4B4D4B4B4B4B4B4B4B4B),
    .INIT_3D(256'hDDDDDDDDBDBDBDBD9B9B9B9B7937F3AF8D4B6B6D4B4B6B6D6D4B496B6D6D8F8F),
    .INIT_3E(256'h5979799B9BBDBDBDDDDDDDDFDFDDBD9B797757575757351313577959575737F3),
    .INIT_3F(256'h1537F3AFD1F517F3D1D1B1B1B1B1B1D1F5F5F5F3F515F5F31515151515353757),
    .INIT_40(256'hAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_41(256'hD1D1D1D1D1D3F3F3D3D3D1D1D1B1B1B1B1B1AFAFAFAFAFAFAFAFAFAFAFAF8F8F),
    .INIT_42(256'h7B7B79797979797B797959593735151513F3F3F3F1F1F1F1D1D1D1D1D1D1D3D3),
    .INIT_43(256'hF3F315153737373757575959595959595957575757573737375757575979797B),
    .INIT_44(256'h4B4B4B4B6B6D6D6D8FAFB1D1D3D3D3D3D1D1D1D1D1D1D1D1D1D1D1D1D1D3D3F3),
    .INIT_45(256'h8D8FD115595959373715F3D1AF8F6D4D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_46(256'h8FAFAFAFAFAFAFAFB1D1D1B1AF8F8F8F8F6D4B4B4B4B4B2B4B4B4B4B4B4B4B4B),
    .INIT_47(256'hDDDDDDDDDDBDBDBD9B9B9B79795935F3AF6D6B8D8F8DAFAFAF8D6D6D6B6B6B6D),
    .INIT_48(256'h7979799B9BBB9BBBBBBBBDBB9B79573535577979373515153515153735F31357),
    .INIT_49(256'h1515F1D1F3373715D3D3D3D1D1B1D1D1D1B1B1D1D1D3F3F31737373715373759),
    .INIT_4A(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_4B(256'hD1D1D3D3D3D3D3D3D3D3D1D1B1B1AFAFAF8F8F8F8F8F8FAF8F8F8F8F8F8F8F8F),
    .INIT_4C(256'h7B79795979797979373737373515151513F3F3F3D1D1D1D1D1D1D1D1D1D1D1D1),
    .INIT_4D(256'hF31515373737575759797979795959797979797979595979797979797B7B7B7B),
    .INIT_4E(256'h4B4B6B6B6B6D8FAFD1D1D3D3D3D3D1B1B1B1B1AFAFAFB1D1D1D1F3F3D1D1F3F3),
    .INIT_4F(256'h4B6DB1F5395937D3AF8F8D8D6D4D4D2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_50(256'hAFB1D1D1AFAFAFAFD1D1B18F6D8D8D6D6D4B294B4B4B4B2B4B4B4B4B2B2B2B4B),
    .INIT_51(256'hDDDDDDDDDDBDBDBDBDBD9B79575735F38D8DAFF313F3F3F3D1F1D1AF8F8F8D8D),
    .INIT_52(256'h7B797B9B9B9B9B9B9B9B9B9B593513F135577959373535353513F3F3F3D11559),
    .INIT_53(256'hF313F31537153715F3F3F3D1B1AFB1D1B1AFAFB1D1D3F5373737373735375759),
    .INIT_54(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_55(256'hF3F3F3F3F3D3D3D3D1D1D1B1B1AFAFAFAF8F8F8FAFAFAFB1AFAF8F8F8F8F8F8F),
    .INIT_56(256'h59573737575737373515151515151513F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3),
    .INIT_57(256'h15353737375757795979797B79797979797979797979797B9B9B9B9B9B7B7979),
    .INIT_58(256'h4B6B6B6D6D8DAFB1D3D3D1D1B1B1AF8FAFAFAFAFAFAFD1D1D1F1F3F3F3F3F315),
    .INIT_59(256'h4B6D8F8FAFB1AF6D6B4B4B4B292B4B4B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_5A(256'hB1B1AFAF8F8D8DAFD1D1B18D6D6D6D6B4B4B4B4B4B4B2B4B2B4B4B4B2B2B2B4B),
    .INIT_5B(256'hDDDDDFDDDDBDBDBB9B9B9B7979573515D1F113351513151513151513F3F3F3D1),
    .INIT_5C(256'h7979797979595959597979573513F3F135375737373737151515F3D1D3151717),
    .INIT_5D(256'hD1F3F31515F515F3F3F3D3D1D1D1D1D1B1B1B1B1D3F3F5171515151515375759),
    .INIT_5E(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_5F(256'hF3F3D3D3D1D1D1D1B1B1B1B1AFAFAFAFAFAFAFAFB1B1B1B1B1B18F8F8F8F8F8F),
    .INIT_60(256'h353515353535351515151515151313F3F3F3F3F3F315F5F3F5F3F3F3F3F3F3F3),
    .INIT_61(256'h373757575759597979797B7B7B79797979797979797979797B7B797979795757),
    .INIT_62(256'h6B6B8D8D8D8FB1D1B1B1B1B1AFAF8F8F8FAFAFAFAFAFD1D1D1F3F3F313131535),
    .INIT_63(256'h4B4B4D4B4B4B6B6B6B4B4B4B2B2B4D2B2B4B2B2B2B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_64(256'hAF8FAF8F8D8D8D8F8F8F8F6D4B4B4B4B4B4B4D4B4B292B4B4B4B2B2B2B2B2B2B),
    .INIT_65(256'hDDDDDFDDDDBDBB9B7979799B7979575737353535131335153737351513F3CFAD),
    .INIT_66(256'h595959593715F3F315151715F3D1D1D115375959373717153715F3F515151717),
    .INIT_67(256'hB1D3D1D3F3D3D3B1D1B1B1B1D1D1B1B18FB1B1B1D1F3F5F3F3F3F3F315373759),
    .INIT_68(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_69(256'hD1D1B1B1B1B1D1D1B1B1B1AFAFAFAFB1B1B1B1B1B1B1B1B1B1B1B1AF8F8F8F8F),
    .INIT_6A(256'h1515151515151515F3F3131315151515151515151515F3F3F3D3D1D1D1D1D1D1),
    .INIT_6B(256'h57575757797979797B797B797B79795979797979795959795957595959573735),
    .INIT_6C(256'h6B8D8D8F8FAFB1D1B1B1B1AFAF8F8FAF8F8F8FAFAFB1D1D1F3F3F31315353737),
    .INIT_6D(256'h2B2B4B4D4B4B4B4B4B494B4B4B4B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_6E(256'h8D8FAFAFAF8D8D8F8F8F8F8D6D4B4B4B4D4B2B2B4B4B4B4B4B4B4B2B2B2B2B2B),
    .INIT_6F(256'hDDDDDFDDDDBDBB9B797979997957555735353535355757355735153535F3CFAD),
    .INIT_70(256'h391715F5D3B1B1B1D1D1D3F5F5D3D3D317395959393715F5F5F3D3F3F5F5F5D3),
    .INIT_71(256'h8F8FAFB1B18F8F8F8F8F8F8F8F8F8F8F8F8FB1B1B1D1D1D1B1B1D1D1F3153759),
    .INIT_72(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_73(256'hB1AFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAF8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_74(256'h1515151515151313F3F3F3F3F3F3F3F3F5F5F3F3D1D1D1D1D1D1D1D1D1B1D1D1),
    .INIT_75(256'h57575959597979797979797B7B79795979595959595757573757373735353515),
    .INIT_76(256'h6D8D8DAFB1B1B1B1B1AFAF8F8F8F8D8D8D8F8FAFB1D1D1D3F3F3151515353537),
    .INIT_77(256'h4B4B4B4B4B4B4B4B4B4B4B4B4B2B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_78(256'hAF8F8D8D8D6D6D8F8D6D6D6D6D6D4B4B4B4B4B4B4B2B2B2B2B2B2B2B2B2B2B2B),
    .INIT_79(256'hDFDFDFDDDDBDBDBD9B9B9B9B797957573513131335577979797957351313F1D1),
    .INIT_7A(256'hF5D3D1B1B18F8F8F8F8F91B1B3B3B1D3F51739593937F5D3B1B1B1D1D3F5F5F5),
    .INIT_7B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8FAFB1D1D3F31517),
    .INIT_7C(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7D(256'hB1B1B1AFAFAF8F8F8F8F8FAFAFAFAF8FAFAFAFAFAF8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7E(256'h1515151513F3F3F3F3F3F3F3F3F3F3F3F3F3F3D1D1D1D1D1D1D1D1D1D1D1D1D1),
    .INIT_7F(256'h5757595959797979797979797959575757575757575757573737373535151515),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized6
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h00000000000000000000000000001800FFFFFFFE00000000000000000000007F),
    .INITP_01(256'h0000000000000000FFFFFFFC0000000000000000000000FFFFFFFFFFF0000000),
    .INITP_02(256'hFFFFFFFE0000000000000000000001FFFFFFFFFFF00000000000000000000000),
    .INITP_03(256'h00000000000001FFFFFFFFFFFC00000000000000000000000000000000000000),
    .INITP_04(256'hFFFFFFF7FF00000000000000000000000000000000000000FFFFFFFF00000000),
    .INITP_05(256'h00000000000000000000000000000000FFFFFFF000000000000000000000003F),
    .INITP_06(256'h0000000000000000FFFF83F000000000000000000000003FFFFF1FFFFF800000),
    .INITP_07(256'hFFFFCF8000000000000000000000007FFFFF5FFFFFC000000000000000000000),
    .INITP_08(256'h00000000000001FFFFFFFFFFFFF8000000000000000000000000000000000000),
    .INITP_09(256'hFFFFFFFFFFC0000000000000000000000000000000000000FFFFFFF800000000),
    .INITP_0A(256'h00000000000000000000000000000000FFFFFFFE0000000000000000000001FF),
    .INITP_0B(256'h0000000000000000FFFFFFFC0000000000000000000003FFFFFFFFFFFF000000),
    .INITP_0C(256'hFFFFFFC00000000000000000000007FFFEFFFFFFF30000000000000000000000),
    .INITP_0D(256'h0000000000000FFFE09FFFFFE700000000000000000000000000000000000000),
    .INITP_0E(256'hC01FFFFFE000000000000000000000000000000000000000FFFFFF8000000000),
    .INITP_0F(256'h00000000000000000000000000000000FFFFFF800000000000000000000007FF),
    .INIT_00(256'h8FAFB1D1D1AFAFAFAF8F8F8F8F8F8D8D8FAFAFB1D1D1D3F3F315151535353737),
    .INIT_01(256'h2B2B4B4B4B4B4B4B4B4B4B4B4B2B2B2B2B4B4B4B4B4B4B4B4B4B4B4D4D4D4D6D),
    .INIT_02(256'hAF8D6B6B6B4B4B6B6D6D6D6D6D6D4B4B4B4B4B4B4B2B2B2B2B2B2B2B2B2B2B2B),
    .INIT_03(256'hDFDFDFDDDDBDBDBDBDBDBB9B9B797977573513131557797979795737353513F1),
    .INIT_04(256'hB18F8F8F8F6F6F8F6F6F6F91918F9191B1D3F51517F5D3B1AF8F8FB1B1D3D3D3),
    .INIT_05(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8FAFB1D1D3D3),
    .INIT_06(256'h8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_07(256'hB1B1B1B1AFAFAFAFAFAFAFAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_08(256'h15151513F3F3F3F3F3F3F3F3F3F3F3F3F3F3D3D1D1D1D1D1D1D1D1D1D1D1D1D1),
    .INIT_09(256'h5757575959797979797979797959575757373737373737373535353515151515),
    .INIT_0A(256'hAFD1F3F3D3D1B1B18F8F8F8F8F8F8F8FAFB1B1D1D1D3F3F31515151535353737),
    .INIT_0B(256'h2B2B4B4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B2B4B4B4B4B4B4B4B4B4D4D4D6D8D),
    .INIT_0C(256'h8D6B6B8D8D6D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B2B2B2B2B2B),
    .INIT_0D(256'hDFDFDDDDDDBDBDBDBDBB9B9B9B7979795957351535797979797937351313D1AF),
    .INIT_0E(256'h8F8F8F8F8F6F6F91916F6F6F6F6F6F6F8FAFB1B1B1B1B18FB1AF8F8F91B19191),
    .INIT_0F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8FB1B18F8F),
    .INIT_10(256'h8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_11(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1AF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_12(256'h15151515F3F3F3F3F3F3F3F3F3F3F3F3D3D3D1D1D1D1D1D1D1D1D3D3D3D1D1D1),
    .INIT_13(256'h5757575759797979797979795957575757373735353537353535151513131515),
    .INIT_14(256'hD1D3D3F3D3D1B1B18F8F8F8F8F8F8FAFB1B1D1D1F3F3F5151515151515353737),
    .INIT_15(256'h2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4D4B4D6D8F),
    .INIT_16(256'hF3D1D3D3D3B16D6D4B4B2B4B4B4B4B2B4B4B4B4B4B4B2B2B4B4B2B2B2B2B2B2B),
    .INIT_17(256'hDFDFDDDDBDBDBDBD9B9B9B9B9B797979573535353557575757373515151513F3),
    .INIT_18(256'h8F8F8F8F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191),
    .INIT_19(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F),
    .INIT_1A(256'h8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_1B(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_1C(256'h151515151513F3F3F3F3F3F3F3F3F3F3D1D1D1D1D1D1D1D1D1D1D1D1B1B1B1B1),
    .INIT_1D(256'h5757575759797979797979795757373737351515153535351515151313131515),
    .INIT_1E(256'hD3D3B1B1AFAF8F8D8F8F8D8D8FAFAFB1D1D1D1D3F3F3F3151315151515353737),
    .INIT_1F(256'h4B4B4B4B4B4D4D4B4B4B4B4B4B4B4B4B4B2B4B4B4B4B4B4B4B4B4B4D4D6D8FB1),
    .INIT_20(256'hF3F3D3B1AF8F8F8F4D4D4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B2B2B2B2B2B2B2B),
    .INIT_21(256'hDDDDDDBDBDBDBDBD9D9B9B9B9B9B797935353535353535353515151515151313),
    .INIT_22(256'h8F8F8F8F8F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F8F8F919191),
    .INIT_23(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F6F6F6F6F6F6F6F8F8F8F8F),
    .INIT_24(256'h8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_25(256'hB1B1AFAF8F8F8F8F8F8F8F8F8FB1AFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_26(256'h1515151515151515F3F3F3F3F3F3D1D1D3D1D1D1D1D1D1D1B1B1B1B1B1B1AFAF),
    .INIT_27(256'h59595759797979797979595737353515151515151515151515151513F3131515),
    .INIT_28(256'hB1B18F8D8F8F8D8D8D8D8D8D8FAFB1D1D1D1D3D3D3F3F3F3F3F3131535373757),
    .INIT_29(256'h2B4B4B4B6D6F6D4D4B4B4B4B4B4B4B4B4B2B4B4B4B4B4B4B4B4B4B4D6D6D8FB1),
    .INIT_2A(256'h8D8D8D6D6D6D6D6F6D4D4B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B2B2B2B2B),
    .INIT_2B(256'hBDBDBDBDBDBDBDBDBD9B9B9B9B9B7979371513131313153535353515F3D1AF8D),
    .INIT_2C(256'h8F8F8F8F8F8F8F6F8F8F8F8F8F8F8F8F8F8F8F91918F8F8F919191918F919191),
    .INIT_2D(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F8F),
    .INIT_2E(256'h8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_2F(256'hAFAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_30(256'h151515151515151515F3F3F3F3F3F3D3F3D3D1D1D1D1D1D1B1B1B1AFAFAFAFAF),
    .INIT_31(256'h57575759797979795957373535351513F3F3F313151515151515151515151515),
    .INIT_32(256'h8F8F8D8D8D8F8F8D8D8D8F8FAFB1B1D1D1D1D1D1D1D1D1F3F3F3153537373757),
    .INIT_33(256'h4B4B4D6D6F8F8F6D4B4B4B4B4B4B4B4B4B2B2B4B4B4B4B4B4B4B4B6D6D6D8F91),
    .INIT_34(256'h8F8F8F8D6D4B4B4B4B4B2B2B2B2B2B2B4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B4B),
    .INIT_35(256'h9B9B9BBDBDBDBDBD9D9B9B9B9B7B795935F3D1D1F1F3131313131515F3D1B1AF),
    .INIT_36(256'h8F8F8F8F8F8F8F6F8F8F8F8F8F8F8F8F918F8F8F91918F8F8F8F91918F919191),
    .INIT_37(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F6F6F6F8F6F8F8F6F6F6F8F6F6F6F6F8F),
    .INIT_38(256'h8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_39(256'hB1AFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_3A(256'h37373735151515151515F3F3F3F3F3F5F3D3D1D1D1D1B1AFAFAFAFAFAFAFAFAF),
    .INIT_3B(256'h37375759797959575735351535151513F313F315151515151515151515151515),
    .INIT_3C(256'h8F8F8F8F8D8D8D8D8F8FAFB1B1B1B1B1B1D1D1D1D1D1D1F3F315153737373737),
    .INIT_3D(256'h4B4B4D6D8FB18F4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4D4B6D6D8F8F8F8F),
    .INIT_3E(256'hAF8F6D6B4B4B4B4B4B2B2B2B4B4B4B4B2B4B2B4B4B4B4B4B4B4B2B2B2B2B2B4B),
    .INIT_3F(256'h7979799B9BBBBDBDBDBD9B9B9B9B79575715F1F31535353513F3F1F3F3F3D1D1),
    .INIT_40(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_41(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F6F8F8F8F8F8F8F8F8F),
    .INIT_42(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_43(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F),
    .INIT_44(256'h37373737373535151515151515F3F3D1D1B1AFAF8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_45(256'h3759797979573737151515151515151515151515151515151515151515151515),
    .INIT_46(256'h6D6D6D6D8D8D8F8FB1B1B1AFAFAFAFB1B1B1B1D1D1D3F3151515373735353535),
    .INIT_47(256'h6F6D6D6D6D8F8F6D4D4B4B4B4B4B4B4B4D4B4B4D6D6D4B4B6D6D6D6F6F6F8F8F),
    .INIT_48(256'hB16D4B6DB1D18F4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B2B2B4B4B4D),
    .INIT_49(256'h1557799BBDBDBDBDBDBDBBBBBBBB9B99795757799B7979573513133515F3D1D1),
    .INIT_4A(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_4B(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_4C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_4D(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_4E(256'h37373737351515151515F5F3F3D1D1B1AFAFAFAF8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_4F(256'h5779795957353535151313151515151515131315151515151515151515353737),
    .INIT_50(256'h6D8D8D8F8F8F8F8F8F8F8F8F8F8F8FAFAFAFB1D1D3F3F5151515151515353737),
    .INIT_51(256'h6F6D6D4D6D6D6D4D4B4B4B4B4B4B4B4B4B4B4D6D8F6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_52(256'hD18F6D8FB1D3B18F4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B2B2B4B4B4D),
    .INIT_53(256'h37599B9DBDBDBDBDBDBBBBBBBBBBBB9B9B9B9B9BBD9B9B9B79795957573513F3),
    .INIT_54(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_55(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_56(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_57(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_58(256'h3735151515151515F3F3F3F3D3D1B1AFB1B1B1B1B1B1AFAF8F8F8F8F8F8F8F8F),
    .INIT_59(256'h7959573535131313131313151537373715353535351515153537373737373737),
    .INIT_5A(256'h6D6D6F8F8F8F8F8F8F8F8F8D8D8F8FAFAFAFD1F3F3F315151313151515355757),
    .INIT_5B(256'h6D6D4D4B4B4D4B4B4B4B4B4B4B4B4B4B4B4D6D8F8F6F6D6D6D6D6D6D6D6D6D6D),
    .INIT_5C(256'h8F6D6D8F8F8F6D4B4B4B4B4B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B2B4B4B4B),
    .INIT_5D(256'h59799B9B9B9BBDBDBBBDBDBDBDBDBDBDBDBDBDBDBD9B9B9B7B7959373715F3D3),
    .INIT_5E(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_5F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_60(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_61(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_62(256'h15151313F3F31315F3F3F3F3D3D1D1D1D1D1D1B1B1B1AFAF8F8F8F8F8F8F8F8F),
    .INIT_63(256'h59371513131313F3151313151537373537373737353535373737373735151515),
    .INIT_64(256'h6D6D6D6F8F8F6F8F8F8F8D8D8D8D8FAFAFD1F1F3F31315151515153535575979),
    .INIT_65(256'h4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4D6D8F8F8F6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_66(256'h6D8F8F8F8F6D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_67(256'h7979797B9B9B9B9BBBBDBDBDBD9D9D9D9D9DBD9D9D9B9B7B5735F3F3D3D1B1AF),
    .INIT_68(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_69(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_6A(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6C(256'h151513F3F3151515F3F3F3F3F3D3D3D3D1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_6D(256'h371513F3F3F3F3F313F3F3131515151515151515151515353535353515151515),
    .INIT_6E(256'h6D6D6D6F6F8F6D8D8D8D8D8D8D8D8FAFD1F1F3F3131313131535353757575757),
    .INIT_6F(256'h4B4B4B4B4B4B4B4D4B4B4B4B4B4B4B4D6D8F8F8F6F6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_70(256'h8F8F8F6D4D4B4B4D4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_71(256'h595757577979797B9B9B9B9B9B9B9D9D9D9D9D9D9D9D7B7B37F3AFAFB18F8D6D),
    .INIT_72(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191919191919191),
    .INIT_73(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_74(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_75(256'h8F8F8F8F6F6F6F6F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_76(256'h151515F3F3F3F3F3F3F3D3D1D1D1D1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_77(256'h1515F3F3F3F3F3F1F1F1F3131535353535353535353737373737373535151515),
    .INIT_78(256'h6D6D6D6D6D6D6D6D6D6D8D8D8F8FAFB1D1F1F3F3F31313151515353535353535),
    .INIT_79(256'h4B4B4B4D4B4B4B4B4B4B4B4B4B4B4D4D6D6F8F6F6D6D6D6D4D6D6D6D6D6D6D6D),
    .INIT_7A(256'h6D6D6D4D4B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_7B(256'h57575757575779797B7B7B7B7B7B7B7B7B7B7B7B7B79593917D18F8F8F8D6D6D),
    .INIT_7C(256'h8F8F8F8F8F8F8F8F8F8F8F919191919191919191919191919191919191919191),
    .INIT_7D(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7E(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_7F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized7
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000FFFFF8000000000000000000000001FC003FFFFF00000000),
    .INITP_01(256'hFFFF0000000000000000000000000020007FFF80000000000000000000000000),
    .INITP_02(256'h000000000000000000FFFE000000000000000000000000000000000000000000),
    .INITP_03(256'h03FFFFF80000000000000000000000000000000000000000FFF8000000000000),
    .INITP_04(256'h00000000000000000000000000000000FFF00000000000000000000000000000),
    .INITP_05(256'h0000000000000000FFC000000000000000000000000000000FE7FFF000000000),
    .INITP_06(256'h190000000000000000000000000000003FFFFFE0000000000000000000000000),
    .INITP_07(256'h00000000000000007FFFFFC00000000000000000000000000000000000000000),
    .INITP_08(256'hFFFFFF0000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000001),
    .INITP_0A(256'h000000000000000000000000000000000000000000000007FFFFFC0000000000),
    .INITP_0B(256'h0000000000000000000000000000000FFFFFF000000000000000000000000000),
    .INITP_0C(256'h0000000000000003FFFFC0000000000000000000000000000000000000000000),
    .INITP_0D(256'h7FF8000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h00000000000000000000000000000000000000000000000000000000000000E0),
    .INITP_0F(256'h0000000000000000000000000000000000000000000001F1FFC0000000000000),
    .INIT_00(256'hF3F3F3F3F3D1D1D1D1D1D1B1B1B1AFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_01(256'hF3F3D1D1D1F1D1D1D1F113353757595959595959595757573737151515151515),
    .INIT_02(256'h4D4D4D4D6D6D6D6D6D8D8F8F8FAFB1D1D1D1F1F3F3F3F313131515151513F3F3),
    .INIT_03(256'h4D4B4B4D4D4B4B4B4B4D4D4D4B4B4D4D4D6D6D6D6D6D6D4D4D4D6D6D6D6D4D6D),
    .INIT_04(256'h4B4B4D6D4D4D4B2B4B4B4B2B2B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_05(256'h595757575757575759797979595939373737373515F3F3F3D38F6D6D6D4D4B4B),
    .INIT_06(256'h8F8F8F8F8F8F8F8F8F8F8F919191919191919191919191919191919191919191),
    .INIT_07(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_08(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_09(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_0A(256'hF3F3F3D3D1D1D1B1B1B1AFB1AFAFAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_0B(256'hD1D1D1D1D1D1D1D1F113353757575979795959575737351513F3F3F1F3F3F3F3),
    .INIT_0C(256'h4D4D4D4D6D6D6D6D8F8F8FAFAFB1B1D1D1D1D1F3D3F3F3F3F3F313F3F3F3D1D1),
    .INIT_0D(256'h4D4B4B4B4D4B4B4B4B4D4D4D4D4B4B4D4B4D4D4D6D6D6D4D4D4D6D6D6D4D4D4D),
    .INIT_0E(256'h4B4B4B4B4B4B4B4B4B4B2B4B4B4B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_0F(256'h79595757575757575759595937371515F3F3F1D1AF8D8D8F8F8D6B4B4B4B4B4B),
    .INIT_10(256'h8F8F8F8F8F919191919191919191919191919191919191919191919191919191),
    .INIT_11(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_12(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_13(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_14(256'hD3D3D1D1D1D1B1B1B1B1B1B1B1B1B1AF8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F),
    .INIT_15(256'hB1D1D1D1D1D1F3F3131537595959573735353535151513F3F3F3F3F3F3F3F3F3),
    .INIT_16(256'h4D4D4D4D6D6D6D6D8F8FAFB1B1B1D1D1D1D1D1D1D1D1D1D3F3F3D3D1D1B1B1D1),
    .INIT_17(256'h4B4B4D4D4D4D4D4D4B4D4D4D4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_18(256'h4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_19(256'h79795757577959575759573715F3F3D1D1D1D1AF8D6B6B6D4B4B4B4B4B4B4B4B),
    .INIT_1A(256'h8F8F8F8F8F919191919191919191919191919191919191919191919191919191),
    .INIT_1B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91),
    .INIT_1C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_1D(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_1E(256'hD1D1D1D1D1B1B1B1B1B1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F),
    .INIT_1F(256'hAFD1D1D1F3F31515373735351515131315151515151515151515151515F3F3F3),
    .INIT_20(256'h4D4D4D6D6D6D6D6F8F8FB1B1B1B1B1D1D1D1D1D1D1B1D1D1D1D1B1B1B1B1B1B1),
    .INIT_21(256'h4D4D4D4D4D4D4D4D6D6D6F6D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_22(256'h4B4B4B4B4B4B4B4B4B4B2B2B2B2B2B2B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B),
    .INIT_23(256'h795957575757595959573715F3D1D1AF8D8D8D8D6B6B4B4B4B4B4B4B4B4B4B4B),
    .INIT_24(256'h8F8F8F9191919191919191919191919191919191919191919191919191919191),
    .INIT_25(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F9191),
    .INIT_26(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_27(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_28(256'hD1D1D1B1B1B1AFAFB1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F),
    .INIT_29(256'hD1D1D3F315151515373515F3F3131315373535353535351515151515F3F3F3D3),
    .INIT_2A(256'h4D4D4D6D6D6D6F6F8F8FAFAFB1B1B1B1B1B1B1B1B1B1B1B1AF8F8F8FAFB1B1AF),
    .INIT_2B(256'h4D4D4D4D4D4D4D4D6D6D6D6D4D4D4D4D4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_2C(256'h4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4B),
    .INIT_2D(256'h37373737373737573735F3D1D1AF8F8F8D8D8D6D6D6D4B4B4B4B4B4B4B4B4B4B),
    .INIT_2E(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_2F(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F9191),
    .INIT_30(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_31(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_32(256'hD1D1B1B1B1B1AFB1B1B1B1918F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F),
    .INIT_33(256'hF3F315153537373715151515353537575757573737373515151513F3F3F3D1D1),
    .INIT_34(256'h4D6D6D6D6D6D6D6D8F8F8F8F8FAFAFAF8FB1B1B18F8F8F8F8F8F8F8FAFB1B1D1),
    .INIT_35(256'h4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_36(256'h4D4B4D4B4D4D4D4D4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_37(256'hD3D1F31515F3F315F3F3AF8F8D8D8F8F8FB1D18F6D4B4D4D4B4D4D4D4D4D4D4D),
    .INIT_38(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_39(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F9191),
    .INIT_3A(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_3B(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_3C(256'hD1D1B1B1B1B1B1B1B191918F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_3D(256'hF315373737375757373737375757575737373737373735151515F3F3F3D1D1D1),
    .INIT_3E(256'h4D6D6D6D6D6D6D6D6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8FAFB1B1B1D1D1F3F3),
    .INIT_3F(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_40(256'h4D4D4D4D4D4D4B4B4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_41(256'h8D8DAFD3D3B1AFB1B1AF8F8D8D8F8F8F8FB1B18F4B4B4D4B4D4D4D4D4D4D4D4D),
    .INIT_42(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_43(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191),
    .INIT_44(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_45(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_46(256'hB1B1B1B1AFAF8F8F8F8F8F8F8F8F6F6F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_47(256'h153737373737373737373735353537373737373737351515F3F3D3D1D1D1D1B1),
    .INIT_48(256'h4D6D6D6D6D6D6D6D6F8F8F8F8F8F8F8F8F8F8F8F8F8FAFAFB1D1D1D1D1F3F315),
    .INIT_49(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_4A(256'h4D4D4D4D4D4D4D4D4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_4B(256'h4B6D6F8F8F8F6D6D6D6D6D6D6F6D6D6D6F8F6D4D4D4D4D2B4D4D4D4D4D4D4D4D),
    .INIT_4C(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_4D(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191B1B1918F),
    .INIT_4E(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_4F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_50(256'hAF8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_51(256'h37373735151515353535353535353737573737351515F3F3D3D1D1B1B1B1AFAF),
    .INIT_52(256'h6D6D6D6D6D6D6D6D6D8F8F8F8F8F8F8F8F8F8F8FAFAFB1D1D1D1D1F3F3131515),
    .INIT_53(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_54(256'h4D4D4D4D4D4D4D4D4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_55(256'h4D4D6D6D6D6D4D4B4B4D4D4D4D4D4D4D4D4D4B4B4D4D4D2B2D2D4B4D4D4B4B4B),
    .INIT_56(256'h91919191919191919191919191919191919191919191919191B191B191B191B1),
    .INIT_57(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191B1B1918F),
    .INIT_58(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_59(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_5A(256'h8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_5B(256'h3735151313151515353737375757575737351515F3F3F3D3D3D1B1B1AFAF8F8F),
    .INIT_5C(256'h6D6D6D6D6D6D6D6D6D6F6F8F8F8F8F8F8FAFAFAFAFAFD1D1F3F1F3F315353737),
    .INIT_5D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_5E(256'h4D4D4D4D4D4D4D4D4B4B4B4B4B4B4B4B4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_5F(256'h4D4D4D4D4D6D4D4D4D4D4D4D2B2D4D4D4D4D4D4D4D2B2B4D2D2D4B4B4B4B4B4B),
    .INIT_60(256'h91919191919191919191919191B1B1B191B191B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_61(256'h8F8F8F8F8F8F919191918F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_62(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_63(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_64(256'h8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_65(256'h151515151535373757573737373715151515F3F3F3D3D1D1B1B1B1918F8F8F8F),
    .INIT_66(256'h6D6D6D6D6D6D6F6F6D6D6F8F6F8F8F8F8FAFAFB1D1D1D1D1F3F3F3F3F3F31315),
    .INIT_67(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_68(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_69(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_6A(256'h91919191919191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_6B(256'h8F8F8F8F8F91919191918F8F8F8F8F8F91919191919191919191919191919191),
    .INIT_6C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_6D(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6E(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6F(256'hF3131535375759595937373715F5F3F3F3F3D3D1D1B1B18F91918F8F8F6F6F6F),
    .INIT_70(256'h6D6D6D6D6F6F6F6F6F8F8F8F8F8F8F8FAFB1D1D1F3F3F3F3131513F3F3F1F3F3),
    .INIT_71(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_72(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_73(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_74(256'h9191919191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_75(256'h8F8F8F8F8F8F919191919191918F8F8F9191919191918F8F8FAFAFB1B1B1B1B1),
    .INIT_76(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F918F6F6F6F6F8F8F8F8F8F919191),
    .INIT_77(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_78(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_79(256'h15353737575737371515F3F3D3D1D1D1B1B1B1AF8F8F8F8F8F8F8F6F6F6F6F6F),
    .INIT_7A(256'h6D6D6D6D6F6F8F8F8F8F8F8F8F8F8F8FAFD1D1D3F3F3F31315151513F3F3F313),
    .INIT_7B(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_7C(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_7D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_7E(256'hB191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_7F(256'h8F8F8F8F91919191919191919191919191B1B1AFAFAFAFAFAFAFAFAFB1B1B1B1),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized8
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h000000000000000000000000000001FFFC000000000000000000000000000000),
    .INITP_01(256'h00000000000007FFC00000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000001FF8),
    .INITP_04(256'h000000000000000000000000000000000000000000007FC00000000000000000),
    .INITP_05(256'h0000000000000000000000000000FF0000000000000000000000000000000000),
    .INITP_06(256'h000000000001FC0000000000000000000000000000003F800000000000000000),
    .INITP_07(256'h00000000000000000000000000007FC00007F0C0000000000000000000000000),
    .INITP_08(256'h000000000000FFFC001FFFFF0000000000000000000000000000000000000000),
    .INITP_09(256'h80FFFFFF00000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h000000000000000000000000000000000000000000000000000000000000FFFF),
    .INITP_0B(256'h00000000000000000000000000000000000000000001FFFFC3FFFFFC00000000),
    .INITP_0C(256'h0000000000000000000000000001FFF007FFFFF0000000000000000000000000),
    .INITP_0D(256'h000000000003FF0000FFFF000000000000000000000000000000000000000000),
    .INITP_0E(256'h0003000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h000000000000000000000000000000000000000000000000000000000003FC00),
    .INIT_00(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F7191716F6F6F6F8F8F8F8F71717171),
    .INIT_01(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_02(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_03(256'h373737371515F3D1B1B1AFAFAFAFAFAF8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F),
    .INIT_04(256'h6D6D6D6F6F8F8F8F8F8F8F8F8F8F8FAFB1D1D1D1F3F3F3151515151515153737),
    .INIT_05(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_06(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_07(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_08(256'hB191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_09(256'h91919191919191919191919191919191B1B1B1B1B1AFAFAFAFB1B1B1B1B1B1B1),
    .INIT_0A(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F7171716F8F91918F8F917171717171),
    .INIT_0B(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_0C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_0D(256'h1515F3F3D1D1B1AFAFAF8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F),
    .INIT_0E(256'h6D6D6D6D6F6F8F8F8F8F8FAFB1B1B1D1D1D3F3F3F31515351515153717373737),
    .INIT_0F(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_10(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_11(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_12(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_13(256'h91919191919191919191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_14(256'h8F8F8F8F8F8F6F6F6F6F6F6F6F8F8F8F8F6F6F6F6F8F8F8F918F8F9191717171),
    .INIT_15(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_16(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_17(256'hD3D1B1AFAFAFAFAF8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_18(256'h4D6D6D6D6D6D6D6D8F8F8FAFB1B1D1D3F3F3F515153737373737371515F3F3F3),
    .INIT_19(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_1A(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_1B(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_1C(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B1B1B1B1B1B1),
    .INIT_1D(256'h9191919191919191919191919191919191919191919191919191919191919191),
    .INIT_1E(256'h8F8F8F8F8F8F6F6F6F6F8F8F8F8F8F8F8F8F8F8F91B18F8F918F8F8F91919191),
    .INIT_1F(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F),
    .INIT_20(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_21(256'hB1AF8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_22(256'h4D4D6D6D6D6D6D6D8F8F8FAFB1D1D1D3F3151537373737373715F5D3D1B1B1AF),
    .INIT_23(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_24(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_25(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_26(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1D3B3B3B3B1B1B1B3),
    .INIT_27(256'h9191919191919191919191B1B1B1B1B1B1B1B191919191919191919191919191),
    .INIT_28(256'h8F8F8F8F8F8F6F6F6F8F8F8F8F8F8F8F8F8FAFD1D3F3D3D1B1AFAFAF8F8F8F91),
    .INIT_29(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F),
    .INIT_2A(256'h6F6F6F6F6F6F6F6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_2B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F8F8F6F6F6F8F8F6F6F6F),
    .INIT_2C(256'h6D6D6D6D6D6D6D6D8F8FAFB1B1D1D3F31515373737371515F3F3D1AFAF8F8F8F),
    .INIT_2D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_2E(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_2F(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_30(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_31(256'h8FB1B1918F91B1B1AFAFB1B1AFAFD1F3D1B18FB1B1B1B1B1B1B3B19191B1B191),
    .INIT_32(256'h8F8F8F8F8F8F8F8F8F918F8F918F8FB1CFF135797B7B7B5915D1AFAFB1AFAFAF),
    .INIT_33(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_34(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_35(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_36(256'h6D6D6D6D6D6D6D6D8F8FB1D1D1D3F315171737371515F3F3D1D1B1AF8F8F8F8F),
    .INIT_37(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_38(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_39(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_3A(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_3B(256'hB1B1B1B1B1B1AFAFB1AFAFD1F3377B9D59373715F3B1AFB11515F5D1B1B1D1B1),
    .INIT_3C(256'h8F8F8F8F8F8F8F8F6F8F8F8F918F8FB1F3579BBDBDBDBD9D5715F3F1D1D1B1B1),
    .INIT_3D(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_3E(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_3F(256'h8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_40(256'h6D6D6D6D6D6F8F8FB1B1D3D3D3F3F3F5F5F5F3F3D1D1D1D1B1B1AF8F8F8F8F8F),
    .INIT_41(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D),
    .INIT_42(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_43(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_44(256'hD3B3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_45(256'hD1AFAFB1B1B1AFB1AFB1F3599DBFBFBFBFBDBD9B571313159B9B593715151515),
    .INIT_46(256'h8F8F8F8F8F8F8F8F8F918F8F918F8FD13579BDBDBDDFDFBDBF9B79593715F3F3),
    .INIT_47(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_48(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_49(256'h8F8F8F8F8F8F6F6F8F8F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_4A(256'h6D6D6F6F6F8F8FB1B1D3D3F5F3D3D3D3D3D1D1B1AFAFAFAFAF8F8F8F8F8F8F8F),
    .INIT_4B(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D),
    .INIT_4C(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_4D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_4E(256'hD3D3B3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_4F(256'h37F5D1B1B1B1B1D317375B9DDFDFDFDFDFDFDFBD9979799BBD9D9B5937351515),
    .INIT_50(256'h8F8F8F8F8F8F8F8F9191918F8FAFB1F3799BBBBDBDDFDFBFBFBD9D9D9D7B5959),
    .INIT_51(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_52(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_53(256'h8F8F8F8F8F8F6F6F6F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_54(256'h6F6F8F8F919191B1B3D3D3D3D3B1B1B1B1AFB1B1B1B1AFAFAF8F8F8F8F8F8F8F),
    .INIT_55(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D),
    .INIT_56(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_57(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_58(256'hD3B3B3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_59(256'h1715D3D1D3F537399D9FBFBFBFBFBFDFDFDFDFBDBD9BBDBF9D9B79371515F3D1),
    .INIT_5A(256'h8F8F8F8F8F8F91918F918F8FAFB1D1159B9BBBBDBDBDBDBDBD9D7B7B59391717),
    .INIT_5B(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_5C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_5D(256'h6F8F8F8F8F8F8F8F8F8F6F8F6F8F6F6F8F8F8F8F8F8F6F6F8F8F6F6F6F6F6F6F),
    .INIT_5E(256'h6F8F8F9191918F8FB1B1B1B1B18F8F8F8F8F8FB1B1B1AF8F8F8F8F8F8F8F8F8F),
    .INIT_5F(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D),
    .INIT_60(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_61(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_62(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_63(256'hB1D3D3B1D317595B5B7B9D9D7B797B9BBDBDBDBDBD9D9D9D59591715F3F3D3D1),
    .INIT_64(256'h8F8F8F91919191918F9191B1D1D3F3377B9BBDBD9B5957597B593715F3D3B1B1),
    .INIT_65(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_66(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_67(256'h6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F),
    .INIT_68(256'h6F6F8F8F8F8F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F),
    .INIT_69(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D),
    .INIT_6A(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_6B(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_6C(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_6D(256'hB1B1B1B1B1D3D3F315151515131335575757595959593937F5D3D1B1B1B1B1B1),
    .INIT_6E(256'h8F8F8F919191919191B1B1D3F3F515377B9DBD9D79371513F3F3D1B1B1B1B3B3),
    .INIT_6F(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_70(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_71(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F),
    .INIT_72(256'h6F6F6F6F6F6F6F6F6F6D6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_73(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D),
    .INIT_74(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_75(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_76(256'hB1B1D3D3D3D3B3B3B1B1B1B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3),
    .INIT_77(256'hB1B1B1B1B3B1B1B1D1D1D1F3F3F31313F3F3F3F3F3F3D3D3D3B1B1B1B1B1B1B1),
    .INIT_78(256'h919191919191919191B1B1D3F5F515377B7D7B593715D3AFB1B1B1B1B1B1B1B1),
    .INIT_79(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7A(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_7B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_7C(256'h6D6F6F6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F6F6F6F6F6F6F6F6F6F6F),
    .INIT_7D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D),
    .INIT_7E(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_7F(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_prim_wrapper_init" *) 
module blk_mem_gen_0_blk_mem_gen_prim_wrapper_init__parameterized9
   (\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ,
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ,
    clka,
    ena_array,
    addra,
    dina,
    wea);
  output [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  output [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  input clka;
  input [0:0]ena_array;
  input [11:0]addra;
  input [8:0]dina;
  input [0:0]wea;

  wire [7:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 ;
  wire [0:0]\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 ;
  wire [11:0]addra;
  wire clka;
  wire [8:0]dina;
  wire [0:0]ena_array;
  wire [0:0]wea;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ;
  wire \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ;
  wire [31:8]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED ;
  wire [31:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED ;
  wire [3:1]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED ;
  wire [3:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED ;
  wire [7:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED ;
  wire [8:0]\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  RAMB36E1 #(
    .DOA_REG(1),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000001800000000000000000000),
    .INITP_01(256'h0000000000000000000000000018000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000001000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000300000000000000000000000000000),
    .INITP_07(256'h0000000000000000007800000000000000000000000000000000000000000000),
    .INITP_08(256'h00FC000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000018000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h00000000000000000000000000000000000000000000F07E0000000000000000),
    .INITP_0D(256'h000000000000000000000000001FFFFFF0000000000000000000000000000000),
    .INITP_0E(256'h00000000007FFFFFF00000000000000000000000000000000000000000000000),
    .INITP_0F(256'hF800000000000000000000000000000000000000030000000000000000000000),
    .INIT_00(256'hF5F5F3D3D3D3B3B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3D3D3D3D3D3D3B3B3),
    .INIT_01(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1D1B1B1D1D1B1B1B1B3D3B1B1B1B1B1B1B3D3),
    .INIT_02(256'h919191919191B1B1B3B1D31739F5F3F5F5F5F5D3D3D1B1B1B1B1B1B1B1B1B1B1),
    .INIT_03(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191),
    .INIT_04(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_05(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F6F),
    .INIT_06(256'h6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F),
    .INIT_07(256'h4F4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D),
    .INIT_08(256'h4D4F4D4D4D4D4D4D4D6D6D6D6D6D6D6D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4F),
    .INIT_09(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D4D4D4D4D4D4D4D4D),
    .INIT_0A(256'hF5F5F3D3D3D3B1B1B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3D3D3D3D3D3D3D3),
    .INIT_0B(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B3B1B1B1B1B1B1B1B1B1B1B3B3D3D3D3D3D3D3),
    .INIT_0C(256'h8F91919191B1B1D3D3D3F51515F3D3D3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_0D(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F8F8F9191919191919191),
    .INIT_0E(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_0F(256'h8F8F8F8F8F8F8F8F6F8F8F6F8FAFD3F5B1B1B18F8F8F8F8F8F6F6F6F6F6F6F6F),
    .INIT_10(256'h6D6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F),
    .INIT_11(256'h4F4F4D4D4D4D4D4D6D6D6D6D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D),
    .INIT_12(256'h4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4F),
    .INIT_13(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D4D4D4D4D),
    .INIT_14(256'hD3D3D3D3D3D3D3D3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3D3D3D3B3B3D3D3),
    .INIT_15(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3D3D3D3D3B3B3D3D3D3D3D3D3D3),
    .INIT_16(256'h9191919191B1B1D3D3F5F5D3D1D1B1B1B191B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_17(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191919191),
    .INIT_18(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_19(256'h8F8F8F8F8F8F8F8F8F8F8F8FB1D3F537D3D3B18F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_1A(256'h6D6D6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F),
    .INIT_1B(256'h4D4F4D4D4D4D4D4D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_1C(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_1D(256'h4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D6D4D4D4D),
    .INIT_1E(256'hD1D1D1D1D3D3D3D3D3D3B3B3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_1F(256'hB1B1B1B1B1B1B1B1B1B1B3D3B1B1B1B1B3B3B3D3D5D5D3D3D3D3D3D3D3D3D3D3),
    .INIT_20(256'h919191B1B1B1B1D3D1D3D3B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_21(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F8F919191919191919191),
    .INIT_22(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_23(256'h8F8F8F8F8F8F8F8F8F8FB1B191B1B1D3D3B18F8F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_24(256'h6D6D6D6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F),
    .INIT_25(256'h4D4D4D4D4D4D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_26(256'h6D6D6D6D4D4D4D4D4D4F4F4F4F4F4F4F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_27(256'h6F6F6F6F6F4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6F6D6D4D4D4D4D),
    .INIT_28(256'hF5F3D3D3D3D3D3D3D3D3D3D3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D5D5D3D3),
    .INIT_29(256'hB1B1B1B1B1B1B1B1B1D3F3F5D3D3D3D3D3D3D3D3D5D5D3D3D3D3D3D3D3D3D3D3),
    .INIT_2A(256'h91B1B1B1B1D3D3D3D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_2B(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191918F91919191919191919191),
    .INIT_2C(256'h6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_2D(256'h8F8F8F8F8F8F8F8F8F91B1B1918F8F8F8F8F8F8F8F8F8F8F8F8F6F6F6F6F6F6F),
    .INIT_2E(256'h6D6D6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F),
    .INIT_2F(256'h4D4D4D4D4D4D4D4D4D6D6F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_30(256'h6D6D6D6D6D4D4D4D4F4F4F4F4F4F4F4F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_31(256'h6F6F6F6F6F6F4D4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6F6D4D4D4D4D4D),
    .INIT_32(256'hF3D3D3D3D3D3D3D3D3D3D3D3D3D3B3D3D3D3D3D3D3D3D3D3D3D3D3D5F5F5F5D3),
    .INIT_33(256'hB1B1B1B3B3B3D3D3D3F51515F5F3F3F3D3D3D3D3D3D3D3B1D3D3B3B3B3B3D3D3),
    .INIT_34(256'hD3D3D3D3D3D3D3D3D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_35(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191919191919191919191B1),
    .INIT_36(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_37(256'hB1B1B18F8F8F8F8F8F91B1B1B18F8F8F8F8FAFB1B1B191918F8F6F6F6F6F6F8F),
    .INIT_38(256'h6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F),
    .INIT_39(256'h6D6D6D6D6D4D4D4D6F6F6F6F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6F6F6F),
    .INIT_3A(256'h6D6D6D6D6D4D4D4D4F4F4F4F4F4F4F4F4F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D),
    .INIT_3B(256'h6F6F6F6F6F6F4D4D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D4D4D4D4D4D4D),
    .INIT_3C(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3F5F5D5D3),
    .INIT_3D(256'hB1B1B3B3D3D3D3D3F515171715F5F5F5D3D3D3D3D3D3B3B3D3D3D3D3D3D3D3D3),
    .INIT_3E(256'hD3D3D3D3D1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_3F(256'h918F8F8F8F8F8F8F8F8F8F8F8F8F8F8F91919191919191919191919191B1B1B1),
    .INIT_40(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_41(256'hD3D3D3B18F8F8F6F6F8F8F8F6F8F8F8F8FB1B1B3B3B1B1B18F8F8F8F6F8F8F8F),
    .INIT_42(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F),
    .INIT_43(256'h6D6D6D6D6D6D6F6F6F6F6F6F6F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6F6F6F6F6F),
    .INIT_44(256'h6F6F6F6F6F6F4F4F4F4F4F4F4F4F4F4F4F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_45(256'h6F6F6F6F6F6F4D4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D4D4D4D4D4F4F4F4F4F),
    .INIT_46(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_47(256'hB1B1B3D3D3D3F5F5171717171515F5F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3B3),
    .INIT_48(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_49(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F9191919191919191B1B19191B1B1B1B1),
    .INIT_4A(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_4B(256'hF5F5F5B18F8F8F6F918F8F6F6F8F8F8F8F91B1B1B1918F8F8F8F8F8F6F8F8F8F),
    .INIT_4C(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F8F),
    .INIT_4D(256'h6D6D6D6D6D6D6F6F6F6F6F6F6F6D6D6D6D6D6F6D6F6D6F6D6F6D6F6F6F6F6F6F),
    .INIT_4E(256'h4F4F4F4F6F6F6F6D6D6D6D6D6D6D6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_4F(256'h6F6F6F6F6F6F4D4D4D4D4D4D4D4D4D4D6D6D6D6D6D6D4D4D4D4D4F4F4F4F4F4F),
    .INIT_50(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_51(256'hB3B3B1B1B3D3D5F5F5F5F51717F5F3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_52(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3),
    .INIT_53(256'h919191919191919191919191919191919191919191919191919191B1B1B1B1B1),
    .INIT_54(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F918F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_55(256'hD3D3D3B1B18F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_56(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8FB1B3),
    .INIT_57(256'h6D6D8F6D6D8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_58(256'h6F4F4D4F6F6F4D6F6D6D8F8F8F8D6D6D6D6D6F6D6D6D6D6D8F6D6D8F91B1916D),
    .INIT_59(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6D6D6D6D4F4F6F4F4F4F6F6F6F4F),
    .INIT_5A(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_5B(256'hB3B3B3B3B3D3D3D3D3D3D3F3F3D3D1D1D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_5C(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3),
    .INIT_5D(256'h9191919191919191919191919191919191919191919191919191B1B1B1B1B1B1),
    .INIT_5E(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F919191919191918F),
    .INIT_5F(256'hB1B1B1B18F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_60(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F8F8F91B1),
    .INIT_61(256'hD18DAF8F6D8F6F6F6F6F6F6F6F6F6F6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_62(256'h4D6F6F6F6F6F6F6D8F8D8D8D8DAFD1D137373715F3D1D1D1D115597B9D5B15B1),
    .INIT_63(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6D6F6D4F4F6F6F6F6F6F6F6F4D),
    .INIT_64(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_65(256'hB3B3B3B3B3B3D3D3D3D3D3D3D3D1D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_66(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B3B3B3B3B3),
    .INIT_67(256'h91919191919191919191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_68(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F9191919191919191),
    .INIT_69(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_6A(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F8F8F8F91918F8F8F8F),
    .INIT_6B(256'h57153715AF6D6D6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6C(256'h6F6F6D4D6D6D6D8F6D8DD11337799BBDDFFFFFDFDFBDBDBDBDDFDFDFDFDFDFBD),
    .INIT_6D(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_6E(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_6F(256'hB3B3D3B3B3B3B3B1D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_70(256'hB1B1B1B1B1D3D3F3B1B1B1B1B1B1B1B1B1B1B1B3B3B1B1B1B3B3B3B3B3B3B3B3),
    .INIT_71(256'h91919191919191919191919191919191B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_72(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F9191919191919191),
    .INIT_73(256'h8F8F8F8F8F8F6F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_74(256'h6F6F6F6F6F6F6F6F6F6F6F6F8F8F8F8F6F6F8F8F8F8F8F8F8F8F91918F8F8F8F),
    .INIT_75(256'hDD9BDF9BF38F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_76(256'h6F6D6DB1B18F8DAFF1357BBDDFDFFFFFDDDFFFFFFFFFFFFFFFFFFFDFDFFFFFFF),
    .INIT_77(256'h6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F4D6D6F6F6F6F6F6F),
    .INIT_78(256'hD3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_79(256'hB3D3D3B3B3B3B3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3),
    .INIT_7A(256'hB1B1B1B1D3F51517B1B1B1B1B1B1B1B1B1B3B3B3B3B3B3B1B3B3B3B3B3B3B3B3),
    .INIT_7B(256'hB1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1),
    .INIT_7C(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F9191919191919191),
    .INIT_7D(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7E(256'h8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F),
    .INIT_7F(256'hFFDDDF9B15F3B16F6F6F6F6F6F6F6F8F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram 
       (.ADDRARDADDR({1'b1,addra,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASCADEINA(1'b0),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED ),
        .CASCADEOUTB(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED ),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED ),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dina[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DIPADIP({1'b0,1'b0,1'b0,dina[8]}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED [31:8],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_0 }),
        .DOBDO(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED [31:0]),
        .DOPADOP({\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED [3:1],\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_1 }),
        .DOPBDOP(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED [3:0]),
        .ECCPARITY(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED [7:0]),
        .ENARDEN(ena_array),
        .ENBWREN(1'b0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED [8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(\NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED ),
        .WEA({wea,wea,wea,wea}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_top" *) 
module blk_mem_gen_0_blk_mem_gen_top
   (douta,
    clka,
    addra,
    dina,
    wea);
  output [11:0]douta;
  input clka;
  input [16:0]addra;
  input [11:0]dina;
  input [0:0]wea;

  wire [16:0]addra;
  wire clka;
  wire [11:0]dina;
  wire [11:0]douta;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "17" *) (* C_ADDRB_WIDTH = "17" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "0" *) (* C_COUNT_18K_BRAM = "1" *) 
(* C_COUNT_36K_BRAM = "26" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     9.042558 mW" *) 
(* C_FAMILY = "artix7" *) (* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "0" *) 
(* C_HAS_ENB = "0" *) (* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
(* C_HAS_MEM_OUTPUT_REGS_B = "0" *) (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
(* C_HAS_REGCEA = "0" *) (* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) 
(* C_HAS_RSTB = "0" *) (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
(* C_INITA_VAL = "0" *) (* C_INITB_VAL = "0" *) (* C_INIT_FILE = "blk_mem_gen_0.mem" *) 
(* C_INIT_FILE_NAME = "blk_mem_gen_0.mif" *) (* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "1" *) 
(* C_MEM_TYPE = "0" *) (* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) 
(* C_READ_DEPTH_A = "76800" *) (* C_READ_DEPTH_B = "76800" *) (* C_READ_LATENCY_A = "1" *) 
(* C_READ_LATENCY_B = "1" *) (* C_READ_WIDTH_A = "12" *) (* C_READ_WIDTH_B = "12" *) 
(* C_RSTRAM_A = "0" *) (* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) 
(* C_RST_PRIORITY_B = "CE" *) (* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) 
(* C_USE_BYTE_WEA = "0" *) (* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) 
(* C_USE_ECC = "0" *) (* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) 
(* C_WEA_WIDTH = "1" *) (* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "76800" *) 
(* C_WRITE_DEPTH_B = "76800" *) (* C_WRITE_MODE_A = "WRITE_FIRST" *) (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
(* C_WRITE_WIDTH_A = "12" *) (* C_WRITE_WIDTH_B = "12" *) (* C_XDEVICEFAMILY = "artix7" *) 
(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3" *) (* downgradeipidentifiedwarnings = "yes" *) 
module blk_mem_gen_0_blk_mem_gen_v8_4_3
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [16:0]addra;
  input [11:0]dina;
  output [11:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [16:0]addrb;
  input [11:0]dinb;
  output [11:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [16:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [11:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [11:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [16:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [16:0]addra;
  wire clka;
  wire [11:0]dina;
  wire [11:0]douta;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign doutb[11] = \<const0> ;
  assign doutb[10] = \<const0> ;
  assign doutb[9] = \<const0> ;
  assign doutb[8] = \<const0> ;
  assign doutb[7] = \<const0> ;
  assign doutb[6] = \<const0> ;
  assign doutb[5] = \<const0> ;
  assign doutb[4] = \<const0> ;
  assign doutb[3] = \<const0> ;
  assign doutb[2] = \<const0> ;
  assign doutb[1] = \<const0> ;
  assign doutb[0] = \<const0> ;
  assign rdaddrecc[16] = \<const0> ;
  assign rdaddrecc[15] = \<const0> ;
  assign rdaddrecc[14] = \<const0> ;
  assign rdaddrecc[13] = \<const0> ;
  assign rdaddrecc[12] = \<const0> ;
  assign rdaddrecc[11] = \<const0> ;
  assign rdaddrecc[10] = \<const0> ;
  assign rdaddrecc[9] = \<const0> ;
  assign rdaddrecc[8] = \<const0> ;
  assign rdaddrecc[7] = \<const0> ;
  assign rdaddrecc[6] = \<const0> ;
  assign rdaddrecc[5] = \<const0> ;
  assign rdaddrecc[4] = \<const0> ;
  assign rdaddrecc[3] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[16] = \<const0> ;
  assign s_axi_rdaddrecc[15] = \<const0> ;
  assign s_axi_rdaddrecc[14] = \<const0> ;
  assign s_axi_rdaddrecc[13] = \<const0> ;
  assign s_axi_rdaddrecc[12] = \<const0> ;
  assign s_axi_rdaddrecc[11] = \<const0> ;
  assign s_axi_rdaddrecc[10] = \<const0> ;
  assign s_axi_rdaddrecc[9] = \<const0> ;
  assign s_axi_rdaddrecc[8] = \<const0> ;
  assign s_axi_rdaddrecc[7] = \<const0> ;
  assign s_axi_rdaddrecc[6] = \<const0> ;
  assign s_axi_rdaddrecc[5] = \<const0> ;
  assign s_axi_rdaddrecc[4] = \<const0> ;
  assign s_axi_rdaddrecc[3] = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  blk_mem_gen_0_blk_mem_gen_v8_4_3_synth inst_blk_mem_gen
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .wea(wea));
endmodule

(* ORIG_REF_NAME = "blk_mem_gen_v8_4_3_synth" *) 
module blk_mem_gen_0_blk_mem_gen_v8_4_3_synth
   (douta,
    clka,
    addra,
    dina,
    wea);
  output [11:0]douta;
  input clka;
  input [16:0]addra;
  input [11:0]dina;
  input [0:0]wea;

  wire [16:0]addra;
  wire clka;
  wire [11:0]dina;
  wire [11:0]douta;
  wire [0:0]wea;

  blk_mem_gen_0_blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .clka(clka),
        .dina(dina),
        .douta(douta),
        .wea(wea));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
