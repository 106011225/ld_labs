# Logic design (Verilog, FPGA): Digital photo frame (**VGA**)


## Goal

- Implement a digital photo frame with fancy transition effects on the **VGA display**.
    - make the image scroll up or down ([Demo video on YouTube](https://youtu.be/chPyVra7fyY))
    - spilt the image into left and right parts ([Demo video on YouTube](https://youtu.be/q_32RRG_PPk))


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found in [this directory](./vivado\ files/lab07.srcs/sources_1/new/)

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
