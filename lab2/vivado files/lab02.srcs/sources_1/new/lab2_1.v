module lab2_1(
    input clk,
    input rst,
    input en,
    input dir,
    input load,
    input [5:0] data,
    output reg [3:0] out
);

    reg [3:0] next_out;

    always @ (negedge clk, posedge rst) begin
        if (rst == 1)
            out = 4'b0000;
        else 
            out = next_out;
    end

    always @ (*) begin
        next_out = out;
        if ((out != 4'b1111) && (en == 1)) begin
            if (load == 0) begin
                if ((dir == 1) && (out < 4'b1100))
                    next_out = out + 1;
                else if ((dir == 0) && (out > 4'b0000))
                    next_out = out - 1;
            end
            else begin // load == 1
                next_out = (data <= 6'b001100) ? data[3:0] : 4'b1111;
            end
        end
    end

endmodule
