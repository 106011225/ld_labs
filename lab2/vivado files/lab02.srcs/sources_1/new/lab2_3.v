module lab2_3(
    input clk,
    input rst,
    output [7:0] out
);

    reg [7:0] bin, next_bin;

    always @ (posedge clk, posedge rst) begin 
        if (rst == 1) 
            bin = 8'b0;
        else 
            bin = next_bin;
    end
    
    always @ (*) begin
        if (bin < 8'b1111_1111)
            next_bin = bin + 1;
        else 
            next_bin = bin;
    end

    // binary code to gray code
    assign out = bin ^ (bin >> 1);

endmodule
