module lab2_2(
    input clk,
    input rst,
    output reg [7:0] fn
);

    reg [7:0] fm, fl;
    reg [7:0] next_fn, next_fm, next_fl;
    reg dir, next_dir;

    always @ (posedge clk, posedge rst) begin
        if (rst == 1) begin
            fn <= 8'b1;
            fm <= 8'b0;
            fl <= 8'b1;
            dir <= 1'b1;
        end
        else begin
            fn <= next_fn;
            fm <= next_fm;
            fl <= next_fl;
            dir <= next_dir;
        end
    end

    always @ (*) begin
        if (dir == 1) begin
            next_fn = fn + fm;
            next_fm = fn;
            next_fl = fm;
            next_dir = ((fn + fm) == 8'd233) ? 0 : 1;
        end
        else begin // dir == 0
            next_fn = (fm == 8'b0) ? 8'b1 : fm; 
            next_fm = (fm == 8'b0) ? 8'b0 : fl; 
            next_fl = (fm == 8'b0) ? 8'b1 : fm - fl; 
            next_dir = (fm == 8'b0) ? 8'b1 : 8'b0; 
        end
    end
endmodule
