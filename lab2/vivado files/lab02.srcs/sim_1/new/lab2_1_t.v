module lab2_1_t;

    reg clk, rst, en, dir, load;
    reg [5:0] data;
    wire [3:0] out;
    reg [3:0] ans;
    reg pass;

    lab2_1 counter (.clk(clk), .rst(rst), .en(en), .dir(dir), .load(load), .data(data), .out(out));

    always
        #5 clk = ~clk;

    initial begin
        clk = 0;
        pass = 1;

        en = 0;
        dir = 1;
        load = 0;
        data = 6'b001011;
        rst = 0;

        ans = 4'b0;

        $display("Starting the simulation");

        #5 rst = 1;
        #15 en = 1; //testing: en==1 && rst==1
        #10 rst = 0;
    
        ans = ans + 1;//testing: ascending seq. & hold
        repeat (15) begin    
           #10 if (ans < 4'b1100)
                   ans = ans + 1;
        end
    
        #5 dir = 0;//testing: descending seq. & hold
        #5 ans = ans - 1;
        repeat (15) begin
           #10 if (ans > 4'b0000)
                   ans = ans - 1;
        end

        #5 load = 1; // testing: load valid 
        #5 ans = data;
        #5 load = 0;
        #5 ans = ans - 1;
        repeat (15) begin
          #10 if (ans > 4'b0000)
                  ans = ans - 1;
        end

        data = 6'b101011;//testing: load invalid
        #5 load = 1;
        #5 ans = 4'b1111;
        #5 load = 0;
        #100 // ans should hold on 4'b1111
    
        dir = 1;
        rst = 1;
        ans = 4'b0000;//ans become 0000 after reset
        #10 rst = 0;
        #5 ans = ans + 1; // "out" change its value @ negedge clk
        repeat (5) begin    
           #10 if (ans < 4'b1100)
                   ans = ans + 1;
        end
        #5 en = 0; //testing: en==0 => hold
        #50 

        $display("%g Terminating the simulation...", $time);

        if (pass) $display(">>>> [PASS 1]");
        else      $display(">>>> [ERROR]");

        $finish;
    end

    
    always @ (negedge clk, posedge rst) begin
        #1
        if (ans != out) begin
            pass = 0;
        end
    end

endmodule
