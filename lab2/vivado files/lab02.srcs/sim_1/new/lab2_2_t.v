module lab2_2_t;

    reg clk, rst;
    wire [7:0] fn;
    reg pass;
    reg [7:0] ans, fm, fl;

    lab2_2 counter (.clk(clk), .rst(rst), .fn(fn));

    always
        #5 clk = ~clk;

    initial begin
        clk = 0;
        rst = 0;
        pass = 1;
        ans = 8'b1;
        fm = 8'b0;
        fl = 8'b1;

        $display("Starting the simulation");

        #5 rst = 1;
        #10 rst = 0;
        
        ans <= ans + fm; // ascending seq.
        fm <= ans;
        fl <= fm;
        repeat (11) begin
            #10 
            ans <= ans + fm;
            fm <= ans;
            fl <= fm;
        end
        repeat (12) begin // descending seq.
            #10 
            ans <= fm;
            fm <= fl;
            fl <= fm - fl;
        end
        
        #10// ascending seq.
        ans = 8'b1;
        fm = 8'b0;
        fl = 8'b1;
        repeat (12) begin
            #10 
            ans <= ans + fm;
            fm <= ans;
            fl <= fm;
        end
        repeat (12) begin //descending seq.
            #10 
            ans <= fm;
            fm <= fl;
            fl <= fm - fl;
        end

        $display("%g Terminating the simulation...", $time);

        if (pass) $display(">>>> [PASS 2]");
        else      $display(">>>> [ERROR]");

        $finish;
    end

    always @ (posedge clk, posedge rst) begin 
        #1 
        if (ans != fn) begin 
            pass = 0;
        end
    end

endmodule
