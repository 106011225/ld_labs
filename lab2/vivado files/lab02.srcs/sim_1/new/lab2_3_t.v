module lab2_3_t;

    reg clk, rst;
    wire [7:0] out;
    reg pass;
    reg [7:0] bin;
    wire [7:0] ans;

    lab2_3 counter (.clk(clk), .rst(rst), .out(out));

    assign ans = bin ^ (bin >> 1); // binary code to gray code

    always
        #5 clk = ~clk;

    initial begin
        clk = 0;
        rst = 0;
        pass = 1;
        bin = 8'b0;
        
        $display("Starting the simulation");
        
        #5 rst = 1;
        #10 rst = 0;

        // binary counter
        bin = bin + 1;
        repeat (2 ** 8 - 1) begin
            #10
            bin = bin + 1;
        end
        
        $display("%g Terminating the simulation...", $time);

        if (pass) $display(">>>> [PASS 3]");
        else      $display(">>>> [ERROR]");

        $finish;
    end

    always @ (posedge clk, posedge rst) begin 
        #1 
        if (ans != out) begin 
            pass = 0;
        end
    end

endmodule
