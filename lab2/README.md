# Logic design (Verilog, FPGA): Normal counter, Fibonacci counter, Gray code counter


## Goal

1. Design a **4-bit counter** between 0 and 12 with error detection.
2. Design an **8-bit Fibonacci counter**. The counter counts up from 1 to 233 and then counts down from 233 to 1, where 233 is the largest Fibonacci number that can fit in 8 bits.
3. Design a positive edge triggered counter to generate the sequence that represents Tower of Hanoi disks movement (**Gray code**), which support 8 disks. 



## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found in [this directory](./vivado\ files/lab02.srcs/sources_1/new/).
- The source code of testbench can be found in [this directory](./vivado\ files/lab02.srcs/sim_1/new/).

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
