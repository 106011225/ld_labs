
module lab5(clk, rst, money_5, money_10, cancel, check, count_down, LED, DIGIT, DISPLAY);
    input clk;
    input rst; //sw
    input money_5; // btnD
    input money_10; // btnU
    input cancel; // btnC
    input check; // btnR
    input count_down; // btnL
    output reg [15:0] LED;
    output [3:0] DIGIT;
    output [6:0] DISPLAY;


    parameter INITIAL = 0;
    parameter DEPOSIT = 1;
    parameter AMOUNT = 2;
    parameter RELEASE = 3;
    parameter CHANGE = 4;

    parameter maskPrice = 5;

    wire clk_div16;
    wire clk_div13;
    wire m5_db, m5_1p;
    wire m10_db, m10_1p;
    wire cancel_db, cancel_1p;
    wire check_db, check_1p;
    wire cntd_db, cntd_1p;
    
    reg [2:0] state, next_state;
    reg [11:0] timer, next_timer;
    reg [2:0] cnt5, next_cnt5;
    reg [5:0] balance, next_balance;
    reg [3:0] numMask, next_numMask;
    reg [3:0] maxMask, next_maxMask;
    reg showWord, next_showWord;
    reg [15:0] next_LED;
    reg [14:0] idleTimer, next_idleTimer;
    
    Clock_divider #(16) c0(.clk_in(clk), .clk_out(clk_div16));
    Clock_divider #(13) c1(.clk_in(clk), .clk_out(clk_div13));
    Debounce d0(.out(m5_db), .in(money_5), .clock(clk_div16));
    Debounce d1(.out(m10_db), .in(money_10), .clock(clk_div16));
    Debounce d2(.out(cancel_db), .in(cancel), .clock(clk_div16));
    Debounce d3(.out(check_db), .in(check), .clock(clk_div16));
    Debounce d4(.out(cntd_db), .in(count_down), .clock(clk_div16));
    Onepulse o0(.out(m5_1p), .in(m5_db), .clock(clk_div16));
    Onepulse o1(.out(m10_1p), .in(m10_db), .clock(clk_div16));
    Onepulse o2(.out(cancel_1p), .in(cancel_db), .clock(clk_div16));
    Onepulse o3(.out(check_1p), .in(check_db), .clock(clk_div16));
    Onepulse o4(.out(cntd_1p), .in(cntd_db), .clock(clk_div16));
    Display dd0(.clock(clk_div13), .numMask(numMask), .balance(balance), .showWord(showWord), .digit(DIGIT), .display(DISPLAY), .reset(rst));
    
    
    //DFF
    always @ (posedge clk_div16, posedge rst) begin
        if (rst == 1) begin
            state = INITIAL;
            timer = 0;
            cnt5 = 0;
            balance = 0;
            numMask = 0;
            maxMask = 0;
            showWord = 0;
            LED = 0;
            idleTimer = 15'd10240;
        end
        else begin
            state = next_state;
            timer = next_timer;
            cnt5 = next_cnt5;
            balance = next_balance;
            numMask = next_numMask;
            maxMask = next_maxMask;
            showWord = next_showWord;
            LED = next_LED;
            idleTimer = next_idleTimer;
        end
    end

    //Combinational
    always @ (*) begin
        next_state = state;
        next_timer = timer;
        next_cnt5 = cnt5;
        next_balance = balance;
        next_numMask = numMask;
        next_maxMask = maxMask;
        next_showWord = showWord;
        next_LED = LED;
        next_idleTimer = idleTimer;

        case (state)
            INITIAL: begin
                next_state = DEPOSIT;
            end
            DEPOSIT: begin
                // deposit coin
                if (m5_1p) begin 
                    if (balance+5 >= 50) begin
                        next_balance = 50;
                        next_numMask = 9;
                    end
                    else begin
                        next_balance = balance + 5;
                        next_numMask = (balance+5) / maskPrice;
                    end
                end
                if (m10_1p) begin
                    if (balance+10 >= 50) begin
                        next_balance = 50;
                        next_numMask = 9;
                    end
                    else begin
                        next_balance = balance + 10;
                        next_numMask = (balance+10) / maskPrice;
                    end
                end

                // press btn 
                if (cancel_1p && balance>0) begin
                    next_state = CHANGE;
                    next_numMask = 0;
                    next_timer = 12'd2048;
                end
                else if (check_1p && balance>0) begin
                    next_state = AMOUNT;
                    next_maxMask = numMask;
                    next_idleTimer = 15'd10240;
                end
            end
            AMOUNT: begin
                if (idleTimer == 0) begin
                    next_state = CHANGE;
                    next_numMask = 0;
                    next_timer = 12'd2048;
                end
                else begin
                    next_idleTimer = idleTimer - 1;

                    // adjust numMask;
                    if (cntd_1p) begin
                        next_idleTimer = 15'd10240;

                        if (numMask > 1)
                            next_numMask = numMask - 1;
                        else 
                            next_numMask = maxMask;
                    end

                    //press btn
                    if (cancel_1p) begin
                        next_state = CHANGE;
                        next_numMask = 0;
                        next_timer = 12'd2048;
                    end
                    else if (check_1p) begin
                        next_state = RELEASE;
                        next_timer = 12'd2048; // 2^11
                        next_cnt5 = 0;
                        next_showWord = 1;
                        next_LED = 16'b1111_1111_1111_1111;
                    end
                end
            end
            RELEASE: begin
                if ((cnt5==4) && (timer==0)) begin
                    next_state = CHANGE;
                    next_showWord = 0;
                    next_LED = 0;
                    next_balance = balance - (numMask * 5);
                    next_numMask = 0;
                    next_timer = 12'd2048;
                end
                else begin
                    if (timer==0) begin
                        next_timer = 12'd2048;
                        next_LED = ~LED;
                        next_cnt5 = cnt5 + 1;
                    end
                    else 
                        next_timer = timer - 1;
                end
            end
            CHANGE: begin
                if ((balance==0) && (timer==0))
                    next_state = INITIAL;
                else begin
                    if (timer==0) begin
                        next_timer = 12'd2048;
                        if (balance >= 10) 
                            next_balance = balance - 10;
                        else 
                            next_balance = balance - 5;
                    end
                    else 
                        next_timer = timer - 1;
                end
            end
        endcase
    end

endmodule



module Display (clock, numMask, balance, showWord, digit, display, reset);
    input clock, reset;
    input [3:0] numMask;
    input [5:0] balance;
    input showWord;
    output reg [3:0] digit;
    output reg [6:0] display;

    reg [3:0] bcd0, bcd1, bcd2, bcd3;
    reg [3:0] value;
    
    always @ (*) begin
        if (showWord) 
            {bcd3, bcd2, bcd1, bcd0} = {4'd10, 4'd11, 4'd12, 4'd13};
        else begin
            bcd0 = balance % 10;
            bcd1 = balance / 10;
            bcd2 = numMask % 10;
            bcd3 = numMask / 10;
        end
    end

    always @ (posedge clock, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b0000;
            value = 4'b0000;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd2;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd3;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1000000;
            4'd1: display = 7'b1111001;
            4'd2: display = 7'b0100100;
            4'd3: display = 7'b0110000;
            4'd4: display = 7'b0011001;
            4'd5: display = 7'b0010010;
            4'd6: display = 7'b0000010;
            4'd7: display = 7'b1111000;
            4'd8: display = 7'b0000000;
            4'd9: display = 7'b0010000;
            4'd10: display = 7'b0101010; // m
            4'd11: display = 7'b0100000; // a
            4'd12: display = 7'b1010010; // s
            4'd13: display = 7'b0001010; // k
            default: display = 7'b1111111;
        endcase
    end

endmodule

module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule


// module Debounce(out, in, clock);
//     input in, clock;
//     output out;
    
//     reg [3:0] tmp;

//     always @ (posedge clock) begin
//         tmp[3:1] <= tmp[2:0];
//         tmp[0] <= in;
//     end

//     assign out = &(tmp[3:0]);
// endmodule

// module Onepulse(out, in, clock);
//     input in, clock;
//     output reg out;

//     reg in_delayn;

//     always @ (posedge clock) begin
//         in_delayn <= ~in;

//         if (in & in_delayn)
//             out <= 1;
//         else 
//             out <= 0;
//     end
// endmodule
