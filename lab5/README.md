# Logic design (Verilog, FPGA): The fancy mask vending machine (**7-segment display**, **push buttons**)


## Goal

- Design the Fancy Mask Vending Machine, which behaves as follows:
    1. Initially, the machine will show "0000" on the 7-segment display. All LEDs turn off.
    2. When the customer presses the money button, the corresponding coin will be deposited into the machine. The two rightmost 7-segment digits will show the balance (the amount of deposited money); the two leftmost 7-segment digits will show the number of masks that the customer can buy with the current balance.
    3. After finishing depositing coins, the customer can adjust the number of masks to purchase.
    4. When the customer presses the cancel button, the machine will return the deposited money.
    5. When the customer presses the check button after depositing coins and adjusting the purchase amount, masks will come out. While releasing the masks, the 7-segment display will show "MASK" (or customized alphabetic messages), and all LEDs will be flashing to emulate the masks coming out.
    6. After the masks are released, the change will be returned. The 7-segment display will show the decreasing balance to indicate that the change is returning coin by coin.

Please refer to the demo video:  
https://youtu.be/F1P1whgtG1w


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found [here](./vivado\ files/lab05.srcs/sources_1/new/lab5.v).

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
