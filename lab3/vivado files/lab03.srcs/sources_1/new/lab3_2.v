module clock_divider (clk, clk_div);
    parameter n = 25;
    input clk;
    output clk_div;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_div = num[n-1];
endmodule


module lab3_2(
    input clk,
    input rst,
    input en,
    input speed,
    output [15:0] led
);

    wire [15:0] posM1, posM3;
    wire [15:0] next_posM1, next_posM3;
    reg [15:0] posM1_0, posM1_1;
    reg [15:0] posM3_0, posM3_1;

    wire clk_div23, clk_div25;
    
    clock_divider #(23) m0(.clk(clk), .clk_div(clk_div23));
    clock_divider #(25) m1(.clk(clk), .clk_div(clk_div25));

    // Mr. 1 
    assign next_posM1 = (en == 0) ? posM1 : { posM1[0], posM1[15:1] };

    always @ (posedge clk_div23, posedge rst) begin
        if (rst == 1)
            posM1_0 = 16'b1000_0000_0000_0000;
        else
            posM1_0 = next_posM1;
    end

    always @ (posedge clk_div25, posedge rst) begin
        if (rst == 1)
            posM1_1 = 16'b1000_0000_0000_0000;
        else
            posM1_1 = next_posM1;
    end

    assign posM1 = (speed == 0) ? posM1_0 : posM1_1;


    // Mr. 3 
    assign next_posM3 = (en == 0) ? posM3 : { posM3[14:0], posM3[15] };

    always @ (posedge clk_div23, posedge rst) begin
        if (rst == 1) 
            posM3_1 = 16'b0000_0000_0000_0111;
        else
            posM3_1 = next_posM3;
    end

    always @ (posedge clk_div25, posedge rst) begin
        if (rst == 1)
            posM3_0 = 16'b0000_0000_0000_0111;
        else
            posM3_0 = next_posM3;
    end

    assign posM3 = (speed == 0) ? posM3_0 : posM3_1;

    
    // conbine Mr. 1 & Mr. 3 
    assign led = posM1 | posM3;

endmodule
