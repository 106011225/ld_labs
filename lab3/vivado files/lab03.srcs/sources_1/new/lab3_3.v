module clock_divider (clk, clk_div);
    parameter n = 25;
    input clk;
    output clk_div;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_div = num[n-1];
endmodule


module lab3_3(
    input clk,
    input rst,
    input en,
    input speed,
    output [15:0] led
);

    
    wire [15:0] posM1A, posM1B, posM3;
    reg [15:0] next_posM1A, next_posM1B, next_posM3;
    reg [15:0] posM1A_0, posM1A_1;
    reg [15:0] posM1B_0, posM1B_1;
    reg [15:0] posM3_0, posM3_1;
    wire dir1A, dir1B, dir3;
    reg next_dir1A, next_dir1B, next_dir3;
    reg dir1A_0, dir1A_1;
    reg dir1B_0, dir1B_1;
    reg dir3_0, dir3_1;
    
    wire clk_div23, clk_div25;
    
    clock_divider #(23) m0(.clk(clk), .clk_div(clk_div23));
    clock_divider #(25) m1(.clk(clk), .clk_div(clk_div25));

   
    // Mr. 3
    always @ (*) begin
        {next_posM3, next_dir3} = {posM3, dir3};
        if (en == 1) begin
            if (dir3 == 1) begin
                if (posM3 > 16'b0000_0000_0111_0000)
                    next_posM3 = {posM3[0], posM3[15:1]};
                else begin // change dir
                    next_posM3 = {posM3[14:0], posM3[15]};
                    next_dir3 = 1'b0;
                end
            end
            else begin // dir3 == 0
                if (posM3 < 16'b0000_1110_0000_0000)
                    next_posM3 = {posM3[14:0], posM3[15]};
                else begin // change dir
                    next_posM3 = {posM3[0], posM3[15:1]};
                    next_dir3 = 1'b1;
                end
            end
        end
    end
    always @ (posedge clk_div23, posedge rst) begin
        if (rst == 1)
            {posM3_1, dir3_1} = {16'b0000_1110_0000_0000, 1'b1};
        else
            {posM3_1, dir3_1} = {next_posM3, next_dir3};
    end
    always @ (posedge clk_div25, posedge rst) begin
        if (rst == 1) 
            {posM3_0, dir3_0} = {16'b0000_1110_0000_0000, 1'b1};
        else
            {posM3_0, dir3_0} = {next_posM3, next_dir3};
    end
    assign {posM3, dir3} = (speed == 0) ? {posM3_0, dir3_0} : {posM3_1, dir3_1};


    // Mr. 1A
    always @ (*) begin
        {next_posM1A, next_dir1A} = {posM1A, dir1A};
        if (en == 1) begin
            if (dir1A == 1) begin
                if (((posM1A & posM3) != 0) || (((posM1A>>1) & posM3) != 0)) begin // collide
                    next_posM1A = {posM1A[14:0], posM1A[15]};
                    next_dir1A = 1'b0;
                end
                else 
                    next_posM1A = {posM1A[0], posM1A[15:1]};
            end
            else begin // dir1A == 0
                if (posM1A < 16'b1000_0000_0000_0000)
                    next_posM1A = {posM1A[14:0], posM1A[15]};
                else begin // change dir
                    next_posM1A = {posM1A[0], posM1A[15:1]};
                    next_dir1A = 1'b1;
                end
            end
        end
    end
    always @ (posedge clk_div23, posedge rst) begin
        if (rst == 1)
            {posM1A_0, dir1A_0} = {16'b1000_0000_0000_0000, 1'b1};
        else
            {posM1A_0, dir1A_0} = {next_posM1A, next_dir1A};
    end
    always @ (posedge clk_div25, posedge rst) begin
        if (rst == 1)
            {posM1A_1, dir1A_1} = {16'b1000_0000_0000_0000, 1'b1};
        else
            {posM1A_1, dir1A_1} = {next_posM1A, next_dir1A};
    end
    assign {posM1A, dir1A} = (speed == 0) ? {posM1A_0, dir1A_0} : {posM1A_1, dir1A_1};

    // Mr. 1B
    always @ (*) begin
        {next_posM1B, next_dir1B} = {posM1B, dir1B};
        if (en == 1) begin
            if (dir1B == 0) begin
                if (((posM1B & posM3) != 0) || (((posM1B<<1) & posM3) != 0)) begin // collide
                    next_posM1B = {posM1B[0], posM1B[15:1]};
                    next_dir1B = 1'b1;
                end
                else 
                    next_posM1B = {posM1B[14:0], posM1B[15]};
            end
            else begin // dir1B == 1
                if (posM1B > 16'b0000_0000_0000_0001)
                    next_posM1B = {posM1B[0], posM1B[15:1]};
                else begin // change dir
                    next_posM1B = {posM1B[14:0], posM1B[15]};
                    next_dir1B = 1'b0;
                end
            end
        end
    end
    always @ (posedge clk_div23, posedge rst) begin
        if (rst == 1)
            {posM1B_0, dir1B_0} = {16'b0000_0000_0000_0001, 1'b0};
        else
            {posM1B_0, dir1B_0} = {next_posM1B, next_dir1B};
    end
    always @ (posedge clk_div25, posedge rst) begin
        if (rst == 1)
            {posM1B_1, dir1B_1} = {16'b0000_0000_0000_0001, 1'b0};
        else
            {posM1B_1, dir1B_1} = {next_posM1B, next_dir1B};
    end
    assign {posM1B, dir1B} = (speed == 0) ? {posM1B_0, dir1B_0} : {posM1B_1, dir1B_1};



    assign led = posM1A | posM1B | posM3;

endmodule
