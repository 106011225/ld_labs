module clock_divider (clk, clk_div);
    parameter n = 25;
    input clk;
    output clk_div;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_div = num[n-1];
endmodule


module lab3_1(clk, rst, en, dir, led);

    input clk;
    input rst;
    input en;
    input dir;
    output reg [15:0] led;

    wire clk_div;
    reg [15:0] next_led;

    clock_divider clk0(.clk(clk), .clk_div(clk_div));

    always @ (posedge clk_div, posedge rst) begin
        if (rst == 1)
            led = 16'b1000_0000_0000_0000;
        else
            led = next_led;
    end

    always @ (*) begin
        next_led = led;
        if (en == 1) begin
            if (dir == 1)
                next_led = { led[14:0], led[15] }; 
            else // dir == 0
                next_led = { led[0], led[15:1] };
        end
    end


endmodule
