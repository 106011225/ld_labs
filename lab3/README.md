# Logic design (Verilog, FPGA): Clock divider and **LED** controller


## Goal 

1. Write a Verilog module for the **clock divider** that divides the frequency of the input clock by 225 to get the output clock.
2. Write a Verilog module of the **LED Controller** which is synchronous with the positive clock edges.


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found in [this directory](./vivado\ files/lab03.srcs/sources_1/new/).

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
