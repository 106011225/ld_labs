module lab4_2(
    input clk,
    input rst,
    input en,
    input record,
    input display_1,
    input display_2,
    output [3:0] DIGIT,
    output [6:0] DISPLAY
);

    parameter paused = 0;
    parameter counting = 1;

    wire clk_cnt, clk_dbn, clk_1p, clk_7s;
    wire en_dbn, en_1p;
    wire rd_dbn, rd_1p;

    reg state, next_state;
    reg [10:0] num, next_num;
    reg [10:0] counter, next_counter;
    reg [10:0] record1, next_record1;
    reg [10:0] record2, next_record2;
    reg [1:0] hasRecord, next_hasRecord;

    // Clock_divider #(21) c0(.clk_in(clk), .clk_out(clk_cnt)); //23 is about 0.1s
    ExactClock e0(.clk_in(clk), .clk_out(clk_cnt));
    Clock_divider #(21) c1(.clk_in(clk), .clk_out(clk_dbn));
    Clock_divider #(23) c2(.clk_in(clk), .clk_out(clk_1p));
    Clock_divider #(13) c3(.clk_in(clk), .clk_out(clk_7s));
    Debounce d0(.out(en_dbn), .in(en), .clock(clk_dbn)); 
    Debounce d1(.out(rd_dbn), .in(record), .clock(clk_dbn)); 
    Onepulse o0(.out(en_1p), .in(en_dbn), .clock(clk_1p));
    Onepulse o1(.out(rd_1p), .in(rd_dbn), .clock(clk_1p));
    Display dd0(.clock(clk_7s), .num(num), .digit(DIGIT), .display(DISPLAY), .reset(rst));

    //DFF
    always @ (posedge clk_1p, posedge rst) begin
        if (rst == 1) begin
            num = 11'd0;
            record1 = 11'd0;
            record2 = 11'd0;
            hasRecord = 2'd0;
            state = 0;
        end
        else begin
            num = next_num;
            record1 = next_record1;
            record2 = next_record2;
            hasRecord = next_hasRecord;
            state = next_state;
        end
    end
    always @ (posedge clk_cnt, posedge rst) begin
        if (rst == 1) 
            counter = 11'd0;
        else 
            counter = next_counter;
    end

    //combinatoinal
    always @ (*) begin
        next_num = counter;
        if ((state == paused) || (counter == 1200)) begin
            if ((display_1==1) && (display_2==0))
                next_num = record1;
            else if ((display_1==0) && (display_2==1))
                next_num = record2;
            else if ((display_1==1) && (display_2==1))
                next_num = 11'd1201; // error, display ----
        end
    end
    always @ (*) begin
        if (en_1p) 
            next_state = ~state;
        else
            next_state = state;
    end
    always @ (*) begin
        if ((state==counting) && (counter < 1200))
            next_counter = counter + 1;
        else 
            next_counter = counter;
    end
    always @ (*) begin
        next_record1 = record1;
        next_record2 = record2;
        next_hasRecord = hasRecord;
        if (rd_1p && (state==counting)) begin
            if (hasRecord == 0) begin
                next_record1 = counter;
                next_hasRecord = hasRecord + 1;
            end
            else if (hasRecord == 1) begin
                next_record2 = counter;
                next_hasRecord = hasRecord + 1;
            end
        end
    end
endmodule

module ExactClock(clk_in, clk_out);
    input clk_in;
    output reg clk_out;

    reg [23:0] counter, next_counter;
    reg next_clk_out;

    always @ (posedge clk_in ) begin 
        counter = next_counter;
        clk_out = next_clk_out;
    end

    always @ (*) begin
        if ((counter + 24'd1) < 24'd500_0000) begin // ~clk_out only half of a cycle
            next_counter = counter + 1;
            next_clk_out = clk_out;
        end
        else begin
            next_counter = 24'd0;
            next_clk_out = ~clk_out;
        end
    end
endmodule

module Display (clock, num, digit, display, reset);
    input clock, reset;
    input [10:0] num;
    output reg [3:0] digit;
    output reg [6:0] display;

    reg [3:0] bcd0, bcd1, bcd2, bcd3;
    reg [3:0] value;
    
    always @ (*) begin
        if (num <= 1200) begin
            bcd0 = num%10;
            bcd1 = ((num/10)%60)%10;
            bcd2 = ((num/10)%60)/10;
            bcd3 = num/600;
        end
        else begin //error: display ----
            bcd0 = 10;
            bcd1 = 10;
            bcd2 = 10;
            bcd3 = 10;
        end
    end

    always @ (posedge clock, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b1110;
            value = 4'b0000;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd2;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd3;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1000000;
            4'd1: display = 7'b1111001;
            4'd2: display = 7'b0100100;
            4'd3: display = 7'b0110000;
            4'd4: display = 7'b0011001;
            4'd5: display = 7'b0010010;
            4'd6: display = 7'b0000010;
            4'd7: display = 7'b1111000;
            4'd8: display = 7'b0000000;
            4'd9: display = 7'b0010000;
            4'd10: display = 7'b0111111; // display - 
            default: display = 7'b1111111;
        endcase
    end

endmodule

module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule

module Debounce(out, in, clock);
    input in, clock;
    output out;
    
    reg [3:0] tmp;

    always @ (posedge clock) begin
        tmp[3:1] <= tmp[2:0];
        tmp[0] <= in;
    end

    assign out = &(tmp[3:0]);
endmodule

module Onepulse(out, in, clock);
    input in, clock;
    output reg out;

    reg in_delayn;

    always @ (posedge clock) begin
        in_delayn <= ~in;

        if (in & in_delayn)
            out <= 1;
        else 
            out <= 0;
    end
endmodule
