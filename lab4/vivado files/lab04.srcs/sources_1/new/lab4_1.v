module lab4_1(
    input clk,
    input rst,
    input en, 
    input dir,
    output [3:0] DIGIT,
    output [6:0] DISPLAY,
    output reg max,
    output reg min
);
    
    wire clk_cnt, clk_dbn, clk_1p, clk_7s;
    wire en_dbn, en_1p;
    wire dir_dbn, dir_1p;

    reg [7:0] num, next_num;
    reg isUp, next_isUp;
    reg isPause, next_isPause;

    Clock_divider #(23) c0(.clk_in(clk), .clk_out(clk_cnt));
    Clock_divider #(21) c1(.clk_in(clk), .clk_out(clk_dbn));
    Clock_divider #(23) c2(.clk_in(clk), .clk_out(clk_1p));
    Clock_divider #(13) c3(.clk_in(clk), .clk_out(clk_7s));
    Debounce d0(.out(en_dbn), .in(en), .clock(clk_dbn)); 
    Debounce d1(.out(dir_dbn), .in(dir), .clock(clk_dbn)); 
    Onepulse o0(.out(en_1p), .in(en_dbn), .clock(clk_1p));
    Onepulse o1(.out(dir_1p), .in(dir_dbn), .clock(clk_1p));
    Display dd0(.clock(clk_7s), .isUp(isUp), .num(num), .digit(DIGIT), .display(DISPLAY), .reset(rst));

    always @ (posedge clk_cnt, posedge rst) begin   
        if (rst == 1) begin
            num = 8'b0;
            isUp = 1;
            isPause = 1;
        end
        else begin
            num = next_num;
            isUp = next_isUp;
            isPause = next_isPause;
        end
    end

    always @ (*) begin
        next_num = num;
        next_isUp = isUp;
        next_isPause = isPause; 

        if (dir_1p)
            next_isUp = ~isUp;
        if (en_1p)
            next_isPause = ~isPause;
        if (!isPause) begin
            if (isUp && num < 8'd99)
                next_num = num + 1;
            else if (!isUp && num > 8'd0)
                next_num = num - 1;
        end
    end

    always @ (*) begin
        max = (num == 8'd99) ? 1 : 0;
        min = (num == 8'd0)  ? 1 : 0;
    end
endmodule

module Display (clock, isUp, num, digit, display, reset);
    input clock, isUp, reset;
    input [7:0] num;
    output reg [3:0] digit;
    output reg [6:0] display;

    wire [3:0] bcd0, bcd1, bcd23;
    reg [3:0] value;
    
    assign bcd0 = num % 10;
    assign bcd1 = num / 10;
    assign bcd23 = (isUp == 0) ? 4'd10 : 4'd11;

    always @ (posedge clock, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b1110;
            value = 4'b0000;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd23;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd23;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1000000;
            4'd1: display = 7'b1111001;
            4'd2: display = 7'b0100100;
            4'd3: display = 7'b0110000;
            4'd4: display = 7'b0011001;
            4'd5: display = 7'b0010010;
            4'd6: display = 7'b0000010;
            4'd7: display = 7'b1111000;
            4'd8: display = 7'b0000000;
            4'd9: display = 7'b0010000;
            4'd10: display = 7'b1100011; // down
            4'd11: display = 7'b1011100; // up
            default: display = 7'b1111111;
        endcase
    end

endmodule

module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule

module Debounce(out, in, clock);
    input in, clock;
    output out;
    
    reg [3:0] tmp;

    always @ (posedge clock) begin
        tmp[3:1] <= tmp[2:0];
        tmp[0] <= in;
    end

    assign out = &(tmp[3:0]);
endmodule

module Onepulse(out, in, clock);
    input in, clock;
    output reg out;

    reg in_delayn;

    always @ (posedge clock) begin
        in_delayn <= ~in;

        if (in & in_delayn)
            out <= 1;
        else 
            out <= 0;
    end
endmodule
