# Logic design (Verilog, FPGA): Counters (**7-segment display**, **push buttons**)


## Goal

1. Design the **2-digit BCD up/down counter** and display the counting direction.
2. Implement a **stopwatch** with the record function.


## Usage

- The [vivado files/](./vivado\ files) is a vivado project.
- The source code can be found in [this directory](./vivado\ files/lab04.srcs/sources_1/new/).

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
